package cn.ideamake.bgyjssq.group;

import cn.ideamake.bgyjssq.common.enums.DictType;
import cn.ideamake.bgyjssq.pojo.query.StaffGroupQuery;
import cn.ideamake.bgyjssq.service.IDictService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DictTest {

    @Autowired
    private IDictService dictService;

    @Test
    public void testFindPage() {
        StaffGroupQuery query = new StaffGroupQuery();
        query.setProjectGroupId(21);
        query.setProjectName("碧");
        dictService.findPage(query, DictType.STAFF);
    }

}