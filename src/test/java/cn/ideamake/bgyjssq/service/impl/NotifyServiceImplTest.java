package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.pojo.query.StaffNotificationDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffNotificationQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffNotifyListQuery;
import cn.ideamake.bgyjssq.service.IResourceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class NotifyServiceImplTest {

    @Autowired
    private IResourceService resourceService;

    @Test
    public void testGetProjectQrCode() {
        StaffNotificationQuery query = new StaffNotificationQuery();
        query.setType(2);
        query.setUserid("1144204630668181506");
        query.setSearchString("添加");
        query.setStartTime("2019-06-01");
        query.setEndTime("2019-08-01");
        System.out.println(resourceService.getStaffNotification(query));
    }

    @Test
    public void testSelectArticleDetailByArticleId() {
        StaffNotificationDetailQuery query = new StaffNotificationDetailQuery();
        query.setResourceId(1);
        query.setUserId("1149260592422764545");
        System.out.println(resourceService.selectArticleDetailByArticleId(query));
    }

    @Test
    public void testSelectResourceDetail() {
        StaffNotificationDetailQuery query = new StaffNotificationDetailQuery();
        query.setResourceId(1);
        query.setUserId("1149260592422764545");
        System.out.println(resourceService.selectResourceDetail(query));
    }

    @Test
    public void testGetUserGroup() {
        StaffNotificationDetailQuery query = new StaffNotificationDetailQuery();
        query.setResourceId(1);
        query.setUserId("1149260592422764545");
        System.out.println(resourceService.getUserGroup(query));
    }

    @Test
    public void testGetUserList() {
        StaffNotifyListQuery query = new StaffNotifyListQuery();
        query.setGroupId(21);
        query.setResourceId(1);
        System.out.println(resourceService.getUserList(query));
    }

}