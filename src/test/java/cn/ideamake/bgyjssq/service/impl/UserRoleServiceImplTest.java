package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.service.IUserRoleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserRoleServiceImplTest {

    @Autowired
    private IUserRoleService userRoleService;

    @Test
    public void test() {
        List<String> userIdList = userRoleService.selectAllUserIdListByParentId("1152095371988590593");
        System.out.println(userIdList);
    }


}