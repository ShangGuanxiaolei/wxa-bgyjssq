package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.pojo.dto.CollectForAllParamDTO;
import cn.ideamake.bgyjssq.service.ICollectorAllRecordService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@SpringBootTest
@RunWith(SpringRunner.class)
public class CollectorAllRecordServiceImplTest {

    @Autowired
    private ICollectorAllRecordService collectorAllRecordService;

    @Test
    public void test () {
        CollectForAllParamDTO collect = new CollectForAllParamDTO();
        collect.setType(3);
        collect.setVisitorId("1149260592422764545");
        collect.setProjectId(13);
        collect.setResourceId(1);
        collect.setResourceType(1);
        collect.setStayTime(30);
        collectorAllRecordService.collectData(collect);
    }

}