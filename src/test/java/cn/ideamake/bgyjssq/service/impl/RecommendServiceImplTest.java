package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.pojo.query.QueryPage;
import cn.ideamake.bgyjssq.pojo.vo.BrainMappingVO;
import cn.ideamake.bgyjssq.pojo.vo.CustomerShareVO;
import cn.ideamake.bgyjssq.pojo.vo.RecommendCustomerVO;
import cn.ideamake.bgyjssq.service.IRecommendService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@SpringBootTest
@RunWith(SpringRunner.class)
public class RecommendServiceImplTest {

    @Autowired
    private IRecommendService recommendService;

    @Test
    public void testSelectRecommendCustomer() {
        QueryPage page = new QueryPage(10, 0);
        String sourceId = "1151049906801811458";
        List<RecommendCustomerVO> records = recommendService.selectRecommendCustomer(page, sourceId).getRecords();
        System.out.println(records);
    }

    @Test
    public void testSelectCustomerShare() {
        QueryPage page = new QueryPage(10, 0);
        String sourceId = "1153137502488489985";
        List<CustomerShareVO> records = recommendService.selectCustomerShare(page, sourceId).getRecords();
        System.out.println(records);
    }

    @Test
    public void testGetBrainMapping() {
        List<BrainMappingVO> brainMapping = recommendService.getBrainMapping("1169795000059404289", 13);
        System.out.println(brainMapping);
    }

}