package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.pojo.query.DashBoardQuery;
import cn.ideamake.bgyjssq.service.ICollectorResourceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;


@SpringBootTest
@RunWith(SpringRunner.class)
public class CollectorResourceServiceImplTest {

    @Autowired
    private ICollectorResourceService collectorResourceService;

    @Test
    public void testGetArticleDataLineChart() {
        DashBoardQuery query = new DashBoardQuery();
        List<Integer> projectIds = new ArrayList<>();
        projectIds.add(13);
        query.setProjectIds(projectIds);
        query.setStartTime("2019-07-01");
        query.setEndTime("2019-07-31");
        collectorResourceService.getArticleDataLineChart(query);
    }

    @Test
    public void testGetArticleDataPieChart() {
        DashBoardQuery query = new DashBoardQuery();
        List<Integer> projectIds = new ArrayList<>();
        projectIds.add(13);
        query.setProjectIds(projectIds);
//        query.setToady(1);
        query.setStartTime("2019-07-01");
        query.setEndTime("2019-07-31");
        collectorResourceService.getArticleDataPieChart(query);
    }

}