package cn.ideamake.bgyjssq.service.impl;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author imyzt
 * @date 2019/07/09
 * @description 描述信息
 */
public class ArrayListToStringTest {

    @Test
    public void test() {

        ArrayList<String> strings = new ArrayList<String>(3) {{
            add("a");
            add("b");
            add("c");
        }};

        System.out.println(arrayListToString(strings));
    }

    private String arrayListToString(Collection<String> collection) {
        StringBuffer sb = new StringBuffer();
        collection.forEach(s -> sb.append(", ").append("\"").append(s).append("\""));
        return String.format("[%s]", sb.substring(2));
    }
}
