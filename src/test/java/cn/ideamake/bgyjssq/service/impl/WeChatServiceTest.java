package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.common.util.WeChatService;
import cn.ideamake.bgyjssq.pojo.bo.WeChatPhone;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author: Walt
 * @Data: 2019-08-22 13:48
 * @Version: 1.0
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class WeChatServiceTest {
    @Autowired
    private WeChatService weChatService;

    @Test
    public void phoneSubmitTest(){
        WeChatPhone weChatPhone = new WeChatPhone();
        weChatPhone.setSessionKey("FvU4gOxbuVl3UNp2A5BTdg==");
        weChatPhone.setEncryptedData("xfmsvlRzDh965i3b0+C7p8v/s2UCOdOeMXvxKRE9oIKcebSX2dQIqoHzfzPYMaeDZc2pUEm288ZUFQLxWaUKqVsPFZr85Al9OfKeLtFdKG5hOpIM+fliNrSx8roaygNXtAEqf9oz5AdXjPLyeZeXgCQSAyq+V/XbEDe2qT1R32PM6jc+d9Jc8u68CPcU+yEZa43iO+6Bp0xF983TXanY2A==");
        weChatPhone.setIv("MdUhDJARYTOpphznXOVSeQ==");
        String phone =weChatService.decodeUserPhoneUseTokenSessionKey(weChatPhone);
        System.out.println(phone);
    }
}

