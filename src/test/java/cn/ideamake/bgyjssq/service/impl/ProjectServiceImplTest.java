package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.pojo.dto.ProjectGroupListDTO;
import cn.ideamake.bgyjssq.pojo.entity.ProjectManager;
import cn.ideamake.bgyjssq.pojo.query.VisitorVisitAnalysisQuery;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StopWatch;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


@SpringBootTest
@RunWith(SpringRunner.class)
public class ProjectServiceImplTest {

    @Autowired
    private ProjectServiceImpl projectService;

    @Autowired
    private ProjectManagerServiceImpl projectManagerService;

    @Test
    public void selectProjectGroupListDTO() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        List<ProjectGroupListDTO> projectGroupListDTOS = projectService.selectProjectGroupTree("1144204630668181506");
        System.out.println(projectGroupListDTOS);
        stopWatch.stop();
        System.out.println(stopWatch.getTotalTimeSeconds());
    }

    @Test
    public void testSelectProjectIdListByUserId() {
        System.out.println(projectService.selectProjectIdListByUserId("1144204630668181506"));
    }

    @Test
    public void testGetProjectManagerList() {
        Set<Integer> projects = new HashSet<>();
        for (int i = -1; i < 20; i++) {
            projects.add(i);
        }
        List<ProjectManager> list =
                projectManagerService.lambdaQuery()
                        .in(ProjectManager::getProjectId, projects).list();
        System.out.println(list);
    }
}