package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.common.enums.ResourceStatus;
import cn.ideamake.bgyjssq.pojo.entity.Resource;
import cn.ideamake.bgyjssq.service.IResourceService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Author: Walt
 * @Data: 2019-08-14 11:21
 * @Version: 1.0
 */
@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class ResourceServiceTest {

    @Autowired
    IResourceService resourceService;

    @Test
    public void checkPublishTime(){
        List<Resource> list = resourceService.lambdaQuery().select(Resource::getId,Resource::getStatus).le(Resource::getPublishTime, LocalDateTime.now())
                .eq(Resource::getStatus, ResourceStatus.PENDING_PUBLISH).list();
        if(null!=list&&!list.isEmpty()){
            list.forEach(resource -> {
                resource.setStatus(ResourceStatus.PUBLISHED.getCode());
            });
            resourceService.updateBatchById(list);
        }
        System.out.println(list);
    }
}
