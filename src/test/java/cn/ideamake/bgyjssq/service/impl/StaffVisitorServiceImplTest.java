package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.dao.mapper.StaffVisitorMapper;
import cn.ideamake.bgyjssq.pojo.entity.UserInfo;
import cn.ideamake.bgyjssq.pojo.query.StaffVisitorQuery;
import cn.ideamake.bgyjssq.pojo.vo.SellerVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffVisitorVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class StaffVisitorServiceImplTest {

    @Autowired
    private StaffVisitorServiceImpl staffVisitorService;

    @Autowired
    private StaffVisitorMapper staffVisitorMapper;

    @Autowired
    private UserInfoServiceImpl userInfoService;

    @Test
    public void getCustomerList() {

        StaffVisitorQuery query = new StaffVisitorQuery();
//        query.setSearch("12111111111");

        query.setSellerId("1154282109244112897");

        IPage<StaffVisitorVO> customerList =
                staffVisitorService.getStaffVisitorByFilter(query);
        log.info(customerList.toString());
    }

    @Test
    public void getCustomerList2() {
        StaffVisitorQuery query = new StaffVisitorQuery();
        query.setSellerId("1144204630668181506");
        List<String> list = new ArrayList<>();
        List<SellerVO> userInfoList = userInfoService.getUserInfoList(21);
        userInfoList.forEach(v -> list.add(v.getId()));
        List<StaffVisitorVO> staffVisitorByFilter = staffVisitorMapper.getStaffVisitorByFilter(query,
                new Page<>(10, 0),
                "v.create_at",
                true,
                list);
        System.out.println(staffVisitorByFilter);
    }
}