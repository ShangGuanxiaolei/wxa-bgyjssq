package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.pojo.dto.ProjectQRCodeDTO;
import cn.ideamake.bgyjssq.pojo.query.StaffKpiQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffListQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorInfoDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorTrendQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorVisitAnalysisQuery;
import cn.ideamake.bgyjssq.pojo.query.WeChatQRQuery;
import cn.ideamake.bgyjssq.pojo.vo.StaffKpiVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffListVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorInfoDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorVisitAnalysisVO;
import cn.ideamake.bgyjssq.service.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceImplTest {

    @Autowired
    private IUserService userService;

    //生成项目二维码
    @Test
    public void testGetProjectQrCode() {
        ProjectQRCodeDTO projectQrCodeDTO = new ProjectQRCodeDTO();
        projectQrCodeDTO.setSellerId("1144204630668181506");
        projectQrCodeDTO.setProjectId(1);
        projectQrCodeDTO.setWeChatQRQuery(WeChatQRQuery.of());
        System.out.println(userService.getProjectQrCode(projectQrCodeDTO));
    }

    //客户详情
    @Test
    public void testGetVisitorInfoDetail() {
        VisitorInfoDetailQuery query = new VisitorInfoDetailQuery();
        query.setSellerId("1145523236213411842");
        query.setUserid("1144425658048638978");
        System.out.println(userService.getVisitorInfoDetail(query));
    }

    //客户动态
    @Test
    public void testGetVisitorTrend() {
        VisitorTrendQuery query = new VisitorTrendQuery();
        query.setSellerId("1169795000059404289");
        query.setVisitorId("1171008370573938689");
        System.out.println(userService.getVisitorTrend(query).getRecords());
    }

    @Test
    public void selectStaffTopTenList() {

        StaffKpiQuery query = new StaffKpiQuery();
        List<StaffKpiVO> topTenVOList = userService.selectStaffTopTenList(query);

        System.out.println(topTenVOList);

    }

    @Test
    public void testGetVisitorVisitAnalysis() {
        VisitorVisitAnalysisQuery query = new VisitorVisitAnalysisQuery();
        query.setSellerId("1144204630668181506");
        Set<Integer> set = new HashSet<>();
        for (int i = -1; i < 15; i++) {
            set.add(i);
        }
        query.setProjects(set);
        VisitorVisitAnalysisVO visitorVisitAnalysis = userService.getVisitorVisitAnalysis(query);
        System.out.println(visitorVisitAnalysis);
    }

    @Test
    public void testGetVisitorVisitAnalysisDrilling() {
        VisitorVisitAnalysisQuery query = new VisitorVisitAnalysisQuery();
        query.setSellerId("1144204630668181506");
        Set<Integer> set = new HashSet<>();
        for (int i = -1; i < 15; i++) {
            set.add(i);
        }
        query.setProjects(set);
        System.out.println(userService.getVisitorList(query));
    }

    @Test
    public void testGetStaffInfoList() {
        StaffListQuery query = new StaffListQuery();
        query.setActive(1)
                .setCompanyName("")
                .setCondition("")
                .setGroupId(1)
                .setStartTime("")
                .setEndTime("");
        List<StaffListVO> records = userService.getStaffInfoList(query).getRecords();
        System.out.println(records);
    }

    @Test
    public void testGetVisitorInfoDetailForLeader() {
        VisitorInfoDetailQuery query = new VisitorInfoDetailQuery();
        query.setSellerId("1149260592422764545");
        query.setUserid("1158220682895376386");
        VisitorInfoDetailVO visitorInfoDetailForLeader = userService.getVisitorInfoDetailForLeader(query);
        System.out.println(visitorInfoDetailForLeader);
    }
}