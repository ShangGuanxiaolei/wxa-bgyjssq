package cn.ideamake.bgyjssq.service.impl;

import cn.hutool.json.JSONObject;
import cn.ideamake.bgyjssq.pojo.query.DashBoardQuery;
import cn.ideamake.bgyjssq.pojo.query.DistributionMapQuery;
import cn.ideamake.bgyjssq.pojo.vo.CustomerDistributionMapVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;


@SpringBootTest
@RunWith(SpringRunner.class)
public class CollectorVisitorServiceImplTest {

    @Autowired
    private CollectorVisitorServiceImpl collectorVisitorService;

    @Test
    public void test() {
        DashBoardQuery dashBoardQuery = new DashBoardQuery();
        dashBoardQuery.setType(1);
        JSONObject customerVisitData = collectorVisitorService.getCustomerVisitData(dashBoardQuery);
        System.out.println(customerVisitData);
    }

    @Test
    public void testGetCustomerDistributionMap() {
        DistributionMapQuery query = new DistributionMapQuery();
        List<Integer> ids1 = new ArrayList<>();
        List<Integer> ids2 = new ArrayList<>();
        List<Integer> ids3 = new ArrayList<>();
        List<Integer> type1 = new ArrayList<>();
        List<Integer> type2 = new ArrayList<>();
        List<Integer> source = new ArrayList<>();
        List<Integer> promotionLabel = new ArrayList<>();
        Integer hasPhone = 1;
        Integer type3 = 1;
        for (int i = 0; i < 20; i++) {
            ids1.add(i);
            ids2.add(i);
            ids3.add(i);
        }
        for (int i = 0; i < 4; i++) {
            type1.add(i);
            type2.add(i);
            source.add(i);
            promotionLabel.add(i);
        }
        query.setStartTime("2019-06-01").setEndTime("2019-08-30")
                .setProjectGroupIds(ids1).setProjectIds(ids2).setUserGroupIds(ids3)
                .setSellerType(type1).setCustomerType(type2).setCustomerAttributes(type3)
                .setVisitorSource(source).setHasPhone(hasPhone).setPromotionLabel(promotionLabel);
        List<CustomerDistributionMapVO> customerDistributionMap =
                collectorVisitorService.getCustomerDistributionMap(query);
        System.out.println(customerDistributionMap);
    }

}