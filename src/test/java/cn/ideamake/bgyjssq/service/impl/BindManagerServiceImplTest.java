package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.pojo.query.CustomerListQuery;
import cn.ideamake.bgyjssq.pojo.vo.CustomerListVO;
import cn.ideamake.bgyjssq.service.IBindingManagerService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class BindManagerServiceImplTest {

    @Autowired
    private IBindingManagerService bindingManagerService;

    @Test
    public void testCustomerListQuery() {
        CustomerListQuery query = new CustomerListQuery();
        List<Integer> list = new ArrayList<>();
        int i = 0;
        while (i < 10) {
            list.add(i);
            i++;
        }
        query.setStartTime("2019-06-01").setEndTime("2019-08-30").setSortKey(0)
                .setProjectGroupIds(list).setUserGroupIds(list)
                .setVisitStartTime("2019-06-01").setVisitEndTime("2019-08-30")
                .setPositioningStatus(0).setBeforeVisit(0);
        IPage<CustomerListVO> customerListVOIPage = bindingManagerService.customerListQuery(query);
        System.out.println(customerListVOIPage.getRecords());
    }
}