package cn.ideamake.bgyjssq.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static cn.ideamake.bgyjssq.common.util.Constants.TOURIST_UUID;

/**
 * @Author: gxl
 * @Data: 2019-09-26 14:36
 * @Version: 1.0
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class OauthWeChatServiceTest {
    @Autowired
    private OauthWechatServiceImpl oauthWechatServiceImpl;

    @Test
    public void phoneSubmitTest(){
        oauthWechatServiceImpl.cleanWeChatUserInfo(TOURIST_UUID);
    }
}

