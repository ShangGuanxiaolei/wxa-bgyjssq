package cn.ideamake.bgyjssq.QRCode;

import cn.binarywang.wx.miniapp.api.WxMaQrcodeService;
import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.ideamake.bgyjssq.common.config.wxconfig.WxMaConfiguration;
import cn.ideamake.bgyjssq.common.config.wxconfig.WxMaProperties;
import cn.ideamake.bgyjssq.common.util.AliyunOssKit;
import me.chanjar.weixin.common.error.WxErrorException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestUploadQRCode {

    @Autowired
    private AliyunOssKit aliyunOssKit;

    @Autowired
    private WxMaProperties wxMaProperties;

    @Test
    public void test() {
        String fileName = createQrCodeAndUploadOSS();
        String urlByFileName = aliyunOssKit.getUrlByFileName(fileName);
        System.out.println("=====" + urlByFileName);
    }

    private String createQrCodeAndUploadOSS() {
        byte[] appletsCode = getAppletsCode();
        if (appletsCode == null) {
            return null;
        }
        return aliyunOssKit.upload(appletsCode);
    }

    public byte[] getAppletsCode() {
        final WxMaService wxService = WxMaConfiguration.getMaService(wxMaProperties.getConfigs().get(0).getAppid());
        WxMaQrcodeService wxMaQrcodeService = wxService.getQrcodeService();
        try {
            return file2Byte(wxMaQrcodeService.createQrcode("测试数据", 100));
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] file2Byte(File tradeFile) {
        byte[] buffer = null;
        try (FileInputStream fis = new FileInputStream(tradeFile);
             ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            byte[] b = new byte[1024];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            buffer = bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer;
    }
}
