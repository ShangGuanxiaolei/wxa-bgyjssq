package cn.ideamake.bgyjssq.common.util;

import cn.ideamake.bgyjssq.pojo.entity.CollectorVisitor;
import cn.ideamake.bgyjssq.service.ICollectorVisitorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TenCentMapUtilTest {


    @Autowired
    private ICollectorVisitorService collectorVisitorService;

    @Test
    public void getAddress() {

        List<CollectorVisitor> list = collectorVisitorService.lambdaQuery().isNotNull(CollectorVisitor::getLatitude).list();

        list.forEach(collectorVisitor -> {

            String address = TenCentMapUtil.getAddress(Double.valueOf(collectorVisitor.getLatitude()), Double.valueOf(collectorVisitor.getLongitude()));

            collectorVisitor.setAddress(address);
            System.out.println(address);
        });

        collectorVisitorService.saveOrUpdateBatch(list);

    }
}