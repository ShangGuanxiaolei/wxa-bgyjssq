package cn.ideamake.bgyjssq.dashboard;

import cn.ideamake.bgyjssq.dao.mapper.CollectorVisitorMapper;
import cn.ideamake.bgyjssq.pojo.query.DashBoardQuery;
import cn.ideamake.bgyjssq.pojo.vo.dashboard.MiniProgramDataVO;
import cn.ideamake.bgyjssq.service.ICollectorVisitorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AccessTest {

    @Autowired
    private ICollectorVisitorService collectorVisitorService;

    @Autowired
    private CollectorVisitorMapper collectorVisitorMapper;


    @Test
    public void testTotal() {
        DashBoardQuery query = new DashBoardQuery();
        query.setProjectIds(null);
        query.setStartTime("2019-08-16");
        MiniProgramDataVO visitData = collectorVisitorService.getVisitData(query);
        System.out.println(visitData);
    }

    @Test
    public void testSelectPercent1() {
        DashBoardQuery query = new DashBoardQuery();
        query.setProjectIds(null);
        query.setStartTime(null);
        System.out.println(collectorVisitorMapper.selectNumberOfVisits(query));
    }

    @Test
    public void testSelectPercent2() {
        DashBoardQuery query = new DashBoardQuery();
        query.setProjectIds(null);
        query.setStartTime(null);
        System.out.println(collectorVisitorMapper.selectNumberOfPeople(query));
    }

    @Test
    public void testSelectPercent3() {
        DashBoardQuery query = new DashBoardQuery();
        query.setProjectIds(null);
        query.setStartTime(null);
        System.out.println(collectorVisitorMapper.selectPeopleOfAccess(query));
    }

    @Test
    public void testSelectPercent4() {
        DashBoardQuery query = new DashBoardQuery();
        query.setProjectIds(null);
        query.setStartTime(null);
        System.out.println(collectorVisitorMapper.selectPeopleOfRemainPhone(query));
    }
}
