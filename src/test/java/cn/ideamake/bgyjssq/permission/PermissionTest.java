package cn.ideamake.bgyjssq.permission;

import cn.ideamake.bgyjssq.common.enums.PermissionTypeEnum;
import cn.ideamake.bgyjssq.pojo.entity.SysPermission;
import cn.ideamake.bgyjssq.pojo.vo.permission.MenuListChildrenVO;
import cn.ideamake.bgyjssq.pojo.vo.permission.MenuListVO;
import cn.ideamake.bgyjssq.pojo.vo.PermissionVO;
import cn.ideamake.bgyjssq.pojo.vo.permission.NewMenuListVO;
import cn.ideamake.bgyjssq.service.permission.impl.SysPermissionServiceImpl;
import io.jsonwebtoken.lang.Collections;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PermissionTest {

    @Autowired
    private SysPermissionServiceImpl sysPermissionService;

    @Test
    public void testSave() {
        SysPermission sysPermission = new SysPermission();
        System.out.println(sysPermissionService.saveAndUpdate(sysPermission));
    }

    @Test
    public void testSelectById() {
        System.out.println(sysPermissionService.selectByPermissionId(7));
    }

    @Test
    public void testSelectByType() {
        System.out.println(sysPermissionService.selectByType(PermissionTypeEnum.OTHER));
    }

    @Test
    public void testSelectByParentId() {
        System.out.println(sysPermissionService.selectByParentId(0));
    }

    @Test
    public void testSelectByRoleIdAndType() {
        System.out.println(sysPermissionService.selectByRoleIdAndType(9, PermissionTypeEnum.DATA));
    }

    @Test
    public void testSelectByRoleIdAndTypeAndParentId() {
        System.out.println(getMenuList(9));
    }

    @Test
    public void testSelectByCache() {
        List<PermissionVO> test = sysPermissionService.selectPermissionByParentId(0);
        System.out.println(test);
    }

    @Test
    public void testSelectByCacheMenu() {
        List<NewMenuListVO> test = sysPermissionService.getMenuListByCache(9);
        System.out.println(test);
    }

    public List<MenuListVO> getMenuList(Integer roleId) {
        List<MenuListVO> menuList =
                sysPermissionService.selectByRoleIdAndTypeAndParentId(roleId, PermissionTypeEnum.MENU, 0);
        for (MenuListVO menuListVO : menuList) {
            List<MenuListChildrenVO> list =
                    sysPermissionService.selectByRoleIdAndTypeAndParentIdChildren(
                            roleId,
                            PermissionTypeEnum.MENU,
                            menuListVO.getId());
            menuListVO.setChildren(setChildren(roleId, list));
        }
        return menuList;
    }

    private List<MenuListChildrenVO> setChildren(Integer roleId, List<MenuListChildrenVO> menuListChildrenList) {
        for (MenuListChildrenVO menuListVO : menuListChildrenList) {
            menuListVO.setChildren(sysPermissionService.selectByRoleIdAndTypeAndParentIdChildren(
                    roleId,
                    PermissionTypeEnum.MENU,
                    menuListVO.getId()));
            if (!Collections.isEmpty(menuListVO.getChildren())) {
                setChildren(roleId, menuListVO.getChildren());
                menuListVO.setLevel(2);
            } else {
                menuListVO.setLevel(1);
            }
        }
        return menuListChildrenList;
    }
}