package cn.ideamake.bgyjssq.permission;

import cn.ideamake.bgyjssq.pojo.query.AdminQuery;
import cn.ideamake.bgyjssq.pojo.query.QueryPage;
import cn.ideamake.bgyjssq.service.permission.ISysAdminService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AdminTest {

    @Autowired
    private ISysAdminService sysAdminService;

    @Test
    public void testFindPage() {
        System.out.println(sysAdminService
                .findPage(new AdminQuery("a", 9, "2019-07-12"))
                .getRecords());
    }

    @Test
    public void testFindByRoleId() {
        System.out.println(sysAdminService.findByRoleId(9, new QueryPage(10, 0)).getRecords());
    }

    @Test
    public void testFindPermission() {
        System.out.println(sysAdminService.findPermission(9));
    }

}