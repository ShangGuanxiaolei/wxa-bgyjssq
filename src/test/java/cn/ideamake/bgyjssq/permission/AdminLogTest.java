package cn.ideamake.bgyjssq.permission;

import cn.ideamake.bgyjssq.pojo.query.AdminLogQuery;
import cn.ideamake.bgyjssq.service.permission.IAdminLogService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AdminLogTest {

    @Autowired
    private IAdminLogService adminLogService;

    @Test
    public void testFindPage() {
        System.out.println(adminLogService
                .findPage(new AdminLogQuery("t", 9, "2019-07-09"))
                .getRecords());
    }

}