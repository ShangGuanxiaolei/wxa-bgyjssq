package cn.ideamake.bgyjssq.permission;

import cn.ideamake.bgyjssq.pojo.query.QueryPage;
import cn.ideamake.bgyjssq.service.permission.ISysRoleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RoleTest {

    @Autowired
    private ISysRoleService sysRoleService;

    @Test
    public void  testFindPage() {
        System.out.println(sysRoleService.findByPage(new QueryPage(10, 0), "普").getRecords());
    }

    @Test
    public void  testFindEffective() {
        System.out.println(sysRoleService.selectEffectiveRole());
    }

    @Test
    public void  testProhibit() {
        System.out.println(sysRoleService.prohibit(12));
    }

}
