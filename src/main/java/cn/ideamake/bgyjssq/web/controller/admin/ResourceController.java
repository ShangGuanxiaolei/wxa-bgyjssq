package cn.ideamake.bgyjssq.web.controller.admin;

import cn.hutool.core.lang.Assert;
import cn.ideamake.bgyjssq.common.enums.DictType;
import cn.ideamake.bgyjssq.common.enums.ResourceType;
import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.CheckPermission;
import cn.ideamake.bgyjssq.pojo.dto.PublishResourceDTO;
import cn.ideamake.bgyjssq.pojo.dto.ResourceBulkOperation;
import cn.ideamake.bgyjssq.pojo.entity.Resource;
import cn.ideamake.bgyjssq.pojo.query.QueryPage;
import cn.ideamake.bgyjssq.pojo.query.ResourceListQuery;
import cn.ideamake.bgyjssq.pojo.vo.ResourceBaseVO;
import cn.ideamake.bgyjssq.pojo.vo.ResourceDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.ResourceDigDataQuery;
import cn.ideamake.bgyjssq.pojo.vo.ResourceDigDataVO;
import cn.ideamake.bgyjssq.service.IResourceService;
import cn.ideamake.bgyjssq.service.common.SaveAdminLogService;
import cn.ideamake.bgyjssq.service.permission.IRolePermissionService;
import cn.ideamake.bgyjssq.service.permission.ISysAdminProjectService;
import cn.ideamake.bgyjssq.web.controller.AbstractController;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Objects;

import static cn.ideamake.bgyjssq.common.enums.ResourceType.FLOOR_BOOK;
import static cn.ideamake.bgyjssq.common.util.Constants.PROTECT_CUSTOMER_PHONE;

/**
 * <p>
 * 资源操作.PC后台
 * <p>
 * 资源类型标识
 * {@link ResourceType#ARTICLE} 文章
 * {@link ResourceType#POSTER} 海报
 * {@link ResourceType#FLOOR_BOOK} 楼书
 * {@link ResourceType#HOME} 首页
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
@AllArgsConstructor
@RestController
@Slf4j
@RequestMapping("/admin/resource")
public class ResourceController extends AbstractController<IResourceService, Resource, Resource> {

    private final SaveAdminLogService saveAdminLogService;

    private final ISysAdminProjectService sysAdminProjectService;

    private final IRolePermissionService rolePermissionService;

    /**
     * 系统推送
     */
    private static final String SYSTEM_PUSH_ARTICLE = "spreadArticleAddSystem";

    @CheckPermission
    @PostMapping("publish/home")
    public Rest publishHome(@RequestBody PublishResourceDTO resource) {

        if (Objects.isNull(resource.getId())) {
            log.error("修改首页未传递首页ID");
            return fail("修改首页必须传递首页编号");
        }

        if (baseService.publishResource(resource, ResourceType.HOME)) {
            log.info("edit home, resource = {}", resource);
            saveAdminLogService.saveSystemLog("发布首页");
            return ok();
        }
        return fail();
    }

    @GetMapping("{resourceType:article|home|poster|floor_book}/{id}")
    public Rest getResourceByTypeAndId(@PathVariable Integer id, @PathVariable String resourceType) {

        resourceType = resourceType.toUpperCase();

        ResourceDetailVO resourceDetailVO = baseService.getResourceByTypeAndId(id, ResourceType.valueOf(resourceType));
        if (null == resourceDetailVO) {
            return fail("资源不存在或已被删除");
        }
        return okObj(resourceDetailVO);
    }

    /**
     * 获取资源列表
     *
     * @param query        查询参数
     * @param resourceType 资源类型
     */
    @CheckPermission
    @GetMapping("list/{resourceType:article|poster|floor_book|activity}")
    public Rest list(@Valid ResourceListQuery query, @PathVariable String resourceType) {
        resourceType = resourceType.toUpperCase();
        // 通过URL约束了资源类型,进入方法后一定不是脏数据,直接使用枚举的valueOf()方法拿到对应的类型
        if (CollectionUtils.isEmpty(query.getProjectIds())) {
            query.setProjectIds(sysAdminProjectService.getProjectList());
        }
        return okObj(baseService.selectResourceList(
                query, ResourceType.valueOf(resourceType), DictType.valueOf(resourceType)));
    }

    /**
     * 通过项目id获取该项目下的资源列表
     *
     * @param query        查询参数
     * @param resourceType 资源类型
     * @param projectId    项目id
     */
    @GetMapping("list/{resourceType:article|poster|floor_book|activity}/{projectId}")
    public Rest getResource(QueryPage query, @PathVariable String resourceType, @PathVariable Integer projectId) {
        resourceType = resourceType.toUpperCase();
        // 通过URL约束了资源类型,进入方法后一定不是脏数据,直接使用枚举的valueOf()方法拿到对应的类型
        IPage<ResourceBaseVO> page = baseService
                .selectResourcesByProjectId(query, ResourceType.valueOf(resourceType).getCode(), projectId, 1);
        return okObj(page);
    }

    /**
     * 通过项目id获取该项目下的资源列表
     *
     * @param query 查询参数
     */
    @GetMapping("digData")
    public Rest getResource(ResourceDigDataQuery query) {
        IPage<ResourceDigDataVO> page = baseService.digResourceBrowseData(query);
        if (!rolePermissionService.hasProperties(PROTECT_CUSTOMER_PHONE)) {
            page.getRecords().parallelStream().forEach(r -> r.setUserPhone("***"));
        }
        return okObj(page);
    }

    @PostMapping("publish/{resourceType:article|poster}")
    public Rest publish(@Valid @RequestBody PublishResourceDTO resource, @PathVariable String resourceType) {

        resourceType = resourceType.toUpperCase();

        //文章类型检测校验是否允许系统推送
        if (ResourceType.ARTICLE.name().equals(resourceType)) {
            if (!rolePermissionService.hasProperties(SYSTEM_PUSH_ARTICLE)) {
                return fail("您无权限发布系统通知类文章");
            }
        }

        if (baseService.publishResource(resource, ResourceType.valueOf(resourceType))) {
            log.info("saveOrUpdate resource, type = {}, resource = {}", resourceType, resource);
            return ok();
        }
        return fail();
    }

    @CheckPermission
    @PostMapping("publish/floor_book")
    public Rest publishFloorBook(@Valid @RequestBody PublishResourceDTO resource) {
        if (baseService.publishResource(resource, FLOOR_BOOK)) {
            return ok();
        }
        return fail();
    }

    /**
     * 批量操作
     *
     * @param resourceType 操作映射类型
     * @param type         操作类型
     */
    @CheckPermission
    @PostMapping("{resourceType:article|poster|floor_book}/bulk/{type:TOP|RELEASE|GROUP|DELETE}")
    public Rest editReleaseStatus(@Valid @RequestBody ResourceBulkOperation operation, @PathVariable String type, @PathVariable String resourceType) {

        resourceType = resourceType.toUpperCase();

        // 映射地址到操作类型
        operation.setResourceType(ResourceType.valueOf(resourceType).getCode());

        boolean result;
        switch (BulkOperationType.valueOf(type)) {
            case RELEASE:
                Assert.isTrue(null != operation.getStatus(), "请选择发布状态");
                result = baseService.editReleaseStatus(operation);
                break;
            case TOP:
                Assert.isTrue(null != operation.getTop(), "请选择是否置顶");
                result = baseService.topping(operation);
                break;
            case GROUP:
                Assert.isTrue(null != operation.getGroupId(), "请选择分组");
                result = baseService.editArticleGroup(operation);
                break;
            case DELETE:
                result = baseService.removeByIds(operation.getIds());
                break;
            default:
                throw new BusinessException("未知类型");
        }
        if (result) {
            saveAdminLogService.saveSystemLog("批量更新资源");
            return ok();
        }
        return fail();
    }

    /**
     * 临时接口，更新楼书为空
     *
     * @param projectId 项目id
     * @return Rest
     */
    @CheckPermission
    @PostMapping("/deleteFloorBook/{projectId}")
    public Rest removeFloorBook(@PathVariable Integer projectId) {
        return okObj(baseService.lambdaUpdate()
                .set(Resource::getResourceContent, "[]")
                .eq(Resource::getProjectId, projectId)
                .update());
    }

    /**
     * @author imyzt
     * @date 2019/07/02
     * @description 资源-批量操作类型
     */
    @Getter
    @AllArgsConstructor
    enum BulkOperationType {

        /**
         * 设置发布状态
         */
        RELEASE,
        /**
         * 设置置顶
         */
        TOP,
        /**
         * 设置分组
         */
        GROUP,

        /**
         * 删除资源
         */
        DELETE,
        ;

    }
}

