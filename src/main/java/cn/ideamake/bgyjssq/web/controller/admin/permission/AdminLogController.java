package cn.ideamake.bgyjssq.web.controller.admin.permission;


import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.CheckPermission;
import cn.ideamake.bgyjssq.pojo.entity.AdminLog;
import cn.ideamake.bgyjssq.pojo.query.AdminLogQuery;
import cn.ideamake.bgyjssq.service.permission.IAdminLogService;
import cn.ideamake.bgyjssq.web.controller.AbstractController;
import io.jsonwebtoken.lang.Collections;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.List;

/**
 * <p>
 * 管理员操作记录 前端控制器
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@AllArgsConstructor
@RestController
@RequestMapping("/admin/admin-log")
public class AdminLogController extends AbstractController<IAdminLogService, AdminLog, AdminLog> {

    /**
     * 分页查询管理员日志
     *
     * @return 查询结果
     */
    @CheckPermission
    @GetMapping("/list")
    public Rest findPage(AdminLogQuery adminQuery) {
        return okObj(baseService.findPage(adminQuery));
    }

    /**
     * 生成excel
     *
     * @param response response
     * @return String
     * @throws Exception Exception
     */
    @GetMapping("/exportExcel")
    @ApiOperation("导出Excel")
    public String exportExcel(HttpServletResponse response) throws Exception {
        //查询出需要导出的数据
        List<AdminLog> adminLogList = baseService.list();
        if (adminLogList == null || Collections.isEmpty(adminLogList)) {
            return "数据为空";
        }

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("统计表");
        //创建表头
        createTitle(workbook, sheet);
        //设置日期格式
        HSSFCellStyle style = workbook.createCellStyle();
        style.setDataFormat(HSSFDataFormat.getBuiltinFormat("yyyy-MM-dd HH:mm:ss"));
        //新增数据行，并且设置单元格数据
        int rowNum = 1;
        for (AdminLog adminLog : adminLogList) {
            HSSFRow row = sheet.createRow(rowNum);
            row.createCell(0).setCellValue(adminLog.getCreateAt() + "");
            row.createCell(1).setCellValue(adminLog.getAdminName());
            row.createCell(2).setCellValue(adminLog.getRoleName());
            row.createCell(3).setCellValue(adminLog.getPhone());
            row.createCell(4).setCellValue(adminLog.getOperatorContent());
            row.createCell(5).setCellValue(adminLog.getIpAddress());
            HSSFCell cell = row.createCell(6);
            cell.setCellStyle(style);
            rowNum++;
        }

        String fileName = "操作日志统计";
        //浏览器下载excel
        // 将文件存到浏览器设置的下载位置
        // 指定下载的文件名
        // +new String(filename.getBytes("gbk"), "iso8859-1")+
        response.setHeader("Content-Disposition", "attachment;filename="
                + new String(fileName.getBytes("GBK"), "ISO8859-1") + ".xls");
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setCharacterEncoding("utf-8");
        try (OutputStream out = response.getOutputStream()) {
            try {
                workbook.write(out);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 创建表头
     *
     * @param workbook workbook
     * @param sheet    sheet
     */
    private void createTitle(HSSFWorkbook workbook, HSSFSheet sheet) {
        HSSFRow row = sheet.createRow(0);
        //设置列宽，setColumnWidth的第二个参数要乘以256，这个参数的单位是1/256个字符宽度
        int width = 20 * 256;
        sheet.setColumnWidth(0, width);
        sheet.setColumnWidth(1, width);
        sheet.setColumnWidth(2, width);
        sheet.setColumnWidth(3, width);
        sheet.setColumnWidth(4, width);
        sheet.setColumnWidth(5, width);

        //设置为居中加粗
        HSSFCellStyle style = workbook.createCellStyle();
        HSSFFont font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);

        HSSFCell cell;
        cell = row.createCell(0);
        cell.setCellValue("操作时间");
        cell.setCellStyle(style);


        cell = row.createCell(1);
        cell.setCellValue("操作成员");
        cell.setCellStyle(style);

        cell = row.createCell(2);
        cell.setCellValue("角色");
        cell.setCellStyle(style);

        cell = row.createCell(3);
        cell.setCellValue("操作账号");
        cell.setCellStyle(style);

        cell = row.createCell(4);
        cell.setCellValue("操作内容");
        cell.setCellStyle(style);

        cell = row.createCell(5);
        cell.setCellValue("IP地址");
        cell.setCellStyle(style);
    }

}

