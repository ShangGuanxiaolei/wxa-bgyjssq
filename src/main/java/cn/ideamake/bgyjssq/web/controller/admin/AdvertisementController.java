package cn.ideamake.bgyjssq.web.controller.admin;

import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.common.validate.CheckPermission;
import cn.ideamake.bgyjssq.pojo.dto.AdvertisementDTO;
import cn.ideamake.bgyjssq.pojo.dto.TestDTO;
import cn.ideamake.bgyjssq.pojo.entity.Advertisement;
import cn.ideamake.bgyjssq.pojo.query.AdvertisementQuery;
import cn.ideamake.bgyjssq.service.IAdvertisementService;
import cn.ideamake.bgyjssq.web.controller.AbstractController;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ideamake
 * @since 2019-08-07
 */
@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping("/admin/advertisement")
public class AdvertisementController extends AbstractController<IAdvertisementService, Advertisement, Advertisement> {

    @AuthCheck(value = "保存广告")
    @PostMapping
    public Rest addOrEditAdvertisement(@Valid @RequestBody AdvertisementDTO dto) {
        return Optional.ofNullable(dto.getId())
                .map(s -> {
                    boolean result = baseService.editAdvertisement(dto);
                    if (result) {
                        return ok();
                    } else {
                        return fail();
                    }
                }).orElseGet(() -> {
                    boolean result = baseService.addAdvertisement(dto);
                    if (result) {
                        return ok();
                    } else {
                        return fail();
                    }
                });
    }

    @CheckPermission
    @AuthCheck(value = "分页查询广告列表")
    @PostMapping("/list")
    public Rest selectAdver(@RequestBody AdvertisementQuery advertisementQuery) {
        return okObj(baseService.selectAdver(advertisementQuery));
    }

    @CheckPermission
    @AuthCheck(value = "根据广告id对广告进行删除，可以批量删除")
    @PostMapping("/deleteAdver")
    public Rest deleteAdver(@Valid @RequestBody TestDTO testDTO) {
        String[] split = testDTO.getId().split(",");
        List<String> ids = Arrays.asList(split);
        return okObj(baseService.deleteAdver(ids));
    }

    @CheckPermission
    @AuthCheck(value = "批量更新广告状态")
    @GetMapping("/updateStatus")
    public Rest batchUpdateStatus(@RequestParam List<Integer> ids, @RequestParam Integer status) {
        return okObj(baseService.lambdaUpdate()
                .set(Advertisement::getStatus, status)
                .in(Advertisement::getId, ids)
                .update());
    }
}

