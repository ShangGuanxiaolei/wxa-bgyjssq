package cn.ideamake.bgyjssq.web.controller.admin.permission;


import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.CheckPermission;
import cn.ideamake.bgyjssq.pojo.entity.RolePermission;
import cn.ideamake.bgyjssq.pojo.entity.SysAdmin;
import cn.ideamake.bgyjssq.pojo.entity.SysRole;
import cn.ideamake.bgyjssq.pojo.query.QueryPage;
import cn.ideamake.bgyjssq.pojo.vo.permission.SysRolePermissionVO;
import cn.ideamake.bgyjssq.service.permission.IRolePermissionService;
import cn.ideamake.bgyjssq.service.permission.ISysAdminService;
import cn.ideamake.bgyjssq.service.permission.ISysRoleService;
import cn.ideamake.bgyjssq.web.controller.AbstractController;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.jsonwebtoken.lang.Collections;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static cn.ideamake.bgyjssq.common.util.Constants.ROOT;

/**
 * <p>
 * 系统角色表，记录系统中的角色信息 前端控制器
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping("/admin/sys-role")
public class SysRoleController extends AbstractController<ISysRoleService, SysRole, SysRole> {

    private final IRolePermissionService rolePermissionService;

    private final ISysAdminService sysAdminService;

    /**
     * 选择角色（添加、修改管理员使用）
     *
     * @return List<SysRole>
     */
    @GetMapping("/findAll")
    public Rest findAll() {
        return okObj(baseService.selectEffectiveRole());
    }

    /**
     * 分页查询角色列表
     *
     * @param name      角色名称
     * @param queryPage 分页
     * @return 分页数据
     */
    @CheckPermission
    @GetMapping("/list")
    public Rest findPage(QueryPage queryPage, String name) {
        return okObj(baseService.findByPage(queryPage, name));
    }

    /**
     * 保存角色信息
     *
     * @param rolePermissionVO 角色VO对象
     * @return 保存结果
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public Rest save(@RequestBody @Valid SysRolePermissionVO rolePermissionVO) {
        SysRole role = new SysRole();
        BeanUtils.copyProperties(rolePermissionVO, role);
        //保存操作
        return Optional.ofNullable(rolePermissionVO.getId())
                .map(s -> this.updateRolePermission(rolePermissionVO, role))
                .orElseGet(() -> this.addRolePermission(rolePermissionVO, role));

    }

    /**
     * 分页查询角色用户列表
     *
     * @param queryPage 分页
     * @param roleId    角色id
     * @return 分页数据
     */
    @CheckPermission
    @GetMapping("/listMember/{roleId}")
    public Rest listMember(@PathVariable Integer roleId, QueryPage queryPage) {
        return okObj(sysAdminService.findByRoleId(roleId, queryPage));
    }

    /**
     * 禁用角色
     *
     * @param id 角色id
     * @return 更新结果
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/prohibit/{id}")
    public Rest prohibit(@PathVariable Integer id) {
        if (baseService.prohibit(id)) {
            return ok();
        }
        return fail();
    }

    /**
     * 删除角色
     *
     * @param id 角色id
     * @return Rest
     */
    @CheckPermission
    @Transactional(rollbackFor = Exception.class)
    @DeleteMapping("/delete/{id}")
    public Rest deleteRole(@PathVariable Integer id) {
        if (ROOT == id) {
            return fail("禁止删除超级管理员角色");
        }
        List<SysAdmin> list = sysAdminService.lambdaQuery().eq(SysAdmin::getRoleId, id).list();
        if (!Collections.isEmpty(list)) {
            return fail("禁止删除含有成员的角色");
        }
        if (baseService.removeById(id)) {
            return ok();
        }
        return fail();
    }

    //=================私有方法专区=================//

    /**
     * 添加角色，分配权限
     *
     * @param rolePermissionVO 角色权限VO
     * @param role             角色
     * @return Rest
     */
    private Rest addRolePermission(SysRolePermissionVO rolePermissionVO, SysRole role) {
        if (baseService.save(role)) {
            if (savePermission(rolePermissionVO, role)) {
                return ok();
            }
        }
        return fail();
    }

    /**
     * 更新角色权限
     *
     * @param rolePermissionVO 角色权限VO
     * @param role             角色
     * @return Rest
     */
    private Rest updateRolePermission(SysRolePermissionVO rolePermissionVO, SysRole role) {
        if (ROOT == rolePermissionVO.getId()) {
            return fail("禁止修改超级管理员权限，请联系客服！");
        }
        //更新角色信息
        baseService.updateById(role);
        //处理角色权限，先根据角色id删除所有权限，再添加权限
        QueryWrapper<RolePermission> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("role_id", rolePermissionVO.getId());
        rolePermissionService.remove(queryWrapper);
        if (savePermission(rolePermissionVO, role)) {
            return ok();
        }
        return fail();
    }

    /**
     * 抽出的保存角色权限方法
     *
     * @param rolePermissionVO 封装的VO对象
     * @param role             角色
     */
    private boolean savePermission(SysRolePermissionVO rolePermissionVO, SysRole role) {
        //批量保存角色权限
        List<Integer> permissionIds = Optional.ofNullable(rolePermissionVO.getPermissionIds())
                .orElse(java.util.Collections.emptyList());
        List<RolePermission> permissions = new ArrayList<>();
        Integer roleId = role.getId();
        permissionIds.forEach(p -> {
            RolePermission permission = new RolePermission();
            permission.setPermissionId(p);
            //设置保存成功的角色id
            permission.setRoleId(roleId);
            permissions.add(permission);
        });
        return rolePermissionService.saveBatch(permissions);
    }

}

