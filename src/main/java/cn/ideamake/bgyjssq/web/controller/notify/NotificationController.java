package cn.ideamake.bgyjssq.web.controller.notify;

import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.pojo.entity.Resource;
import cn.ideamake.bgyjssq.pojo.query.StaffNotificationDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffNotificationQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffNotifyListQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorNotificationQuery;
import cn.ideamake.bgyjssq.pojo.vo.NotifyDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.UserRoleListVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.StaffNotificationResourceVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.StaffNotificationVO;
import cn.ideamake.bgyjssq.service.IAdvertisementService;
import cn.ideamake.bgyjssq.service.IResourceService;
import cn.ideamake.bgyjssq.web.controller.AbstractController;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.jsonwebtoken.lang.Collections;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static cn.ideamake.bgyjssq.common.util.Constants.HasGroup.HAS_GROUP;
import static cn.ideamake.bgyjssq.common.util.Constants.HasGroup.HAS_NOT_GROUP;

/**
 * <p>
 * 小程序端通知控制器
 * </p>
 *
 * @author ideamake
 * @since 2019-06-14
 */
@AllArgsConstructor
@RestController
@RequestMapping("/notification")
@Slf4j
public class NotificationController extends AbstractController<IResourceService, Resource, Resource> {

    private final IAdvertisementService advertisementService;

    @Deprecated
    @AuthCheck(value = "访客获取通知")
    @GetMapping("/visitorNotice")
    public Rest getVisitorNotification(@Valid VisitorNotificationQuery vn) {
        return okObj(baseService.getVisitorNotification(vn));
    }

    @AuthCheck(value = "访客获取通知")
    @GetMapping("/visitorNoticeNew")
    public Rest getVisitorNotificationNew(@Valid VisitorNotificationQuery vn) {
        return okObj(baseService.getVisitorNotificationNew(vn));
    }

    @AuthCheck(value = "访客获取通知")
    @GetMapping("/visitorNotice/group")
    public Rest getVisitorNotificationGroup(@Valid VisitorNotificationQuery vn) {
        return okObj(baseService.getVisitorNotificationResource(vn));
    }

    /**
     * 获取员工通知
     */
    @AuthCheck(value = "员工获取通知")
    @PostMapping("staffNotice")
    public Rest getStaffNotification(@Valid @RequestBody StaffNotificationQuery query) {
        StaffNotificationVO result = baseService.getStaffNotification(query);
        log.info("组:{}", result.getGroups().getRecords().toString());
        log.info("资源{}", result.getResources().getRecords().toString());
        return okObj(result);
    }

    /**
     * 通过资源组id获取组下面的所有资源列表，和基本pv，uv
     */
    @AuthCheck(value = "员工获取通知")
    @PostMapping("group/projects")
    public Rest getResourceIdByGroupId(@Valid @RequestBody StaffNotificationQuery query) {
        IPage<StaffNotificationResourceVO> resourceVO = baseService.getResourcesByGroupId(query);
        return okObj(resourceVO);
    }

    /**
     * 查询员工分组
     *
     * @param query 查询条件
     * @return Rest
     */
    @GetMapping("userGroup")
    public Rest getUserGroup(@Valid StaffNotificationDetailQuery query) {
        NotifyDetailVO notifyDetailVO = new NotifyDetailVO();
        List<UserRoleListVO> list = baseService.getUserGroup(query);
        if (Collections.isEmpty(list)) {
            notifyDetailVO.setArticleDetail(baseService.selectArticleDetailByArticleId(query));
            notifyDetailVO.setHasGroup(HAS_NOT_GROUP);
            return okObj(notifyDetailVO);
        }
        notifyDetailVO.setUserGroupList(list);
        notifyDetailVO.setHasGroup(HAS_GROUP);
        baseService.getHigherSellerData(notifyDetailVO, query);
        return okObj(notifyDetailVO);
    }

    /**
     * 查询员工列表数据
     *
     * @param query 查询条件
     * @return Rest
     */
    @GetMapping("userList")
    public Rest getUserList(@Valid StaffNotifyListQuery query) {
        return okObj(baseService.getUserList(query));
    }

    /**
     * 查询文章访问详情
     *
     * @return 查询结果
     */
    @GetMapping("articleReaderDetail")
    public Rest getArticleReaderDetail(@Valid StaffNotificationDetailQuery query) {
        return okObj(baseService.selectArticleDetailByArticleId(query));
    }

    /**
     * 查询广告图片
     *
     * @param id 项目id
     * @return Rest
     */
    @GetMapping("/getAdvertisement/{id}")
    public Rest getAdvertisement(@PathVariable String id) {
        Integer integer;
        try {
            integer = Integer.valueOf(id);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return fail("参数：项目id错误");
        }
        return okObj(advertisementService.selectEffectiveAdver(integer));
    }
}

