package cn.ideamake.bgyjssq.web.controller.collect;

import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.pojo.dto.CollectForAllParamDTO;
import cn.ideamake.bgyjssq.service.ICollectorAllRecordService;
import cn.ideamake.bgyjssq.web.controller.IController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: Walt
 * @Data: 2019-07-03 16:34
 * @Version: 1.0
 * 数据相关接口
 */
@Slf4j
@RestController
@RequestMapping("/data")
public class DataCollectorController implements IController {

    private final ICollectorAllRecordService collectorAllRecordService;

    public DataCollectorController(ICollectorAllRecordService collectorAllRecordService) {
        this.collectorAllRecordService = collectorAllRecordService;
    }

    /**
     * 小程序数据汇报接口
     *
     * @param collectForAllParamDTO 数据对象
     */
    @AuthCheck(value = "小程序数据汇报接口", skipAuth = 2)
    @PostMapping("/collect")
    public void dataCollector(@RequestBody CollectForAllParamDTO collectForAllParamDTO) {
        try {
            collectorAllRecordService.collectData(collectForAllParamDTO);
        } catch (Exception e) {
            log.error("上报数据失败，原因：" + e.getMessage());
            e.printStackTrace();
        }
    }
}
