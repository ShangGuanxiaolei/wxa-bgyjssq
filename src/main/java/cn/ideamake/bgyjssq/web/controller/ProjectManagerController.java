package cn.ideamake.bgyjssq.web.controller;


import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.pojo.dto.StaffHideProjectDTO;
import cn.ideamake.bgyjssq.pojo.entity.ProjectManager;
import cn.ideamake.bgyjssq.service.IProjectManagerService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * <p>
 * 员工项目管理表 前端控制器
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
@RestController
@RequestMapping("/project-manager")
public class ProjectManagerController extends AbstractController<IProjectManagerService,ProjectManager,ProjectManager> {

    /**
     * 员工隐藏展示项目
     */
    @AuthCheck(value = "员工隐藏展示项目")
    @PostMapping("ctrlProject")
    public Rest hideOrShowRecommendProject(@Valid @RequestBody StaffHideProjectDTO staffHideProjectDTO){
        if(baseService.hideProjectForStaff(staffHideProjectDTO)){
            return ok();
        }
        return fail();
    }

    @AuthCheck(skipAuth = 2)
    @RequestMapping("tmpTest")
    public Rest tmpTest(String userId,String proIds){
        return okObj(baseService.tmpTest(userId,proIds));
    }
}

