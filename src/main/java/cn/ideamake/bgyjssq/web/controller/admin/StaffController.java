package cn.ideamake.bgyjssq.web.controller.admin;

import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.CheckPermission;
import cn.ideamake.bgyjssq.pojo.dto.StaffBulkOperation;
import cn.ideamake.bgyjssq.pojo.entity.ProjectManager;
import cn.ideamake.bgyjssq.pojo.entity.User;
import cn.ideamake.bgyjssq.pojo.entity.UserInfo;
import cn.ideamake.bgyjssq.pojo.query.DigDataQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffKpiQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffListQuery;
import cn.ideamake.bgyjssq.pojo.vo.StaffListVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffVO;
import cn.ideamake.bgyjssq.service.IProjectManagerService;
import cn.ideamake.bgyjssq.service.IUserGroupManagerService;
import cn.ideamake.bgyjssq.service.IUserInfoService;
import cn.ideamake.bgyjssq.service.IUserService;
import cn.ideamake.bgyjssq.service.common.SaveAdminLogService;
import cn.ideamake.bgyjssq.service.permission.IRolePermissionService;
import cn.ideamake.bgyjssq.service.permission.ISysAdminProjectService;
import cn.ideamake.bgyjssq.web.controller.IController;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.jsonwebtoken.lang.Collections;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;

import static cn.ideamake.bgyjssq.common.util.Constants.PROTECT_STAFF_PHONE;
import static cn.ideamake.bgyjssq.common.util.Constants.PROTECT_STAFF_WE_CHAT;

/**
 * @author imyzt
 * @date 2019/07/15
 * @description 员工列表
 */
@AllArgsConstructor
@RestController
@RequestMapping("admin/staff")
@Slf4j
public class StaffController implements IController {

    private final IUserService userService;

    private final IUserInfoService userInfoService;

    private final IProjectManagerService projectManagerService;

    private final IUserGroupManagerService userGroupManagerService;

    private final SaveAdminLogService saveAdminLogService;

    private final ISysAdminProjectService sysAdminProjectService;

    private final IRolePermissionService rolePermissionService;

    @CheckPermission
    @GetMapping("list")
    public Rest list(StaffListQuery query) {
        if (CollectionUtils.isEmpty(query.getProjects())) {
            query.setProjects(new HashSet<>(sysAdminProjectService.getProjectList()));
        }
        IPage<StaffListVO> page = userService.getStaffInfoList(query);
        if (!rolePermissionService.hasProperties(PROTECT_STAFF_PHONE)) {
            page.getRecords().parallelStream().forEach(s -> s.setPhone("***"));
        }
        if (!rolePermissionService.hasProperties(PROTECT_STAFF_WE_CHAT)) {
            page.getRecords().parallelStream().forEach(s -> s.setWechatId("***"));
        }
        return okObj(page);
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("save")
    public Rest save(@Valid @RequestBody StaffVO staff) {
        User user = new User();
        BeanUtils.copyProperties(staff, user);
        user.setNickName(staff.getName());
        userService.save(user);
        UserInfo userInfo = new UserInfo();
        BeanUtils.copyProperties(staff, userInfo);
        userInfo.setUserUuid(user.getUuid());
        userInfoService.save(userInfo);
        saveAdminLogService.saveSystemLog("保存员工信息");
        return ok();
    }

    @CheckPermission
    @GetMapping("top10")
    public Rest top10(StaffKpiQuery query) {
        if (CollectionUtils.isEmpty(query.getProjectIds())) {
            query.setProjectIds(sysAdminProjectService.getProjectList());
        }
        return okObj(userService.selectStaffTopTenList(query));
    }

    @CheckPermission
    @GetMapping("digData")
    public Rest digData(@Valid DigDataQuery query) {
        if (CollectionUtils.isEmpty(query.getProjectIds())) {
            query.setProjectIds(sysAdminProjectService.getProjectList());
        }
        return okObj(userService.selectStaffDigData(query));
    }

    @CheckPermission
    @GetMapping("kpi")
    public Rest kpi(StaffKpiQuery query) {
        if (CollectionUtils.isEmpty(query.getProjectIds())) {
            query.setProjectIds(sysAdminProjectService.getProjectList());
        }
        return okObj(userService.selectStaffKpiList(query));
    }

    @CheckPermission
    @PostMapping("batchImportStaff")
    public Rest batchImportStaff(MultipartFile file) {
        if (userService.importStaff(file)) {
            saveAdminLogService.saveSystemLog("批量导入员工数据");
            return ok();
        }
        return fail("导入失败");
    }

    @CheckPermission
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("batchUpdateStaff/{type:DEPARTMENT|GROUP|STATUS|POSITION|BIND|UNBOUND|STAFF_TYPE}")
    public Rest batchUpdateStaff(@RequestBody StaffBulkOperation operation, @PathVariable String type) {
        type = type.toUpperCase();
        List<String> ids = operation.getIds();
        List<Integer> projectIds = operation.getProjectIds();
        switch (BulkOperationType.valueOf(type)) {
            case DEPARTMENT:
                userInfoService.lambdaUpdate()
                        .set(null != operation.getProjectId(),
                                UserInfo::getProjectId, operation.getProjectId())
                        .in(UserInfo::getUserUuid, ids)
                        .update();
                break;
            case GROUP:
                Integer groupId = operation.getGroupId();
                userInfoService.lambdaUpdate()
                        .set(null != groupId, UserInfo::getGroupId, groupId)
                        .in(UserInfo::getUserUuid, ids)
                        .update();
                break;
            case STATUS:
                userService.lambdaUpdate()
                        .set(null != operation.getStatus(),
                                User::getIsActive, operation.getStatus())
                        .in(User::getUuid, ids)
                        .update();
                break;
            case POSITION:
                userInfoService.lambdaUpdate()
                        .set(null != operation.getPosition(),
                                UserInfo::getPositionName, operation.getPosition())
                        .in(UserInfo::getUserUuid, ids)
                        .update();
                break;
            case BIND:
                if (Collections.isEmpty(projectIds)) {
                    throw new BusinessException("请选择项目");
                }
                projectIds.forEach(p -> ids.forEach(u -> {
                            List<ProjectManager> list = projectManagerService.lambdaQuery()
                                    .eq(ProjectManager::getUserUuid, u)
                                    .eq(ProjectManager::getProjectId, p)
                                    .list();
                            if (!Collections.isEmpty(list)) {
                                return;
                            }
                            projectManagerService.save(new ProjectManager(p, u));
                        }
                ));
                break;
            case UNBOUND:
                if (Collections.isEmpty(projectIds)) {
                    throw new BusinessException("请选择项目");
                }
                ids.forEach(u -> {
                    QueryWrapper<ProjectManager> queryWrapper = new QueryWrapper<>();
                    queryWrapper.lambda()
                            .eq(ProjectManager::getUserUuid, u)
                            .in(ProjectManager::getProjectId, projectIds);
                    projectManagerService.remove(queryWrapper);
                });
                break;
            case STAFF_TYPE:
                userInfoService.lambdaUpdate()
                        .set(null != operation.getStaffType(),
                                UserInfo::getStaffType, operation.getStaffType())
                        .in(UserInfo::getUserUuid, ids)
                        .update();
                break;
            default:
                throw new BusinessException("未知类型");
        }
        saveAdminLogService.saveSystemLog("批量更新员工信息");
        return ok();
    }

    @GetMapping("/getUserGroup")
    public Rest getUserAndGroup() {
        return okObj(userGroupManagerService.getUserGroup());
    }

    /**
     * @author gxl
     * @date 2019/08/10
     * @description 资源-批量操作类型
     */
    @Getter
    @AllArgsConstructor
    enum BulkOperationType {

        /**
         * 部门
         */
        DEPARTMENT,

        /**
         * 设置分组
         */
        GROUP,

        /**
         * 设置状态
         */
        STATUS,

        /**
         * 设置职务
         */
        POSITION,

        /**
         * 绑定
         */
        BIND,

        /**
         * 解除绑定
         */
        UNBOUND,

        /**
         * 用户类别
         */
        STAFF_TYPE,
        ;
    }
}
