package cn.ideamake.bgyjssq.web.controller.resource;

import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.util.AliyunOssKit;
import cn.ideamake.bgyjssq.pojo.entity.Resource;
import cn.ideamake.bgyjssq.service.IResourceService;
import cn.ideamake.bgyjssq.web.controller.AbstractController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * 资源controller
 *
 * @author gxl
 * @since 2019-10-14
 */
@RestController
@RequestMapping("/resource")
public class ResourceForMiniController
        extends AbstractController<IResourceService, Resource, Resource> {

    private final AliyunOssKit aliyunOssKit;

    public ResourceForMiniController(AliyunOssKit aliyunOssKit) {
        this.aliyunOssKit = aliyunOssKit;
    }

    /**
     * 查询 资源背景图
     *
     * @param resourceId 资源id
     * @return Rest
     */
    @GetMapping("/findResourceBg")
    public Rest findResourceBg(@RequestParam Integer resourceId) {
        return okObj(Optional.ofNullable(baseService.lambdaQuery()
                .eq(Resource::getId, resourceId)
                .one())
                .map(s -> aliyunOssKit.checkReturnUrl(s.getResourceBg()))
                .orElse(""));
    }

}

