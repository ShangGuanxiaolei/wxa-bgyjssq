package cn.ideamake.bgyjssq.web.controller.admin;


import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.CheckPermission;
import cn.ideamake.bgyjssq.pojo.dto.UserGroupEditDTO;
import cn.ideamake.bgyjssq.pojo.entity.UserGroupManager;
import cn.ideamake.bgyjssq.service.IUserGroupManagerService;
import cn.ideamake.bgyjssq.service.common.SaveAdminLogService;
import cn.ideamake.bgyjssq.web.controller.AbstractController;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * <p>
 * 员工分组管理表 前端控制器
 * </p>
 *
 * @author ideamake
 * @since 2019-08-13
 */
@AllArgsConstructor
@RestController
@RequestMapping("admin/user/group")
public class UserGroupManagerController extends AbstractController<IUserGroupManagerService, UserGroupManager, UserGroupManager> {

    private final SaveAdminLogService saveAdminLogService;

    /**
     * 保存员工组
     *
     * @param userGroupManager 员工组信息
     * @return Rest'
     */
    @CheckPermission
    @PostMapping("save")
    public Rest save(@RequestBody UserGroupManager userGroupManager) {
        if (baseService.save(userGroupManager)) {
            saveAdminLogService.saveSystemLog("保存员工组信息");
            return ok();
        }
        return fail();
    }

    /**
     * 更新组管理员
     */
    @CheckPermission
    @PostMapping("update")
    public Rest updateGroupManager(@Valid @RequestBody UserGroupEditDTO userGroupEditDTO) {
        if (baseService.updateGroupManagerInfo(userGroupEditDTO)) {
            saveAdminLogService.saveSystemLog("更新员工组信息");
            return ok();
        }
        return fail();
    }

    /**
     * 通过组id获取组管理员和人员列表
     */
    @GetMapping("members/{groupId}")
    public Rest getUserAndManagerList(@PathVariable Integer groupId) {
        return okObj(baseService.selectUserAndManagerByGroupId(groupId));
    }

    @GetMapping("users")
    public Rest getUsers() {
        return okObj(baseService.selectUsers());
    }

    /**
     * 获取项目下的所有销售信息
     *
     * @param projectId 项目id
     * @return Rest
     */
    @GetMapping("projectUsers")
    public Rest getProjectUsers(@RequestParam Integer projectId) {
        return okObj(baseService.getGroupByProjectId(projectId));
    }

}

