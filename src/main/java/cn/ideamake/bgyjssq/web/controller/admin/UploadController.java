package cn.ideamake.bgyjssq.web.controller.admin;

import cn.hutool.core.util.StrUtil;
import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.util.AliyunOssKit;
import cn.ideamake.bgyjssq.common.util.Constants;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.web.controller.IController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.concurrent.TimeUnit;

/**
 * @author imyzt
 * @date 2019/07/06
 * @description 文件上传服务, 此接口必须加限制防止资源滥用
 */
@RequestMapping("admin/common")
@RestController
@Slf4j
public class UploadController implements IController {

    @Value("${floor_book.url.page}")
    private String suffix;

    private final AliyunOssKit aliyunOssKit;

    private final RedisTemplate<String, String> redisTemplate;

    public UploadController(AliyunOssKit aliyunOssKit, RedisTemplate<String, String> redisTemplate) {
        this.aliyunOssKit = aliyunOssKit;
        this.redisTemplate = redisTemplate;
    }

    /**
     * 文件上传接口
     */
    @PostMapping("upload")
    public Rest upload(MultipartFile file, String type) {
        log.info("type = {}, fileName = {}", type, file.getOriginalFilename());
        String fileName = aliyunOssKit.upload(file);
        String urlByFileName = aliyunOssKit.getUrlByFileName(fileName);

        // 存储在redis中
        redisTemplate.opsForHash().put(Constants.REDIS_CACHE.UPLOAD_IMAGE, urlByFileName, fileName);
        // 一天有效期
        redisTemplate.expire(Constants.REDIS_CACHE.UPLOAD_IMAGE, 1, TimeUnit.DAYS);

        return okObj(urlByFileName);
    }

    @AuthCheck(value = "720文件上传")
    @PostMapping("/upload720/{projectId}")
    public Rest upload720page(MultipartFile file, @PathVariable Integer projectId) {
        if (file == null) {
            throw new BusinessException("请选择上传文件");
        }
        if (projectId == null) {
            throw new BusinessException("请选择项目");
        }
        String path = aliyunOssKit.uploadZip(file, projectId);
        log.info("===============[ " + path + " ]===============");
        if (StrUtil.isBlank(path)) {
            throw new BusinessException("上传失败，请重试");
        }
        return okObj(path + suffix);
    }
}
