package cn.ideamake.bgyjssq.web.controller.admin.permission;


import cn.ideamake.bgyjssq.cache.DoGuavaCache;
import cn.ideamake.bgyjssq.cache.constant.AbstractCacheKeyConstant;
import cn.ideamake.bgyjssq.cache.constant.ExpireAt;
import cn.ideamake.bgyjssq.common.enums.PermissionTypeEnum;
import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.pojo.entity.SysPermission;
import cn.ideamake.bgyjssq.service.permission.ISysPermissionService;
import cn.ideamake.bgyjssq.web.controller.AbstractController;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 系统权限资源表，记录系统的权限资源信息 前端控制器
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@RestController
@RequestMapping("/admin/sys-permission")
public class SysPermissionController extends AbstractController<ISysPermissionService, SysPermission, SysPermission> {

    /**
     * 保存系统权限
     *
     * @param permission 系统权限对象实体
     * @return 操作结果
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public Rest<SysPermission> save(@RequestBody @Valid SysPermission permission) {
        if (baseService.saveAndUpdate(permission)) {
            return ok(permission);
        }
        return fail();
    }

    /**
     * 查询系统资源类型
     *
     * @return Rest
     */
    @DoGuavaCache(key = AbstractCacheKeyConstant.PERMISSION_TYPE, expireAt = ExpireAt.SIXTH)
    @GetMapping("/list/type")
    public Rest list() {
        PermissionTypeEnum[] promotionTypes = PermissionTypeEnum.values();
        PermissionTypeEnum key;
        Map<PermissionTypeEnum, Object> map = new HashMap<>(8);
        for (PermissionTypeEnum permissionTypeEnum :
                promotionTypes) {
            key = permissionTypeEnum;
            map.put(key, permissionTypeEnum.getName());
        }
        return okObj(map);
    }

    /**
     * 查询系统权限
     *
     * @return List<PermissionVO>
     */
    @DoGuavaCache(key = AbstractCacheKeyConstant.PERMISSION_LIST_ALL, expireAt = ExpireAt.SIXTH)
    @GetMapping("/list/permission")
    public Rest selectByParentId() {
        return okObj(baseService.selectAllByParent());
    }

    /**
     * 根据权限类型查询角色权限
     *
     * @param roleId 角色id
     * @param type   权限类型
     * @return 权限VO
     */
    @GetMapping("/list/permission/{roleId}/{type}")
    public Rest selectByRoleIdAndType(@PathVariable Integer roleId, @PathVariable PermissionTypeEnum type) {
        return okObj(baseService.selectByRoleIdAndType(roleId, type));
    }

}

