package cn.ideamake.bgyjssq.web.controller.visitor;

import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.pojo.entity.Visitor;
import cn.ideamake.bgyjssq.pojo.query.VisitorLogQuery;
import cn.ideamake.bgyjssq.service.IVisitorService;
import cn.ideamake.bgyjssq.web.controller.AbstractController;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 访客表 前端控制器
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
@RestController
@RequestMapping("/visitor")
public class VisitorController extends AbstractController<IVisitorService, Visitor, Visitor> {

    /**
     * 访客获取我的日志
     */
    @AuthCheck(value = "访客获取我的日志")
    @GetMapping("/logs")
    public Rest getVisitorLogs(VisitorLogQuery visitorLogQuery, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            FieldError fieldError = bindingResult.getFieldError();
            return fail(fieldError != null ? fieldError.getDefaultMessage() : null);
        }
        return okObj(baseService.getVisitorLog(visitorLogQuery));
    }


}

