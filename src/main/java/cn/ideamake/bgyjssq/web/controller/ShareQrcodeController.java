package cn.ideamake.bgyjssq.web.controller;

import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.util.Constants;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.pojo.bo.QrCodeUrlSaveBO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @Author: Walt
 * @Data: 2019-08-23 16:42
 * @Version: 1.0
 */
@RestController
@RequestMapping("/code")
@AuthCheck(skipAuth = 2)
public class ShareQrcodeController implements IController {

    private final RedisTemplate<String, String> redisTemplate;

    public ShareQrcodeController(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @PostMapping("save")
    public Rest setQrCodeKv(@Valid @RequestBody QrCodeUrlSaveBO kv) {
        redisTemplate.opsForHash().put(Constants.CACHE_PREFIX_LABEL.PROJECT_QRCODE, kv.getKey(), kv.getValue());
        return ok();
    }

    @GetMapping("{key}")
    public Rest getQrCodeKv(@PathVariable String key) {
        String undefined = "undefined";
        if (StringUtils.isBlank(key) || undefined.equals(key)) {
            return fail();
        }
        String value = (String) redisTemplate.opsForHash().get(Constants.CACHE_PREFIX_LABEL.PROJECT_QRCODE, key);
        return okObj(value);
    }
}
