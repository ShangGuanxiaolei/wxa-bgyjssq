package cn.ideamake.bgyjssq.web.controller;


import cn.ideamake.bgyjssq.common.enums.RestEnum;
import cn.ideamake.bgyjssq.common.response.Rest;

/**
 * @author imyzt
 * @date 2019/1/7 11:39
 * @description IController
 */
public interface IController<T, V> {

    /**
     * 成功的返回
     * @return 响应对象
     */
    default Rest<T> ok() {
        Rest <T> rest = new Rest <>();
        rest.setCode(RestEnum.SUCCESS.getCode());
        rest.setMsg("操作成功");
        return rest;
    }

    /**
     * 成功的返回
     * @param obj 参数
     * @return 响应对象
     */
    default Rest<Object> okObj(Object obj) {
        Rest <Object> rest = new Rest <>();
        rest.setMsg("操作成功");
        rest.setCode(RestEnum.SUCCESS.getCode());
        rest.setData(obj);
        return rest;
    }

    /**
     * 成功的返回
     * @param data 数据
     * @return 响应对象
     */
    default Rest<T> ok(T data) {
        Rest <T> rest = new Rest <>();
        rest.setCode(RestEnum.SUCCESS.getCode());
        rest.setData(data);
        return rest;
    }

    /**
     * 响应视图对象
     * @param vo 视图对象
     * @return 响应视图对象
     */
    default Rest<V> okVo(V vo) {
        Rest <V> rest = new Rest <>();
        rest.setCode(RestEnum.SUCCESS.getCode());
        rest.setData(vo);
        return rest;
    }

    /**
     * 失败的返回
     * @return 错误响应
     */
    default Rest<T> fail() {
        Rest <T> rest = new Rest <>();
        rest.setCode(RestEnum.FAIL.getCode());
        rest.setMsg("操作失败");
        return rest;
    }

    /**
     * 失败的返回
     * @param msg 消息
     * @return 错误响应
     */
    default Rest<T> fail(String msg) {
        Rest <T> rest = new Rest <>();
        rest.setCode(RestEnum.FAIL.getCode());
        rest.setMsg(msg);
        return rest;
    }

    /**
     * 参数错误
     * @param msg 具体参数
     * @return  参数错误
     */
    default Rest<T> parameter(String msg) {
        Rest <T> rest = new Rest <>();
        rest.setCode(RestEnum.PARAMETER_ERROR.getCode());
        rest.setMsg(msg);
        return rest;
    }
}
