package cn.ideamake.bgyjssq.web.controller.admin;


import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.common.validate.CheckPermission;
import cn.ideamake.bgyjssq.pojo.dto.FloorBook720DTO;
import cn.ideamake.bgyjssq.pojo.dto.FloorBook720StatusDTO;
import cn.ideamake.bgyjssq.pojo.dto.ProjectGroupListDTO;
import cn.ideamake.bgyjssq.pojo.dto.SaveOrChangeProjectDTO;
import cn.ideamake.bgyjssq.pojo.dto.project.ProjectDTO;
import cn.ideamake.bgyjssq.pojo.entity.Project;
import cn.ideamake.bgyjssq.pojo.entity.Resource720;
import cn.ideamake.bgyjssq.pojo.entity.SysAdminProject;
import cn.ideamake.bgyjssq.pojo.query.DefaultQueryPage;
import cn.ideamake.bgyjssq.pojo.query.VisitorProjectQuery;
import cn.ideamake.bgyjssq.service.IProjectService;
import cn.ideamake.bgyjssq.service.IResource720Service;
import cn.ideamake.bgyjssq.service.common.SaveAdminLogService;
import cn.ideamake.bgyjssq.service.permission.ISysAdminProjectService;
import cn.ideamake.bgyjssq.web.controller.AbstractController;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.jsonwebtoken.lang.Collections;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

import static cn.ideamake.bgyjssq.common.util.Constants.Archive.IS_DELETE;
import static java.util.stream.Collectors.toList;

/**
 * <p>
 * 楼盘(项目)PC后台操作控制器
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
@AllArgsConstructor
@RestController
@RequestMapping("admin/project")
@Slf4j
public class ProjectController extends AbstractController<IProjectService, Project, Project> {

    private final IResource720Service resource720Service;

    private final SaveAdminLogService saveAdminLogService;

    private final ISysAdminProjectService sysAdminProjectService;

    /**
     * 作为查询条件使用
     *
     * @param query 查询条件
     * @return Rest
     */
    @GetMapping("list")
    public Rest getConditionAttention(VisitorProjectQuery query) {
        return getProjectList(query);
    }

    /**
     * 权限适配-楼盘列表
     *
     * @param query 查询条件
     * @return Rest
     */
    @CheckPermission
    @GetMapping("/realEstate/list")
    public Rest getRealEstateList(VisitorProjectQuery query) {
        return getProjectList(query);
    }

    /**
     * 权限适配-项目首页
     *
     * @param query 查询条件
     * @return Rest
     */
    @CheckPermission
    @GetMapping("/homePage/list")
    public Rest getHomePage(VisitorProjectQuery query) {
        return getProjectList(query);
    }

    /**
     * 权限适配-项目楼书
     *
     * @param query 查询条件
     * @return Rest
     */
    @CheckPermission
    @GetMapping("/floorBook/list")
    public Rest getFloorBookList(VisitorProjectQuery query) {
        return getProjectList(query);
    }

    @CheckPermission
    @GetMapping("listFloorBook720")
    public Rest getFloorBook720List(VisitorProjectQuery query) {
        if (CollectionUtils.isEmpty(query.getProjectIds())) {
            query.setProjectIds(sysAdminProjectService.getProjectList());
        }
        return okObj(baseService.selectProjectFloorBook720List(query));
    }

    @CheckPermission
    @PostMapping("delete/{projectId}")
    public Rest deleteById(@PathVariable Integer projectId) {
        if (baseService.lambdaUpdate()
                .set(Project::getDeleteMark, 1)
                .eq(Project::getId, projectId)
                .update()) {
            //移除项目授权
            QueryWrapper<SysAdminProject> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda().eq(SysAdminProject::getProjectId, projectId);
            sysAdminProjectService.remove(queryWrapper);
            log.info("delete project id = {}", projectId);
            saveAdminLogService.saveSystemLog("删除项目");
            return ok();
        }
        return fail("删除失败");
    }

    @PostMapping
    public Rest save(@RequestBody SaveOrChangeProjectDTO project) {
        boolean saveOrUpdate = baseService.saveOrUpdateProject(project);
        if (saveOrUpdate) {
            saveAdminLogService.saveSystemLog("保存项目");
            return ok();
        }
        return fail();
    }

    @GetMapping("projectGroupList")
    public Rest projectGroupList(DefaultQueryPage query) {
        return okObj(baseService.selectProjectGroupList(query));
    }

    /**
     * 通过组ID来获取组下项目基本信息
     */
    @GetMapping("projectBaseByGroupId")
    public Rest projectBaseByGroupId(Integer groupId) {
        return okObj(baseService.projectBaseByGroupId(groupId));
    }

    /**
     * 获取项目组和项目
     */
    @GetMapping("projectAndGroupList")
    public Rest getProjectAndGroupList(@RequestParam(value = "sellerId", required = false) String sellerId) {
        List<ProjectGroupListDTO> dtoList = baseService.selectProjectGroupTree(sellerId);
        dtoList.forEach(d -> d.setProjectList(d.getProjectList().stream()
                .filter(p -> sysAdminProjectService.getProjectList()
                        .contains(p.getProjectId()))
                .collect(toList())));
        List<ProjectGroupListDTO> list = dtoList.parallelStream()
                .filter(d -> CollectionUtils.isNotEmpty(d.getProjectList()))
                .collect(toList());
        return okObj(list);
    }

    @CheckPermission
    @PostMapping("/setDefaultSeller/{projectId}/{sellerId}")
    public Rest setDefaultSeller(@PathVariable Integer projectId, @PathVariable String sellerId) {
        if (baseService.lambdaUpdate()
                .set(Project::getDefaultSeller, sellerId)
                .eq(Project::getId, projectId)
                .update()) {
            return ok();
        }
        return fail();
    }

    @CheckPermission
    @Transactional(rollbackFor = Exception.class)
    @AuthCheck(value = "批量删除")
    @DeleteMapping("/720/delete")
    public Rest deleteResource720(@RequestParam List<Integer> ids) {
        if (Collections.isEmpty(ids)) {
            throw new BusinessException("请选择删除的数据");
        }
        saveAdminLogService.saveSystemLog("删除720楼书数据");
        if (resource720Service.lambdaUpdate()
                .set(Resource720::getArchive, IS_DELETE)
                .in(Resource720::getId, ids)
                .update()) {
            return ok();
        }
        return fail();
    }

    @CheckPermission
    @Transactional(rollbackFor = Exception.class)
    @AuthCheck(value = "批量更新状态")
    @PostMapping("/720/updateStatus")
    public Rest updateResource720Status(@Valid @RequestBody FloorBook720StatusDTO floorBook720StatusDTO) {
        saveAdminLogService.saveSystemLog("批量更新720楼书状态");
        if (resource720Service.lambdaUpdate()
                .set(Resource720::getStatus, floorBook720StatusDTO.getStatus())
                .in(Resource720::getId, floorBook720StatusDTO.getIds())
                .update()) {
            return ok();
        }
        return fail();
    }

    @AuthCheck(value = "保存720楼书")
    @PostMapping("/save720")
    public Rest save720(@Valid @RequestBody FloorBook720DTO dto) {
        Resource720 resource720 = new Resource720();
        BeanUtils.copyProperties(dto, resource720);
        if (Optional.ofNullable(dto.getId())
                .map(d -> resource720Service.lambdaUpdate()
                        .eq(Resource720::getId, dto.getId())
                        .update(resource720))
                .orElseGet(() -> resource720Service.save(resource720))) {
            saveAdminLogService.saveSystemLog("保存720楼书数据");
            return ok();
        }
        return fail();
    }

    /**
     * 设置售楼处坐标
     *
     * @param project 项目
     * @return Rest
     */
    @CheckPermission
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/setAddress")
    public Rest setSalesOfficesAddress(@Valid @RequestBody ProjectDTO project) {
        if (baseService.lambdaUpdate()
                .set(Project::getLongitude, project.getLongitude())
                .set(Project::getLatitude, project.getLatitude())
                .set(Project::getRadius, project.getRadius())
                .eq(Project::getId, project.getId())
                .update()) {
            return ok();
        } else {
            return fail();
        }
    }

    /**
     * 上传项目楼书海报图
     *
     * @param poster 海报对象
     * @return Rest
     */
    @PostMapping("/setProfilePoster")
    public Rest setProfilePoster(@RequestBody @Valid ProjectPoster poster) {
        if (baseService.lambdaUpdate()
                .set(Project::getProfilePoster, poster.getProfilePoster())
                .eq(Project::getId, poster.getProjectId())
                .update()) {
            return ok();
        }
        return fail();
    }

    /**
     * 查询项目列表
     *
     * @param query 查询条件
     * @return Rest
     */
    private Rest getProjectList(VisitorProjectQuery query) {
        if (CollectionUtils.isEmpty(query.getProjectIds())) {
            query.setProjectIds(sysAdminProjectService.getProjectList());
        }
        return okObj(baseService.selectProjectList(query));
    }

    /**
     * @author gxl
     */
    @Data
    private static final class ProjectPoster {

        /**
         * 项目id
         */
        @NotNull(message = "项目id不能为空")
        private final Integer projectId;

        /**
         * 项目简介海报
         */
        @NotNull(message = "海报图不能为空")
        private final String profilePoster;
    }

}

