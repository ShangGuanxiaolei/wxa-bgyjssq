package cn.ideamake.bgyjssq.web.controller.wx;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaKefuMessage;
import cn.binarywang.wx.miniapp.bean.WxMaMessage;
import cn.binarywang.wx.miniapp.constant.WxMaConstants;
import cn.ideamake.bgyjssq.common.config.wxconfig.WxMaConfiguration;
import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.util.AliyunOssKit;
import cn.ideamake.bgyjssq.common.util.UrlUtil;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.pojo.bo.URLEntity;
import cn.ideamake.bgyjssq.pojo.entity.OauthWechat;
import cn.ideamake.bgyjssq.pojo.entity.Resource;
import cn.ideamake.bgyjssq.service.IOauthWechatService;
import cn.ideamake.bgyjssq.service.IResourceService;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @author <a href="https://github.com/binarywang">Binary Wang</a>
 */
@RestController
@RequestMapping("/wx/portal/{appid}")
@AuthCheck(skipAuth = 2)
@Slf4j
public class WxPortalController {

    @Value("${server.hostPre.article}")
    private String resourceArticle;

    private static final String RESOURCE_LABEL = "resourceId";

    private static final String MESSAGE_TYPE_MINI_PROGRAM_PAGE = "miniprogrampage";

    private final IOauthWechatService oauthWechatService;

    private final AliyunOssKit aliyunOssKit;

    private final IResourceService resourceService;

    public WxPortalController(IOauthWechatService oauthWechatService, AliyunOssKit aliyunOssKit, IResourceService resourceService) {
        this.oauthWechatService = oauthWechatService;
        this.aliyunOssKit = aliyunOssKit;
        this.resourceService = resourceService;
    }

    @GetMapping(produces = "text/plain;charset=utf-8")
    public String authGet(@PathVariable String appid,
                          @RequestParam(name = "signature", required = false) String signature,
                          @RequestParam(name = "timestamp", required = false) String timestamp,
                          @RequestParam(name = "nonce", required = false) String nonce,
                          @RequestParam(name = "echostr", required = false) String echostr) {
        log.info("\n接收到来自微信服务器的认证消息：" +
                        "appId = [{}], " +
                        "signature = [{}], " +
                        "timestamp = [{}], " +
                        "nonce = [{}], " +
                        "echostr = [{}]",
                appid, signature, timestamp, nonce, echostr);

        if (StringUtils.isAnyBlank(signature, timestamp, nonce, echostr)) {
            throw new IllegalArgumentException("请求参数非法，请核实!");
        }

        final WxMaService wxService = WxMaConfiguration.getMaService(appid);

        if (wxService.checkSignature(timestamp, nonce, signature)) {
            return echostr;
        }

        return "非法请求";
    }

    @PostMapping(produces = "application/xml; charset=UTF-8")
    public String post(@PathVariable String appid,
                       @RequestBody String requestBody,
                       @RequestParam(name = "msg_signature", required = false) String msgSignature,
                       @RequestParam(name = "encrypt_type", required = false) String encryptType,
                       @RequestParam(name = "signature", required = false) String signature,
                       @RequestParam("timestamp") String timestamp,
                       @RequestParam("nonce") String nonce) {
        log.info("\n接收微信请求：" +
                        "appId = [{}]" +
                        "msg_signature = [{}], " +
                        "encrypt_type = [{}], " +
                        "signature = [{}]," +
                        " timestamp = [{}], " +
                        "nonce = [{}], " +
                        "requestBody = [\n{}\n]",
                appid, msgSignature, encryptType, signature, timestamp, nonce, requestBody);

        final WxMaService wxService = WxMaConfiguration.getMaService(appid);

        WxMaMessage inMessage = getWxMaMessage(requestBody, msgSignature, encryptType, timestamp, nonce, wxService);

        //解析请求中的参数内容
        URLEntity hostAndParams = UrlUtil.parse(inMessage.getPagePath());
        log.info("解析后的请求参数信息：{}", hostAndParams);

        if (MESSAGE_TYPE_MINI_PROGRAM_PAGE.equals(inMessage.getMsgType())) {
            log.info("微信返回消息{}", inMessage.toJson());
            OauthWechat oauthWechat = oauthWechatService.getUserWeChatInfoByOpenId(inMessage.getFromUser());

            if (oauthWechat == null) {
                log.error("推送用户不存在");
                throw new BusinessException("推送用户不存在");
            }

            if (hostAndParams == null
                    || hostAndParams.getParams() == null
                    || !hostAndParams.getParams().containsKey(RESOURCE_LABEL)) {
                log.error("未携带资源id,请求内容为:{}", inMessage.toJson());
                return "error";
            }

            Resource resource = resourceService.getById(hostAndParams.getParams().get(RESOURCE_LABEL));
            if (resource == null) {
                log.error("资源不存在{},请求内容为:{}" + hostAndParams.getParams().get(RESOURCE_LABEL), inMessage.toJson());
                return "error";
            }

            return sendMsg(wxService, inMessage, hostAndParams, resource);
        }
        log.info("请求内容不是小程序，忽略消息:{}", inMessage.toJson());
        return "success";
    }

    private String sendMsg(WxMaService wxService, WxMaMessage inMessage, URLEntity hostAndParams, Resource resource) {
        StringBuilder stringBuilder = getParams(hostAndParams);

        try {
            String url = resourceArticle + stringBuilder.toString();
            wxService.getMsgService().sendKefuMsg(WxMaKefuMessage.newLinkBuilder()
                    .url(url)
                    .toUser(inMessage.getFromUser())
                    .title(resource.getResourceTitle())
                    .thumbUrl(aliyunOssKit.checkReturnUrl(resource.getResourceBg()))
                    .description(resource.getResourceTitle())
                    .build());
            log.info("返回前端的链接：{}", url);
            return "success";

        } catch (WxErrorException e) {
            log.error("推送失败：" + e.getMessage());
            return e.getMessage();
        }
    }

    @NotNull
    private WxMaMessage getWxMaMessage(@RequestBody String requestBody,
                                       @RequestParam(name = "msg_signature", required = false) String msgSignature,
                                       @RequestParam(name = "encrypt_type", required = false) String encryptType,
                                       @RequestParam("timestamp") String timestamp,
                                       @RequestParam("nonce") String nonce, WxMaService wxService) {
        final boolean isJson = Objects.equals(wxService.getWxMaConfig().getMsgDataFormat(),
                WxMaConstants.MsgDataFormat.JSON);
        WxMaMessage inMessage = null;
        if (StringUtils.isBlank(encryptType)) {
            // 明文传输的消息
            if (isJson) {
                inMessage = WxMaMessage.fromJson(requestBody);
                log.info("经过解析后的参数，json格式：{}", inMessage);
            } else {//xml
                inMessage = WxMaMessage.fromXml(requestBody);
                log.info("经过解析后的参数，xml格式：{}", inMessage);
            }
        }

        String aes = "aes";
        if (aes.equals(encryptType)) {
            // 是aes加密的消息
            if (isJson) {
                inMessage = WxMaMessage.fromEncryptedJson(requestBody, wxService.getWxMaConfig());
                log.info("经过解密后的参数，json格式：{}", inMessage);
            } else {//xml
                inMessage = WxMaMessage.fromEncryptedXml(requestBody, wxService.getWxMaConfig(),
                        timestamp, nonce, msgSignature);
                log.info("经过解密后的参数，xml格式：{}", inMessage);
            }
        }
        if (inMessage == null) {
            throw new RuntimeException("不可识别的加密类型：" + encryptType);
        }
        return inMessage;
    }

    /**
     * 获取拼接参数
     *
     * @param hostAndParams 参数map
     * @return StringBuilder
     */
    @NotNull
    private StringBuilder getParams(URLEntity hostAndParams) {
        Map<String, String> params = hostAndParams.getParams();
        Iterator<Map.Entry<String, String>> it = params.entrySet().iterator();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("?");
        String stringBgCover = "bgCover";
        while (it.hasNext()) {
            Map.Entry<String, String> entry = it.next();
            String key = entry.getKey();
            if (stringBgCover.equals(key)) {
                continue;
            }
            stringBuilder.append(key);
            stringBuilder.append("=");
            stringBuilder.append(entry.getValue());
            if (it.hasNext()) {
                stringBuilder.append("&");
            }
        }
        //特殊处理icon参数
        String bgCover = Optional.ofNullable(params.get(stringBgCover))
                .map(String::toString)
                .orElse("");
        stringBuilder.append("&bgCover=").append(bgCover);
        return stringBuilder;
    }

}
