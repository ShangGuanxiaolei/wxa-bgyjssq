package cn.ideamake.bgyjssq.web.controller.user;

import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.ideamake.bgyjssq.common.components.LogService;
import cn.ideamake.bgyjssq.common.enums.RestEnum;
import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.handler.UserInfoContext;
import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.util.WeChatService;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.pojo.bo.Token;
import cn.ideamake.bgyjssq.pojo.bo.WXUserInfo;
import cn.ideamake.bgyjssq.pojo.dto.BussinessCardHomeEditDTO;
import cn.ideamake.bgyjssq.pojo.dto.CodeToTokenDTO;
import cn.ideamake.bgyjssq.pojo.dto.InviteCodeGenDTO;
import cn.ideamake.bgyjssq.pojo.dto.ProjectQRCodeDTO;
import cn.ideamake.bgyjssq.pojo.dto.ScanInviteCodeDTO;
import cn.ideamake.bgyjssq.pojo.dto.UserEditDTO;
import cn.ideamake.bgyjssq.pojo.dto.VisitorInfoDTO;
import cn.ideamake.bgyjssq.pojo.dto.VisitorSubmitPhoneDTO;
import cn.ideamake.bgyjssq.pojo.entity.BindingManager;
import cn.ideamake.bgyjssq.pojo.entity.StaffVisitor;
import cn.ideamake.bgyjssq.pojo.entity.User;
import cn.ideamake.bgyjssq.pojo.entity.UserPushActivity;
import cn.ideamake.bgyjssq.pojo.entity.Visitor;
import cn.ideamake.bgyjssq.pojo.query.ActivityDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.BusinessQRCodeQuery;
import cn.ideamake.bgyjssq.pojo.query.SignInAuthQuery;
import cn.ideamake.bgyjssq.pojo.query.SignInQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffVisitorQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffVisitorRankQuery;
import cn.ideamake.bgyjssq.pojo.query.UserProjectQuery;
import cn.ideamake.bgyjssq.pojo.query.UserVisitorProjectQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorInfoDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorTrendQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorVisitAnalysisQuery;
import cn.ideamake.bgyjssq.pojo.vo.BussinessCardHomeVO;
import cn.ideamake.bgyjssq.pojo.vo.ExpireVO;
import cn.ideamake.bgyjssq.pojo.vo.InviteRegisterVO;
import cn.ideamake.bgyjssq.pojo.vo.PushActivitisVO;
import cn.ideamake.bgyjssq.pojo.vo.UserDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.UserSessionVO;
import cn.ideamake.bgyjssq.service.IBindingManagerService;
import cn.ideamake.bgyjssq.service.IProjectService;
import cn.ideamake.bgyjssq.service.ISignInQrCodeService;
import cn.ideamake.bgyjssq.service.IStaffVisitorService;
import cn.ideamake.bgyjssq.service.IUserPushActivityService;
import cn.ideamake.bgyjssq.service.IUserService;
import cn.ideamake.bgyjssq.service.IVisitorService;
import cn.ideamake.bgyjssq.web.controller.AbstractController;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static cn.ideamake.bgyjssq.common.util.Constants.TOURIST_UUID;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
@AllArgsConstructor
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController extends AbstractController<IUserService, User, User> {

    /**
     * 微信登录错误提示
     */
    private static final String ERROR_MESSAGE = "微信授权失败，请检查微信功能是否正常";

    private final WeChatService weChatService;

    private final IStaffVisitorService staffVisitorService;

    private final IProjectService projectService;

    private final ISignInQrCodeService signInQrCodeService;

    private final IUserPushActivityService userPushActivityService;

    private final IVisitorService visitorService;

    private final IBindingManagerService bindingManagerService;

    private final LogService logService;

    /**
     * 获取销售或者详情
     */
    @AuthCheck(value = "获取用户详情")
    @GetMapping("/detail")
    public Rest getUserInfo(String userId) {
        if (StringUtils.isBlank(userId)) {
            userId = UserInfoContext.get().getUserId();
        }
        if (userId == null) {
            log.error("token error,userId is null");
            return fail(RestEnum.TOKEN_ERROR.getMsg());
        }
        UserDetailVO userDetailVO = baseService.getUserInfo(userId);
        if (userDetailVO == null) {
            log.error("user id = {}, not found", userId);
            return fail(RestEnum.USER_NOT_FOUND.getMsg());
        }
        userDetailVO.setUserId(userId);
        return okObj(userDetailVO);
    }

    /**
     * 更新用户微信信息
     */
    @AuthCheck(value = "更新微信授权信息")
    @PostMapping("/info")
    public Rest info(@RequestBody WXUserInfo wxUserInfo) {
        //游客身份校验
        if (TOURIST_UUID.equals(wxUserInfo.getUserId())) {
            return ok();
        }
        if (baseService.saveWxUserInfo(wxUserInfo)) {
            return ok();
        }
        return fail();
    }

    /**
     * 删除销售相关信息,临时调试使用
     */
    @GetMapping("/cleanSeller")
    @AuthCheck(skipAuth = 2)
    public Rest deleteUser(String sellerId) {
        baseService.removeSellerById(sellerId);
        return ok();
    }

    /**
     * 删除员工相关信息,临时调试使用
     */
    @GetMapping("/cleanVisitor")
    @AuthCheck(skipAuth = 2)
    public Rest deleteVisitor(String visitorId) {
        baseService.removeVisitorById(visitorId);
        return ok();
    }

    /**
     * 获取小程序授权
     */
    @GetMapping("/codeToToken")
    @AuthCheck(value = "用户通过授权码获取登录信息", skipAuth = 2)
    public Rest login(CodeToTokenDTO codeToTokenDTO, HttpServletRequest request) {
        WxMaJscode2SessionResult wxAuthResponse = weChatService.codeAndEncryptedData2Session(codeToTokenDTO);
        return Optional.ofNullable(wxAuthResponse)
                .map(s -> okObj(baseService.initLoginByCode(s, request, Integer.parseInt(codeToTokenDTO.getOrigin()))))
                .orElseThrow(() -> new BusinessException(ERROR_MESSAGE));
    }

    /**
     * 获取公众号授权
     */
    @AuthCheck(value = "微信公众号授权", skipAuth = 2)
    @GetMapping("/getAccessToken")
    public Rest accessToToken(CodeToTokenDTO codeToTokenDTO, HttpServletRequest request) {
        WxMpUser wxMpUser = weChatService.oauth2getUserInfo(codeToTokenDTO.getCode());
        return Optional.ofNullable(wxMpUser)
                .map(s -> okObj(baseService.initLoginByAccessToken(s, request, Integer.parseInt(codeToTokenDTO.getOrigin()))))
                .orElseThrow(() -> new BusinessException(ERROR_MESSAGE));
    }

    /**
     * 获取微信签名
     */
    @AuthCheck(value = "获取微信签名", skipAuth = 2)
    @GetMapping("/getSignature")
    public Rest getSignature(@RequestParam String url) {
        return okObj(weChatService.getWxJsApiSignature(url));
    }

    @GetMapping("/tmp/{userId}")
    @AuthCheck(value = "临时生成token", skipAuth = 2)
    public Rest tmpGetToken(@PathVariable @NonNull String userId, HttpServletRequest request) {
        UserSessionVO userSessionVO = new UserSessionVO();
        userSessionVO.setUserId(userId);
        Token token = baseService.createToken(userSessionVO, request);
        log.info("{} logged in", userSessionVO.getUserId());
        return okObj(token);
    }

    /**
     * 获取员工注册码
     */
    @AuthCheck(value = "获取员工注册码")
    @PostMapping("/invite/code")
    public Rest getUserProjectsInviteCode(@RequestBody InviteCodeGenDTO inviteCodeGenDTO) {
        log.info(inviteCodeGenDTO.getProjects().toString());
        InviteRegisterVO inviteRegisterVO = baseService.getInviteRegisterCode(inviteCodeGenDTO);
        if (inviteRegisterVO != null) {
            return okObj(inviteRegisterVO);
        }
        return fail();
    }

    /**
     * 用户上报手机号
     */
    @AuthCheck(value = "用户上报手机号")
    @PostMapping("/phone/submit")
    public Rest submitVisitorPhone(@Valid @RequestBody VisitorSubmitPhoneDTO visitorSubmitPhoneDTO) {
        log.debug("请求参数:" + visitorSubmitPhoneDTO);
        if (baseService.visitorSubmitPhone(visitorSubmitPhoneDTO)) {
            return ok();
        }
        return fail();
    }

    /**
     * 用户获取名片首页
     */
    @AuthCheck(value = "用户获取名片首页", skipAuth = 2)
    @GetMapping("/card/info")
    public Rest getBusinessCardHome(String sellerId) {
        BussinessCardHomeVO bch = baseService.getBusinessCardHome(sellerId);
        if (bch != null) {
            return okObj(bch);
        }
        log.error("user not exist");
        return fail();
    }

    /**
     * 修改获取名片首页
     */
    @AuthCheck(value = "修改获取名片首页")
    @PostMapping("/card/update")
    public Rest editBusinessCardHome(@Valid BussinessCardHomeEditDTO bc) {
        BussinessCardHomeVO bch = baseService.updateBusinessCardHome(bc);
        if (bch != null) {
            return okObj(bch);
        }
        return fail();
    }

    /**
     * 刷新用户token
     */
    @PostMapping("/auth/refreshToken")
    @AuthCheck(value = "根据refresh_token获取access_token")
    public Rest refreshToken(String refreshToken, HttpServletRequest request) {
        Token token = baseService.refreshToken(refreshToken, request);
        return okObj(token);
    }

    /**
     * 通过用户id和项目id获取小程序二维码
     */
    @AuthCheck(value = "通过用户id和项目id获取小程序二维码")
    @PostMapping("/projectQRcode")
    public Rest getProjectQrCode(@RequestBody @Valid ProjectQRCodeDTO qrCodeDTO) {
        return okObj(baseService.getProjectQrCode(qrCodeDTO));
    }

    /**
     * vr720海报生成
     *
     * @param qrCodeDTO dto对象
     * @return Rest
     */
    @PostMapping("/vrQrCode")
    public Rest getVrQrCode(@RequestBody @Valid ProjectQRCodeDTO qrCodeDTO) {
        return okObj(baseService.getVrQrCode(qrCodeDTO));
    }

    /**
     * 项目简介海报生成
     *
     * @param qrCodeDTO dto对象
     * @return Rest
     */
    @PostMapping("/profileQrCode")
    public Rest getFloorBookQrCode(@RequestBody @Valid ProjectQRCodeDTO qrCodeDTO) {
        return okObj(baseService.getProfileQrCode(qrCodeDTO));
    }

    /**
     * 访客扫描邀请码更换身份为员工
     */
    @AuthCheck(value = "访客扫描邀请码更换身份为员工")
    @PostMapping("scanInviteCode")
    public Rest scanInviteCode(@RequestBody ScanInviteCodeDTO scanInviteCodeDTO) {
        if (baseService.scanInviteRegisterCode(scanInviteCodeDTO)) {
            return ok();
        }
        return fail();
    }

    /**
     * 访客转销售后门
     */
    @AuthCheck(value = "访客转销售后门")
    @PostMapping("sellerBackDoor")
    public Rest scanInviteCodeBackDoor(Integer inviteId) {
        if (baseService.scanInviteRegisterCodeBackDoor(inviteId)) {
            return ok();
        }
        return fail();
    }

    /**
     * 将销售切换为访客
     */
    @AuthCheck(value = "将销售切换为访客")
    @PostMapping("changeSellerToVisitor")
    public Rest changeSellerToVisitor(String sellerId) {
        if (baseService.removeSellerById(sellerId)) {
            return ok();
        }
        return fail();
    }

    /**
     * 通过用户id获取用户名片码
     */
    @AuthCheck(value = "通过用户id获取用户名片码")
    @PostMapping("/businessQRcode")
    public Rest getBusinessQrCode(@RequestBody BusinessQRCodeQuery query) {
        return okObj(baseService.getBusinessQrCode(query));
    }

    /**
     * 员工修改信息
     */
    @AuthCheck(value = "员工修改信息")
    @PostMapping("/edit")
    public Rest editUserInfo(@RequestBody @Valid UserEditDTO userEditDTO) {
        if (baseService.updateUserInfo(userEditDTO)) {
            return ok();
        }
        return fail("修改失败");
    }

    /**
     * 员工获取项目
     */
    @AuthCheck(value = "员工获取项目")
    @GetMapping("/projects")
    public Rest getStaffProject(@Valid UserProjectQuery userProjectQuery) {
        return okObj(baseService.getStaffProject(userProjectQuery));
    }

    /**
     * 通过邀请id来获取项目名称列表
     */
    @AuthCheck(value = "通过邀请id来获取项目名称列表")
    @GetMapping("/project/getProjectNames")
    public Rest getStaffProjectDetail(Integer inviteId) {
        List<String> projects = baseService.getStaffProjectNameListByInviteId(inviteId);
        return okObj(projects);
    }

    /**
     * 销售过滤获取所管理的访客信息
     */
    @AuthCheck(value = "销售过滤获取所管理的访客信息")
    @PostMapping("/filterVisitors")
    public Rest getFilterVisitors(@RequestBody StaffVisitorQuery query) {
        return okObj(staffVisitorService.getStaffVisitorByFilter(query));
    }

    @AuthCheck(value = "高等级角色获取管理的组")
    @GetMapping("/getUserGroup")
    public Rest getUserGroup(@RequestParam String sellerId, @RequestParam List<Integer> projects) {
        if (StringUtils.isBlank(sellerId)) {
            sellerId = UserInfoContext.get().getUserId();
        }
        return okObj(staffVisitorService.getUserGroup(sellerId, projects));
    }

    @AuthCheck(value = "高等级角色获取管理的销售")
    @GetMapping("/getUserList")
    public Rest getUserList(@RequestParam List<Integer> groupIds, @RequestParam List<Integer> projects) {
        return okObj(staffVisitorService.getUserList(groupIds, projects));
    }

    @AuthCheck(value = "销售查询所管理的项目组以及项目数")
    @GetMapping("/projectGroupTree")
    public Rest getProjectGroupTree(@RequestParam String sellerId) {
        return okObj(projectService.selectProjectGroupTree(sellerId));
    }

    /**
     * 员工获取客户详情
     *
     * @param visitorInfoDetailQuery 查询条件
     * @return Rest
     */
    @AuthCheck(value = "员工获取客户详情")
    @GetMapping("/customer/detail")
    public Rest getVisitorInfoDetail(@Valid VisitorInfoDetailQuery visitorInfoDetailQuery) {
        return okObj(baseService.getVisitorInfoDetail(visitorInfoDetailQuery));
    }

    /**
     * 高级员工获取客户详情
     *
     * @param visitorInfoDetailQuery 查询条件
     * @return Rest
     */
    @AuthCheck(value = "高级员工获取客户详情")
    @GetMapping("/customer/detailForLeader")
    public Rest getVisitorInfoDetailForLeader(@Valid VisitorInfoDetailQuery visitorInfoDetailQuery) {
        return okObj(baseService.getVisitorInfoDetailForLeader(visitorInfoDetailQuery));
    }

    /**
     * 修改客户备注信息
     *
     * @author imyzt
     */
    @AuthCheck(value = "修改客户备注信息")
    @PostMapping("/customer/edit")
    public Rest editVisitorRemark(@Valid @RequestBody VisitorInfoDTO visitorInfoDTO) {

        // 员工鉴权
        String userUuid = visitorInfoDTO.getUserUuid();
        String visitorUuid = visitorInfoDTO.getVisitorUuid();
        String visitorPhone = visitorInfoDTO.getVisitorPhone();
        String remark = visitorInfoDTO.getRemark();

        StaffVisitor staffVisitor = new StaffVisitor();
        BeanUtils.copyProperties(visitorInfoDTO, staffVisitor);

        // 员工-客户记录是否存在
        StaffVisitor visitor = staffVisitorService.lambdaQuery()
                .eq(StaffVisitor::getVisitorUuid, visitorUuid)
                .eq(StaffVisitor::getUserUuid, userUuid)
                .one();
        if (visitor == null) {
            log.error("访客不存在记录中, uuid ={}, visitorUuid = {}", userUuid, visitorUuid);
            return fail("您无权限修改此客户信息");
        }

        // 更新操作
        boolean update = staffVisitorService.lambdaUpdate()
                .eq(StaffVisitor::getUserUuid, userUuid)
                .eq(StaffVisitor::getVisitorUuid, visitorUuid)
                .update(staffVisitor);

        //同步访客信息
        visitorService.lambdaUpdate()
                .set(visitorPhone != null,
                        Visitor::getCommentPhone, visitorPhone)
                .eq(Visitor::getUuid, visitorUuid)
                .update();
        if (update) {
            if (StringUtils.isNotBlank(visitorPhone)) {
                String str = "您把客户" + visitor.getVisitorName() + "的";
                str += "手机信息由" + visitor.getVisitorPhone()
                        + "更改为" + visitorPhone;
                logService.recordUserLog(userUuid, str, false);
            }
            if (StringUtils.isNotBlank(remark)) {
                String str = "您把客户" + visitor.getVisitorName() + "的";
                str += "备注信息由" + visitor.getRemark()
                        + "更改为" + remark;
                logService.recordUserLog(userUuid, str, false);
            }
            log.info("员工修改客户信息,userId = {}, visitorId = {}", userUuid, visitorUuid);
            return ok();
        }
        return fail("客户信息修改失败");
    }

    /**
     * 获取访客对应项目的销售信息
     */
    @AuthCheck(value = "获取访客对应项目的销售信息")
    @GetMapping("/getSeller")
    public Rest getVisitorProjectUserInfo(@Valid UserVisitorProjectQuery userVisitorProjectQuery) {
        return okObj(baseService.getVisitorProjectUserInfo(userVisitorProjectQuery));
    }


    /**
     * 客户动态查看
     */
    @AuthCheck(value = "客户动态查看")
    @GetMapping("/customer/trend")
    public Rest getVisitorTrend(@Valid VisitorTrendQuery visitorTrendQuery) {
        return okObj(baseService.getVisitorTrend(visitorTrendQuery));
    }

    /**
     * 员工查看访客访问数据分析
     * 此处查询包含项目列表，暂定用POST请求
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @return Rest
     */
    @AuthCheck(value = "员工查看访客访问数据分析")
    @PostMapping("/customer/analyse")
    public Rest getVisitorVisitAnalyse(@Valid @RequestBody VisitorVisitAnalysisQuery visitorVisitAnalysisQuery) {
        return okObj(baseService.getVisitorVisitAnalysis(visitorVisitAnalysisQuery));
    }

    /**
     * 员工查看访客访问数据分析（钻取组）
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @return Rest
     */
    @AuthCheck(value = "钻取访客访问数据员工分组")
    @PostMapping("/customer/analyse/group")
    public Rest drillingVisitorVisitAnalyse(@Valid @RequestBody VisitorVisitAnalysisQuery visitorVisitAnalysisQuery) {
        return okObj(baseService.getVisitorList(visitorVisitAnalysisQuery));
    }

    /**
     * 员工查看访客访问数据分析（钻取员工）
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @return Rest
     */
    @AuthCheck(value = "钻取访客访问数据员工列表")
    @PostMapping("/customer/analyse/seller")
    public Rest drillingSellerList(@Valid @RequestBody VisitorVisitAnalysisQuery visitorVisitAnalysisQuery) {
        return okObj(baseService.getSellerDetailList(visitorVisitAnalysisQuery));
    }

    /**
     * 员工查看访客访问数据分析（钻取客户）
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @return Rest
     */
    @AuthCheck(value = "钻取访客访问数据访客列表")
    @PostMapping("/customer/analyse/visitor")
    public Rest drillingVisitorList(@Valid @RequestBody VisitorVisitAnalysisQuery visitorVisitAnalysisQuery) {
        return okObj(baseService.getVisitorDetailList(visitorVisitAnalysisQuery));
    }

    /**
     * 员工查看数据排名接口
     * 此处查询包含项目列表，暂定用POST请求
     */
    @AuthCheck(value = "员工查看访客访问数据分析")
    @PostMapping("/customer/rank")
    public Rest getStaffVisitorRankData(@RequestBody StaffVisitorRankQuery visitorRankQuery) {
        return okObj(baseService.getStaffVisitorRank(visitorRankQuery));
    }

    /**
     * 变更测试接口
     */
    @AuthCheck(skipAuth = 2)
    @GetMapping("/tmptests3")
    public Rest tmp() {
        return okObj("hello,3333");
    }

    /**
     * 访客获取项目销售和项目详情
     */
    @AuthCheck(value = "访客获取项目销售详情")
    @GetMapping("/getProject")
    public Rest getProjectSellerForVisitor(@RequestParam String projectId) {
        Integer integer;
        try {
            integer = Integer.valueOf(projectId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return fail("参数：项目id错误");
        }
        return okObj(baseService.getProjectSellerInfo(integer));
    }

    /**
     * 员工查看他的二维码活动
     */
    @AuthCheck(value = "员工查看他的二维码活动")
    @GetMapping("/activitis")
    public Rest getSellerActivitiesList(@RequestParam String sellerId) {
        return okObj(baseService.getSellerActivitiesList(sellerId));
    }

    /**
     * 员工查看活动中客户信息
     */
    @AuthCheck(value = "员工查看活动中客户信息", skipAuth = 2)
    @PostMapping("/getActivitisDetail")
    public Rest getActivitiesDetail(@NotNull @RequestBody ActivityDetailQuery query) {
        return okObj(signInQrCodeService.getActivitiesDetail(query));
    }

    /**
     * 员工查看活动二维码
     */
    @AuthCheck(value = "员工查看活动二维码", skipAuth = 2)
    @GetMapping("/activitisShow")
    public Rest getSellerActivitiesShow(@RequestParam Integer id) {
        boolean state = signInQrCodeService.isExpire(id);
        if (state) {
            throw new BusinessException(RestEnum.ACTIVITY_END);
        } else {
            String rec = signInQrCodeService.updateQr(id);
            String s = "-1";
            if (s.equals(rec)) {
                return fail();
            } else {
                return okObj(rec);
            }
        }
    }

    /**
     * 获得推送员工
     */
    @AuthCheck(value = "获得推送员工", skipAuth = 2)
    @GetMapping("/getActivitisPushStaff")
    public Rest getSellerPushStaff(@RequestParam Integer projectId) {
        return okObj(baseService.getSellerPushStaff(projectId));
    }

    /**
     * 员工选择推送人推送
     */
    @AuthCheck(value = "员工选择推送人推送", skipAuth = 2)
    @PostMapping("/activitisPushStaff")
    public Rest activitisPushStaff(@NotNull @RequestBody PushActivitisVO pushActivitisVO) {
        if (userPushActivityService.isExist(pushActivitisVO)) {
            return okObj("已存在");
        }
        UserPushActivity userPushActivity = new UserPushActivity();
        BeanUtils.copyProperties(pushActivitisVO, userPushActivity);
        return okObj(userPushActivityService.save(userPushActivity));
    }

    /**
     * 员工获得推送活动
     */
    @AuthCheck(value = "员工获得被推送的活动", skipAuth = 2)
    @GetMapping("/getPushActivity")
    public Rest getPushActivity(@RequestParam String userUuid) {
        return okObj(baseService.getPushActivity(userUuid));
    }

    /**
     * 签到校验
     */
    @AuthCheck(value = "签到校验", skipAuth = 2)
    @PostMapping("/signInAuth")
    public Rest signInAuth(@NotNull @RequestBody SignInAuthQuery query) {
        ExpireVO expireVO = new ExpireVO();
        LocalDateTime now = LocalDateTime.now();
        expireVO.setNowTime(now);
        expireVO.setEndTime(query.getEndTime());
        expireVO.setId(query.getId());
        return okObj(signInQrCodeService.getQrExpire(expireVO));
    }

    /**
     * 签到预查询
     */
    @AuthCheck(value = "签到预查询", skipAuth = 2)
    @GetMapping("/preSignIn")
    public Rest preSignIn(Integer id) {
        return okObj(signInQrCodeService.getPreMessage(id));
    }

    /**
     * 客户签到
     */
    @AuthCheck(value = "客户签到")
    @PostMapping("/signIn")
    public Rest signIn(@RequestBody @Valid SignInQuery query) {
        return okObj(signInQrCodeService.signIn(query));
    }

    @AuthCheck(value = "修改访客类别")
    @GetMapping("/updateVisitorCategory")
    public Rest updateVisitorCategory(@RequestParam Integer id, @RequestParam Integer category) {
        return okObj(bindingManagerService.lambdaUpdate()
                .set(BindingManager::getSelfLabel, category)
                .eq(BindingManager::getId, id)
                .update());
    }

}