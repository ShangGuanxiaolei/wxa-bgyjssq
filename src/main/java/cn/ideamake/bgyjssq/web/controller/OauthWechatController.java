package cn.ideamake.bgyjssq.web.controller;


import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.pojo.dto.FormIdDTO;
import cn.ideamake.bgyjssq.pojo.entity.OauthWechat;
import cn.ideamake.bgyjssq.service.IOauthWechatService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户微信登录表 前端控制器
 * </p>
 *
 * @author ideamake
 * @since 2019-06-11
 */
@RestController
@RequestMapping("/oauth-wechat")
@Slf4j
public class OauthWechatController extends AbstractController<IOauthWechatService,OauthWechat,OauthWechat> {

    /**
     * 报错或更新用户form-id
     */
    @AuthCheck(value = "更新用户form_id")
    @PostMapping("/formId")
    public Rest saveOrUpdateFormId(@RequestBody FormIdDTO formIdDTO){
        log.info("formId:{}",formIdDTO.getFormId());
        baseService.saveOrUpdateFormId(formIdDTO.getUserId(),formIdDTO.getFormId());
        return okObj(baseService.getFormId(formIdDTO.getUserId()));
    }

}

