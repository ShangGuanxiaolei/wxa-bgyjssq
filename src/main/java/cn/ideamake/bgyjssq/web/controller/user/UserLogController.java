package cn.ideamake.bgyjssq.web.controller.user;


import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.pojo.entity.UserLog;
import cn.ideamake.bgyjssq.pojo.query.user.UserLogQuery;
import cn.ideamake.bgyjssq.service.IUserLogService;
import cn.ideamake.bgyjssq.web.controller.AbstractController;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户日志记录表 前端控制器
 * </p>
 *
 * @author ideamake
 * @since 2019-06-21
 */
@RestController
@RequestMapping("/user-log")
public class UserLogController extends AbstractController<IUserLogService, UserLog, UserLog> {

    /**
     * 员工获取我的日志
     */
    @AuthCheck(value = "员工获取我的日志")
    @GetMapping("/logs")
    public Rest getVisitorLogs(UserLogQuery query, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            FieldError fieldError = bindingResult.getFieldError();
            return fail(fieldError != null ? fieldError.getDefaultMessage() : null);
        }
        return okObj(baseService.getUserLog(query));
    }
}

