package cn.ideamake.bgyjssq.web.controller.admin.permission;


import cn.ideamake.bgyjssq.cache.DoGuavaCache;
import cn.ideamake.bgyjssq.cache.constant.AbstractCacheKeyConstant;
import cn.ideamake.bgyjssq.cache.constant.ExpireAt;
import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.handler.UserInfoContext;
import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.util.Md5Utils;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.common.validate.CheckPermission;
import cn.ideamake.bgyjssq.pojo.entity.SysAdmin;
import cn.ideamake.bgyjssq.pojo.entity.SysAdminProject;
import cn.ideamake.bgyjssq.pojo.entity.SysPermission;
import cn.ideamake.bgyjssq.pojo.query.AdminQuery;
import cn.ideamake.bgyjssq.pojo.vo.permission.AdminLoginVO;
import cn.ideamake.bgyjssq.pojo.vo.permission.NewMenuListVO;
import cn.ideamake.bgyjssq.service.common.SaveAdminLogService;
import cn.ideamake.bgyjssq.service.permission.ISysAdminProjectService;
import cn.ideamake.bgyjssq.service.permission.ISysAdminService;
import cn.ideamake.bgyjssq.service.permission.ISysPermissionService;
import cn.ideamake.bgyjssq.web.controller.AbstractController;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static cn.ideamake.bgyjssq.common.enums.PermissionTypeEnum.DATA;
import static cn.ideamake.bgyjssq.common.util.Constants.ROOT;
import static java.util.stream.Collectors.toList;

/**
 * <p>
 * 系统管理员列表 前端控制器
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@AllArgsConstructor
@RestController
@RequestMapping("/admin/sys-admin")
@Slf4j
public class SysAdminController extends AbstractController<ISysAdminService, SysAdmin, SysAdmin> {

    private final ISysPermissionService sysPermissionService;

    private final SaveAdminLogService saveAdminLogService;

    private final ISysAdminProjectService sysAdminProjectService;

    /**
     * 特殊处理接口
     */
    private static final List<String> URL_LIST = new ArrayList<>();

    static {
        URL_LIST.add("articleGroupAdd");
        URL_LIST.add("posterGroupAdd");
        URL_LIST.add("activityGroupAdd");
        URL_LIST.add("projectGroupAdd");
        URL_LIST.add("articleGroupUpdate");
        URL_LIST.add("posterGroupUpdate");
        URL_LIST.add("activityGroupUpdate");
        URL_LIST.add("projectGroupUpdate");
        URL_LIST.add("articleGroupDelete");
        URL_LIST.add("posterGroupDelete");
        URL_LIST.add("activityGroupDelete");
        URL_LIST.add("projectGroupDelete");
        URL_LIST.add("customerDetails");
        URL_LIST.add("projectAdd");
        URL_LIST.add("projectUpdate");
        URL_LIST.add("adAdd");
        URL_LIST.add("adUpdate");
        URL_LIST.add("dataExport");
        URL_LIST.add("customerExport");
        URL_LIST.add("staffListExport");
        URL_LIST.add("staffKpiExport");
        URL_LIST.add("logsExport");
        URL_LIST.add("activityAdd");
        URL_LIST.add("activityUpdate");
        URL_LIST.add("articleAdd");
        URL_LIST.add("articleUpdate");
        URL_LIST.add("posterAdd");
        URL_LIST.add("posterUpdate");
        URL_LIST.add("roleAdd");
        URL_LIST.add("roleUpdate");
        URL_LIST.add("articleDetails");
        URL_LIST.add("posterDetails");
        URL_LIST.add("vrAdd");
        URL_LIST.add("vrUpdate");
        URL_LIST.add("staffGroupDelete");
    }

    /**
     * 保存管理员信息
     *
     * @param admin 管理员对象实体
     * @return 操作结果
     */
    @CheckPermission
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public Rest saveAndUpdate(@RequestBody @Valid SysAdmin admin) {
        if (StringUtils.isBlank(admin.getPassword())) {
            admin.setPassword(null);
        }
        Optional.ofNullable(admin.getPassword()).ifPresent(s -> {
            try {
                admin.setPassword(Md5Utils.getEncryptedPwd(admin.getPassword()));
            } catch (NoSuchAlgorithmException e) {
                log.error(e.getMessage(), e);
                throw new BusinessException(e.getMessage());
            }
        });
        return Optional.ofNullable(admin.getId())
                .map(s -> this.updateById(admin))
                .orElseGet(() -> this.save(admin));
    }

    /**
     * 查询管理员列表
     *
     * @param adminQuery 查询条件
     * @return 查询结果
     */
    @CheckPermission
    @GetMapping("/list")
    public Rest findPage(AdminQuery adminQuery) {
        return okObj(baseService.findPage(adminQuery));
    }

    /**
     * 批量删除管理员信息接口
     *
     * @param ids customerIds
     * @return 删除结果
     */
    @Transactional(rollbackFor = Exception.class)
    @DeleteMapping("/delete")
    public Rest delete(@RequestParam Integer[] ids) {
        try {
            for (Integer id : ids) {
                baseService.removeById(id);
            }
            return ok();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new BusinessException(e.getMessage());
        }
    }

    @CheckPermission
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/updateStatus/{id}/{status}")
    public Rest updateStatus(@PathVariable @NotNull Integer id, @PathVariable @NotNull Integer status) {
        SysAdmin admin = baseService.lambdaQuery().eq(SysAdmin::getId, id).one();
        if (ROOT == admin.getRoleId()) {
            return fail("禁止禁用超级管理员角色");
        }
        return okObj(baseService.lambdaUpdate()
                .set(SysAdmin::getStatus, status)
                .eq(SysAdmin::getId, id)
                .update());
    }

    /**
     * 获取当前登录用户拥有的权限
     *
     * @return 系统权限列表
     */
    @DoGuavaCache(key = AbstractCacheKeyConstant.PERMISSION_LIST, expireAt = ExpireAt.FIVE)
    @GetMapping("/findPermission")
    public Rest findPermission() {
        Integer roleId = Optional.ofNullable(UserInfoContext.getRoleId()).orElse(0);

        List<NewMenuListVO> menuList = sysPermissionService.getMenuListByCache(roleId);

        //获取特殊权限接口
        List<String> interfaceList = Optional.ofNullable(sysPermissionService
                .selectAllByRoleIdAndType(roleId, DATA))
                .map(s -> s.parallelStream()
                        .map(SysPermission::getInterfaceUrl)
                        .filter(URL_LIST::contains)
                        .collect(toList()))
                .orElse(Collections.emptyList());

        Map<String, Object> map = new HashMap<>(2);
        map.put("menuList", menuList);
        map.put("interfaceList", interfaceList);
        return okObj(map);
    }

    /**
     * 查询单个管理员信息
     *
     * @param adminId 管理员id
     * @return 管理员实体
     */
    @GetMapping("/findOne/{adminId}")
    public Rest<SysAdmin> findOne(@PathVariable Integer adminId) {
        return ok(baseService.getById(adminId));
    }

    @AuthCheck(skipAuth = 2)
    @PostMapping("/login")
    public Rest login(@Valid AdminLoginVO adminLoginVO,
                      HttpServletRequest request,
                      HttpServletResponse response,
                      HttpSession session) {
        saveAdminLogService.saveSystemLog("管理员登录后台");
        return okObj(baseService.login(adminLoginVO, request, response, session));
    }

    @GetMapping("/logout")
    public Rest logout(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        return okObj(baseService.logout(request, response, session));
    }

    /**
     * 给账户授权项目权限
     *
     * @param adminProject 管理员-项目
     * @return Rest
     */
    @CheckPermission
    @PostMapping("/empowerProject")
    public Rest empowerProject(@Valid @RequestBody AdminProject adminProject) {
        List<Integer> list = adminProject.getIds();
        QueryWrapper<SysAdminProject> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().in(SysAdminProject::getSysAdminId, list);
        sysAdminProjectService.remove(queryWrapper);

        List<SysAdminProject> sysadminProjectList = new ArrayList<>();
        list.forEach(id -> adminProject.getProjectIds()
                .forEach(p -> sysadminProjectList.add(new SysAdminProject(id, p))));
        if (sysAdminProjectService.saveBatch(sysadminProjectList)) {
            return ok();
        }
        return fail();
    }

    /**
     * 添加
     *
     * @param entity 对象实体
     * @return 添加结果
     */
    private Rest<SysAdmin> save(@RequestBody SysAdmin entity) {
        if (baseService.save(entity)) {
            return ok(entity);
        }
        throw new BusinessException("添加失败");
    }

    /**
     * 修改
     *
     * @param entity 对象实体
     * @return 更新结果
     */
    private Rest<SysAdmin> updateById(SysAdmin entity) {
        if (baseService.updateById(entity)) {
            return ok(entity);
        }
        throw new BusinessException("修改失败");
    }

    /**
     * 管理员-项目对象
     *
     * @author gxl
     */
    @Data
    static class AdminProject {

        /**
         * 管理员id列表
         */
        @NotNull(message = "管理员id不能为空")
        List<Integer> ids;

        /**
         * 项目id列表
         */
        @NotNull(message = "项目id不能为空")
        List<Integer> projectIds;
    }

}