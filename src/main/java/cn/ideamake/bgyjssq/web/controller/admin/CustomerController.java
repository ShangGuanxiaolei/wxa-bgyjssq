package cn.ideamake.bgyjssq.web.controller.admin;

import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.common.validate.CheckPermission;
import cn.ideamake.bgyjssq.pojo.bo.BindRecordBO;
import cn.ideamake.bgyjssq.pojo.dto.CustomerBulkOperation;
import cn.ideamake.bgyjssq.pojo.dto.CustomerDTO;
import cn.ideamake.bgyjssq.pojo.dto.SaveOrChangeBindDTO;
import cn.ideamake.bgyjssq.pojo.entity.BindingManager;
import cn.ideamake.bgyjssq.pojo.entity.CollectorAllRecord;
import cn.ideamake.bgyjssq.pojo.entity.CollectorVisitor;
import cn.ideamake.bgyjssq.pojo.entity.StaffVisitor;
import cn.ideamake.bgyjssq.pojo.entity.Visitor;
import cn.ideamake.bgyjssq.pojo.query.CustomerListQuery;
import cn.ideamake.bgyjssq.pojo.query.CustomerLocusQuery;
import cn.ideamake.bgyjssq.pojo.query.DefaultQueryPage;
import cn.ideamake.bgyjssq.pojo.query.DistributionMapQuery;
import cn.ideamake.bgyjssq.pojo.query.QueryPage;
import cn.ideamake.bgyjssq.pojo.query.TopTenQuery;
import cn.ideamake.bgyjssq.pojo.vo.CustomerInfoDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.CustomerListVO;
import cn.ideamake.bgyjssq.pojo.vo.RecommendCustomerVO;
import cn.ideamake.bgyjssq.service.IBindingManagerService;
import cn.ideamake.bgyjssq.service.ICollectorAllRecordService;
import cn.ideamake.bgyjssq.service.ICollectorVisitorService;
import cn.ideamake.bgyjssq.service.IRecommendService;
import cn.ideamake.bgyjssq.service.IStaffVisitorService;
import cn.ideamake.bgyjssq.service.IVisitorService;
import cn.ideamake.bgyjssq.service.common.SaveAdminLogService;
import cn.ideamake.bgyjssq.service.permission.IRolePermissionService;
import cn.ideamake.bgyjssq.service.permission.ISysAdminProjectService;
import cn.ideamake.bgyjssq.web.controller.IController;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Optional;

import static cn.ideamake.bgyjssq.common.util.Constants.PROTECT_CUSTOMER_PHONE;

/**
 * @author imyzt
 * @date 2019/07/08
 * @description 客户管理
 */
@AllArgsConstructor
@RequestMapping("admin/customer")
@RestController
@Slf4j
public class CustomerController implements IController {

    private final IBindingManagerService bindingManagerService;

    private final ICollectorVisitorService collectorVisitorService;

    private final ICollectorAllRecordService collectorAllRecordService;

    private final IVisitorService visitorService;

    private final IStaffVisitorService staffVisitorService;

    private final IRecommendService recommendService;

    private final SaveAdminLogService saveAdminLogService;

    private final ISysAdminProjectService sysAdminProjectService;

    private final IRolePermissionService rolePermissionService;

    @CheckPermission
    @GetMapping("list")
    public Rest list(CustomerListQuery query) {
        if (CollectionUtils.isEmpty(query.getProjectIds())) {
            query.setProjectIds(sysAdminProjectService.getProjectList());
        }
        IPage<CustomerListVO> page = bindingManagerService.customerListQuery(query);
        if (!rolePermissionService.hasProperties(PROTECT_CUSTOMER_PHONE)) {
            page.getRecords().parallelStream().forEach(c -> c.setVisitorPhone("***"));
        }
        saveAdminLogService.saveSystemLog("查看客户列表");
        return okObj(page);
    }

    @GetMapping("detail/{id}/{projectId}/{sellerId}")
    public Rest detail(@PathVariable String id, @PathVariable String projectId, @PathVariable String sellerId) {
        saveAdminLogService.saveSystemLog("查看客户详情");
        CustomerInfoDetailVO detail = bindingManagerService.getCustomerInfoDetailById(id, projectId, sellerId);
        if (!rolePermissionService.hasProperties(PROTECT_CUSTOMER_PHONE)) {
            detail.setPhone("***");
        }
        return okObj(detail);
    }

    /**
     * 获取客户的运动轨迹
     */
    @GetMapping("locus")
    public Rest locus(@Valid CustomerLocusQuery query) {
        IPage<CollectorVisitor> page = collectorVisitorService.lambdaQuery()
                .select(CollectorVisitor::getId,
                        CollectorVisitor::getLatitude,
                        CollectorVisitor::getLongitude,
                        CollectorVisitor::getAddress,
                        CollectorVisitor::getCreateAt,
                        CollectorVisitor::getStayTime)
                .eq(CollectorVisitor::getVisitorId, query.getVisitorId())
                .eq(CollectorVisitor::getProjectId, query.getProjectId())
                .eq(CollectorVisitor::getSellerId, query.getSellerId())
                .between(StringUtils.isNotBlank(query.getStartTime())
                                && StringUtils.isNotBlank(query.getEndTime()),
                        CollectorVisitor::getCreateAt,
                        query.getStartTime(),
                        query.getEndTime())
                .orderByDesc(CollectorVisitor::getCreateAt)
                .page(new Page<>(query.getPage(), query.getLimit()));
        saveAdminLogService.saveSystemLog("查看客户运动轨迹");
        return okObj(page);
    }

    /**
     * 日志记录
     */
    @GetMapping("logs")
    public Rest logs(@Valid BindRecordBO bind) {
        IPage<CollectorAllRecord> page = collectorAllRecordService.lambdaQuery()
                .select(CollectorAllRecord::getCreateAt, CollectorAllRecord::getVisitorContent, CollectorAllRecord::getStayTime)
                .eq(CollectorAllRecord::getSellerId, bind.getSellerId())
                .eq(CollectorAllRecord::getProjectId, bind.getProjectId())
                .eq(CollectorAllRecord::getVisitorId, bind.getVisitorId())
                .orderByDesc(CollectorAllRecord::getCreateAt)
                .page(new Page<>(bind.getPage(), bind.getLimit()));
        return okObj(page);
    }

    @GetMapping("/brainMapping/{sourceId}/{projectId}")
    public Rest brainMapping(@PathVariable @NotNull String sourceId, @PathVariable @NotNull Integer projectId) {
        return okObj(recommendService.getBrainMapping(sourceId, projectId));
    }

    /**
     * 推介分析-访问统计 & 推介分析-发散图
     *
     * @param sourceId 推荐人id
     * @return Rest<RecommendAnalysisVO>
     */
    @GetMapping("/recommendAnalysis1/{sourceId}")
    public Rest recommendedAnalysis1(@PathVariable @NotNull String sourceId) {
        return okObj(collectorVisitorService.selectRecommendAllTotal(sourceId));
    }

    /**
     * 推介分析-推介名单
     *
     * @param sourceId 推荐人id
     * @return Rest<RecommendCustomerVO>
     */
    @GetMapping("/recommendAnalysis2/{sourceId}")
    public Rest recommendedAnalysis2(QueryPage page, @PathVariable @NotNull String sourceId) {
        IPage<RecommendCustomerVO> iPage = recommendService.selectRecommendCustomer(page, sourceId);
        if (!rolePermissionService.hasProperties(PROTECT_CUSTOMER_PHONE)) {
            iPage.getRecords().parallelStream().forEach(r -> r.setPhone("***"));
        }
        return okObj(iPage);
    }

    /**
     * 推介分析-分享数据
     *
     * @param sourceId 推荐人id
     * @return Rest<CustomerShareVO>
     */
    @GetMapping("/recommendAnalysis3/{sourceId}")
    public Rest recommendedAnalysis3(QueryPage page, @PathVariable @NotNull String sourceId) {
        return okObj(recommendService.selectCustomerShare(page, sourceId));
    }

    /**
     * 设置跟办员工
     *
     * @param operation 参数
     * @return Rest
     */
    @CheckPermission
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/bulk/STAFF")
    public Rest bulk(@Valid @RequestBody CustomerBulkOperation operation) {
        Optional.ofNullable(operation.getProjectId())
                .orElseThrow(() -> new BusinessException("项目id不能为空"));
        SaveOrChangeBindDTO bind = new SaveOrChangeBindDTO();
        bind.setProjectId(operation.getProjectId());
        bind.setVisitorIds(operation.getCustomerIds());
        bind.setSellerIds(operation.getSellerIds());
        if (bindingManagerService.saveOrChangeBinding(bind)) {
            return ok();
        }
        return fail();
    }

    /**
     * 设置客户类别
     *
     * @param operation 参数
     * @return Rest
     */
    @CheckPermission
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/bulk/LABEL")
    public Rest bulkLabel(@Valid @RequestBody CustomerBulkOperation operation) {
        Optional.ofNullable(operation.getSelfLabel())
                .orElseThrow(() -> new BusinessException("客户类别不能为空"));
        if (bindingManagerService.lambdaUpdate()
                .set(BindingManager::getSelfLabel, operation.getSelfLabel())
                .eq(BindingManager::getHistoryLabel, 0)
                .in(BindingManager::getVisitorUuid, operation.getCustomerIds())
                .update()) {
            saveAdminLogService.saveSystemLog("修改客户类别");
            return ok();
        }
        return fail();
    }

    /**
     * 客户分布地图
     */
    @CheckPermission
    @GetMapping("map")
    public Rest customerMapList(DistributionMapQuery query) {
        if (CollectionUtils.isEmpty(query.getProjectIds())) {
            query.setProjectIds(sysAdminProjectService.getProjectList());
        }
        saveAdminLogService.saveSystemLog("查看客户分布地图");
        return okObj(collectorVisitorService.getCustomerDistributionMap(query));
    }

    @CheckPermission
    @GetMapping("top10")
    public Rest top10(TopTenQuery query) {
        if (CollectionUtils.isEmpty(query.getProjectIds())) {
            query.setProjectIds(sysAdminProjectService.getProjectList());
        }
        saveAdminLogService.saveSystemLog("查看客户业绩top10");
        return okObj(visitorService.selectCustomerTopTenList(query));
    }

    /**
     * 编辑客户信息
     *
     * @param customerDTO 客户信息VO
     * @return Rest
     */
    @CheckPermission
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("edit")
    public Rest editCustomer(@Valid @RequestBody CustomerDTO customerDTO) {

        String customerId = customerDTO.getCustomerId();

        if (StringUtils.isNotBlank(customerDTO.getName())) {
            staffVisitorService.lambdaUpdate()
                    .set(StaffVisitor::getVisitorName, customerDTO.getName())
                    .eq(StaffVisitor::getVisitorUuid, customerId)
                    .update();
        }

        String phone = customerDTO.getPhone();
        if (StringUtils.isNotBlank(phone)) {
            bindingManagerService.lambdaUpdate()
                    .set(BindingManager::getPhone, phone)
                    .eq(BindingManager::getVisitorUuid, customerId)
                    .update();

            //同步访客信息
            visitorService.lambdaUpdate()
                    .set(Visitor::getCommentPhone, phone)
                    .eq(Visitor::getUuid, customerId)
                    .update();
        }

        Optional.ofNullable(customerDTO.getSourceType()).ifPresent(t ->
                bindingManagerService.lambdaUpdate()
                        .set(BindingManager::getPromotionLabel, t)
                        .eq(BindingManager::getProjectId, customerDTO.getProjectId())
                        .eq(BindingManager::getVisitorUuid, customerId)
                        .update());

        String remark = customerDTO.getRemark();
        if (StringUtils.isNotBlank(remark)) {
            staffVisitorService.lambdaUpdate()
                    .set(StaffVisitor::getRemark, remark)
                    .eq(StaffVisitor::getVisitorUuid, customerId)
                    .update();
        }

        saveAdminLogService.saveSystemLog("批量更新客户信息");

        return ok();
    }

    @AuthCheck(value = "查询访客关注项目")
    @GetMapping("/focusProject")
    public Rest focusProject(DefaultQueryPage page, @RequestParam @NotNull String userId) {
        return okObj(visitorService.selectFocusProject(page, userId));
    }

}
