package cn.ideamake.bgyjssq.web.controller;


import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.pojo.dto.SaveOrChangeBindDTO;
import cn.ideamake.bgyjssq.pojo.entity.BindingManager;
import cn.ideamake.bgyjssq.pojo.query.VisitorAttentionQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorHistoryBindQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorProjectQuery;
import cn.ideamake.bgyjssq.pojo.vo.VisitorAttentionVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorHistoryBindVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorProjectVO;
import cn.ideamake.bgyjssq.service.IBindingManagerService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static cn.ideamake.bgyjssq.common.util.Constants.TOURIST_UUID;

/**
 * <p>
 * 员工的访客管理 前端控制器
 * </p>
 *
 * @author ideamake
 * @since 2019-06-12
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/visitor-manager")
public class BindingManagerController extends AbstractController<IBindingManagerService, BindingManager, BindingManager> {

    /**
     * 获取历史绑定
     */
    @AuthCheck(value = "访客获取历史绑定")
    @GetMapping("historyBind")
    public Rest getVisitorHistoryBind(VisitorHistoryBindQuery visitorHistoryBindQuery) {
        IPage<VisitorHistoryBindVO> page = baseService.selectVisitorHistoryBind(
                visitorHistoryBindQuery.getPageInfo(),
                visitorHistoryBindQuery
        );
        if (page != null) {
            return okObj(page);
        }
        return fail();

    }

    /**
     * 获取关注的绑定
     */
    @AuthCheck(value = "获取关注的绑定")
    @GetMapping("attention")
    public Rest getVisitorAttention(VisitorAttentionQuery visitorAttentionQuery) {
        IPage<VisitorAttentionVO> page = baseService.selectVisitorAttention(
                visitorAttentionQuery.getPageInfo(),
                visitorAttentionQuery
        );
        if (page != null) {
            return okObj(page);
        }
        return fail();
    }

    /**
     * 按条件获取项目内容
     */
    @AuthCheck(value = "访客按条件获取项目内容")
    @GetMapping("projectsByScope")
    public Rest getConditionAttention(VisitorProjectQuery visitorProjectQuery) {
        IPage<VisitorProjectVO> page = baseService.selectConditionAttention(visitorProjectQuery);
        if (page != null) {
            return okObj(page);
        }
        return fail();
    }

    /**
     * 添加或者更改绑定
     */
    @AuthCheck(value = "添加或者更改绑定")
    @PostMapping("saveOrChangeBind")
    public Rest saveOrChangeBind(@RequestBody SaveOrChangeBindDTO saveOrChangeBindDTO) {
        if (TOURIST_UUID.equals(saveOrChangeBindDTO.getVisitorId())) {
            return ok();
        }
        if (baseService.saveOrChangeBinding(saveOrChangeBindDTO)) {
            return ok();
        } else {
            return fail();
        }
    }

}

