package cn.ideamake.bgyjssq.web.controller;

import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * @Author: gxl
 * @Data: 2019-09-25 10:24
 * @description 临时controller，用于控制小程序游客登录入口
 * @Version: 1.0
 */
@AllArgsConstructor
@RestController
@RequestMapping("/temporary")
@AuthCheck(skipAuth = 2)
public class TemporaryController implements IController {

    private final RedisTemplate<String, String> redisTemplate;

    private final static String KEY = "temporary::visitor::entrance::status";

    private final static String VALUE = "1";

    @GetMapping
    public Rest getVisitorEntrance() {
        String str = redisTemplate.opsForValue().get(KEY);
        if (StringUtils.isBlank(str)) {
            return okObj(false);
        }
        return okObj(Objects.equals(str, VALUE));
    }

    @PostMapping
    public Rest updateVisitorEntrance(String value) {
        redisTemplate.opsForValue().set(KEY, value);
        return ok();
    }

}
