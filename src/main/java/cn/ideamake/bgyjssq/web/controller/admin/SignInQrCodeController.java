package cn.ideamake.bgyjssq.web.controller.admin;

import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.common.validate.CheckPermission;
import cn.ideamake.bgyjssq.pojo.dto.qrcode.BatchBindUserDTO;
import cn.ideamake.bgyjssq.pojo.dto.qrcode.BatchUpdateStatusDTO;
import cn.ideamake.bgyjssq.pojo.dto.qrcode.SignInQrCodeDTO;
import cn.ideamake.bgyjssq.pojo.entity.SignInQrCode;
import cn.ideamake.bgyjssq.pojo.entity.SignInQrCodePushUser;
import cn.ideamake.bgyjssq.pojo.query.ActivityDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.BatchActivityQuery;
import cn.ideamake.bgyjssq.pojo.query.QrCodeQuery;
import cn.ideamake.bgyjssq.pojo.vo.StaffVisitorInProjectVO;
import cn.ideamake.bgyjssq.service.ISignInQrCodePushUserService;
import cn.ideamake.bgyjssq.service.ISignInQrCodeService;
import cn.ideamake.bgyjssq.service.common.SaveAdminLogService;
import cn.ideamake.bgyjssq.service.permission.IRolePermissionService;
import cn.ideamake.bgyjssq.service.permission.ISysAdminProjectService;
import cn.ideamake.bgyjssq.web.controller.AbstractController;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Set;

import static cn.ideamake.bgyjssq.common.util.Constants.PROTECT_CUSTOMER_PHONE;

/**
 * <p>
 * 签到二维码 前端控制器
 * </p>
 *
 * @author ideamake
 * @since 2019-07-10
 */
@AllArgsConstructor
@RestController
@RequestMapping("/admin/sign-in-qr-code")
public class SignInQrCodeController extends AbstractController<ISignInQrCodeService, SignInQrCode, SignInQrCode> {

    private final ISignInQrCodePushUserService signInQrCodePushUserService;

    private final SaveAdminLogService saveAdminLogService;

    private final ISysAdminProjectService sysAdminProjectService;

    private final IRolePermissionService rolePermissionService;

    /**
     * 生成或者修改签到码信息
     *
     * @param dto 签到码参数
     * @return 生成结果
     */
    @CheckPermission
    @AuthCheck(value = "生成或者修改签到码信息")
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/saveAndUpdate")
    public Rest saveAndUpdate(@RequestBody @Valid SignInQrCodeDTO dto) {
        if (baseService.saveAndUpdate(dto)) {
            saveAdminLogService.saveSystemLog("保存签到活动信息");
            return ok();
        } else {
            return fail();
        }
    }

    /**
     * 得到某个活动的信息（方便修改）
     */
    @AuthCheck(value = "得到某个活动的信息（方便修改）")
    @GetMapping("/getActivity")
    public Rest getActivity(@RequestParam Integer activityId) {
        return okObj(baseService.getActivity(activityId));
    }

    /**
     * 查询签到码列表
     *
     * @param query 查询条件
     * @return 查询结果
     */
    @CheckPermission
    @AuthCheck(value = "查询签到码列表")
    @GetMapping("/list")
    public Rest findPage(QrCodeQuery query) {
        if (CollectionUtils.isEmpty(query.getProjectIds())) {
            query.setProjectIds(sysAdminProjectService.getProjectList());
        }
        return okObj(baseService.findPage(query));
    }

    /**
     * 手动更新qrCode
     *
     * @return 查询结果
     */
    @AuthCheck(value = "手动更新qrCode")
    @GetMapping("/updateQrCode")
    public Rest updateQrCode(Integer id) {
        String rec = baseService.updateQr(id);
        String s = "-1";
        if (s.equals(rec)) {
            return fail();
        } else {
            return ok();
        }
    }

    /**
     * 查看特定二维码中客户信息
     */
    @AuthCheck(value = "查看特定二维码中客户信息")
    @PostMapping("/activityDetail")
    public Rest getActivityDetail(@RequestBody @Valid ActivityDetailQuery query) {
        IPage<StaffVisitorInProjectVO> page = baseService.getActivitiesDetail(query);
        if (!rolePermissionService.hasProperties(PROTECT_CUSTOMER_PHONE)) {
            page.getRecords().parallelStream().forEach(s -> s.setPhoneNum("***"));
        }
        return okObj(page);
    }

    /**
     * 批量刷新二维码
     */
    @CheckPermission
    @AuthCheck(value = "批量刷新二维码")
    @PostMapping("/batchActivitiesUpdate")
    public Rest batchActivitiesUpdate(@RequestBody @Valid BatchActivityQuery query) {
        try {
            query.getIds().parallelStream().forEach(s -> baseService.updateQr(s));
            saveAdminLogService.saveSystemLog("批量刷新签到活动二维码");
            return ok();
        } catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
    }

    @CheckPermission
    @AuthCheck(value = "批量更新活动状态")
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/batchUpdateStatus")
    public Rest batchUpdateStatus(@RequestBody @Valid BatchUpdateStatusDTO dto) {
        if (baseService.lambdaUpdate()
                .set(SignInQrCode::getIsActive, dto.getStatus())
                .in(SignInQrCode::getId, dto.getIds())
                .update()) {
            saveAdminLogService.saveSystemLog("批量更新签到活动状态");
            return ok();
        } else {
            return fail();
        }
    }

    @CheckPermission
    @AuthCheck(value = "批量绑定员工项目空间")
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/batchBindUser")
    public Rest batchBindUser(@RequestBody @Valid BatchBindUserDTO dto) {
        if (baseService.lambdaUpdate()
                .set(SignInQrCode::getUserUuid, dto.getSellerId())
                .set(SignInQrCode::getProjectId, dto.getProjectId())
                .in(SignInQrCode::getId, dto.getIds())
                .update()) {
            saveAdminLogService.saveSystemLog("批量绑定员工项目空间");
            return ok();
        } else {
            return fail();
        }
    }

    @CheckPermission
    @AuthCheck(value = "批量删除签到二维码")
    @DeleteMapping("/delete")
    public Rest delete(@RequestParam Set<Integer> ids) {
        QueryWrapper<SignInQrCodePushUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().in(SignInQrCodePushUser::getSignQrCodeId, ids);
        if (signInQrCodePushUserService.remove(queryWrapper)) {
            if (baseService.removeByIds(ids)) {
                saveAdminLogService.saveSystemLog("批量删除签到活动信息");
                return ok();
            }
        }
        return fail();
    }

}

