package cn.ideamake.bgyjssq.web.controller;

import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.pojo.dto.RecommendCustomerDTO;
import cn.ideamake.bgyjssq.pojo.dto.RecordByClickLinkDTO;
import cn.ideamake.bgyjssq.pojo.dto.ShareSubmitDTO;
import cn.ideamake.bgyjssq.pojo.entity.Recommend;
import cn.ideamake.bgyjssq.pojo.query.RecommendDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.RecommendQuery;
import cn.ideamake.bgyjssq.pojo.vo.RecommendDetailPageVO;
import cn.ideamake.bgyjssq.pojo.vo.RecommendPageVO;
import cn.ideamake.bgyjssq.service.IRecommendService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * <p>
 * 推荐记录管理 前端控制器
 * </p>
 *
 * @author ideamake
 * @since 2019-06-06
 */
@RestController
@RequestMapping("/recommend")
public class RecommendController extends AbstractController<IRecommendService,Recommend,Recommend> {

    @GetMapping("list")
    public Rest getRecommend(){
        return fail();
    }

    /**
     * 访客获取推荐列表
     */
    @AuthCheck(value = "访客获取推荐列表")
    @GetMapping("introlist")
    public Rest getRecommend(RecommendQuery recommendQuery) {
        IPage<RecommendPageVO> page = baseService.selectRecommendVO(recommendQuery.getPageInfo(), recommendQuery);
        if (page != null) {
            return okObj(page);
        }
        return fail();
    }

    /**
     * 访客获取推荐详情
     */
    @AuthCheck(value = "访客获取推荐详情")
    @GetMapping("detail")
    public Rest getRecommendDetail(RecommendDetailQuery recommendDetailQuery) {
        IPage<RecommendDetailPageVO> recommendDetailVO =
                baseService.selectRecommendDetailVO(recommendDetailQuery.getPageInfo(), recommendDetailQuery);
        if (recommendDetailVO != null) {
            return okObj(recommendDetailVO);
        }
        return fail();
    }

    /**
     * 用户分享行为
     * @return 分享id
     */
    @AuthCheck(value = "用户分享行为",skipAuth = 2)
    @PostMapping("share")
    public Rest submitShare(@RequestBody @Valid ShareSubmitDTO shareSubmitDTO){
        return okObj(baseService.shareResourceByUser(shareSubmitDTO));
    }

    /**
     * 用户拓客新增到推荐表
     */
    @AuthCheck(value = "用户拓客新增到推荐表",skipAuth = 2)
    @PostMapping("join")
    public Rest addToRecommend(@RequestBody @Valid RecommendCustomerDTO joinByShareDTO){
        if(baseService.recommendRecord(joinByShareDTO)){
            return ok();
        }
        return fail();
    }

    /**
     * 用户通过分享链接点击进入系统
     */
    @AuthCheck(value = "用户通过分享链接接入到小程序",skipAuth = 2)
    @PostMapping("click")
    public Rest clickByShareLink(@RequestBody @Valid RecordByClickLinkDTO recordByClickLinkDTO){
        if(baseService.clickInByShareLink(recordByClickLinkDTO)){
            return ok();
        }
        return fail();
    }

}

