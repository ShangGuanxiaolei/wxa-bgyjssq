package cn.ideamake.bgyjssq.web.controller.admin;


import cn.ideamake.bgyjssq.common.enums.DictType;
import cn.ideamake.bgyjssq.common.enums.ResourceType;
import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.handler.UserInfoContext;
import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.CheckPermission;
import cn.ideamake.bgyjssq.pojo.dto.DictDTO;
import cn.ideamake.bgyjssq.pojo.entity.Dict;
import cn.ideamake.bgyjssq.pojo.entity.Project;
import cn.ideamake.bgyjssq.pojo.entity.Resource;
import cn.ideamake.bgyjssq.pojo.entity.SignInQrCode;
import cn.ideamake.bgyjssq.pojo.entity.SysAdminProject;
import cn.ideamake.bgyjssq.pojo.query.StaffGroupQuery;
import cn.ideamake.bgyjssq.service.IDictService;
import cn.ideamake.bgyjssq.service.IProjectService;
import cn.ideamake.bgyjssq.service.IResourceService;
import cn.ideamake.bgyjssq.service.ISignInQrCodeService;
import cn.ideamake.bgyjssq.service.IUserService;
import cn.ideamake.bgyjssq.service.common.SaveAdminLogService;
import cn.ideamake.bgyjssq.service.permission.ISysAdminProjectService;
import cn.ideamake.bgyjssq.web.controller.AbstractController;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * <p>
 * 字典表，用于记录相同字段结构 前端控制器
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@AllArgsConstructor
@RestController
@RequestMapping("/admin/dict")
@Slf4j
public class DictController extends AbstractController<IDictService, Dict, Dict> {

    private final IResourceService resourceService;

    private final ISignInQrCodeService signInQrCodeService;

    private final IUserService userService;

    private final SaveAdminLogService saveAdminLogService;

    private final IProjectService projectService;

    private final ISysAdminProjectService sysAdminProjectService;

    /**
     * 保存分组
     *
     * @param dict 分组对象
     * @return 保存结果
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/save")
    public Rest save(@RequestBody @Valid DictDTO dict) {
        Dict dictEntity = new Dict();
        BeanUtils.copyProperties(dict, dictEntity);

        List<Integer> resourceIds = dict.getResourceIds();
        dictEntity.setResources(resourceIds == null ? null : resourceIds.toString());

        try {
            checkup(resourceIds);
        } catch (Exception e) {
            e.printStackTrace();
            return fail(e.getMessage());
        }

        if (Optional.ofNullable(dict.getId())
                .map(s -> updateDict(dict, dictEntity))
                .orElseGet(() -> saveDict(dict, dictEntity))) {
            saveAdminLogService.saveSystemLog("保存分组信息");
            return ok();
        }
        return fail();
    }

    /**
     * 分页查询分组数据
     *
     * @param query 分页
     * @param type  类型
     * @return 分组数据
     */
    @CheckPermission
    @GetMapping("/list/{type:ARTICLE|POSTER|STAFF|ACTIVITY}")
    public Rest list(StaffGroupQuery query, @PathVariable DictType type) {
        return getDictPage(query, type);
    }

    /**
     * 特殊处理项目组查询
     *
     * @param query 查询条件
     * @param type  类型
     * @return Rest
     */
    @GetMapping("/list/{type:PROJECT}")
    public Rest listProject(StaffGroupQuery query, @PathVariable DictType type) {
        return getDictPage(query, type);
    }

    /**
     * 查询单个分组信息
     *
     * @param id 分组id
     * @return 分组信息
     */
    @GetMapping("/{id}")
    public Rest findOne(@PathVariable Integer id) {
        return ok(baseService.getById(id));
    }

    /**
     * 删除分组
     *
     * @param id id
     * @return 删除结果
     */
    @Transactional(rollbackFor = Exception.class)
    @DeleteMapping("/{id}")
    public Rest delete(@PathVariable Integer id) {
        Dict dict = baseService.getById(id);
        if (null == dict) {
            log.error("资源已被删除");
            return ok();
        }
        if (dict.getType() == null) {
            throw new BusinessException("组类型错误，请检查数据");
        }
        if (ResourceType.POSTER.name().equals(dict.getType().name())
                || ResourceType.ARTICLE.name().equals(dict.getType().name())) {
            if (!resourceService.verifyResourceInDeleteGroup(id)) {
                throw new BusinessException("组中有资源存在");
            }
        }
        String project = "PROJECT";
        if (project.equals(dict.getType().name())) {
            if (!userService.checkDeletedGroupIncludeProject(id)) {
                throw new BusinessException("组中有资源存在");
            }
        }

        List<SignInQrCode> list = signInQrCodeService.lambdaQuery().eq(SignInQrCode::getGroupId, id).list();
        if (!list.isEmpty()) {
            throw new BusinessException("禁止删除包含活动的小组");
        }

        if (baseService.removeById(id)) {
            return ok();
        }

        saveAdminLogService.saveSystemLog("删除分组信息");
        return fail();
    }

    /**
     * 根据分组类型查询分组数据
     *
     * @param type 分组类型
     * @return Rest<List < Dict>>
     */
    @GetMapping("/find/{type:ARTICLE|POSTER|STAFF|PROJECT}")
    public Rest findProjectGroup(@PathVariable DictType type) {
        List<Dict> list = baseService.lambdaQuery().eq(Dict::getType, type).list();
        if (CollectionUtils.isEmpty(list)) {
            return okObj(Collections.emptyList());
        }

        Stream<Dict> stream = getDictStream(list);

        List<Map> maps = new ArrayList<>();
        stream.sorted(Comparator.comparing(Dict::getProjectId))
                .forEach(l -> getGroupAndProjectName(maps, l));

        return okObj(maps);
    }

    /**
     * 查询活动分组，无需进行项目过滤
     *
     * @param type 类型
     * @return Rest
     */
    @GetMapping("/find/{type:ACTIVITY}")
    public Rest findActivityGroup(@PathVariable DictType type) {
        List<Dict> list = baseService.lambdaQuery().eq(Dict::getType, type).list();
        if (CollectionUtils.isEmpty(list)) {
            return okObj(Collections.emptyList());
        }

        List<Map> maps = new ArrayList<>();
        list.stream().sorted(Comparator.comparing(Dict::getProjectId))
                .forEach(l -> getGroupAndProjectName(maps, l));

        return okObj(maps);
    }

    private void checkup(List<Integer> resourceIds) {
        if (CollectionUtils.isNotEmpty(resourceIds)) {
            resourceIds.parallelStream().forEach(r -> {
                if (resourceService.lambdaQuery()
                        .eq(Resource::getId, r)
                        .list()
                        .parallelStream()
                        .anyMatch(v -> v.getGroupId() > 0)) {
                    throw new BusinessException("您选择的资源已在其他分组，请确认后重试");
                }
            });
        }
    }

    @NotNull
    private Boolean saveDict(@RequestBody @Valid DictDTO dict, Dict dictEntity) {
        baseService.save(dictEntity);
        if (dict.getType().name().equals(ResourceType.ARTICLE.name())
                || dict.getType().name().equals(ResourceType.POSTER.name())) {
            resourceService.updateGroupIdByResourceIdsBatch(dict.getResourceIds(), dictEntity.getId());
        }

        return true;
    }

    @NotNull
    private Boolean updateDict(@RequestBody @Valid DictDTO dict, Dict dictEntity) {
        if (!baseService.verifyDictExists(dict.getId(), dict.getType().name())) {
            throw new BusinessException("错误组类型转换");
        }
        if (dict.getType().name().equals(ResourceType.ARTICLE.name())
                || dict.getType().name().equals(ResourceType.POSTER.name())) {
            resourceService.updateGroupIdByResourceIdsBatch(dict.getResourceIds(), dictEntity.getId());
        }
        return baseService.updateById(dictEntity);
    }

    /**
     * 分页查询分组数据
     *
     * @param query 查询条件
     * @param type  类型
     * @return Rest
     */
    private Rest getDictPage(StaffGroupQuery query, DictType type) {
        if (CollectionUtils.isEmpty(query.getProjectIds())) {
            query.setProjectIds(sysAdminProjectService.getProjectList());
        }
        return okObj(baseService.findPage(query, type));
    }

    /**
     * 获取分组以及项目名
     *
     * @param maps 分组数据
     * @param dict 字典
     */
    private void getGroupAndProjectName(List<Map> maps, Dict dict) {
        Map<String, Object> map = new HashMap<>(4);
        String projectTitle = Optional.ofNullable(projectService.lambdaQuery()
                .eq(Project::getId, dict.getProjectId())
                .one())
                .map(Project::getProjectTitle)
                .orElse("");
        map.put("id", dict.getId());
        map.put("name", dict.getName());
        map.put("projectId", dict.getProjectId());
        map.put("projectName", projectTitle);
        maps.add(map);
    }

    /**
     * 获取分组集合流
     *
     * @param list 集合
     * @return Stream<Dict>
     */
    private Stream<Dict> getDictStream(List<Dict> list) {
        Integer adminId = UserInfoContext.getAdminId();
        List<SysAdminProject> adminProjects = sysAdminProjectService.lambdaQuery()
                .eq(SysAdminProject::getSysAdminId, adminId)
                .list();
        List<Integer> projectIds;
        Stream<Dict> stream;
        if (CollectionUtils.isNotEmpty(adminProjects)) {
            projectIds = adminProjects.stream().map(SysAdminProject::getProjectId).collect(toList());
            stream = list.stream().filter(l -> projectIds.contains(l.getProjectId()));
        } else {
            stream = list.stream();
        }
        return stream;
    }

}

