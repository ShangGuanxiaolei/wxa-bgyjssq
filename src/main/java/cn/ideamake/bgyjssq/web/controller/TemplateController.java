package cn.ideamake.bgyjssq.web.controller;

import cn.ideamake.bgyjssq.common.enums.FloorBook720Status;
import cn.ideamake.bgyjssq.common.enums.ResourceType;
import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.pojo.entity.Project;
import cn.ideamake.bgyjssq.pojo.entity.Resource;
import cn.ideamake.bgyjssq.pojo.vo.ResourceDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.floorbook.FloorBook720VO;
import cn.ideamake.bgyjssq.pojo.vo.floorbook.FloorBookVO;
import cn.ideamake.bgyjssq.service.IProjectService;
import cn.ideamake.bgyjssq.service.IResource720Service;
import cn.ideamake.bgyjssq.service.IResourceService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @Author: Walt
 * @Data: 2019-07-22 15:23
 * @Version: 1.0
 */
@AllArgsConstructor
@Controller
@RequestMapping("/template")
@AuthCheck(skipAuth = 2)
public class TemplateController implements IController {

    private final IResource720Service resource720Service;

    private final IResourceService resourceService;

    private final IProjectService projectService;

    /**
     * 获取首页及楼书 TODO 临时去除首页条件，分开处理
     *
     * @param resourceType 资源类型
     * @param id           项目id
     * @return Rest
     */
    @ResponseBody
    @RequestMapping("/{resourceType:article}/{id}")
    public Rest getArticle(@PathVariable String resourceType, @PathVariable Integer id) {
        ResourceDetailVO resource =
                resourceService.getResourceByTypeAndId(id, ResourceType.valueOf(resourceType.toUpperCase()));
        if (null == resource) {
            throw new BusinessException("未找到相关资源");
        }
        String projectName = Optional.ofNullable(projectService.lambdaQuery()
                .eq(Project::getId, resource.getProjectId())
                .one())
                .map(Project::getProjectTitle)
                .orElse("");
        Map<String, String> resourceDetail = new HashMap<>(2);
        resourceDetail.put("content", resource.getContent());
        resourceDetail.put("title", projectName + " "
                + Optional.ofNullable(resource.getTitle()).orElse("文章"));
        return okObj(resourceDetail);
    }

    /**
     * 临时分离首页获取，后台渲染方式
     *
     * @param resourceType 资源类型
     * @param id           项目id
     * @return Rest
     */
    @RequestMapping("/{resourceType:home}/{id}")
    public ModelAndView getHome(@PathVariable String resourceType, @PathVariable Integer id) {

        ResourceDetailVO resource = null;

        if (ResourceType.HOME.name().equalsIgnoreCase(resourceType)) {
            resource = resourceService.getResourceByTypeAndProjectId(id, ResourceType.HOME);
        }

        if (null == resource) {
            throw new BusinessException("未找到相关资源");
        }
        ModelAndView modelAndView = new ModelAndView(resourceType);
        modelAndView.addObject("content", resource.getContent());
        return modelAndView;
    }

    /**
     * 获取微楼书详情
     *
     * @param id 项目id
     * @return java.lang.String
     */
    @ResponseBody
    @GetMapping("/floor_book/{id}")
    public String getFloorBook(@PathVariable Integer id) {
        Resource resource = resourceService.lambdaQuery().eq(Resource::getResourceType, ResourceType.FLOOR_BOOK.getCode())
                .eq(Resource::getProjectId, id).one();
        if (resource == null) {
            throw new BusinessException("资源不存在");
        }
        return resource.getResourceContent();
    }

    /**
     * 720楼书
     *
     * @param id 项目id
     * @return Rest
     */
    @ResponseBody
    @GetMapping("/floor_book2/{id}")
    public Rest getFloorBook2(@PathVariable String id) {
        Integer floorBook720Id;
        try {
            floorBook720Id = Integer.valueOf(id);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return fail("720楼书id传递类型错误");
        }
        Project project = projectService.lambdaQuery().eq(Project::getId, id).one();
        List<FloorBook720VO> list =
                resource720Service.selectFloorBook(floorBook720Id, FloorBook720Status.OPEN.getCode());
        return okObj(new FloorBookVO(
                project == null ? null : project.getTinyFloorBook(),
                list
        ));
    }
}
