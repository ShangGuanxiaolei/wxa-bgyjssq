package cn.ideamake.bgyjssq.web.controller.admin;

import cn.hutool.json.JSONObject;
import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.CheckPermission;
import cn.ideamake.bgyjssq.pojo.query.DashBoardQuery;
import cn.ideamake.bgyjssq.service.ICollectorResourceService;
import cn.ideamake.bgyjssq.service.ICollectorVisitorService;
import cn.ideamake.bgyjssq.service.permission.ISysAdminProjectService;
import cn.ideamake.bgyjssq.web.controller.IController;
import lombok.AllArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: Walt
 * @Data: 2019-07-08 10:28
 * @Version: 1.0
 * <p>
 * PC后台-dashboard
 */
@AllArgsConstructor
@RequestMapping("admin/analysis")
@RestController
public class DataAnalysisController implements IController {

    private final ICollectorVisitorService collectorVisitorService;

    private final ICollectorResourceService collectorResourceService;

    private final ISysAdminProjectService sysAdminProjectService;

    /**
     * 客户访问数据接口
     */
    @CheckPermission
    @GetMapping("customerVisit")
    public Rest customerVisit(DashBoardQuery query) {
        if (CollectionUtils.isEmpty(query.getProjectIds())) {
            query.setProjectIds(sysAdminProjectService.getProjectList());
        }
        JSONObject customerVisitDataList = collectorVisitorService.getCustomerVisitData(query);
        return okObj(customerVisitDataList);
    }

    /**
     * 微文章数据右边-折线图
     *
     * @param query 查询条件
     * @return Rest<List < ArticleDataVO>>
     */
    @CheckPermission
    @GetMapping("articleDataLineChart")
    public Rest articleDataLineChart(DashBoardQuery query) {
        if (CollectionUtils.isEmpty(query.getProjectIds())) {
            query.setProjectIds(sysAdminProjectService.getProjectList());
        }
        return okObj(collectorResourceService.getArticleDataLineChart(query));
    }

    /**
     * 微文章数据左边-饼图
     *
     * @param query 查询条件
     * @return Rest<jsonObject>
     */
    @CheckPermission
    @GetMapping("articleDataPieChart")
    public Rest articleDataPieChart(DashBoardQuery query) {
        if (CollectionUtils.isEmpty(query.getProjectIds())) {
            query.setProjectIds(sysAdminProjectService.getProjectList());
        }
        return okObj(collectorResourceService.getArticleDataPieChart(query));
    }

    /**
     * PC后台访问数量/访问人数趋势图
     */
    @CheckPermission
    @GetMapping("visitTrend")
    public Rest visitTrendForPc(DashBoardQuery query) {
        if (CollectionUtils.isEmpty(query.getProjectIds())) {
            query.setProjectIds(sysAdminProjectService.getProjectList());
        }
        return okObj(collectorVisitorService.getVisitTrendForPc(query));
    }

    /**
     * PC后台访问总揽
     */
    @CheckPermission
    @GetMapping("totalVisitTrend")
    public Rest visitTotalTrendForPc(DashBoardQuery query) {
        if (CollectionUtils.isEmpty(query.getProjectIds())) {
            query.setProjectIds(sysAdminProjectService.getProjectList());
        }
        return okObj(collectorVisitorService.getTotalSummaryDataForPc(query));
    }

    /**
     * 查询小程序数据
     *
     * @param query 查询条件
     * @return 查询结果对象
     */
    @CheckPermission
    @GetMapping("getMiniProgramData")
    public Rest miniProgramData(DashBoardQuery query) {
        if (CollectionUtils.isEmpty(query.getProjectIds())) {
            query.setProjectIds(sysAdminProjectService.getProjectList());
        }
        return okObj(collectorVisitorService.getVisitData(query));
    }

}
