package cn.ideamake.bgyjssq.web.controller;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @param <S>
 * @param <T>
 * @param <V>
 * @author ideamake
 */
public abstract class AbstractController<S extends IService<T>, T, V> implements IController<T, V> {

    @Autowired
    protected S baseService;

}
