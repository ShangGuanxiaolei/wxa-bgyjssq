package cn.ideamake.bgyjssq.web.interceptor;

import cn.hutool.core.util.StrUtil;
import cn.ideamake.bgyjssq.common.enums.RestEnum;
import cn.ideamake.bgyjssq.common.exception.AuthenticationException;
import cn.ideamake.bgyjssq.common.exception.UserNotFoundException;
import cn.ideamake.bgyjssq.common.handler.UserInfoContext;
import cn.ideamake.bgyjssq.common.util.Constants;
import cn.ideamake.bgyjssq.common.util.JwtKit;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import cn.ideamake.bgyjssq.pojo.bo.Audience;
import cn.ideamake.bgyjssq.pojo.entity.OauthWechat;
import cn.ideamake.bgyjssq.pojo.vo.UserSessionVO;
import cn.ideamake.bgyjssq.service.IOauthWechatService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * @author ideamake
 */
@AllArgsConstructor
@Slf4j
@Component
public class AuthenticationInterceptor implements HandlerInterceptor {

    private final Audience audience;

    private final IOauthWechatService oauthWeChatService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        // 如果不是映射到方法的请求, 直接通过
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }

        String requestUrl = request.getRequestURI();

        //开放swagger-ui 接口
        String swagger = "swagger";
        if (requestUrl.contains(swagger)) {
            return true;
        }

        /*
        优先判断类是否有注解
        如果类注解=跳过校验,则检查方法注解(方法注解优先级高)
        如果方法没有注解, 则跳过校验
        如果方法有注解且=跳过校验, 则跳过校验
        如果方法有注解且=不跳过校验, 则不跳过校验
        如果类注解=不跳过, 上面的过程反过来
        */
        if (checkHandler((HandlerMethod) handler, requestUrl)) {
            return true;
        }

        //获取token数据
        Claims claims = getClaims(request, requestUrl);

        //后台登录及权限控制处理
        if (systemLoginCheck(request, claims)) {
            return true;
        }

        //获取微信信息
        OauthWechat oauthWechat = getOauthWeChat(requestUrl, claims);

        //设置用户上下文对象
        setUserInfoContext(claims, oauthWechat);
        return true;
    }

    private void setUserInfoContext(Claims claims, OauthWechat oauthWechat) {
        String sessionKey = (String) claims.get("sessionKey");
        UserSessionVO userSessionVO = new UserSessionVO();
        BeanUtils.copyProperties(oauthWechat, userSessionVO);
        userSessionVO.setUserId(oauthWechat.getUserUuid());
        userSessionVO.setUserType(oauthWechat.getUserType());
        userSessionVO.setSessionKey(sessionKey);
        UserInfoContext.set(userSessionVO);
    }

    @NotNull
    private OauthWechat getOauthWeChat(String requestUrl, Claims claims) {
        String userId = (String) claims.get("userId");
        if (null == userId) {
            log.error("token中未包含用户信息, 访问URL={}", requestUrl);
            throw new AuthenticationException(RestEnum.TOKEN_ERROR);
        }
        //先从缓存中读取用户信息，不存在则读取表中数据
        OauthWechat oauthWechat = oauthWeChatService.getUserWeChatInfoByCache(userId);
        if (oauthWechat == null) {
            log.error("用户不存在,token中解析userId={}, 访问URL={}", userId, requestUrl);
            throw new UserNotFoundException(userId);
        }
        return oauthWechat;
    }


    @NotNull
    private Claims getClaims(HttpServletRequest request, String requestUrl) {
        String token = request.getHeader("IM-TOKEN");
        if (StrUtil.isBlank(token)) {
            log.error("请求未携带token信息, 访问URL={}", requestUrl);
            throw new AuthenticationException(RestEnum.NEED_LOGIN);
        }

        Claims claims;
        try {
            claims = JwtKit.parseJwt(token, audience.getBase64Secret());
        } catch (ExpiredJwtException e) {
            log.error("token信息已过时, 访问URL={}", requestUrl);
            throw new AuthenticationException(RestEnum.TOKEN_ERROR);
        } catch (UnsupportedJwtException e) {
            log.error("token参数不受支持, 访问URL={}", requestUrl);
            throw new AuthenticationException(RestEnum.TOKEN_ERROR);
        } catch (MalformedJwtException e) {
            log.error("token格式错误, 访问URL={}", requestUrl);
            throw new AuthenticationException(RestEnum.TOKEN_ERROR);
        } catch (SignatureException e) {
            log.error("签名例外, 访问URL={}", requestUrl);
            throw new AuthenticationException(RestEnum.TOKEN_ERROR);
        } catch (IllegalArgumentException e) {
            log.error("token参数错误, 访问URL={}", requestUrl);
            throw new AuthenticationException(RestEnum.TOKEN_ERROR);
        }

        if (null == claims) {
            log.error("token解析失败, 访问URL={}", requestUrl);
            throw new AuthenticationException(RestEnum.TOKEN_ERROR);
        }
        return claims;
    }

    private boolean checkHandler(HandlerMethod handler, String requestUrl) {
        int skipAuth = 2;
        Class<?> beanType = handler.getBeanType();
        if (beanType.isAnnotationPresent(AuthCheck.class)) {
            AuthCheck typeAuthCheck = beanType.getAnnotation(AuthCheck.class);
            //此处用于后续角色权限校验，接口角色与用户角色比较，具体校验规则待定
            //如果接口不需要鉴权，则略过鉴权直接进入调用
            if (typeAuthCheck.skipAuth() == skipAuth) {
                Method method = handler.getMethod();
                if (method.isAnnotationPresent(AuthCheck.class)) {
                    AuthCheck authCheck = method.getAnnotation(AuthCheck.class);
                    //此处用于后续角色权限校验，接口角色与用户角色比较，具体校验规则待定
                    log.info("用户角色:{} | 访问接口描述信息:{} | 访问URL:{}", authCheck.role(), authCheck.value(), requestUrl);
                    //如果接口不需要鉴权，则略过鉴权直接进入调用
                    return authCheck.skipAuth() == skipAuth;
                } else {
                    return true;
                }
            }
        } else {
            Method method = handler.getMethod();
            if (method.isAnnotationPresent(AuthCheck.class)) {
                AuthCheck authCheck = method.getAnnotation(AuthCheck.class);
                //此处用于后续角色权限校验，接口角色与用户角色比较，具体校验规则待定
                log.info("用户角色:{} | 访问接口描述信息:{} | 访问URL:{}", authCheck.role(), authCheck.value(), requestUrl);
                //如果接口不需要鉴权，则略过鉴权直接进入调用
                return authCheck.skipAuth() == skipAuth;
            }
        }
        return false;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        UserInfoContext.remove();
    }

    /**
     * PC后台校验是否登录
     *
     * @param request request
     * @return boolean
     */
    private boolean systemLoginCheck(HttpServletRequest request, Claims claims) {
        String requestUrl = request.getRequestURI();
        if (requestUrl.startsWith(Constants.CHECK_URL)
                || requestUrl.startsWith(Constants.CHECK_URL_CONTEXT)) {
            UserInfoContext.setRoleId((Integer) claims.get("roleId"));
            UserInfoContext.setAdminId((Integer) claims.get("adminId"));
            return true;
        }
        return false;
    }

}
