package cn.ideamake.bgyjssq.web.resolver;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import cn.ideamake.bgyjssq.common.handler.UserInfoContext;
import cn.ideamake.bgyjssq.common.validate.AuthCheck;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.joor.Reflect;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * @author imyzt
 * @date 2019/07/04
 * @description userid参数填充
 */
@Aspect
@Component
@Slf4j
public class UserIdArgumentAopResolver {

    @Before("execution(* cn.ideamake.bgyjssq.web.controller..*(..))")
    public void before(JoinPoint joinPoint) {

        Object[] args = joinPoint.getArgs();
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();

        /*
         当满足以下条件时,做自动参数填充
         1. 控制器方法包含AuthCheck注解,并且AuthCheck=1
         2. userId属性包含UserId注解时
         */
        if (method.isAnnotationPresent(AuthCheck.class)) {
            AuthCheck authCheck = method.getAnnotation(AuthCheck.class);
            // 0 为AuthCheck定义的值=1时,需要鉴权.
            if (0 == authCheck.skipAuth()) {
                for (Object arg : args) {
                    // 防止参数为空时干扰正常业务
                    if (null != arg) {
                        for (Field field : arg.getClass().getDeclaredFields()) {
                            if (field.isAnnotationPresent(UserId.class)) {
                                Object value = Reflect.on(arg).get(field.getName());
                                // 传递了uuid时,校验与token中的uuid是否匹配,匹配直接返回
                                String userId = UserInfoContext.get().getUserId();
                                if (null != value && Objects.equals(userId, value)) {
                                    return;
                                }
                                // 否则,将uuid设置为token中携带的uuid,保证后续业务的安全性(否则可以伪造uuid修改其他用户的信息)
                                //暂时兼容性考虑，如果传入id，则以传入的id为准

                                Reflect.on(arg).set(field.getName(), value == null ? userId : value);
                                log.info("token uuid:{},field uuid:{}", userId, value);
                            }
                        }
                    }
                }
            }
        }
    }
}
