package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.entity.Visitor;
import cn.ideamake.bgyjssq.pojo.query.DefaultQueryPage;
import cn.ideamake.bgyjssq.pojo.query.TopTenQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorLogQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorVisitAnalysisQuery;
import cn.ideamake.bgyjssq.pojo.vo.CustomerTopTenVO;
import cn.ideamake.bgyjssq.pojo.vo.FocusProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.analysis.DataAnalysisDetailListVO;
import cn.ideamake.bgyjssq.pojo.vo.analysis.VisitorListVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorLogVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 访客表 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
public interface IVisitorService extends IService<Visitor> {


    /**
     * 访客获取日志
     * @param visitorLogQuery
     * @return
     */
    IPage<VisitorLogVO> getVisitorLog(VisitorLogQuery visitorLogQuery);

    /**
     * pc后台-客户top10
     * @param query 参数
     * @return 列表信息
     */
    List<CustomerTopTenVO> selectCustomerTopTenList(TopTenQuery query);

    /**
     * 根据销售id查询访客列表
     *
     * @param query 查询条件
     * @return List<VisitorListVO>
     */
    List<VisitorListVO> selectVisitorByUserId(VisitorVisitAnalysisQuery query);

    /**
     * 查询客户关注项目信息
     *
     * @param page   分页
     * @param userId 客户id
     * @return IPage<FocusProjectVO>
     */
    IPage<FocusProjectVO> selectFocusProject(DefaultQueryPage page, String userId);

    /**
     * 获取访客拓客数据
     *
     * @param uuid 访客id
     * @return DataAnalysisDetailListVO
     */
    DataAnalysisDetailListVO getVisitorVO(String uuid);
}
