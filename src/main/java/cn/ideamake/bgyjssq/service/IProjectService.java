package cn.ideamake.bgyjssq.service;


import cn.ideamake.bgyjssq.pojo.dto.ProjectGroupListDTO;
import cn.ideamake.bgyjssq.pojo.dto.SaveOrChangeProjectDTO;
import cn.ideamake.bgyjssq.pojo.entity.Project;
import cn.ideamake.bgyjssq.pojo.query.DefaultQueryPage;
import cn.ideamake.bgyjssq.pojo.query.VisitorProjectQuery;
import cn.ideamake.bgyjssq.pojo.vo.ProjectGroupListVO;
import cn.ideamake.bgyjssq.pojo.vo.ResourceBaseVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.floorbook.ProjectFloorBook720VO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 项目表 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
public interface IProjectService extends IService<Project> {

    /**
     * 查询项目(楼盘)列表
     *
     * @param query 参数
     * @return 项目列表
     */
    IPage<VisitorProjectVO> selectProjectList(VisitorProjectQuery query);

    /**
     * 查询项目(720楼书)列表
     *
     * @param query 参数
     * @return 项目列表
     */
    IPage<ProjectFloorBook720VO> selectProjectFloorBook720List(VisitorProjectQuery query);

    /**
     * 添加或修改楼盘
     *
     * @param project 楼盘
     * @return 是否成功
     */
    boolean saveOrUpdateProject(SaveOrChangeProjectDTO project);

    /**
     * 楼盘分组列表
     *
     * @param query 参数
     * @return 列表
     */
    IPage<ProjectGroupListVO> selectProjectGroupList(DefaultQueryPage query);

    /**
     * 通过组id获取组下项目的基本信息
     *
     * @param groupId
     * @return
     */

    List<ResourceBaseVO> projectBaseByGroupId(Integer groupId);

    /**
     * 查询组-楼盘树
     *
     * @param sellerId 销售id
     * @return 数据列表
     */
    List<ProjectGroupListDTO> selectProjectGroupTree(String sellerId);

    /**
     * 根据用户id（分角色等级）获取项目id列表
     *
     * @param userId 用户id
     * @return List<Integer>
     */
    List<Integer> selectProjectIdListByUserId(String userId);

    /**
     * 根据用户id查询项目名称列表
     *
     * @param userId 用户id列表
     * @return List<String>
     */
    List<String> selectProjectNameByUserId(String userId);
}
