package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.dto.CollectForAllParamDTO;
import cn.ideamake.bgyjssq.pojo.entity.CollectorAllRecord;
import cn.ideamake.bgyjssq.pojo.query.StaffVisitorRankQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorTrendQuery;
import cn.ideamake.bgyjssq.pojo.vo.StaffVisitorRankVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorTrendVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户行为全量记录，用于销售对用户行为的观测 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
public interface ICollectorAllRecordService extends IService<CollectorAllRecord> {

    /**
     * 数据上报
     *
     * @param collectForAllParamDTO 数据对象
     */
    void collectData(CollectForAllParamDTO collectForAllParamDTO);

    /**
     * 访问趋势
     *
     * @param visitorTrendQuery visitorTrendQuery
     * @param sellerIdList sellerIdList
     * @return IPage<VisitorTrendVO>
     */
    IPage<VisitorTrendVO> getVisitorTrend(VisitorTrendQuery visitorTrendQuery, List<String> sellerIdList);

    /**
     * 访问数据
     *
     * @param staffVisitorRankQuery staffVisitorRankQuery
     * @return IPage<StaffVisitorRankVO>
     */
    IPage<StaffVisitorRankVO> getStaffVisitorRank(StaffVisitorRankQuery staffVisitorRankQuery);
}
