package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.entity.StaffVisitor;
import cn.ideamake.bgyjssq.pojo.query.StaffVisitorQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorVisitAnalysisQuery;
import cn.ideamake.bgyjssq.pojo.vo.StaffVisitorVO;
import cn.ideamake.bgyjssq.pojo.vo.analysis.DataAnalysisDetailListVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户员工对于绑定自己的客户的管理 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-06-19
 */
public interface IStaffVisitorService extends IService<StaffVisitor> {

    /**
     * 员工获取客户列表
     *
     * @param query 查询参数
     * @return 返回
     */
    IPage<StaffVisitorVO> getStaffVisitorByFilter(StaffVisitorQuery query);

    /**
     * 获取高等级角色管理的组
     *
     * @param sellerId 用户id
     * @param projects 项目id列表
     * @return List<DataAnalysisDetailListVO>
     */
    List<DataAnalysisDetailListVO> getUserGroup(String sellerId, List<Integer> projects);

    /**
     * 获取高等级角色管理的组-访客动态分析
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @return List<DataAnalysisDetailListVO>
     */
    List<DataAnalysisDetailListVO> getUserGroupAnalyse(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery);

    /**
     * 获取高等级角色管理的销售
     *
     * @param groupIds 查询条件
     * @param projects 项目id列表
     * @return List<DataAnalysisDetailListVO>
     */
    List<DataAnalysisDetailListVO> getUserList(List<Integer> groupIds, List<Integer> projects);

    /**
     * 获取各种数据类型钻取数据统计
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param list                      userIdlList
     * @return java.lang.Long
     */
    Long getData(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> list);

    /**
     * 获取查询条件列表
     *
     * @param list 销售列表
     * @return List<String>
     */
    List<String> getQueryListByCollection(List<String> list);
}
