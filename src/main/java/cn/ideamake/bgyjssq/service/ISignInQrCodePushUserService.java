package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.entity.SignInQrCodePushUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 签到二维码推送员工列表 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-09-16
 */
public interface ISignInQrCodePushUserService extends IService<SignInQrCodePushUser> {

}
