package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.entity.OauthWechat;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户微信登录表 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-06-11
 */
public interface IOauthWechatService extends IService<OauthWechat> {
    /**
     * 通过缓存获取用户微信信息
     *
     * @param userId
     * @return
     */
    OauthWechat getUserWeChatInfoByCache(String userId);

    /**
     * 根据openId查询用户信息
     *
     * @param openId openId
     * @return OauthWechat
     */
    OauthWechat getUserWeChatInfoByOpenId(String openId);

    /**
     * 根据unionId查询用户信息
     *
     * @param unionId unionId
     * @return OauthWechat
     */
    OauthWechat getUserWeChatInfoByUnionId(String unionId);

    /**
     * 根据unionId和openId查询用户信息
     *
     * @param unionId unionId
     * @param openId  openId
     * @return OauthWechat
     */
    OauthWechat getUserWeChatInfoByUnionIdAndOpenId(String unionId, String openId);

    /**
     * 通过缓存更新用户微信信息
     *
     * @param oauthWechat
     * @return
     */
    OauthWechat updateUserWeChatInfoByIdByCache(OauthWechat oauthWechat);

    /**
     * 通过openid更新微信信息
     *
     * @param openId
     * @return
     */
    OauthWechat updateUserWeChatInfoByOpenIdByCache(String openId, OauthWechat newData);

    /**
     * 通过userId更新微信信息
     *
     * @param userId
     * @return
     */
    OauthWechat updateUserWeChatInfoByUserIdByCache(String userId, OauthWechat newData);

    /**
     * 通过userId清楚微信记录是信息
     */
    OauthWechat cleanWeChatUserInfo(String userId);

    /**
     * 保存用户formId
     *
     * @param userId
     * @param fromId
     * @return
     */
    boolean saveOrUpdateFormId(String userId, String fromId);

    /**
     * 获取formId
     *
     * @param userId
     * @return
     */
    String getFormId(String userId);
}
