package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.entity.UserPushActivity;
import cn.ideamake.bgyjssq.pojo.vo.PushActivitisVO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-08-09
 */
public interface IUserPushActivityService extends IService<UserPushActivity> {
    boolean isExist(PushActivitisVO pushActivitisVO);
}
