package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.dto.StaffHideProjectDTO;
import cn.ideamake.bgyjssq.pojo.entity.ProjectManager;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 员工项目管理表 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
public interface IProjectManagerService extends IService<ProjectManager> {

    /**
     * 通过销售id和项目id跟新销售的展示的项目
     *
     * @param staffHideProjectDTO
     * @return
     */
    boolean hideProjectForStaff(StaffHideProjectDTO staffHideProjectDTO);

    String tmpTest(String userId, String proIds);

    /**
     * 获取有效的userId列表
     *
     * @param projects 项目列表
     * @param list     用户列表
     * @return List<String>
     */
    List<String> getEffectiveData(Set<Integer> projects, List<String> list);
}
