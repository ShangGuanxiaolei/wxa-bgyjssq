package cn.ideamake.bgyjssq.service;

import cn.hutool.json.JSONObject;
import cn.ideamake.bgyjssq.pojo.entity.CollectorVisitor;
import cn.ideamake.bgyjssq.pojo.query.DashBoardQuery;
import cn.ideamake.bgyjssq.pojo.query.DistributionMapQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorVisitAnalysisQuery;
import cn.ideamake.bgyjssq.pojo.vo.CustomerDistributionMapVO;
import cn.ideamake.bgyjssq.pojo.vo.RecommendAnalysisVO;
import cn.ideamake.bgyjssq.pojo.vo.dashboard.MiniProgramDataVO;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.OriginCountCell;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.TotalStatistics;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.TotalVisitTrend;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.VisitPeriod;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.VisitTrend;
import com.baomidou.mybatisplus.extension.service.IService;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 项目访问采集 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
public interface ICollectorVisitorService extends IService<CollectorVisitor> {

    /**
     * 获取访客最后访问时间
     * @param visitorUuid 访客id
     * @return 最后访问日期-时间
     */
    LocalDateTime getVisitorLastVisitTime(String visitorUuid);

    /**
     * pc端获取访问趋势
     * @param dashBoardQuery
     * @return 访问次数和访问人数
     */
    VisitTrend getVisitTrendForPc(DashBoardQuery dashBoardQuery);

    /**
     * pc端获取访问趋势
     * @param dashBoardQuery
     * @return 访问次数和访问人数
     */
    TotalVisitTrend getTotalSummaryDataForPc(DashBoardQuery dashBoardQuery);

    /**
     * 获取客户访问分析中的统计总揽
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return TotalStatistics
     */
    TotalStatistics getVisitorVisitTotalData(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
                                             List<String> userIdList);

    /**
     * 查询访问总数
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getNumberOfVisits(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList);

    /**
     * 查询访问总数
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getNumberOfPeople(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList);

    /**
     * 查询访问总数
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getPeopleOfRemainPhone(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList);

    /**
     * 查询访问总数
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getPeopleOfAccess(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList);

    /**
     * 查询访问总数
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getShareCount(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList);

    /**
     * 查询访问总数
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getSharePeople(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList);

    /**
     * 查询访问总数
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getReadCount(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList);

    /**
     * 查询访问总数
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getReadPeople(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList);

    /**
     * 员工拓展
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getSellerRecommend(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList);

    /**
     * 客户转介
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getCustomerRecommend(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList);

    /**
     * 获取客户访问分析中的来源统计数据
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return List<OriginCountCell>
     */
    List<OriginCountCell> getVisitorVisitOriginData(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
                                                    List<String> userIdList);

    /**
     * 获取客户访问分析中访问趋势数据,目前统计的粒度是访问到退出小程序算一次访问
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return VisitTrend
     */
    VisitTrend getVisitorVisitTrendData(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
                                        List<String> userIdList);

    /**
     * 获取访客分析中访客活跃时间段分析
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return List<VisitPeriod>
     */
    List<VisitPeriod> getVisitorVisitPeriodData(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
                                                List<String> userIdList);

    /**
     * 客户访问数据
     * @param query 查询参数
     * @return 列表信息
     */
    JSONObject getCustomerVisitData(DashBoardQuery query);

    /**
     * 查询访客总数
     *
     * @param query 查询条件
     * @return 总数对象
     */
    MiniProgramDataVO getVisitData(DashBoardQuery query);

    /**
     * 推荐分析
     *
     * @param sourceId 推荐人id
     * @return RecommendAnalysisVO
     */
    RecommendAnalysisVO selectRecommendAllTotal(String sourceId);

    /**
     * 客户分布地图
     * @param query 参数
     * @return 客户经纬度列表
     */
    List<CustomerDistributionMapVO> getCustomerDistributionMap(DistributionMapQuery query);
}
