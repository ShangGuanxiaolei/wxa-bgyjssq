package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.entity.ClickRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 点击行为记录统计表 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-08-06
 */
public interface IClickRecordService extends IService<ClickRecord> {

}
