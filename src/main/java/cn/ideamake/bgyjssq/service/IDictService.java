package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.common.enums.DictType;
import cn.ideamake.bgyjssq.pojo.entity.Dict;
import cn.ideamake.bgyjssq.pojo.query.StaffGroupQuery;
import cn.ideamake.bgyjssq.pojo.vo.DictVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典表，用于记录相同字段结构 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
public interface IDictService extends IService<Dict> {

    /**
     * 分页查询分组数据
     *
     * @param queryPage 分页
     * @param type      类型
     * @return 分组数据
     */
    IPage<DictVO> findPage(StaffGroupQuery queryPage, DictType type);

    /**
     * 校验字典是否存在
     *
     * @param groupId 组id
     * @param type    类型
     * @return 是否存在
     */
    boolean verifyDictExists(Integer groupId, String type);

}
