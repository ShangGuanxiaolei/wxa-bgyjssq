package cn.ideamake.bgyjssq.service.permission;

import cn.ideamake.bgyjssq.common.enums.PermissionTypeEnum;
import cn.ideamake.bgyjssq.pojo.entity.SysPermission;
import cn.ideamake.bgyjssq.pojo.vo.PermissionVO;
import cn.ideamake.bgyjssq.pojo.vo.permission.MenuListChildrenVO;
import cn.ideamake.bgyjssq.pojo.vo.permission.MenuListVO;
import cn.ideamake.bgyjssq.pojo.vo.permission.NewMenuListVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 系统权限资源表，记录系统的权限资源信息 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
public interface ISysPermissionService extends IService<SysPermission> {

    /**
     * 保存系统权限信息
     *
     * @param sysPermission 系统权限实体
     * @return 保存结果
     */
    boolean saveAndUpdate(SysPermission sysPermission);

    /**
     * 根据权限id查询权限
     *
     * @param permissionId 权限id
     * @return 系统权限
     */
    SysPermission selectByPermissionId(Integer permissionId);

    /**
     * 根据类型查询系统权限
     *
     * @param type 类型
     * @return 权限列表
     */
    List<SysPermission> selectByType(PermissionTypeEnum type);

    /**
     * 查询所有权限
     *
     * @return List<PermissionVO>
     */
    List<PermissionVO> selectAllByParent();

    /**
     * 根据父类id查询
     *
     * @param parentId 父类id
     * @return List<PermissionVO>
     */
    List<PermissionVO> selectByParentId(Integer parentId);

    /**
     * 根据角色id和权限类型查询权限
     *
     * @param roleId 角色id
     * @param type   权限类型
     * @return List<SysPermission>
     */
    List<SysPermission> selectByRoleIdAndType(Integer roleId, PermissionTypeEnum type);

    /**
     * 根据角色id和权限类型查询权限
     *
     * @param roleId   角色id
     * @param type     权限类型
     * @param parentId 父类id
     * @return List<MenuListVO>
     */
    List<MenuListVO> selectByRoleIdAndTypeAndParentId(Integer roleId, PermissionTypeEnum type, Integer parentId);

    /**
     * 根据角色id和权限类型查询权限
     *
     * @param roleId   角色id
     * @param type     权限类型
     * @param parentId 父类id
     * @return List<MenuListVO>
     */
    List<MenuListChildrenVO> selectByRoleIdAndTypeAndParentIdChildren(Integer roleId, PermissionTypeEnum type, Integer parentId);

    /**
     * 根据父类id查询权限菜单（缓存形式）
     *
     * @param parentId 父类id
     * @return List<PermissionVO>
     */
    List<PermissionVO> selectPermissionByParentId(Integer parentId);

    /**
     * 查询角色权限菜单列表(高效率)
     *
     * @param roleId 角色id
     * @return List<MenuListAllVO>
     */
    List<NewMenuListVO> getMenuListByCache(Integer roleId);

    /**
     * 根据角色id及权限类型查询权限列表
     *
     * @param roleId 角色id
     * @param type   权限类型
     * @return List<SysPermission>
     */
    List<SysPermission> selectAllByRoleIdAndType(Integer roleId, PermissionTypeEnum type);
}
