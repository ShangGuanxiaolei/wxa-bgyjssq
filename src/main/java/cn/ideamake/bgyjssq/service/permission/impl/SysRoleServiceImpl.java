package cn.ideamake.bgyjssq.service.permission.impl;

import cn.ideamake.bgyjssq.dao.mapper.RolePermissionMapper;
import cn.ideamake.bgyjssq.dao.mapper.SysPermissionMapper;
import cn.ideamake.bgyjssq.dao.mapper.SysRoleMapper;
import cn.ideamake.bgyjssq.pojo.entity.SysPermission;
import cn.ideamake.bgyjssq.pojo.entity.SysRole;
import cn.ideamake.bgyjssq.pojo.query.QueryPage;
import cn.ideamake.bgyjssq.pojo.vo.permission.SysRoleVO;
import cn.ideamake.bgyjssq.service.permission.ISysRoleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.jsonwebtoken.lang.Collections;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static cn.ideamake.bgyjssq.common.enums.PermissionTypeEnum.DATA;
import static java.util.stream.Collectors.toList;

/**
 * <p>
 * 系统角色表，记录系统中的角色信息 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@AllArgsConstructor
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    private final RolePermissionMapper rolePermissionMapper;

    private SysPermissionMapper sysPermissionMapper;

    /**
     * 客户地图
     */
    private static final String MAP = "dataOverviewTrack";

    /**
     * 业绩排名
     */
    private static final String TOP_TEN = "dataOverviewRank";

    @Override
    public IPage<SysRoleVO> findByPage(QueryPage queryPage, String name) {
        IPage<SysRoleVO> page = baseMapper.findByPage(queryPage.getPageInfo(), name);
        Optional.ofNullable(page).ifPresent(p -> {
            List<SysRoleVO> records = p.getRecords();
            Optional.ofNullable(records).ifPresent(r ->
                    r.forEach(v -> v.setPermissionIds(getPermissionIds(v.getId()))));
        });
        return page;
    }

    @Override
    public Boolean prohibit(Integer id) {
        return baseMapper.prohibit(id) > 0;
    }

    @Override
    public List<SysRole> selectEffectiveRole() {
        return baseMapper.selectEffectiveRole();
    }

    /**
     * 获取数据权限列表
     *
     * @param roleId 角色id
     * @return List<Integer>
     */
    private List<Integer> getPermissionIds(Integer roleId) {
        List<SysPermission> list = rolePermissionMapper.selectPermissionByRoleId(roleId);
        if (Collections.isEmpty(list)) {
            return new ArrayList<>();
        }
        QueryWrapper<SysPermission> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .in(SysPermission::getId, list.parallelStream()
                        .map(SysPermission::getId)
                        .collect(toList()));
        List<SysPermission> sysPermissionList = sysPermissionMapper.selectList(queryWrapper);
        return sysPermissionList.stream()
                .filter(s -> DATA.equals(s.getType())
                        || MAP.equals(s.getName())
                        || TOP_TEN.equals(s.getName()))
                .map(SysPermission::getId)
                .collect(toList());
    }
}
