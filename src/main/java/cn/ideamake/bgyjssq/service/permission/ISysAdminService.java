package cn.ideamake.bgyjssq.service.permission;

import cn.ideamake.bgyjssq.pojo.entity.SysAdmin;
import cn.ideamake.bgyjssq.pojo.query.AdminQuery;
import cn.ideamake.bgyjssq.pojo.query.QueryPage;
import cn.ideamake.bgyjssq.pojo.vo.PermissionVO;
import cn.ideamake.bgyjssq.pojo.vo.permission.AdminLoginVO;
import cn.ideamake.bgyjssq.pojo.vo.permission.SysAdminVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统管理员列表 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
public interface ISysAdminService extends IService<SysAdmin> {

    /**
     * 根据角色id分页查询用户
     *
     * @param roleId    角色id
     * @param queryPage 查询参数
     * @return 分页数据
     */
    IPage<SysAdmin> findByRoleId(Integer roleId, QueryPage queryPage);

    /**
     * 获取当前登录用户拥有的权限
     *
     * @param roleId 角色id
     * @return 系统权限列表
     */
    List<PermissionVO> findPermission(Integer roleId);

    /**
     * 分页查询管理员列表
     *
     * @param adminQuery 参数
     * @return 分页数据
     */
    IPage<SysAdminVO> findPage(AdminQuery adminQuery);

    /**
     * 登录
     *
     * @param admin    用户信息
     * @param request  request
     * @param response response
     * @param session  session
     * @return String
     */
    Map<String, Object> login(AdminLoginVO admin, HttpServletRequest request, HttpServletResponse response, HttpSession session);

    /**
     * 退出登录
     *
     * @param request  request
     * @param response response
     * @param session  session
     * @return String
     */
    String logout(HttpServletRequest request, HttpServletResponse response, HttpSession session);
}
