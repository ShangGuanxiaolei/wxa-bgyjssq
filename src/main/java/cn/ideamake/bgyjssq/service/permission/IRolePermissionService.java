package cn.ideamake.bgyjssq.service.permission;

import cn.ideamake.bgyjssq.pojo.entity.RolePermission;
import cn.ideamake.bgyjssq.pojo.entity.SysPermission;
import cn.ideamake.bgyjssq.pojo.vo.PermissionVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 角色权限绑定表，记录角色和权限的绑定关系 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
public interface IRolePermissionService extends IService<RolePermission> {

    /**
     * 根据角色id查询权限
     *
     * @param roleId 角色id
     * @return 权限列表
     */
    List<PermissionVO> selectPermissionByRoleId(Integer roleId);

    /**
     * 根据角色id查询权限列表
     *
     * @param roleId 角色id
     * @return 权限列表
     */
    List<SysPermission> selectPermissionListByRoleId(Integer roleId);

    /**
     * 是否包含属性
     *
     * @param properties 属性字段
     * @return Boolean
     */
    Boolean hasProperties(String properties);

}
