package cn.ideamake.bgyjssq.service.permission.impl;

import cn.ideamake.bgyjssq.common.handler.UserInfoContext;
import cn.ideamake.bgyjssq.dao.mapper.RolePermissionMapper;
import cn.ideamake.bgyjssq.pojo.entity.RolePermission;
import cn.ideamake.bgyjssq.pojo.entity.SysPermission;
import cn.ideamake.bgyjssq.pojo.vo.PermissionVO;
import cn.ideamake.bgyjssq.service.permission.IRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.jsonwebtoken.lang.Collections;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 角色权限绑定表，记录角色和权限的绑定关系 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements IRolePermissionService {

    @Override
    public List<PermissionVO> selectPermissionByRoleId(Integer roleId) {
        return setChildren(baseMapper.selectPermissionListByRoleId(roleId, 0), roleId);
    }

    @Override
    public List<SysPermission> selectPermissionListByRoleId(Integer roleId) {
        return baseMapper.selectPermissionByRoleId(roleId);
    }

    @Override
    public Boolean hasProperties(String properties) {
        Integer roleId = UserInfoContext.getRoleId();
        List<SysPermission> list = this.selectPermissionListByRoleId(roleId);
        assert list != null;
        return list.parallelStream()
                .map(SysPermission::getName)
                .anyMatch(properties::equals);
    }

    private List<PermissionVO> setChildren(List<PermissionVO> permissionList, Integer roleId) {
        for (PermissionVO permission : permissionList) {
            permission.setChildren(baseMapper.selectPermissionListByRoleId(roleId, permission.getId()));
            if (!Collections.isEmpty(permission.getChildren())) {
                setChildren(permission.getChildren(), roleId);
            }
        }
        return permissionList;
    }
}
