package cn.ideamake.bgyjssq.service.permission.impl;

import cn.ideamake.bgyjssq.common.enums.PermissionTypeEnum;
import cn.ideamake.bgyjssq.common.handler.UserInfoContext;
import cn.ideamake.bgyjssq.dao.mapper.RolePermissionMapper;
import cn.ideamake.bgyjssq.dao.mapper.SysPermissionMapper;
import cn.ideamake.bgyjssq.dao.mapper.SysRoleMapper;
import cn.ideamake.bgyjssq.pojo.entity.SysPermission;
import cn.ideamake.bgyjssq.pojo.entity.SysRole;
import cn.ideamake.bgyjssq.pojo.vo.PermissionVO;
import cn.ideamake.bgyjssq.pojo.vo.permission.MenuListChildrenVO;
import cn.ideamake.bgyjssq.pojo.vo.permission.MenuListVO;
import cn.ideamake.bgyjssq.pojo.vo.permission.NewMenuListVO;
import cn.ideamake.bgyjssq.service.permission.ISysPermissionService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.jsonwebtoken.lang.Collections;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static cn.ideamake.bgyjssq.common.enums.PermissionTypeEnum.MENU;
import static java.util.stream.Collectors.toList;

/**
 * <p>
 * 系统权限资源表，记录系统的权限资源信息 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@AllArgsConstructor
@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements ISysPermissionService {

    private final SysRoleMapper sysRoleMapper;

    private final RolePermissionMapper rolePermissionMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean saveAndUpdate(SysPermission permission) {
        return Optional.ofNullable(permission.getId())
                .filter(p -> baseMapper.updateById(permission) > 0)
                .orElseGet(() -> baseMapper.insert(permission)) > 0;
    }

    @Override
    public SysPermission selectByPermissionId(Integer permissionId) {
        if (permissionId == null) {
            return null;
        }
        return baseMapper.selectById(permissionId);
    }

    @Override
    public List<SysPermission> selectByType(PermissionTypeEnum type) {
        if (type == null) {
            return null;
        }
        return baseMapper.selectList(new QueryWrapper<SysPermission>().lambda().eq(SysPermission::getType, type));
    }

    @Override
    public List<PermissionVO> selectAllByParent() {
        return this.selectPermissionByParentId(0);
    }

    @Override
    public List<PermissionVO> selectByParentId(Integer parentId) {
        return baseMapper.selectByParentId(parentId);
    }

    @Override
    public List<SysPermission> selectByRoleIdAndType(Integer roleId, PermissionTypeEnum type) {
        return baseMapper.selectByRoleIdAndType(roleId, type);
    }

    @Override
    public List<MenuListVO> selectByRoleIdAndTypeAndParentId(Integer roleId, PermissionTypeEnum type, Integer parentId) {
        return baseMapper.selectByRoleIdAndTypeAndParentId(roleId, type, parentId);
    }

    @Override
    public List<MenuListChildrenVO> selectByRoleIdAndTypeAndParentIdChildren(Integer roleId, PermissionTypeEnum type, Integer parentId) {
        return baseMapper.selectByRoleIdAndTypeAndParentIdChildren(roleId, type, parentId);
    }

    @Override
    public List<PermissionVO> selectPermissionByParentId(Integer parentId) {
        Integer roleId = UserInfoContext.getRoleId();
        SysRole role = sysRoleMapper.selectById(roleId);
        if (role == null || !role.getStatus()) {
            return new ArrayList<>();
        }
        List<SysPermission> list = rolePermissionMapper.selectPermissionByRoleId(roleId);
        if (Collections.isEmpty(list)) {
            return new ArrayList<>();
        }
        List<Integer> permissionIds = list.parallelStream()
                .map(SysPermission::getId)
                .collect(toList());
        List<SysPermission> sysPermissionList = this.lambdaQuery()
                .in(SysPermission::getId, permissionIds)
                .list();
        List<PermissionVO> permissionList = getPermissionList(sysPermissionList, parentId);
        setChildren(sysPermissionList, permissionList);
        return permissionList;
    }

    @Override
    public List<NewMenuListVO> getMenuListByCache(Integer roleId) {
        List<SysPermission> sysPermissionList = this.selectAllByRoleIdAndType(roleId, MENU);

        List<NewMenuListVO> list = selectMenuListByCache(sysPermissionList, 0);
        setChildrenByCache(sysPermissionList, list);

        return list;
    }

    @Override
    public List<SysPermission> selectAllByRoleIdAndType(Integer roleId, PermissionTypeEnum type) {
        return baseMapper.selectAllByRoleIdAndType(roleId, type);
    }

    /**
     * 设置子类（缓存形式）
     *
     * @param sysPermissionList 权限列表
     * @param permissionList    权限菜单
     */
    private void setChildren(List<SysPermission> sysPermissionList, List<PermissionVO> permissionList) {
        permissionList.forEach(p -> {
            List<PermissionVO> list = getPermissionList(sysPermissionList, p.getId());
            p.setChildren(list);
            if (!list.isEmpty()) {
                setChildren(sysPermissionList, list);
            }
        });
    }

    /**
     * 根据parentId查询权限（缓存形式）
     *
     * @param sysPermissionList 权限列表
     * @param parentId          父类id
     * @return List<SysPermission>
     */
    private List<PermissionVO> getPermissionList(List<SysPermission> sysPermissionList, Integer parentId) {

        List<PermissionVO> list = new ArrayList<>();

        Optional.ofNullable(sysPermissionList).ifPresent(s -> s.forEach(v -> {
            if (parentId.equals(v.getParentId())) {
                PermissionVO permissionVO = new PermissionVO();
                permissionVO.setId(v.getId());
                permissionVO.setTitle(v.getDescription());
                permissionVO.setParentId(v.getParentId());
                list.add(permissionVO);
            }
        }));

        return list;
    }

    /**
     * 查询菜单
     *
     * @param sysPermissionList 权限列表
     * @param parentId          父类id
     * @return List<MenuListAllVO>
     */
    private List<NewMenuListVO> selectMenuListByCache(List<SysPermission> sysPermissionList, Integer parentId) {

        List<NewMenuListVO> list = new ArrayList<>();

        Optional.ofNullable(sysPermissionList).ifPresent(s -> s.forEach(v -> {
            if (parentId.equals(v.getParentId())) {
                NewMenuListVO menuListVO = new NewMenuListVO();
                menuListVO.setId(v.getId());
                menuListVO.setPath(v.getUrl());
                menuListVO.setName(v.getName());
                menuListVO.setRedirect(v.getRedirect());
                menuListVO.setComponent(v.getComponent());
                NewMenuListVO.MetaBean metaBean = new NewMenuListVO.MetaBean();
                metaBean.setTitle(v.getDescription());
                metaBean.setRequireAuth(v.isRequireAuth());
                metaBean.setIcon(v.getIcon());
                menuListVO.setMeta(metaBean);
                list.add(menuListVO);
            }
        }));

        return list;
    }

    /**
     * 设置子节点
     *
     * @param sysPermissionList 权限列表
     * @param menuListAllList   菜单
     */
    private void setChildrenByCache(List<SysPermission> sysPermissionList, List<NewMenuListVO> menuListAllList) {
        menuListAllList.forEach(p -> {
            List<NewMenuListVO> list = selectMenuListByCache(sysPermissionList, p.getId());
            p.setChildren(list);
            if (!list.isEmpty()) {
                setChildrenByCache(sysPermissionList, list);
            }
        });
    }

}
