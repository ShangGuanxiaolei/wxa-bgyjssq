package cn.ideamake.bgyjssq.service.permission.impl;

import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.util.Md5Utils;
import cn.ideamake.bgyjssq.dao.mapper.SysAdminMapper;
import cn.ideamake.bgyjssq.pojo.bo.Token;
import cn.ideamake.bgyjssq.pojo.entity.Project;
import cn.ideamake.bgyjssq.pojo.entity.SysAdmin;
import cn.ideamake.bgyjssq.pojo.entity.SysAdminProject;
import cn.ideamake.bgyjssq.pojo.query.AdminQuery;
import cn.ideamake.bgyjssq.pojo.query.QueryPage;
import cn.ideamake.bgyjssq.pojo.vo.PermissionVO;
import cn.ideamake.bgyjssq.pojo.vo.ProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.UserSessionVO;
import cn.ideamake.bgyjssq.pojo.vo.permission.AdminLoginVO;
import cn.ideamake.bgyjssq.pojo.vo.permission.SysAdminVO;
import cn.ideamake.bgyjssq.service.IProjectService;
import cn.ideamake.bgyjssq.service.IUserService;
import cn.ideamake.bgyjssq.service.permission.IRolePermissionService;
import cn.ideamake.bgyjssq.service.permission.ISysAdminProjectService;
import cn.ideamake.bgyjssq.service.permission.ISysAdminService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * <p>
 * 系统管理员列表 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@AllArgsConstructor
@Service
public class SysAdminServiceImpl extends ServiceImpl<SysAdminMapper, SysAdmin> implements ISysAdminService {

    private final IRolePermissionService rolePermissionService;

    private final IUserService userService;

    private final ISysAdminProjectService sysAdminProjectService;

    private final IProjectService projectService;

    /**
     * 管理状态，1表示启用
     */
    private final static Integer STATUS = 1;

    @Override
    public IPage<SysAdmin> findByRoleId(Integer roleId, QueryPage queryPage) {
        QueryWrapper<SysAdmin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("role_id", roleId);
        Page<SysAdmin> tPage = new Page<>(queryPage.getPage(), queryPage.getLimit());
        return this.page(tPage, queryWrapper);
    }

    @Override
    public List<PermissionVO> findPermission(Integer roleId) {
        return rolePermissionService.selectPermissionByRoleId(roleId);
    }

    @Override
    public IPage<SysAdminVO> findPage(AdminQuery adminQuery) {
        IPage<SysAdminVO> page = baseMapper.selectByPage(new Page<>(adminQuery.getPage(), adminQuery.getLimit()), adminQuery);
        List<SysAdminVO> records = page.getRecords();
        Optional.ofNullable(records).ifPresent(r -> r.forEach(s -> {
            List<SysAdminProject> list = sysAdminProjectService.lambdaQuery()
                    .eq(SysAdminProject::getSysAdminId, s.getId())
                    .list();
            List<ProjectVO> vos = new ArrayList<>();
            Optional.ofNullable(list).ifPresent(l -> l.forEach(v -> {
                Integer projectId = v.getProjectId();
                String projectName = Optional.ofNullable(projectService.lambdaQuery()
                        .eq(Project::getId, projectId)
                        .one())
                        .map(Project::getProjectTitle)
                        .orElse("");
                vos.add(new ProjectVO(projectId, projectName));
            }));
            s.setProjectVO(vos);
        }));
        return page;
    }

    @Override
    public Map<String, Object> login(AdminLoginVO admin, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        SysAdmin sysAdmin = lambdaQuery()
                .eq(SysAdmin::getStatus, STATUS)
                .eq(SysAdmin::getName, admin.getUsername()).one();
        Optional.ofNullable(sysAdmin).orElseThrow(() -> new BusinessException("用户不存在，或无权限访问后台"));
        try {
            if (Md5Utils.validPassword(admin.getPassword(), sysAdmin.getPassword())) {
                UserSessionVO userSession = new UserSessionVO();
                userSession.setRoleId(sysAdmin.getRoleId());
                userSession.setAdminId(sysAdmin.getId());
                Token token = userService.createToken(userSession, request);
                Map<String, Object> map = new HashMap<>(2);
                map.put("token", token);
                map.put("username", sysAdmin.getName());
                return map;
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new BusinessException("密码错误");
        }
        throw new BusinessException("密码错误");
    }

    @Override
    public String logout(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        //处理退出登录逻辑
        try {
            request.logout();
        } catch (ServletException e) {
            e.printStackTrace();
        }
        return "退出登录成功";
    }

}