package cn.ideamake.bgyjssq.service.permission.impl;

import cn.ideamake.bgyjssq.common.handler.UserInfoContext;
import cn.ideamake.bgyjssq.dao.mapper.SysAdminProjectMapper;
import cn.ideamake.bgyjssq.pojo.entity.SysAdminProject;
import cn.ideamake.bgyjssq.service.permission.ISysAdminProjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

/**
 * <p>
 * 系统账户项目权限表 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-10-21
 */
@Service
public class SysAdminProjectServiceImpl
        extends ServiceImpl<SysAdminProjectMapper, SysAdminProject>
        implements ISysAdminProjectService {

    @Override
    public List<Integer> getProjectList() {
        Integer adminId = UserInfoContext.getAdminId();
        List<Integer> list = new java.util.ArrayList<>(
                Optional.ofNullable(this.lambdaQuery()
                        .eq(SysAdminProject::getSysAdminId, adminId)
                        .list())
                        .map(s -> s.stream()
                                .map(SysAdminProject::getProjectId)
                                .collect(toList()))
                        .orElse(emptyList()));
        //加入系统项目进行适配
        list.add(-1);
        return list;
    }

}
