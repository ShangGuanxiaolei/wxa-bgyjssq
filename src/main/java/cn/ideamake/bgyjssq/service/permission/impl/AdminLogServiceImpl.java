package cn.ideamake.bgyjssq.service.permission.impl;

import cn.ideamake.bgyjssq.dao.mapper.AdminLogMapper;
import cn.ideamake.bgyjssq.pojo.entity.AdminLog;
import cn.ideamake.bgyjssq.pojo.query.AdminLogQuery;
import cn.ideamake.bgyjssq.pojo.vo.permission.AdminLogVO;
import cn.ideamake.bgyjssq.service.permission.IAdminLogService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 管理员操作记录 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Service
public class AdminLogServiceImpl extends ServiceImpl<AdminLogMapper, AdminLog> implements IAdminLogService {

    @Override
    public IPage<AdminLogVO> findPage(AdminLogQuery adminQuery) {
        return baseMapper.selectByPage(new Page<>(adminQuery.getPage(), adminQuery.getLimit()), adminQuery);
    }
}
