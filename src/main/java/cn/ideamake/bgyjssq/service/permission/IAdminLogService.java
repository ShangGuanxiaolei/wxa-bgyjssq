package cn.ideamake.bgyjssq.service.permission;

import cn.ideamake.bgyjssq.pojo.entity.AdminLog;
import cn.ideamake.bgyjssq.pojo.query.AdminLogQuery;
import cn.ideamake.bgyjssq.pojo.vo.permission.AdminLogVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 管理员操作记录 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
public interface IAdminLogService extends IService<AdminLog> {

    /**
     * 分页查询管理员操作日志列表
     *
     * @param adminQuery 查询参数
     * @return 分页数据
     */
    IPage<AdminLogVO> findPage(AdminLogQuery adminQuery);

}
