package cn.ideamake.bgyjssq.service.permission;

import cn.ideamake.bgyjssq.pojo.entity.SysRole;
import cn.ideamake.bgyjssq.pojo.query.QueryPage;
import cn.ideamake.bgyjssq.pojo.vo.permission.SysRoleVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 系统角色表，记录系统中的角色信息 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
public interface ISysRoleService extends IService<SysRole> {

    /**
     * 分页查询角色列表
     *
     * @param queryPage 分页
     * @param name      角色名称
     * @return 分页数据
     */
    IPage<SysRoleVO> findByPage(QueryPage queryPage, String name);

    /**
     * 禁用角色
     *
     * @param id 角色id
     * @return Boolean
     */
    Boolean prohibit(Integer id);

    /**
     * 查询有效角色
     *
     * @return 角色列表
     */
    List<SysRole> selectEffectiveRole();

}
