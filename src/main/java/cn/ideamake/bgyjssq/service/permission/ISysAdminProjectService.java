package cn.ideamake.bgyjssq.service.permission;

import cn.ideamake.bgyjssq.pojo.entity.SysAdminProject;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 系统账户项目权限表 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-10-21
 */
public interface ISysAdminProjectService extends IService<SysAdminProject> {

    /**
     * 获取项目列表
     *
     * @return List<Integer>
     */
    List<Integer> getProjectList();
}
