package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.entity.QrCodeVisitor;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 签到码签到客户 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-10
 */
public interface IQrCodeVisitorService extends IService<QrCodeVisitor> {

}
