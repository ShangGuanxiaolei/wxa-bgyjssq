package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.dto.RecommendCustomerDTO;
import cn.ideamake.bgyjssq.pojo.dto.RecordByClickLinkDTO;
import cn.ideamake.bgyjssq.pojo.dto.ShareSubmitDTO;
import cn.ideamake.bgyjssq.pojo.entity.Recommend;
import cn.ideamake.bgyjssq.pojo.query.QueryPage;
import cn.ideamake.bgyjssq.pojo.query.RecommendDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.RecommendQuery;
import cn.ideamake.bgyjssq.pojo.vo.BrainMappingVO;
import cn.ideamake.bgyjssq.pojo.vo.CustomerShareVO;
import cn.ideamake.bgyjssq.pojo.vo.RecommendCustomerVO;
import cn.ideamake.bgyjssq.pojo.vo.RecommendDetailPageVO;
import cn.ideamake.bgyjssq.pojo.vo.RecommendPageVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 推荐记录管理 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-06-06
 */
public interface IRecommendService extends IService<Recommend> {
    /**
     * 查询推荐列表
     *
     * @param myPage         myPage
     * @param recommendQuery recommendQuery
     * @return IPage<RecommendPageVO>
     */
    IPage<RecommendPageVO> selectRecommendVO(Page<RecommendPageVO> myPage,
                                             RecommendQuery recommendQuery);

    /**
     * 查询推荐详情
     *
     * @param myPage               myPage
     * @param recommendDetailQuery recommendDetailQuery
     * @return IPage<RecommendDetailPageVO>
     */
    IPage<RecommendDetailPageVO> selectRecommendDetailVO(Page<RecommendDetailPageVO> myPage,
                                                         RecommendDetailQuery recommendDetailQuery);

    /**
     * 获取访客推介次数
     *
     * @param uuid 访客id
     * @return 推介次数
     */
    Integer getRecommendCountByVisitorUuid(String uuid);

    /**
     * 用户(销售或访客)分享资源给其他用户
     *
     * @param shareSubmitDTO
     * @return
     */
    Integer shareResourceByUser(ShareSubmitDTO shareSubmitDTO);

    /**
     * 用户拓客记录
     */
    boolean recommendRecord(RecommendCustomerDTO joinByShareDTO);

    /**
     * 用户通过点击分享链接查看分享资源
     */
    boolean clickInByShareLink(RecordByClickLinkDTO joinByShareDTO);


    /**
     * 删除访客分享行为记录和推荐行为记录,暂时用于用户切换身份时数据的清除
     */
    boolean deleteVisitorShareRecord(String visitorId);

    /**
     * 查询推荐客户列表
     *
     * @param page     分页
     * @param sourceId 推荐客户id
     * @return IPage<RecommendCustomerVO>
     */
    IPage<RecommendCustomerVO> selectRecommendCustomer(QueryPage page, String sourceId);

    /**
     * 查询客户分享数据
     *
     * @param page     分页
     * @param sourceId 客户id
     * @return IPage<CustomerShareVO>
     */
    IPage<CustomerShareVO> selectCustomerShare(QueryPage page, @Param("sourceId") String sourceId);

    /**
     * 获取脑图结构
     *
     * @param sourceId 客户id
     * @param projectId
     * @return List<BrainMappingVO>
     */
    List<BrainMappingVO> getBrainMapping(String sourceId, Integer projectId);
}
