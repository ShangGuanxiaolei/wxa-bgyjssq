package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.entity.UserLog;
import cn.ideamake.bgyjssq.pojo.query.user.UserLogQuery;
import cn.ideamake.bgyjssq.pojo.vo.user.UserLogVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
public interface IUserLogService extends IService<UserLog> {

    /**
     * 销售获取日志
     *
     * @param query 查询条件
     * @return IPage<UserLogVO>
     */
    IPage<UserLogVO> getUserLog(UserLogQuery query);
}
