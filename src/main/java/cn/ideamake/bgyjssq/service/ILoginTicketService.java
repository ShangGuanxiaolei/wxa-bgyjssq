package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.entity.LoginTicket;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 登录凭证 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-25
 */
public interface ILoginTicketService extends IService<LoginTicket> {

}
