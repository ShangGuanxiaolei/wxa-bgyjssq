package cn.ideamake.bgyjssq.service;

import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.ideamake.bgyjssq.pojo.bo.Token;
import cn.ideamake.bgyjssq.pojo.bo.WXUserInfo;
import cn.ideamake.bgyjssq.pojo.dto.BussinessCardHomeEditDTO;
import cn.ideamake.bgyjssq.pojo.dto.InviteCodeGenDTO;
import cn.ideamake.bgyjssq.pojo.dto.ProjectQRCodeDTO;
import cn.ideamake.bgyjssq.pojo.dto.ScanInviteCodeDTO;
import cn.ideamake.bgyjssq.pojo.dto.UserEditDTO;
import cn.ideamake.bgyjssq.pojo.dto.UserSaveDTO;
import cn.ideamake.bgyjssq.pojo.dto.VisitorSubmitPhoneDTO;
import cn.ideamake.bgyjssq.pojo.entity.User;
import cn.ideamake.bgyjssq.pojo.query.BusinessQRCodeQuery;
import cn.ideamake.bgyjssq.pojo.query.CustomerManagerQuery;
import cn.ideamake.bgyjssq.pojo.query.DigDataQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffKpiQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffListQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffVisitorQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffVisitorRankQuery;
import cn.ideamake.bgyjssq.pojo.query.UserProjectQuery;
import cn.ideamake.bgyjssq.pojo.query.UserVisitorProjectQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorInfoDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorTrendQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorVisitAnalysisQuery;
import cn.ideamake.bgyjssq.pojo.vo.BusinessQRcodeVO;
import cn.ideamake.bgyjssq.pojo.vo.BussinessCardHomeVO;
import cn.ideamake.bgyjssq.pojo.vo.CustomerManagerVO;
import cn.ideamake.bgyjssq.pojo.vo.DigDataVO;
import cn.ideamake.bgyjssq.pojo.vo.InviteRegisterVO;
import cn.ideamake.bgyjssq.pojo.vo.ProjectQRCodeVO;
import cn.ideamake.bgyjssq.pojo.vo.ProjectSellerInfoVO;
import cn.ideamake.bgyjssq.pojo.vo.PushedActivityVO;
import cn.ideamake.bgyjssq.pojo.vo.SellerActivitisVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffKpiVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffListVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffNameVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffVisitorRankVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffVisitorVO;
import cn.ideamake.bgyjssq.pojo.vo.UserDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.UserProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.UserSessionVO;
import cn.ideamake.bgyjssq.pojo.vo.UserVisitorProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorInfoDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorTrendVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorVisitAnalysisVO;
import cn.ideamake.bgyjssq.pojo.vo.analysis.DataAnalysisDetailListVO;
import cn.ideamake.bgyjssq.pojo.vo.analysis.DataAnalysisDetailVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
public interface IUserService extends IService<User> {

    /**
     * 获取邀请项目码
     *
     * @param inviteCodeGenDTO
     * @return
     */
    InviteRegisterVO getInviteRegisterCode(InviteCodeGenDTO inviteCodeGenDTO);

    /**
     * 邀请注册扫描验证
     *
     * @param scanInviteCodeDTO scanInviteCodeDTO
     * @return boolean
     */
    boolean scanInviteRegisterCode(ScanInviteCodeDTO scanInviteCodeDTO);

    /**
     * 邀请注册扫描验证后门
     *
     * @param inviteId 邀请id
     * @return boolean
     */
    boolean scanInviteRegisterCodeBackDoor(Integer inviteId);

    /**
     * 通过id删除销售所有信息
     *
     * @param sellerId
     * @return boolean
     */
    boolean removeSellerById(String sellerId);

    /**
     * 通过id删除访客所有信息
     *
     * @param sellerId
     * @return
     */
    boolean removeVisitorById(String sellerId);

    /**
     * 访客上报手机号
     *
     * @param visitorSubmitPhoneDTO
     * @return
     */
    boolean visitorSubmitPhone(VisitorSubmitPhoneDTO visitorSubmitPhoneDTO);

    /**
     * 获取员工综合详情
     *
     * @param userId
     * @return
     */
    UserDetailVO getUserInfo(String userId);

    /**
     * 获取员工首页
     *
     * @param userId
     * @return
     */
    BussinessCardHomeVO getBusinessCardHome(String userId);

    /**
     * 更新员工信息
     *
     * @param bc
     * @return
     */
    BussinessCardHomeVO updateBusinessCardHome(BussinessCardHomeEditDTO bc);

    /**
     * 通过微信openid获取用户信息
     *
     * @param result  微信用户信息
     * @param request request
     * @param origin  来源
     * @return Token
     */
    Token initLoginByCode(WxMaJscode2SessionResult result, HttpServletRequest request, Integer origin);

    /**
     * 通过微信openId获取用户信息
     *
     * @param wxMpUser 微信用户信息
     * @param request  request
     * @param origin   来源
     * @return UserSessionVO
     */
    Token initLoginByAccessToken(WxMpUser wxMpUser, HttpServletRequest request, Integer origin);


    /**
     * 通过微信code码登录用户，并添加用户
     *
     * @param wxUserInfo
     * @return
     */

    boolean saveWxUserInfo(WXUserInfo wxUserInfo);

    /**
     * 手工添加用户信息，需要处理user和user_info两张表信息
     *
     * @param userSaveDTO
     * @return
     */
    boolean saveUser(UserSaveDTO userSaveDTO);

    /**
     * 通过用户id获取小程序码
     *
     * @param query
     * @return
     */
    BusinessQRcodeVO getBusinessQrCode(BusinessQRCodeQuery query);

    /**
     * 通过用户id和项目id获取小程序码
     *
     * @param dto dto
     * @return ProjectQRCodeVO
     */
    ProjectQRCodeVO getProjectQrCode(ProjectQRCodeDTO dto);

    /**
     * 获取vr720海报
     *
     * @param dto dto
     * @return ProjectQRCodeVO
     */
    ProjectQRCodeVO getVrQrCode(ProjectQRCodeDTO dto);

    /**
     * 获取项目简介海报
     *
     * @param dto dto
     * @return ProjectQRCodeVO
     */
    ProjectQRCodeVO getProfileQrCode(ProjectQRCodeDTO dto);

    /**
     * 员工修改自身信息
     *
     * @param userEditDTO
     * @return
     */
    boolean updateUserInfo(UserEditDTO userEditDTO);

    /**
     * 获取员工项目
     *
     * @param userProjectQuery
     * @return
     */
    IPage<UserProjectVO> getStaffProject(UserProjectQuery userProjectQuery);

    /**
     * 员工获取访客详情
     *
     * @param visitorInfoDetailQuery
     * @param visitorInfoDetailQuery 查询条件
     * @return VisitorInfoDetailVO
     */
    VisitorInfoDetailVO getVisitorInfoDetail(VisitorInfoDetailQuery visitorInfoDetailQuery);

    /**
     * 高级员工获取访客详情
     *
     * @param query 查询条件
     * @return VisitorInfoDetailVO
     */
    VisitorInfoDetailVO getVisitorInfoDetailForLeader(VisitorInfoDetailQuery query);

    /**
     * 获取员工访客统计信息
     *
     * @param staffVisitorQuery
     * @return
     */
    IPage<StaffVisitorVO> getStaffVisitorByFilter(StaffVisitorQuery staffVisitorQuery);

    /**
     * 员工获取访客动态信息
     *
     * @param visitorTrendQuery visitorTrendQuery
     * @return IPage<VisitorTrendVO>
     */
    IPage<VisitorTrendVO> getVisitorTrend(VisitorTrendQuery visitorTrendQuery);

    /**
     * 员工获取访客排名查询
     *
     * @param staffVisitorRankQuery
     * @return
     */
    IPage<StaffVisitorRankVO> getStaffVisitorRank(StaffVisitorRankQuery staffVisitorRankQuery);

    /**
     * 员工获取访客分析数据
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @return VisitorVisitAnalysisVO
     */
    VisitorVisitAnalysisVO getVisitorVisitAnalysis(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery);

    /**
     * 员工获取访客分析钻取数据
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @return Object
     */
    DataAnalysisDetailVO getVisitorList(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery);

    /**
     * 获取访客某项目对应销售的id
     *
     * @param userVisitorProjectQuery
     * @return
     */
    UserVisitorProjectVO getVisitorProjectUserInfo(UserVisitorProjectQuery userVisitorProjectQuery);

    /**
     * 获取访客首页的项目销售楼书等信息
     *
     * @param projectId
     * @return
     */
    ProjectSellerInfoVO getProjectSellerInfo(Integer projectId);

    /**
     * 创建用户认证token
     *
     * @param userSessionVO
     * @param request
     * @return
     */
    Token createToken(UserSessionVO userSessionVO, HttpServletRequest request);

    /**
     * token过期时刷新token
     *
     * @param refreshToken
     * @param request
     * @return
     */
    Token refreshToken(String refreshToken, HttpServletRequest request);


    List<CustomerManagerVO> customerListQuery(CustomerManagerQuery query);

    /**
     * 小程序-通过id获取项目名称
     *
     * @param id id列表
     * @return 项目名称
     */
    List<String> getStaffProjectNameListByInviteId(Integer id);

    /**
     * PC后台-员工列表
     *
     * @param query 查询参数
     * @return 列表
     */
    IPage<StaffListVO> getStaffInfoList(StaffListQuery query);

    /**
     * 员工推广业绩top10
     *
     * @param query 参数
     * @return top10 列表
     */
    List<StaffKpiVO> selectStaffTopTenList(StaffKpiQuery query);

    /**
     * 员工推广业绩top10
     *
     * @param query 参数
     * @return top10 列表
     */
    IPage<DigDataVO> selectStaffDigData(DigDataQuery query);

    /**
     * 批量导入员工数据
     *
     * @param file Excel 文件
     * @return 是否导入成功(事务)
     */
    boolean importStaff(MultipartFile file);

    /**
     * 查询员工业绩列表
     *
     * @param query 参数
     * @return 列表
     */
    IPage<StaffKpiVO> selectStaffKpiList(StaffKpiQuery query);

    /**
     * 查询销售列表
     *
     * @param query 查询条件
     * @return List<DataAnalysisDetailListVO>
     */
    List<DataAnalysisDetailListVO> getSellerDetailList(VisitorVisitAnalysisQuery query);

    /**
     * 查询访客列表
     *
     * @param query 查询条件
     * @return List<VisitorListVO>
     */
    DataAnalysisDetailVO getVisitorDetailList(VisitorVisitAnalysisQuery query);

    /**
     * 查询员工的活动
     *
     * @param sellerId 参数 销售的id
     * @return 列表
     */
    List<SellerActivitisVO> getSellerActivitiesList(String sellerId);

    /**
     * 得到可以推送的员工
     *
     * @param projectId 参数
     * @return 列表
     */
    List<StaffNameVO> getSellerPushStaff(Integer projectId);

    /**
     * 得到被推送的活动
     *
     * @param userUuid 参数
     * @return 列表
     */
    List<PushedActivityVO> getPushActivity(String userUuid);

    /**
     * 循环依赖暂时加在这
     *
     * @param groupId
     * @return
     */
    boolean checkDeletedGroupIncludeProject(Integer groupId);
}
