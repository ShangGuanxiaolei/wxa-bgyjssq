package cn.ideamake.bgyjssq.service.common.impl;

import cn.ideamake.bgyjssq.common.components.LogService;
import cn.ideamake.bgyjssq.common.handler.UserInfoContext;
import cn.ideamake.bgyjssq.common.util.ContextHolderUtils;
import cn.ideamake.bgyjssq.common.util.IpAddress;
import cn.ideamake.bgyjssq.dao.mapper.SysAdminMapper;
import cn.ideamake.bgyjssq.dao.mapper.SysRoleMapper;
import cn.ideamake.bgyjssq.dao.mapper.UserMapper;
import cn.ideamake.bgyjssq.pojo.entity.AdminLog;
import cn.ideamake.bgyjssq.pojo.entity.Project;
import cn.ideamake.bgyjssq.pojo.entity.SysAdmin;
import cn.ideamake.bgyjssq.pojo.entity.SysRole;
import cn.ideamake.bgyjssq.pojo.entity.User;
import cn.ideamake.bgyjssq.pojo.entity.Visitor;
import cn.ideamake.bgyjssq.service.IVisitorService;
import cn.ideamake.bgyjssq.service.common.ChangeLogService;
import cn.ideamake.bgyjssq.service.common.SaveAdminLogService;
import cn.ideamake.bgyjssq.service.common.SaveUserLogService;
import cn.ideamake.bgyjssq.service.permission.ISysAdminService;
import cn.ideamake.bgyjssq.service.permission.ISysRoleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.net.UnknownHostException;
import java.util.Optional;

/**
 * @author gxl
 * @description 保存日志接口
 * @date 2019/09/11
 */
@AllArgsConstructor
@Service
@Slf4j
public class SaveAdminLogServiceImpl implements SaveAdminLogService {

    private final LogService logService;

    private final SysAdminMapper adminMapper;

    private final SysRoleMapper roleMapper;


    @Override
    public void saveSystemLog(String content) {
        AdminLog adminLog = new AdminLog();
        Integer roleId = UserInfoContext.getRoleId();
        Integer adminId = UserInfoContext.getAdminId();
        SysRole role = roleMapper.selectById(roleId);
        SysAdmin admin = adminMapper.selectById(adminId);
        if (role != null && admin != null) {
            adminLog.setRoleId(roleId.toString());
            adminLog.setRoleName(role.getName());
            adminLog.setPhone(admin.getPhone());
            adminLog.setAdminId(adminId);
            adminLog.setAdminName(admin.getName());
            HttpServletRequest request = ContextHolderUtils.getRequest();
            adminLog.setIpAddress(IpAddress.getClientIpAddress(request));
            String str = "系统管理员" + admin.getName() + content;
            adminLog.setOperatorContent(str);
            logService.recordSystemUserLog(adminLog);
        }
    }
}
