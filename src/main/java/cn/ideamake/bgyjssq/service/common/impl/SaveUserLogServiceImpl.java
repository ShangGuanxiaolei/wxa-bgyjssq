package cn.ideamake.bgyjssq.service.common.impl;

import cn.ideamake.bgyjssq.common.components.LogService;
import cn.ideamake.bgyjssq.dao.mapper.UserMapper;
import cn.ideamake.bgyjssq.pojo.entity.Project;
import cn.ideamake.bgyjssq.pojo.entity.User;
import cn.ideamake.bgyjssq.pojo.entity.Visitor;
import cn.ideamake.bgyjssq.service.IVisitorService;
import cn.ideamake.bgyjssq.service.common.ChangeLogService;
import cn.ideamake.bgyjssq.service.common.SaveUserLogService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author gxl
 * @description 保存日志接口
 * @date 2019/09/11
 */
@AllArgsConstructor
@Service
@Slf4j
public class SaveUserLogServiceImpl implements SaveUserLogService {

    private final IVisitorService visitorService;

    private final LogService logService;

    private final UserMapper userMapper;

    @Override
    public void saveChangeBindLog(String sellerId,
                                  String visitorId,
                                  Project project,
                                  ChangeLogService changeLogService) {
        saveUserLog(sellerId, visitorId, project, changeLogService, true);
    }


    @Override
    public void saveChangeBindLog(String sellerId,
                                  String visitorId,
                                  Project project,
                                  ChangeLogService changeLogService,
                                  boolean flag) {
        saveUserLog(sellerId, visitorId, project, changeLogService, flag);
    }

    /**
     * 保存日志，统一操作
     *
     * @param sellerId         销售id
     * @param visitorId        客户id
     * @param project          项目
     * @param changeLogService 日志内容
     * @param flag             特殊标识
     */
    private void saveUserLog(String sellerId,
                             String visitorId,
                             Project project,
                             ChangeLogService changeLogService,
                             boolean flag) {
        Visitor visitor = visitorService.lambdaQuery()
                .eq(Visitor::getUuid, visitorId)
                .one();
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(User::getUuid, sellerId);
        User user = userMapper.selectOne(queryWrapper);
        Optional.ofNullable(visitor).map(v -> Optional.ofNullable(user)
                .map(u -> {
                    String accept = changeLogService.accept(v, u, project);
                    logService.recordUserLog(sellerId, accept, flag);
                    return u;
                }).orElseGet(() -> {
                    log.error("客户信息不存在, visitorId:{}", visitorId);
                    return null;
                })
        ).orElseGet(() -> {
            log.error("员工信息不存在，userId:{}", sellerId);
            return null;
        });
    }
}
