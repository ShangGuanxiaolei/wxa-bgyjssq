package cn.ideamake.bgyjssq.service.common;

/**
 * @author gxl
 * @description 保存系统日志接口
 * @date 2019/09/11
 */
public interface SaveAdminLogService {

    /**
     * 保存系统管理员操作日志
     *
     * @param content 操作内容
     */
    void saveSystemLog(String content);
}
