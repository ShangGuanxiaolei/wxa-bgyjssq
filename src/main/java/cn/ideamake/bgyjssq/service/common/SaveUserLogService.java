package cn.ideamake.bgyjssq.service.common;

import cn.ideamake.bgyjssq.pojo.entity.Project;

/**
 * @author gxl
 * @description 保存日志接口
 * @date 2019/09/11
 */
public interface SaveUserLogService {

    /**
     * 保存转办日志
     *
     * @param sellerId         员工id
     * @param visitorId        客户id
     * @param project          项目
     * @param changeLogService 函数式接口
     */
    void saveChangeBindLog(String sellerId, String visitorId, Project project, ChangeLogService changeLogService);

    /**
     * 保存日志，重载方法，用于判断是否标红显示日志
     *
     * @param sellerId         员工id
     * @param visitorId        客户id
     * @param project          项目
     * @param changeLogService 函数式接口
     * @param flag             标红显示
     */
    void saveChangeBindLog(String sellerId,
                           String visitorId,
                           Project project,
                           ChangeLogService changeLogService,
                           boolean flag);
}
