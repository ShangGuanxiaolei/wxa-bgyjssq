package cn.ideamake.bgyjssq.service.common;

import cn.ideamake.bgyjssq.pojo.entity.Project;
import cn.ideamake.bgyjssq.pojo.entity.User;
import cn.ideamake.bgyjssq.pojo.entity.Visitor;

/**
 * @author gxl
 * @description 转办日志函数式接口
 * @date 2019/09/10
 */
@FunctionalInterface
public interface ChangeLogService {

    /**
     * 获取日志内容
     *
     * @param visitor 访客信息
     * @param user    用户信息
     * @param project 项目信息
     * @return java.lang.String
     */
    String accept(Visitor visitor, User user, Project project);
}
