package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.entity.VisitorLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户日志记录表 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-06-20
 */
public interface IVisitorLogService extends IService<VisitorLog> {

}
