package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.bo.VisitorActivityStatistics;
import cn.ideamake.bgyjssq.pojo.dto.SaveOrChangeBindDTO;
import cn.ideamake.bgyjssq.pojo.entity.BindingManager;
import cn.ideamake.bgyjssq.pojo.query.CustomerListQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorAttentionQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorHistoryBindQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorProjectQuery;
import cn.ideamake.bgyjssq.pojo.vo.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 员工的访客管理 服务类
 * </p>
 * 暂定service层和mapper层数据机构一致，可根据后续逻辑调整修改
 *
 * @author ideamake
 * @since 2019-06-12
 */
public interface IBindingManagerService extends IService<BindingManager> {

    /**
     * 查询访客历史绑定信息
     *
     * @param myPage
     * @param visitorHistoryBindQuery
     * @return
     */
    IPage<VisitorHistoryBindVO> selectVisitorHistoryBind(Page<VisitorHistoryBindVO> myPage, VisitorHistoryBindQuery visitorHistoryBindQuery);

    /**
     * 查询访客我的关注
     *
     * @param myPage
     * @param visitorAttentionQuery
     * @return
     */
    IPage<VisitorAttentionVO> selectVisitorAttention(Page<VisitorAttentionVO> myPage, VisitorAttentionQuery visitorAttentionQuery);

    /**
     * 查询访客我的关注
     *
     * @param visitorProjectQuery
     * @return
     */
    IPage<VisitorProjectVO> selectConditionAttention(VisitorProjectQuery visitorProjectQuery);

    /**
     * 添加或者更换绑定
     *
     * @param saveOrChangeBindDTO
     * @return
     */
    boolean saveOrChangeBinding(SaveOrChangeBindDTO saveOrChangeBindDTO);

    /**
     * 员工访问客户主页, 获取动态信息
     *
     * @param visitorUuid 客户id
     * @param sellerId  销售ID
     * @return 访客动态信息列表
     */
    List<VisitorActivityStatistics> getVisitorDynamicInfoList(String visitorUuid, String sellerId);

    /**
     * 高级员工访问客户主页, 获取动态信息
     *
     * @param visitorUuid  客户id
     * @param sellerIdList 销售ID列表
     * @return 访客动态信息列表
     */
    List<VisitorActivityStatistics> getVisitorDynamicInfoListForLeader(String visitorUuid, List<String> sellerIdList);

    /**
     * 客户列表查询
     * @param query  查询参数
     * @return 返回客户列表
     */
    IPage<CustomerListVO> customerListQuery(CustomerListQuery query);

    /**
     * 根据访客id-销售id-项目id,在bm表中获取一条唯一的记录.用做前台展示(PC后台)
     * @param id 客户id(访客)
     * @param projectId 项目id
     * @param sellerId 销售id
     * @return 客户详情
     */
    CustomerInfoDetailVO getCustomerInfoDetailById(String id, String projectId, String sellerId);
}
