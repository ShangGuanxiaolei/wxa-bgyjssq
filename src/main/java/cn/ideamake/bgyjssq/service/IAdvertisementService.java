package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.dto.AdvertisementDTO;
import cn.ideamake.bgyjssq.pojo.entity.Advertisement;
import cn.ideamake.bgyjssq.pojo.query.AdvertisementQuery;
import cn.ideamake.bgyjssq.pojo.vo.AdvertisementVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-08-07
 */
public interface IAdvertisementService extends IService<Advertisement> {
    /**
     * 添加广告
     *
     * @param dto dto
     * @return boolean
     */
    boolean addAdvertisement(AdvertisementDTO dto);

    /**
     * 编辑广告
     *
     * @param dto dto
     * @return boolean
     */
    boolean editAdvertisement(AdvertisementDTO dto);

    /**
     * 展示广告页面
     *
     * @param advertisementQuery advertisementQuery
     * @return IPage<AdvertisementVO>
     */
    IPage<AdvertisementVO> selectAdver(AdvertisementQuery advertisementQuery);

    /**
     * 删除广告，可批量删除
     *
     * @param ids ids
     * @return Boolean
     */
    Boolean deleteAdver(List<String> ids);

    /**
     * 根据项目id查询有效广告
     *
     * @param projectId 项目id
     * @return AdvertisementVO
     */
    AdvertisementVO selectEffectiveAdver(Integer projectId);
}
