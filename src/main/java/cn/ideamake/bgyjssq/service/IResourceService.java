package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.common.enums.DictType;
import cn.ideamake.bgyjssq.common.enums.ResourceType;
import cn.ideamake.bgyjssq.pojo.dto.PublishResourceDTO;
import cn.ideamake.bgyjssq.pojo.dto.ResourceBulkOperation;
import cn.ideamake.bgyjssq.pojo.entity.Resource;
import cn.ideamake.bgyjssq.pojo.query.QueryPage;
import cn.ideamake.bgyjssq.pojo.query.ResourceListQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffNotificationDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffNotificationQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffNotifyListQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorNotificationQuery;
import cn.ideamake.bgyjssq.pojo.vo.ArticleDetailVisitorListVO;
import cn.ideamake.bgyjssq.pojo.vo.NotifyDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.ResourceBaseVO;
import cn.ideamake.bgyjssq.pojo.vo.ResourceDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.ResourceDigDataQuery;
import cn.ideamake.bgyjssq.pojo.vo.ResourceDigDataVO;
import cn.ideamake.bgyjssq.pojo.vo.ResourceListVO;
import cn.ideamake.bgyjssq.pojo.vo.UserRoleListVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.StaffNotificationResourceVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.StaffNotificationVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.VisitorNotificationOldVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.VisitorNotificationVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
public interface IResourceService extends IService<Resource> {

    /**
     * 查询资源列表对象
     *
     * @param query        参数
     * @param resourceType 资源类型
     * @param dictType     字典类型(组id)
     * @return 列表对象
     */
    IPage<ResourceListVO> selectResourceList(ResourceListQuery query, ResourceType resourceType, DictType dictType);

    /**
     * 通过项目id获取资源基本信息
     *
     * @param query
     * @param resourceType
     * @param projectId
     * @return
     */
    IPage<ResourceBaseVO> selectResourcesByProjectId(QueryPage query, Integer resourceType, Integer projectId, Integer grouping);

    /**
     * 发布(更新)资源, 携带id为更新
     *
     * @param resourceDTO  资源内容
     * @param resourceType 资源类型 {@link ResourceType}
     * @return 是否成功
     */
    boolean publishResource(PublishResourceDTO resourceDTO, ResourceType resourceType);

    /**
     * 设置发布状态
     *
     * @param operation 参数
     * @return 是否成功
     */
    boolean editReleaseStatus(ResourceBulkOperation operation);

    /**
     * 设置置顶
     *
     * @param operation 参数
     * @return 是否成功
     */
    boolean topping(ResourceBulkOperation operation);

    /**
     * 设置文章分组
     *
     * @param operation 参数
     * @return 是否成功
     */
    boolean editArticleGroup(ResourceBulkOperation operation);

    /**
     * 查询员工通知(type=2,海报,type=1,文章)
     *
     * @param query 查询参数
     * @return 分页信息
     */
    StaffNotificationVO getStaffNotification(StaffNotificationQuery query);


    /**
     * 通过组id和过滤条件查询组下的资源的信息(type=2,海报,type=1,文章)
     *
     * @param query 查询参数
     * @return 分页信息
     */
    IPage<StaffNotificationResourceVO> getResourcesByGroupId(StaffNotificationQuery query);

    /**
     * 获取访客通知
     *
     * @param vnf VisitorNotificationQuery
     * @return IPage<VisitorNotificationOldVO>
     */
    IPage<VisitorNotificationOldVO> getVisitorNotification(VisitorNotificationQuery vnf);

    /**
     * 获取访客通知
     *
     * @param vnf VisitorNotificationQuery
     * @return IPage<VisitorNotificationVO>
     */
    VisitorNotificationVO getVisitorNotificationNew(VisitorNotificationQuery vnf);

    /**
     * 根据资源id及用户id查询资源详情
     *
     * @param query 分页
     * @return 详情VO
     */
    ArticleDetailVisitorListVO selectArticleDetailByArticleId(StaffNotificationDetailQuery query);

    /**
     * 根据资源id及用户id查询资源详情
     *
     * @param query 分页
     * @return 详情VO
     */
    List<UserRoleListVO> selectResourceDetail(StaffNotificationDetailQuery query);

    /**
     * 根据资源id及用户id查询资源详情
     *
     * @param query 分页
     * @return 详情VO
     */
    List<UserRoleListVO> getUserGroup(StaffNotificationDetailQuery query);

    /**
     * 查询员工列表数据
     *
     * @param query 查询条件
     * @return List<UserRoleListVO>
     */
    List<UserRoleListVO> getUserList(StaffNotifyListQuery query);

    /**
     * 根据资源类型和资源id查找资源
     *
     * @param id           唯一ID
     * @param resourceType 资源类型
     * @return 资源
     */
    ResourceDetailVO getResourceByTypeAndId(Integer id, ResourceType resourceType);

    /**
     * 根据资源类型和资源id查找资源
     *
     * @param projectId    项目ID
     * @param resourceType 资源类型
     * @return 资源
     */
    ResourceDetailVO getResourceByTypeAndProjectId(Integer projectId, ResourceType resourceType);

    /**
     * 批量更新资源所在的组id
     *
     * @param integers
     * @param groupId
     * @return
     */
    boolean updateGroupIdByResourceIdsBatch(List<Integer> integers, Integer groupId);

    /**
     * 校验删除的组中是否有资源归属，有则删除失败
     *
     * @param groupId
     * @return
     */
    boolean verifyResourceInDeleteGroup(Integer groupId);

    /**
     * pc后台资源数据钻取
     *
     * @param query
     * @return
     */
    IPage<ResourceDigDataVO> digResourceBrowseData(ResourceDigDataQuery query);

    /**
     * 访客查询资源
     *
     * @param query 查询条件
     * @return IPage<VisitorNotificationResourceVO>
     */
    VisitorNotificationVO getVisitorNotificationResource(VisitorNotificationQuery query);

    /**
     * 获取高等级角色数据
     *
     * @param notifyDetailVO 返回对象
     * @param query          查询条件
     */
    void getHigherSellerData(NotifyDetailVO notifyDetailVO, StaffNotificationDetailQuery query);

}
