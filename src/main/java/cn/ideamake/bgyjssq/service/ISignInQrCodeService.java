package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.dto.qrcode.SignInQrCodeDTO;
import cn.ideamake.bgyjssq.pojo.entity.Project;
import cn.ideamake.bgyjssq.pojo.entity.SignInQrCode;
import cn.ideamake.bgyjssq.pojo.entity.User;
import cn.ideamake.bgyjssq.pojo.query.ActivityDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.QrCodeQuery;
import cn.ideamake.bgyjssq.pojo.query.SignInQuery;
import cn.ideamake.bgyjssq.pojo.vo.ExpireVO;
import cn.ideamake.bgyjssq.pojo.vo.PreSignInVO;
import cn.ideamake.bgyjssq.pojo.vo.QrCodeVO;
import cn.ideamake.bgyjssq.pojo.vo.SignInReturnVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffVisitorInProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.qrcode.SignInQrCodeVO;
import cn.ideamake.bgyjssq.service.impl.SignInQrCodeServiceImpl;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.time.LocalDateTime;

/**
 * <p>
 * 签到二维码 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-10
 */
public interface ISignInQrCodeService extends IService<SignInQrCode> {

    /**
     * 保存签到二维码
     *
     * @param dto 数据对象
     * @return boolean
     */
    boolean saveAndUpdate(SignInQrCodeDTO dto);

    /**
     * 分页查询签到码列表
     *
     * @param qrCodeQuery 查询条件封装
     * @return List<>
     */
    IPage<QrCodeVO> findPage(QrCodeQuery qrCodeQuery);

    /**
     * 根据id查询签到码对象
     *
     * @param id 签到码id
     * @return SignInQrCodeVO
     */
    SignInQrCodeVO getActivity(Integer id);

    /**
     * 查询二维码是否过期
     *
     * @param id 活动ID
     * @return List<>
     */
    Boolean isExpire(Integer id);

    /**
     * 更新过期二维码
     *
     * @param id 更新信息
     * @return List<>
     */
    String updateQr(Integer id);

    /**
     * 查看特定二维码中客户信息
     *
     * @param query 查询条件
     * @return IPage<StaffVisitorInProjectVO>
     */
    IPage<StaffVisitorInProjectVO> getActivitiesDetail(ActivityDetailQuery query);

    /**
     * 查询活动累计人数
     *
     * @param id 项目id
     * @return 人数
     */
    Integer getActivitiesCount(Integer id);

    /**
     * 获取当日签到总数
     *
     * @param id 活动id
     * @return Integer
     */
    Integer getActivitiesDayCount(Integer id);

    /**
     * 查询訪客今日簽到次數
     *
     * @param id   项目id
     * @param uuid visitUuid
     * @param date 日期
     * @return 人数
     */
    Integer getVisitTodayCount(Integer id, String uuid, String date);

    /**
     * 查询訪客今日簽到次數
     *
     * @param id   项目id
     * @param uuid visitUuid
     * @return 人数
     */
    Integer getVisitCount(Integer id, String uuid);

    /**
     * 查询二维码是否失效
     *
     * @param query 查询条件
     * @return Integer
     */
    Integer getQrExpire(ExpireVO query);

    /**
     * 签到预信息
     *
     * @param id id
     * @return PreSignInVO
     */
    PreSignInVO getPreMessage(Integer id);

    /**
     * 客户签到
     *
     * @param query 签到参数
     * @return SignInReturnVO
     */
    SignInReturnVO signIn(SignInQuery query);

    /**
     * 保存签到数据
     *
     * @param signInQrCode   签到码信息
     * @param signInReturnVO 返回对象
     * @param project        项目
     * @param user           员工
     * @param uuid           用户id
     * @param now            当前时间
     * @param today          今天
     * @param signInParams   签到参数
     * @return Rest
     */
    SignInReturnVO getSaveResult(
            SignInQrCode signInQrCode,
            SignInReturnVO signInReturnVO,
            Project project,
            User user,
            String uuid,
            LocalDateTime now,
            String today, SignInQrCodeServiceImpl.SignInParams signInParams);

    /**
     * 查询登录天数
     *
     * @param visitorId 访客id
     * @return Integer
     */
    Integer selectTotalDays(String visitorId);

}
