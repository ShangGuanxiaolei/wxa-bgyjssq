package cn.ideamake.bgyjssq.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.ideamake.bgyjssq.common.components.LogService;
import cn.ideamake.bgyjssq.common.enums.RestEnum;
import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.util.AliyunOssKit;
import cn.ideamake.bgyjssq.common.util.DistanceUtils;
import cn.ideamake.bgyjssq.common.util.WeChatService;
import cn.ideamake.bgyjssq.dao.mapper.SignInQrCodeMapper;
import cn.ideamake.bgyjssq.pojo.dto.SaveOrChangeBindDTO;
import cn.ideamake.bgyjssq.pojo.dto.qrcode.SignInQrCodeDTO;
import cn.ideamake.bgyjssq.pojo.entity.BindingManager;
import cn.ideamake.bgyjssq.pojo.entity.CollectorVisitor;
import cn.ideamake.bgyjssq.pojo.entity.Project;
import cn.ideamake.bgyjssq.pojo.entity.QrCodeVisitor;
import cn.ideamake.bgyjssq.pojo.entity.SignInQrCode;
import cn.ideamake.bgyjssq.pojo.entity.SignInQrCodePushUser;
import cn.ideamake.bgyjssq.pojo.entity.StaffVisitor;
import cn.ideamake.bgyjssq.pojo.entity.User;
import cn.ideamake.bgyjssq.pojo.entity.Visitor;
import cn.ideamake.bgyjssq.pojo.query.ActivityDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.QrCodeQuery;
import cn.ideamake.bgyjssq.pojo.query.SignInQuery;
import cn.ideamake.bgyjssq.pojo.query.WeChatQRQuery;
import cn.ideamake.bgyjssq.pojo.vo.ExpireVO;
import cn.ideamake.bgyjssq.pojo.vo.PreSignInProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.PreSignInVO;
import cn.ideamake.bgyjssq.pojo.vo.QrCodeVO;
import cn.ideamake.bgyjssq.pojo.vo.SignInReturnVO;
import cn.ideamake.bgyjssq.pojo.vo.SignInStaffVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffVisitorInProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.qrcode.SignInQrCodeVO;
import cn.ideamake.bgyjssq.service.IBindingManagerService;
import cn.ideamake.bgyjssq.service.ICollectorVisitorService;
import cn.ideamake.bgyjssq.service.IProjectService;
import cn.ideamake.bgyjssq.service.IQrCodeVisitorService;
import cn.ideamake.bgyjssq.service.ISignInQrCodePushUserService;
import cn.ideamake.bgyjssq.service.ISignInQrCodeService;
import cn.ideamake.bgyjssq.service.IStaffVisitorService;
import cn.ideamake.bgyjssq.service.IUserService;
import cn.ideamake.bgyjssq.service.IVisitorService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 签到二维码 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-10
 */
@Slf4j
@AllArgsConstructor
@Service
public class SignInQrCodeServiceImpl extends ServiceImpl<SignInQrCodeMapper, SignInQrCode> implements ISignInQrCodeService {

    private final SignInQrCodeMapper signInQrCodeMapper;

    private final AliyunOssKit aliyunOssKit;

    private final WeChatService weChatService;

    private final ISignInQrCodePushUserService signInQrCodePushUserService;

    private final IQrCodeVisitorService qrCodeVisitorService;

    private final IBindingManagerService bindingManagerService;

    private final LogService logService;

    private final IUserService userService;

    private final IProjectService projectService;

    private final IVisitorService visitorService;

    private final IStaffVisitorService staffVisitorService;

    private final ICollectorVisitorService collectorVisitorService;

    /**
     * 小程序跳转路径
     */
    private final static String PAGE = "pages/loading/index";

    @Override
    public boolean saveAndUpdate(SignInQrCodeDTO dto) {
        SignInQrCode signInQrCode = new SignInQrCode();
        BeanUtils.copyProperties(dto, signInQrCode);

        WeChatQRQuery qrQuery = WeChatQRQuery.of();
        qrQuery.setPage(PAGE);

        return Optional.ofNullable(signInQrCode.getId())
                .map(s -> updateSignInQrCode(dto, signInQrCode, qrQuery))
                .orElseGet(() -> saveSignQrCode(dto, signInQrCode, qrQuery));
    }

    /**
     * 分页查询签到码列表
     *
     * @param qrCodeQuery 查询条件封装
     * @return List<>
     */
    @Override
    public IPage<QrCodeVO> findPage(QrCodeQuery qrCodeQuery) {
        return this.signInQrCodeMapper.selectPage(qrCodeQuery.getPageInfo(), qrCodeQuery);
    }

    @Override
    public SignInQrCodeVO getActivity(Integer id) {
        SignInQrCodeVO activity = baseMapper.getActivity(id);
        Integer activityId = activity.getId();
        List<SignInQrCodePushUser> list = signInQrCodePushUserService.lambdaQuery()
                .eq(SignInQrCodePushUser::getSignQrCodeId, activityId)
                .list();
        if (CollectionUtils.isNotEmpty(list)) {
            Set<String> sellerIds = list.parallelStream()
                    .map(SignInQrCodePushUser::getUserUuid)
                    .collect(Collectors.toSet());
            activity.setSellerIds(sellerIds);
        }
        return activity;
    }

    /**
     * 查询二维码是否过期
     *
     * @param id 活动ID
     * @return List<>
     */
    @Override
    public Boolean isExpire(Integer id) {
        SignInQrCode qrCode = getById(id);
        Optional.ofNullable(qrCode).orElseThrow(() -> new BusinessException(RestEnum.ACTIVITY_NOT_EXIST));
        return LocalDateTime.now().isAfter(qrCode.getStopAt());
    }

    /**
     * 更新过期二维码
     *
     * @param id 更新信息
     * @return List<>
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public String updateQr(Integer id) {
        SignInQrCode qrCode = this.lambdaQuery().eq(SignInQrCode::getId, id).one();
        Optional.ofNullable(qrCode).orElseThrow(() -> new BusinessException(RestEnum.ACTIVITY_NOT_EXIST));

        //二维码已失效，重新获取
        if (qrCode.getEndAt() != null && LocalDateTime.now().isAfter(qrCode.getEndAt())) {
            WeChatQRQuery qrQuery = WeChatQRQuery.of();
            LocalDateTime endAt = qrCode.getRefreshFrequency() == null
                    ? null
                    : LocalDateTime.now().plusSeconds(qrCode.getRefreshFrequency());
            qrQuery.setPage(PAGE);
            qrQuery.setScene("id:" + id + "=shared:checkIn");
            String qrCodeUrl = createQrCodeAndUpload(qrQuery);
            if (this.lambdaUpdate()
                    .set(SignInQrCode::getEndAt, endAt)
                    .set(SignInQrCode::getQrCodeUrl, qrCodeUrl)
                    .eq(SignInQrCode::getId, id)
                    .update()) {
                return qrCodeUrl;
            } else {
                return "-1";
            }
        } else {
            return qrCode.getQrCodeUrl();
        }
    }

    /**
     * 查看特定二维码中客户信息
     */
    @Override
    public IPage<StaffVisitorInProjectVO> getActivitiesDetail(ActivityDetailQuery query) {
        IPage<StaffVisitorInProjectVO> activitiesDetail = baseMapper.getActivitiesDetail(query.getPageInfo(), query);
        Optional.ofNullable(activitiesDetail).map(IPage::getRecords).ifPresent(records -> {
            if (CollectionUtils.isNotEmpty(records)) {
                records.parallelStream()
                        .forEach(s -> s.setTotalDays(baseMapper.selectTotalDays(s.getVisitorId())));
            }
        });
        return activitiesDetail;
    }

    @Override
    public Integer getActivitiesCount(Integer id) {
        return baseMapper.getActivitiesCount(id);
    }

    @Override
    public Integer getActivitiesDayCount(Integer id) {
        return baseMapper.getActivitiesDayCount(id);
    }

    /**
     * 查询訪客今日簽到次數
     *
     * @param id   项目id
     * @param uuid visitUuid
     * @param date 日期
     * @return 人数
     */
    @Override
    public Integer getVisitTodayCount(Integer id, String uuid, String date) {
        return baseMapper.getVisitTodayCount(id, uuid, date);
    }

    /**
     * 查询訪客簽到次數
     *
     * @param id   项目id
     * @param uuid visitUuid
     * @return 人数
     * @
     */
    @Override
    public Integer getVisitCount(Integer id, String uuid) {
        return baseMapper.getVisitCount(id, uuid);
    }

    /**
     * 查询二维码是否失效
     */
    @Override
    public Integer getQrExpire(ExpireVO query) {
        return baseMapper.getQrExpire(query);
    }

    /**
     * 签到预信息
     */
    @Override
    public PreSignInVO getPreMessage(Integer id) {
        PreSignInVO preSignInVO = new PreSignInVO();
        SignInStaffVO signInStaffVO = baseMapper.getSignInStaff(id);
        PreSignInProjectVO preSignInProjectVO = baseMapper.getPreSignInProject(id);
        preSignInVO.setPreSignInProjectVO(preSignInProjectVO);
        preSignInVO.setSignInStaffVO(signInStaffVO);
        return preSignInVO;
    }

    @Override
    public SignInReturnVO signIn(SignInQuery query) {
        Integer id = query.getId();
        String uuid = query.getUuid();

        SignInQrCode signInQrCode = this.lambdaQuery()
                .eq(SignInQrCode::getId, id)
                .eq(SignInQrCode::getIsActive, 1)
                .one();

        return Optional.ofNullable(signInQrCode).map(s -> {
            // 区域签到校验
            boolean areaCheck = designatedAreaCheck(s, query);

            Integer projectId = s.getProjectId();
            String userUuid = s.getUserUuid();

            //绑定员工处理
            User user = userService.lambdaQuery().eq(User::getUuid, userUuid).one();
            Project project = projectService.lambdaQuery().eq(Project::getId, projectId).one();
            bindUserAndSaveLog(uuid, projectId, userUuid, user, project, query);

            //在指定区域签到，记录到访次数
            saveVisitorCount(uuid, areaCheck, projectId);

            //活动累计上限
            Integer allNum = s.getActivityNumTop();
            //活动每日上限
            Integer activityNumDayTop = s.getActivityNumDayTop();
            //单人扫码上限
            Integer singleNumTop = s.getSingleNumTop();
            //单人扫码每日上限
            Integer dayNum = s.getSingleNumDayTop();
            //构造参数
            SignInParams signInParams = new SignInParams(allNum, dayNum, activityNumDayTop, singleNumTop);

            //当前日期定义
            LocalDateTime now = LocalDateTime.now();
            String today = now.toLocalDate().toString();

            SignInReturnVO signInReturnVO = new SignInReturnVO();
            //数据校验
            if (firstCheckData(s, signInReturnVO, uuid, now, today, project, user, signInParams)) {
                log.info("签到失败，返回数据：" + signInReturnVO.toString());
                return signInReturnVO;
            }

            //存在并发问题，采用双重校验处理
            return this.getSaveResult(s, signInReturnVO, project, user, uuid, now, today, signInParams);
        }).orElseThrow(() -> new BusinessException(RestEnum.QR_CODE_EXPIRE));
    }

    private void saveVisitorCount(String uuid, boolean areaCheck, Integer projectId) {
        //在指定区域签到
        if (areaCheck) {
            BindingManager bindingManager = bindingManagerService.lambdaQuery()
                    .eq(BindingManager::getVisitorUuid, uuid)
                    .eq(BindingManager::getProjectId, projectId)
                    .eq(BindingManager::getHistoryLabel, 0)
                    .one();
            bindingManagerService.lambdaUpdate()
                    .set(BindingManager::getVisitorCount, bindingManager.getVisitorCount() + 1)
                    .set(BindingManager::getFirstVisitTime, LocalDateTime.now())
                    .eq(BindingManager::getProjectId, projectId)
                    .eq(BindingManager::getVisitorUuid, uuid)
                    .eq(BindingManager::getHistoryLabel, 0)
                    .update();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public SignInReturnVO getSaveResult(SignInQrCode signInQrCode,
                                        SignInReturnVO signInReturnVO,
                                        Project project,
                                        User user,
                                        String uuid,
                                        LocalDateTime now,
                                        String today,
                                        SignInParams signInParams) {

        Integer id = signInQrCode.getId();
        Integer allNum = signInParams.getAllNum();
        Integer activityNumDayTop = signInParams.getActivityNumDayTop();
        Integer singleNumTop = signInParams.getSingleNumTop();
        Integer dayNum = signInParams.getDayNum();

        QrCodeVisitor qrCodeVisitor = new QrCodeVisitor();
        qrCodeVisitor.setSignInQrCodeId(id);
        qrCodeVisitor.setVisitorUuid(uuid);
        String signInTotal = "(";

        //低效率实现，是否有更好的方法解决？
        synchronized (this) {
            qrCodeVisitorService.save(qrCodeVisitor);
            SignInCheckLimit signInCheckLimit = new SignInCheckLimit(
                    signInReturnVO,
                    project,
                    user,
                    uuid,
                    today,
                    id,
                    allNum,
                    activityNumDayTop,
                    singleNumTop,
                    dayNum,
                    qrCodeVisitor
            ).invoke();
            if (signInCheckLimit.is()) {
                return signInReturnVO;
            }
            Integer total = signInCheckLimit.getTotal();
            Integer visitCount = signInCheckLimit.getVisitCount();
            Integer todayCount = signInCheckLimit.getTodayCount();

            Integer totalDays = this.selectTotalDays(uuid);
            signInReturnVO.setTotalDays(totalDays);
            signInReturnVO.setTotalCount(visitCount);
            signInReturnVO.setCountToday(todayCount);

            signInTotal += todayCount.toString() + "," + total.toString() + "," + totalDays.toString() + ")";
        }

        signInReturnVO.setStatus(1);
        signInReturnVO.setSignInTime(now);

        //保存签到成功日志
        saveSignInLogSuccess(project, user, uuid, signInTotal);

        return signInReturnVO;
    }

    /**
     * 签到成功方法所需参数
     */
    @AllArgsConstructor
    @Data
    public static class SignInParams {
        Integer allNum;
        Integer dayNum;
        Integer activityNumDayTop;
        Integer singleNumTop;
    }

    @Override
    public Integer selectTotalDays(String visitorId) {
        return baseMapper.selectTotalDays(visitorId);
    }

    /**
     * 保存签到成功日志
     *
     * @param project     项目
     * @param user        员工
     * @param uuid        客户id
     * @param signInTotal 签到成功次数统计
     */
    private void saveSignInLogSuccess(Project project, User user, String uuid, String signInTotal) {
        String str = "扫描" + (user == null ? "" : user.getNickName()) + "的 "
                + (project == null ? "" : project.getProjectTitle())
                + " 签到码签到成功" + signInTotal;
        logService.recordVisitorLog(uuid, str);
    }

    private void saveSignInLogFail(Project project, User user, String uuid, String reason) {
        String str = "扫描" + (user == null ? "" : user.getNickName()) + "的 "
                + (project == null ? "" : project.getProjectTitle())
                + " 签到码签到失败，原因：" + reason;
        logService.recordVisitorLog(uuid, str);
    }

    /**
     * 生成签到二维码
     *
     * @param qrQuery 二维码参数
     * @return java.lang.String
     */
    private String createQrCodeAndUpload(WeChatQRQuery qrQuery) {
        byte[] appletsCode = weChatService.getAppletsCode(qrQuery);
        String fileName = aliyunOssKit.upload(appletsCode, ".jpeg");
        return aliyunOssKit.getUrlByFileName(fileName);
    }

    /**
     * 更新签到码信息
     *
     * @param dto          数据对象
     * @param signInQrCode 签到码参数
     * @param qrQuery      微信签到码参数
     * @return Rest<SignInQrCode>
     */
    private boolean updateSignInQrCode(SignInQrCodeDTO dto, SignInQrCode signInQrCode, WeChatQRQuery qrQuery) {
        //TODO (逻辑修改，后续优化)原记录id
        Integer oldId = signInQrCode.getId();

        if (!this.updateById(signInQrCode)) {
            return false;
        }

        //删除后新增的记录id
        Integer newId = signInQrCode.getId();

        //保存推送员工
        saveSignInQrCodePushUser(oldId, newId, dto.getSellerIds());

        //设置二维码参数及二维码结束时间
        setQrCodeUrlAndEndAt(signInQrCode, qrQuery, newId);

        //调用更新方法，更新记录
        return this.updateById(signInQrCode);
    }

    /**
     * 保存签到二维码逻辑
     *
     * @param dto          前端传递签到活动对象
     * @param signInQrCode 签到活动对象
     * @param qrQuery      二维码生成对象
     * @return Boolean
     */
    @NotNull
    private Boolean saveSignQrCode(SignInQrCodeDTO dto, SignInQrCode signInQrCode, WeChatQRQuery qrQuery) {
        if (this.save(signInQrCode)) {
            //保存推送员工
            saveSignInQrCodePushUser(signInQrCode.getId(), dto.getSellerIds());
            //设置签到二维码及过期时间
            setQrCodeUrlAndEndAt(signInQrCode, qrQuery, signInQrCode.getId());
            //更新记录
            return this.updateById(signInQrCode);
        } else {
            return false;
        }
    }

    /**
     * 设置二维码参数及二维码结束时间
     *
     * @param signInQrCode 签到二维码对象
     * @param qrQuery      微信二维码对象
     * @param id           签到二维码id
     */
    private void setQrCodeUrlAndEndAt(SignInQrCode signInQrCode, WeChatQRQuery qrQuery, Integer id) {
        //设置二维码参数
        LocalDateTime endAt = Optional.ofNullable(signInQrCode.getRefreshFrequency())
                .map(s -> {
                    LocalDateTime time = LocalDateTime.now().plusSeconds(signInQrCode.getRefreshFrequency());
                    signInQrCode.setEndAt(time);
                    return time;
                }).orElse(null);
        qrQuery.setScene("id:" + id + "=shared:checkIn");

        signInQrCode.setQrCodeUrl(createQrCodeAndUpload(qrQuery));
        signInQrCode.setEndAt(endAt);
    }

    /**
     * 保存推送员工
     *
     * @param oldId     旧签到码id
     * @param newId     新签到码id
     * @param sellerIds 员工id列表
     */
    private void saveSignInQrCodePushUser(Integer oldId, Integer newId, Set<String> sellerIds) {
        //判断存在先移除数据
        List<SignInQrCodePushUser> pushUsers = signInQrCodePushUserService.lambdaQuery()
                .eq(SignInQrCodePushUser::getSignQrCodeId, oldId)
                .list();

        if (CollectionUtils.isNotEmpty(pushUsers)) {
            QueryWrapper<SignInQrCodePushUser> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda().eq(SignInQrCodePushUser::getSignQrCodeId, oldId);
            //移除推送员工
            if (!signInQrCodePushUserService.remove(queryWrapper)) {
                return;
            }
        }

        //保存记录
        saveSignInQrCodePushUser(newId, sellerIds);
    }

    /**
     * 保存推送员工
     *
     * @param newId     签到码id
     * @param sellerIds 推送员工列表
     */
    private void saveSignInQrCodePushUser(Integer newId, Set<String> sellerIds) {
        //传递为空直接返回
        if (CollectionUtils.isEmpty(sellerIds)) {
            return;
        }

        List<SignInQrCodePushUser> list = new ArrayList<>();
        sellerIds.forEach(s -> {
            SignInQrCodePushUser pushUser = new SignInQrCodePushUser();
            pushUser.setSignQrCodeId(newId);
            pushUser.setUserUuid(s);
            list.add(pushUser);
        });
        signInQrCodePushUserService.saveBatch(list);
    }

    /**
     * 首次校验数据
     *
     * @param signInQrCode   签到码信息
     * @param signInReturnVO 返回对象
     * @param uuid           客户id
     * @param now            当前时间
     * @param today          今天
     * @param project        项目
     * @param user           销售
     * @param signInParams   签到上限参数
     * @return boolean
     */
    private boolean firstCheckData(SignInQrCode signInQrCode,
                                   SignInReturnVO signInReturnVO,
                                   String uuid,
                                   LocalDateTime now,
                                   String today, Project project, User user, SignInParams signInParams) {
        //活动id
        Integer id = signInQrCode.getId();

        //活动签到参数
        Integer allNum = signInParams.getAllNum();
        Integer activityNumDayTop = signInParams.getActivityNumDayTop();
        Integer singleNumTop = signInParams.getSingleNumTop();
        Integer dayNum = signInParams.getDayNum();

        //活动有效期校验
        if (checkActivityDate(signInQrCode, signInReturnVO, uuid, now, project, user, id)) {
            return true;
        }

        //二维码过期校验
        checkQrCodeEffective(signInQrCode, uuid, now, project, user);

        //活动人数上限校验
        return checkSignInLimit(signInReturnVO,
                allNum,
                dayNum,
                uuid,
                today,
                project,
                user,
                activityNumDayTop,
                singleNumTop, id);
    }

    private boolean checkSignInLimit(SignInReturnVO signInReturnVO,
                                     Integer allNum,
                                     Integer dayNum,
                                     String uuid,
                                     String today,
                                     Project project,
                                     User user,
                                     Integer activityNumDayTop,
                                     Integer singleNumTop,
                                     Integer id) {
        //活动扫码总人数
        Integer exitAllNum = this.getActivitiesCount(id);
        if (allNum != null && allNum < exitAllNum) {
            signInReturnVO.setStatus(0);
            signInReturnVO.setIsActivityFull(1);
            //保存签到失败记录
            saveQrCodeVisitorFail(uuid, id);
            saveSignInLogFail(project, user, uuid, "达到签到上限（" + exitAllNum + "）");
            return true;
        }

        //活动当天签到总人数
        Integer activitiesDayCount = this.getActivitiesDayCount(id);
        if (activityNumDayTop != null && activityNumDayTop <= activitiesDayCount) {
            signInReturnVO.setStatus(0);
            signInReturnVO.setIsActivityFull(1);
            //保存签到失败记录
            saveQrCodeVisitorFail(uuid, id);
            saveSignInLogFail(project, user, uuid, "达到每日签到总上限（" + activitiesDayCount + "）");
            return true;
        }

        //单人签到总次数
        Integer visitCount = this.getVisitCount(id, uuid);
        if (singleNumTop != null && singleNumTop <= visitCount) {
            signInReturnVO.setStatus(0);
            signInReturnVO.setIsActivityFull(1);
            //保存签到失败记录
            saveQrCodeVisitorFail(uuid, id);
            saveSignInLogFail(project, user, uuid, "达到单人签到上限（" + visitCount + "）");
            return true;
        }

        //单人当天签到总次数
        Integer visitDayNum = this.getVisitTodayCount(id, uuid, today);
        if (dayNum != null && dayNum <= visitDayNum) {
            signInReturnVO.setStatus(0);
            signInReturnVO.setIsTodayFull(1);
            //保存签到失败记录
            saveQrCodeVisitorFail(uuid, id);
            saveSignInLogFail(project, user, uuid, "达到单日签到上限（" + visitDayNum + "）");
            return true;
        }
        return false;
    }

    private void checkQrCodeEffective(SignInQrCode signInQrCode,
                                      String uuid,
                                      LocalDateTime now,
                                      Project project,
                                      User user) {
        LocalDateTime endAt = signInQrCode.getEndAt();
        if (endAt != null && now.isAfter(endAt)) {
            saveSignInLogFail(project, user, uuid, "二维码过期，过期时间：" + endAt.toString());
            throw new BusinessException(RestEnum.QR_CODE_EXPIRE);
        }
    }

    private boolean checkActivityDate(SignInQrCode signInQrCode,
                                      SignInReturnVO signInReturnVO,
                                      String uuid,
                                      LocalDateTime now,
                                      Project project,
                                      User user,
                                      Integer id) {
        LocalDateTime startAt = signInQrCode.getStartAt();
        if (now.isBefore(startAt)) {
            //保存签到失败记录
            saveQrCodeVisitorFail(uuid, id);
            saveSignInLogFail(project, user, uuid, "活动未开始，开始时间：" + startAt.toString());
            //活动尚未开始
            throw new BusinessException(RestEnum.ACTIVITY_NOT_STARTED);
        }

        LocalDateTime stopAt = signInQrCode.getStopAt();
        if (now.isAfter(stopAt)) {
            signInReturnVO.setStatus(0);
            signInReturnVO.setIsActivityEnd(1);
            //保存签到失败记录
            saveQrCodeVisitorFail(uuid, id);
            saveSignInLogFail(project, user, uuid, "活动已结束，结束时间：" + stopAt.toString());
            return true;
        }
        return false;
    }

    private void saveQrCodeVisitorFail(String uuid, Integer id) {
        QrCodeVisitor qrCodeVisitor = new QrCodeVisitor();
        qrCodeVisitor.setSignInQrCodeId(id);
        qrCodeVisitor.setVisitorUuid(uuid);
        qrCodeVisitor.setStatus(false);
        qrCodeVisitorService.save(qrCodeVisitor);
    }

    /**
     * 绑定员工以及日志记录操作
     *
     * @param uuid      客户id
     * @param projectId 项目id
     * @param userUuid  销售id
     * @param user      销售信息
     * @param project   项目信息
     * @param query     上报数据
     */
    private void bindUserAndSaveLog(String uuid,
                                    Integer projectId,
                                    String userUuid,
                                    User user,
                                    Project project,
                                    SignInQuery query) {
        List<BindingManager> list = bindingManagerService.lambdaQuery()
                .eq(BindingManager::getVisitorUuid, uuid)
                .eq(BindingManager::getProjectId, projectId)
                .eq(BindingManager::getHistoryLabel, 0)
                .list();

        String userName = user == null ? "" : user.getNickName();
        String projectName = project == null ? "" : project.getProjectTitle();

        //之前未绑定此项目员工时才需要绑定
        if (CollectionUtils.isEmpty(list)) {
            //保存绑定
            saveBindManager(uuid, projectId, userUuid, query);

            //数据上报处理
            collectPhoneAndAddress(uuid, userUuid, query);

            String str = "通过扫描签到码成功关注了员工" + userName + "的 " + projectName + " 项目空间";
            logService.recordVisitorLog(uuid, str);
        } else {
            String oldUserUuid = saveVisitorLogAndGetOldUserId(uuid, userUuid, list, userName, projectName);

            //更新用户手机号
            updateVisitorPhone(uuid, projectId, query);

            //数据上报处理
            collectPhoneAndAddress(uuid, oldUserUuid, query);
        }
    }

    private void saveBindManager(String uuid, Integer projectId, String userUuid, SignInQuery query) {
        SaveOrChangeBindDTO dto = new SaveOrChangeBindDTO();
        dto.setVisitorId(uuid);
        dto.setSellerId(userUuid);
        dto.setProjectId(projectId);
        if (bindingManagerService.saveOrChangeBinding(dto)) {
            //更新用户手机号
            try {
                String phone = weChatService.decodeUserPhone(query.getPhone());
                bindingManagerService.lambdaUpdate()
                        .set(BindingManager::getPhone, phone)
                        .eq(BindingManager::getVisitorUuid, uuid)
                        .eq(BindingManager::getUserUuid, userUuid)
                        .eq(BindingManager::getProjectId, projectId)
                        .eq(BindingManager::getHistoryLabel, 0)
                        .update();
            } catch (Exception e) {
                e.printStackTrace();
                log.error("手机号解析失败");
            }
        }
    }

    private void updateVisitorPhone(String uuid, Integer projectId, SignInQuery query) {
        try {
            String phone = weChatService.decodeUserPhone(query.getPhone());
            bindingManagerService.lambdaUpdate()
                    .set(BindingManager::getPhone, phone)
                    .eq(BindingManager::getVisitorUuid, uuid)
                    .eq(BindingManager::getProjectId, projectId)
                    .update();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("手机号解析失败");
        }
    }

    private String saveVisitorLogAndGetOldUserId(String uuid, String userUuid, List<BindingManager> list, String userName, String projectName) {
        String str = "扫描员工" + userName + "的 " + projectName + " 签到码";
        logService.recordVisitorLog(uuid, str);

        //客户冲突日志记录
        Visitor visitor = visitorService.lambdaQuery().eq(Visitor::getUuid, uuid).one();
        String visitorName = visitor == null ? "" : visitor.getNickName();

        String oldUserUuid = list.get(0).getUserUuid();
        User oldUser = userService.lambdaQuery().eq(User::getUuid, oldUserUuid).one();
        String oldUserName = oldUser == null ? "" : oldUser.getNickName();

        String content1 = visitorName + "之前已关注员工" + oldUserName + "的 " + projectName + " 项目空间";
        //当前销售的日志
        logService.recordUserLog(userUuid, content1, false);

        String content2 = "您的客户" + visitorName + "扫描了员工" + userName + "的项目 " + projectName + " 的签到码";
        //已绑定销售日志，默认取第一个销售数据
        logService.recordUserLog(oldUserUuid, content2, false);
        return oldUserUuid;
    }

    /**
     * 手机号及定位上报
     *
     * @param uuid     客户id
     * @param userUuid 销售id
     * @param query    上报数据
     */
    private void collectPhoneAndAddress(String uuid, String userUuid, SignInQuery query) {
        //手机号上报
        try {
            Optional.ofNullable(query.getPhone()).ifPresent(p -> {
                String phone = weChatService.decodeUserPhone(p);
                staffVisitorService.lambdaUpdate()
                        .set(StrUtil.isNotBlank(phone), StaffVisitor::getVisitorPhone, phone)
                        .eq(StaffVisitor::getVisitorUuid, uuid)
                        .eq(StaffVisitor::getUserUuid, userUuid)
                        .update();
                visitorService.lambdaUpdate()
                        .set(StrUtil.isNotBlank(phone), Visitor::getPhone, phone)
                        .set(StrUtil.isNotBlank(phone), Visitor::getCommentPhone, phone)
                        .eq(Visitor::getUuid, uuid)
                        .update();
            });
        } catch (Exception e) {
            log.error("手机号上报解析失败,前端传递参数：{}", query.getPhone());
            e.printStackTrace();
        }
        //上报定位
        if (StrUtil.isNotBlank(query.getLongitude()) && StrUtil.isNotBlank(query.getLatitude())) {
            collectorVisitorService.lambdaUpdate()
                    .set(CollectorVisitor::getLongitude, query.getLongitude())
                    .set(CollectorVisitor::getLatitude, query.getLatitude())
                    .set(CollectorVisitor::getOrigin, 5)
                    .eq(CollectorVisitor::getVisitorId, uuid)
                    .eq(CollectorVisitor::getSellerId, userUuid)
                    .update();
        }
    }

    /**
     * 指定区域签到校验
     *
     * @param s     签到信息
     * @param query 定位信息
     */
    private boolean designatedAreaCheck(SignInQrCode s, SignInQuery query) {
        Boolean designatedArea = s.getDesignatedArea();
        if (Objects.isNull(designatedArea) || !designatedArea) {
            return false;
        }

        Integer radius = s.getRadius();
        if (radius == 0) {
            return false;
        }

        String longitude1 = s.getLongitude();
        String latitude1 = s.getLatitude();
        if (StrUtil.isBlank(longitude1) || StrUtil.isBlank(latitude1)) {
            return false;
        }

        String longitude2 = query.getLongitude();
        String latitude2 = query.getLatitude();
        if (StrUtil.isBlank(longitude2) || StrUtil.isBlank(latitude2)) {
            return false;
        }

        DistanceUtils.Coordinate coordinate1 =
                new DistanceUtils.Coordinate(Double.valueOf(longitude1), Double.valueOf(latitude1));
        DistanceUtils.Coordinate coordinate2 =
                new DistanceUtils.Coordinate(Double.valueOf(longitude2), Double.valueOf(latitude2));

        double distance = DistanceUtils.getDistance(coordinate1, coordinate2);
        if (distance > radius) {
            throw new BusinessException(RestEnum.OUT_OF_SCOPE);
        }
        return true;
    }

    private class SignInCheckLimit {
        private boolean myResult;
        private SignInReturnVO signInReturnVO;
        private Project project;
        private User user;
        private String uuid;
        private String today;
        private Integer id;
        private Integer allNum;
        private Integer activityNumDayTop;
        private Integer singleNumTop;
        private Integer dayNum;
        private QrCodeVisitor qrCodeVisitor;
        private Integer total;
        private Integer visitCount;
        private Integer todayCount;

        SignInCheckLimit(SignInReturnVO signInReturnVO, Project project, User user, String uuid, String today, Integer id, Integer allNum, Integer activityNumDayTop, Integer singleNumTop, Integer dayNum, QrCodeVisitor qrCodeVisitor) {
            this.signInReturnVO = signInReturnVO;
            this.project = project;
            this.user = user;
            this.uuid = uuid;
            this.today = today;
            this.id = id;
            this.allNum = allNum;
            this.activityNumDayTop = activityNumDayTop;
            this.singleNumTop = singleNumTop;
            this.dayNum = dayNum;
            this.qrCodeVisitor = qrCodeVisitor;
        }

        boolean is() {
            return myResult;
        }

        Integer getTotal() {
            return total;
        }

        public Integer getVisitCount() {
            return visitCount;
        }

        Integer getTodayCount() {
            return todayCount;
        }

        SignInCheckLimit invoke() {
            //总上限校验
            total = SignInQrCodeServiceImpl.this.getActivitiesCount(id);
            if (allNum != null && total > allNum) {
                getBaseSignInReturnVO(signInReturnVO, qrCodeVisitor);
                signInReturnVO.setIsActivityFull(1);
                saveSignInLogFail(project, user, uuid, "达到签到上限（" + total + "）");
                myResult = true;
                return this;
            }

            //活动当天签到总人数
            Integer activitiesDayCount = SignInQrCodeServiceImpl.this.getActivitiesDayCount(id);
            if (activityNumDayTop != null && activityNumDayTop <= activitiesDayCount) {
                getBaseSignInReturnVO(signInReturnVO, qrCodeVisitor);
                signInReturnVO.setIsActivityFull(1);
                //保存签到失败记录
                saveQrCodeVisitorFail(uuid, id);
                saveSignInLogFail(project, user, uuid, "达到每日签到总上限（" + activitiesDayCount + "）");
                myResult = true;
                return this;
            }

            //单人签到总次数
            visitCount = SignInQrCodeServiceImpl.this.getVisitCount(id, uuid);
            if (singleNumTop != null && singleNumTop <= visitCount) {
                getBaseSignInReturnVO(signInReturnVO, qrCodeVisitor);
                signInReturnVO.setIsActivityFull(1);
                //保存签到失败记录
                saveQrCodeVisitorFail(uuid, id);
                saveSignInLogFail(project, user, uuid, "达到单人签到上限（" + visitCount + "）");
                myResult = true;
                return this;
            }

            //单日上限校验
            todayCount = SignInQrCodeServiceImpl.this.getVisitTodayCount(id, uuid, today);
            if (dayNum != null && todayCount > dayNum) {
                getBaseSignInReturnVO(signInReturnVO, qrCodeVisitor);
                signInReturnVO.setIsTodayFull(1);
                saveSignInLogFail(project, user, uuid, "达到单日签到上限（" + todayCount + "）");
                myResult = true;
                return this;
            }
            myResult = false;
            return this;
        }

        /**
         * 更新签到状态，返回签到失败结果
         *
         * @param signInReturnVO 返回VO对象
         * @param qrCodeVisitor  签到对象
         */
        private void getBaseSignInReturnVO(SignInReturnVO signInReturnVO, QrCodeVisitor qrCodeVisitor) {
            //更新签到状态
            qrCodeVisitorService.lambdaUpdate()
                    .set(QrCodeVisitor::getStatus, false)
                    .eq(QrCodeVisitor::getId, qrCodeVisitor.getId())
                    .update();
            //达到签到上限
            signInReturnVO.setStatus(0);
        }
    }
}
