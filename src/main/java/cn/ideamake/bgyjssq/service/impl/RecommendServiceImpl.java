package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.dao.mapper.RecommendMapper;
import cn.ideamake.bgyjssq.pojo.dto.RecommendCustomerDTO;
import cn.ideamake.bgyjssq.pojo.dto.RecordByClickLinkDTO;
import cn.ideamake.bgyjssq.pojo.dto.ShareSubmitDTO;
import cn.ideamake.bgyjssq.pojo.entity.ClickRecord;
import cn.ideamake.bgyjssq.pojo.entity.OauthWechat;
import cn.ideamake.bgyjssq.pojo.entity.Recommend;
import cn.ideamake.bgyjssq.pojo.entity.Share;
import cn.ideamake.bgyjssq.pojo.query.QueryPage;
import cn.ideamake.bgyjssq.pojo.query.RecommendDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.RecommendQuery;
import cn.ideamake.bgyjssq.pojo.vo.BrainMappingVO;
import cn.ideamake.bgyjssq.pojo.vo.CustomerShareVO;
import cn.ideamake.bgyjssq.pojo.vo.RecommendCustomerVO;
import cn.ideamake.bgyjssq.pojo.vo.RecommendDetailPageVO;
import cn.ideamake.bgyjssq.pojo.vo.RecommendPageVO;
import cn.ideamake.bgyjssq.service.IClickRecordService;
import cn.ideamake.bgyjssq.service.IOauthWechatService;
import cn.ideamake.bgyjssq.service.IRecommendService;
import cn.ideamake.bgyjssq.service.IShareService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 推荐记录管理 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-06-06
 */
@Slf4j
@AllArgsConstructor
@Service
public class RecommendServiceImpl extends ServiceImpl<RecommendMapper, Recommend> implements IRecommendService {

    private final IShareService shareService;

    private final IOauthWechatService oauthWechatService;

    private final IClickRecordService clickRecordService;

    @Override
    public IPage<RecommendPageVO> selectRecommendVO(Page<RecommendPageVO> myPage, RecommendQuery recommendQuery) {
        return baseMapper.selectRecommendVO(myPage, recommendQuery);
    }

    @Override
    public IPage<RecommendDetailPageVO> selectRecommendDetailVO(Page<RecommendDetailPageVO> myPage,
                                                                RecommendDetailQuery query) {
        return baseMapper.selectRecommendDetailVO(myPage, query);
    }

    @Override
    public Integer getRecommendCountByVisitorUuid(String uuid) {
        if (null == uuid) {
            return 0;
        }
        return lambdaQuery().eq(Recommend::getSourceUuid, uuid).count();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer shareResourceByUser(ShareSubmitDTO shareSubmitDTO) {
        Share share = new Share();
        share.setResourceId(shareSubmitDTO.getResourceId());
        share.setShareType(shareSubmitDTO.getUserType());
        share.setShareUser(shareSubmitDTO.getUserId());
        share.setShareType(shareSubmitDTO.getResourceType());
        share.setProjectId(shareSubmitDTO.getProjectId());
        share.setShareId(shareSubmitDTO.getShareId());
        share.setSellerId(shareSubmitDTO.getSellerId());
        if (!shareService.save(share)) {
            throw new BusinessException("分享保存失败");
        }
        return share.getId();
    }

    /**
     * 此接口用于用户拓客行为加入到推荐表
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean recommendRecord(RecommendCustomerDTO joinByShareDTO) {
        String sourceUuid = joinByShareDTO.getSourceUuid();
        String targetUuid = joinByShareDTO.getTargetUuid();
        if (sourceUuid.equals(targetUuid)) {
            return false;
        }

        //同一项目已有拓客记录，直接返回
        if (this.lambdaQuery()
                .eq(Recommend::getTargetUuid, targetUuid)
                .eq(Recommend::getProjectId, joinByShareDTO.getProjectId())
                .count() > 0) {
            return true;
        }

        OauthWechat oauthWechat = oauthWechatService.getUserWeChatInfoByCache(sourceUuid);
        if (oauthWechat == null) {
            throw new BusinessException("源客户不存在");
        }

        Recommend recommend = new Recommend();
        BeanUtils.copyProperties(joinByShareDTO, recommend);
        recommend.setRecommendType(oauthWechat.getUserType());
        recommend.setPromotionType(joinByShareDTO.getPromotionType());
        return save(recommend);
    }

    @Override
    public boolean clickInByShareLink(RecordByClickLinkDTO joinByShareDTO) {
        //添加该点击行为到点击表

        ClickRecord clickRecord = new ClickRecord();

        //分享人和被分享人的身份暂时由后端来做判断
        OauthWechat sourceUser = oauthWechatService.getUserWeChatInfoByCache(joinByShareDTO.getSourceUuid());
        if (null == sourceUser) {
            throw new BusinessException("分享人的用户信息不存在");
        }
        OauthWechat targetUser = oauthWechatService.getUserWeChatInfoByCache(joinByShareDTO.getTargetUuid());
        if (null == targetUser) {
            throw new BusinessException("被分享人的用户信息不存在");
        }

        BeanUtils.copyProperties(joinByShareDTO, clickRecord);
        clickRecord.setSourceUserType(sourceUser.getUserType());
        clickRecord.setTargetUserType(targetUser.getUserType());
        //保存用户的点击行为
        return clickRecordService.save(clickRecord);
    }

    @Override
    public boolean deleteVisitorShareRecord(String visitorId) {
        Map<String, Object> shareMap = new HashMap<>(1);
        shareMap.put("share_user", visitorId);
        shareService.removeByMap(shareMap);

        Map<String, Object> recommendMap = new HashMap<>(1);
        recommendMap.put("source_uuid", visitorId);
        removeByMap(recommendMap);
        recommendMap.clear();
        recommendMap.put("target_uuid", visitorId);
        removeByMap(recommendMap);
        return true;
    }

    @Override
    public IPage<RecommendCustomerVO> selectRecommendCustomer(QueryPage page, String sourceId) {
        return baseMapper.selectRecommendCustomer(new Page<>(page.getPage(), page.getLimit()), sourceId);
    }

    @Override
    public IPage<CustomerShareVO> selectCustomerShare(QueryPage page, String sourceId) {
        return baseMapper.selectCustomerShare(new Page<>(page.getPage(), page.getLimit()), sourceId);
    }

    @Override
    public List<BrainMappingVO> getBrainMapping(String sourceId, Integer projectId) {
        //获取父级属性
        OauthWechat oauthWechat = oauthWechatService.lambdaQuery().eq(OauthWechat::getUserUuid, sourceId).one();
        BrainMappingVO brainMappingVO = new BrainMappingVO();
        brainMappingVO.setAvatar(oauthWechat.getAvatarUrl());
        brainMappingVO.setNickname(oauthWechat.getNickName());
        brainMappingVO.setUuid(sourceId);
        brainMappingVO.setParentUuid("");

        List<BrainMappingVO> brainMappings = new ArrayList<>();
        brainMappings.add(brainMappingVO);
        //获取子类属性
        return getChildren(sourceId, brainMappings, projectId);
    }

    private List<BrainMappingVO> getChildren(String sourceId, List<BrainMappingVO> brainMappings, Integer projectId) {
        try {
            List<Recommend> list = this.lambdaQuery()
                    .eq(Recommend::getSourceUuid, sourceId)
                    .eq(Recommend::getProjectId, projectId)
                    .list();
            if (CollectionUtils.isNotEmpty(list)) {
                Set<String> targetIds = list.parallelStream().map(Recommend::getTargetUuid).collect(Collectors.toSet());
                targetIds.forEach(t -> {
                    OauthWechat o = oauthWechatService.lambdaQuery().eq(OauthWechat::getUserUuid, t).one();
                    BrainMappingVO vo = new BrainMappingVO();
                    vo.setAvatar(o.getAvatarUrl());
                    vo.setNickname(o.getNickName());
                    vo.setUuid(t);
                    vo.setParentUuid(sourceId);
                    brainMappings.add(vo);
                    getChildren(t, brainMappings, projectId);
                });
            }

            return brainMappings;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("数据有误，请检查数据, sourceId:{}", sourceId);
            return brainMappings;
        }
    }


}
