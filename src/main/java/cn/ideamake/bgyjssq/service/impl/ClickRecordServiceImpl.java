package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.pojo.entity.ClickRecord;
import cn.ideamake.bgyjssq.dao.mapper.ClickRecordMapper;
import cn.ideamake.bgyjssq.service.IClickRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 点击行为记录统计表 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-08-06
 */
@Service
public class ClickRecordServiceImpl extends ServiceImpl<ClickRecordMapper, ClickRecord> implements IClickRecordService {

}
