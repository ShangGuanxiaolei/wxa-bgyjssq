package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.common.enums.DictType;
import cn.ideamake.bgyjssq.dao.mapper.DictMapper;
import cn.ideamake.bgyjssq.dao.mapper.UserMapper;
import cn.ideamake.bgyjssq.pojo.entity.Dict;
import cn.ideamake.bgyjssq.pojo.entity.User;
import cn.ideamake.bgyjssq.pojo.query.StaffGroupQuery;
import cn.ideamake.bgyjssq.pojo.vo.DictVO;
import cn.ideamake.bgyjssq.service.IDictService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * <p>
 * 字典表，用于记录相同字段结构 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@AllArgsConstructor
@Service
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements IDictService {

    private final UserMapper userMapper;

    @Override
    public IPage<DictVO> findPage(StaffGroupQuery query, DictType type) {
        IPage<DictVO> page = baseMapper.selectByType(query.getPageInfo(), type.name(), query);
        //处理返回管理组的销售名称，效率低
        Optional.ofNullable(page.getRecords()).ifPresent(l -> l.forEach(v -> {
            String resourceIds = v.getResourceIds();
            if (StringUtils.isNotBlank(resourceIds)) {
                QueryWrapper<User> queryWrapper = new QueryWrapper<>();
                queryWrapper.lambda()
                        .select(User::getNickName)
                        .in(User::getUuid, (Object[]) resourceIds.split(","));
                List<User> list = userMapper.selectList(queryWrapper);
                List<String> userList = new ArrayList<>();
                list.forEach(n -> userList.add(n.getNickName()));
                v.setUserList(userList);
            }
        }));
        return page;
    }

    @Override
    public boolean verifyDictExists(Integer groupId, String type) {
        Dict dict = lambdaQuery().eq(Dict::getId, groupId)
                .eq(Dict::getType, type)
                .one();
        return Objects.nonNull(dict);
    }


}
