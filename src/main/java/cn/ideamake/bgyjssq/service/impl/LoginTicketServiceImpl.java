package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.dao.mapper.LoginTicketMapper;
import cn.ideamake.bgyjssq.pojo.entity.LoginTicket;
import cn.ideamake.bgyjssq.service.ILoginTicketService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 登录凭证 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-25
 */
@Service
public class LoginTicketServiceImpl extends ServiceImpl<LoginTicketMapper, LoginTicket> implements ILoginTicketService {

}
