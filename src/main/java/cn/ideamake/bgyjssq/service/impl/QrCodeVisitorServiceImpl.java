package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.dao.mapper.QrCodeVisitorMapper;
import cn.ideamake.bgyjssq.pojo.entity.QrCodeVisitor;
import cn.ideamake.bgyjssq.service.IQrCodeVisitorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 签到码签到客户 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-10
 */
@Service
public class QrCodeVisitorServiceImpl extends ServiceImpl<QrCodeVisitorMapper, QrCodeVisitor> implements IQrCodeVisitorService {

}
