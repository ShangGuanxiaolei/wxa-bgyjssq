package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.dao.mapper.UserRoleMapper;
import cn.ideamake.bgyjssq.pojo.entity.UserRole;
import cn.ideamake.bgyjssq.service.IUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.jsonwebtoken.lang.Collections;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 用户角色绑定表，记录用户和角色的绑定关系 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-06-06
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

    @Override
    public List<UserRole> selectUserIdListByParentId(String userId) {
        return getUserRoles(userId);
    }

    @Override
    public List<String> selectAllUserIdListByParentId(String userId) {
        List<UserRole> userRoleList = getUserRoles(userId);
        List<String> list = new ArrayList<>();
        return getAllUserIdList(userRoleList, list);
    }

    /**
     * 递归查询所有下级用户id列表
     *
     * @param userRoleList 角色列表
     * @return List<String>
     */
    private List<String> getAllUserIdList(List<UserRole> userRoleList, List<String> list) {
        for (UserRole userRole : userRoleList) {
            list.add(userRole.getUserUuid());
            List<UserRole> roleList = baseMapper.selectByParentId(userRole.getId());
            if (!Collections.isEmpty(roleList)) {
                getAllUserIdList(roleList, list);
            }
        }
        return list;
    }

    /**
     * 查询下一级用户角色
     *
     * @param userId 用户id
     * @return List<UserRole>
     */
    private List<UserRole> getUserRoles(String userId) {
        if (StringUtils.isBlank(userId)) {
            throw new BusinessException("userId不能为空");
        }
        UserRole userRole = this.lambdaQuery().eq(UserRole::getUserUuid, userId).one();
        return Optional.ofNullable(userRole)
                .map(s -> baseMapper.selectByParentId(s.getId()))
                .orElse(new ArrayList<>());
    }

}
