package cn.ideamake.bgyjssq.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.util.Constants;
import cn.ideamake.bgyjssq.common.util.MyCollectionUtils;
import cn.ideamake.bgyjssq.dao.mapper.BindingManagerMapper;
import cn.ideamake.bgyjssq.dao.mapper.StaffVisitorMapper;
import cn.ideamake.bgyjssq.pojo.entity.BindingManager;
import cn.ideamake.bgyjssq.pojo.entity.Dict;
import cn.ideamake.bgyjssq.pojo.entity.Project;
import cn.ideamake.bgyjssq.pojo.entity.StaffVisitor;
import cn.ideamake.bgyjssq.pojo.entity.UserInfo;
import cn.ideamake.bgyjssq.pojo.query.StaffVisitorQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorVisitAnalysisQuery;
import cn.ideamake.bgyjssq.pojo.vo.SellerVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffVisitorVO;
import cn.ideamake.bgyjssq.pojo.vo.analysis.DataAnalysisDetailListVO;
import cn.ideamake.bgyjssq.service.ICollectorVisitorService;
import cn.ideamake.bgyjssq.service.IDictService;
import cn.ideamake.bgyjssq.service.IProjectManagerService;
import cn.ideamake.bgyjssq.service.IProjectService;
import cn.ideamake.bgyjssq.service.IStaffVisitorService;
import cn.ideamake.bgyjssq.service.IUserGroupManagerService;
import cn.ideamake.bgyjssq.service.IUserInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.jsonwebtoken.lang.Collections;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户员工对于绑定自己的客户的管理 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-06-19
 */
@AllArgsConstructor
@Service
@Slf4j
public class StaffVisitorServiceImpl extends ServiceImpl<StaffVisitorMapper, StaffVisitor> implements IStaffVisitorService {

    private final IUserGroupManagerService userGroupManagerService;

    private final IUserInfoService userInfoService;

    private final IDictService dictService;

    private final IProjectService projectService;

    private final ICollectorVisitorService collectorVisitorService;

    private final IProjectManagerService projectManagerService;

    private final BindingManagerMapper bindingManagerMapper;

    /**
     * 前端排序规则对应的数据库中的列名
     * 0-0
     * 1-首次访问
     * 2-最近访问
     * 3-访问次数
     * 4-推介次数
     * 5-关注项目
     */
    private static final String[] STAFF_VISITOR_FILTER_SORT_COLUMN = {"", "bm.first_visit_time", "cv.create_at", "visitCount", "recomNum", "followProjNum"};
    /**
     * 和前台约定的倒序数字
     * 1-正序
     * 2-倒序
     */
    private static final int DESC_NUM = 2;

    @Override
    public IPage<StaffVisitorVO> getStaffVisitorByFilter(StaffVisitorQuery query) {
        Page<StaffVisitorVO> page = query.getPageInfo();
        // 整理排序
        List<String> sortOptions = query.getSortOptions();
        String columnName = null;
        boolean desc = false;
        int sortOptionsSize = 2;
        if (CollUtil.isNotEmpty(sortOptions) && sortOptions.size() == sortOptionsSize) {
            if (StringUtils.isNotBlank(sortOptions.get(0)) && StringUtils.isNotBlank(sortOptions.get(1))) {
                columnName = STAFF_VISITOR_FILTER_SORT_COLUMN[Integer.valueOf(sortOptions.get(0))];
                int sort = Integer.parseInt(sortOptions.get(1));
                desc = sort == DESC_NUM;
            }
        }

        List<StaffVisitorVO> customerList;

        List<String> sellerIds = query.getSellerIds();
        String sellerId = query.getSellerId();
        //选择了销售id
        if (!Collections.isEmpty(sellerIds)) {
            if (!sellerIds.contains(sellerId)) {
                sellerIds.add(sellerId);
            }
            customerList =
                    baseMapper.getStaffVisitorByFilter(query, page, columnName, desc, sellerIds);
            return page.setRecords(customerList);
        }

        //选择了组id
        if (!Collections.isEmpty(query.getGroupIds()) && Collections.isEmpty(sellerIds)) {
            List<Integer> groupIds = query.getGroupIds();
            List<String> userIdList = userGroupManagerService.getSellerIdListByGroupIds(groupIds);
            userIdList.add(sellerId);
            customerList =
                    baseMapper.getStaffVisitorByFilter(query, page, columnName, desc, userIdList);
            return page.setRecords(customerList);
        }

        //啥也没选
        List<String> list = userGroupManagerService.getSellerIdListAddMe(sellerId);
        //去重操作
        MyCollectionUtils.removeDuplicate(list);
        customerList =
                baseMapper.getStaffVisitorByFilter(query, page, columnName, desc, list);
        return page.setRecords(customerList);

    }

    @Override
    public List<DataAnalysisDetailListVO> getUserGroup(String sellerId, List<Integer> projects) {
        List<DataAnalysisDetailListVO> list = new ArrayList<>();

        List<Integer> groupIdList = userGroupManagerService.getGroupIdList(sellerId, projects);

        Optional.ofNullable(groupIdList).ifPresent(u -> u.forEach(g -> {

            List<String> userList = userInfoService.getUserIdList(g);
            userList.add(sellerId);

            Dict dict = dictService.lambdaQuery().eq(Dict::getId, g).one();

            Long count = baseMapper.countVisitorByUserIds(userList, projects);

            setValue(list, g, dict, count);

        }));

        list.sort(Comparator.reverseOrder());

        return list;
    }

    @Override
    public List<DataAnalysisDetailListVO> getUserGroupAnalyse(VisitorVisitAnalysisQuery query) {
        String sellerId = query.getSellerId();
        List<DataAnalysisDetailListVO> list = new ArrayList<>();

        List<Integer> groupIdList =
                userGroupManagerService.getGroupIdList(sellerId, new ArrayList<>(query.getProjects()));

        UserInfo userInfo = userInfoService.lambdaQuery().eq(UserInfo::getUserUuid, sellerId).one();

        Optional.ofNullable(groupIdList).ifPresent(u -> u.forEach(g -> {

            List<String> userList = userInfoService.getUserIdList(g);

            List<String> effectiveData =
                    projectManagerService.getEffectiveData(query.getProjects(), userList);
            if (userInfo != null && g.equals(userInfo.getGroupId())) {
                effectiveData.remove(userInfo.getUserUuid());
            } else {
                //适配处理
                effectiveData.add("-1");
            }
            Dict dict = dictService.lambdaQuery().eq(Dict::getId, g).one();

            Long count = getCount(query, effectiveData);

            setValue(list, g, dict, count);

        }));

        list.sort(Comparator.reverseOrder());

        return list;
    }

    @Override
    public List<DataAnalysisDetailListVO> getUserList(List<Integer> groupIds, List<Integer> projects) {
        if (Collections.isEmpty(groupIds)) {
            return new ArrayList<>();
        }
        List<DataAnalysisDetailListVO> list = new ArrayList<>();
        List<SellerVO> userInfoList = userInfoService.getUserIdListByGroupIds(groupIds);
        Optional.ofNullable(userInfoList).ifPresent(l -> l.forEach(v -> {
            String userUuid = v.getId();
            Long count = baseMapper.countVisitorByUserId(userUuid, projects);
            list.add(new DataAnalysisDetailListVO(userUuid,
                    v.getName(),
                    v.getAvatar(),
                    count,
                    v.getProjectName()));
        }));
        return list;
    }

    /**
     * 根据参数类型获取对应的数据
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param list                      用户id列表
     * @return Long
     */
    @Override
    public Long getData(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> list) {
        Long data;

        switch (visitorVisitAnalysisQuery.getDataType()) {
            case Constants.DataType
                    .NUMBER_OF_VISITS:
                data = collectorVisitorService.getNumberOfVisits(visitorVisitAnalysisQuery, list);
                break;
            case Constants.DataType
                    .NUMBER_OF_PEOPLE:
                data = collectorVisitorService.getNumberOfPeople(visitorVisitAnalysisQuery, list);
                break;
            case Constants.DataType
                    .PEOPLE_OF_REMAIN_PHONE:
                data = collectorVisitorService.getPeopleOfRemainPhone(visitorVisitAnalysisQuery, list);
                break;
            case Constants.DataType
                    .PEOPLE_OF_ACCESS:
                data = collectorVisitorService.getPeopleOfAccess(visitorVisitAnalysisQuery, list);
                break;
            case Constants.DataType
                    .SHARE_COUNT:
                data = collectorVisitorService.getShareCount(visitorVisitAnalysisQuery, list);
                break;
            case Constants.DataType
                    .SHARE_PEOPLE:
                data = collectorVisitorService.getSharePeople(visitorVisitAnalysisQuery, list);
                break;
            case Constants.DataType
                    .READ_COUNT:
                data = collectorVisitorService.getReadCount(visitorVisitAnalysisQuery, list);
                break;
            case Constants.DataType
                    .READ_PEOPLE:
                data = collectorVisitorService.getReadPeople(visitorVisitAnalysisQuery, list);
                break;
            case Constants.DataType
                    .SELLER_RECOMMEND:
                data = collectorVisitorService.getSellerRecommend(visitorVisitAnalysisQuery, list);
                break;
            case Constants.DataType
                    .CUSTOMER_RECOMMEND:
                data = collectorVisitorService.getCustomerRecommend(visitorVisitAnalysisQuery, list);
                break;
            default:
                data = 0L;
                break;
        }
        return data;
    }

    /**
     * 设置元素属性
     *
     * @param list  集合
     * @param g     组id
     * @param dict  分组
     * @param count 统计数据
     */
    private void setValue(List<DataAnalysisDetailListVO> list, Integer g, Dict dict, Long count) {
        Optional.ofNullable(dict).map(d -> {
            Project project = projectService.lambdaQuery()
                    .select(Project::getProjectTitle)
                    .eq(Project::getId, d.getProjectId())
                    .eq(Project::getDeleteMark, 0)
                    .one();
            return list.add(new DataAnalysisDetailListVO(
                    g.toString(), d.getName(), count, project == null ? "" : project.getProjectTitle()));
        }).orElseThrow(() -> new BusinessException("分组数据不存在"));
    }

    /**
     * 获取总数
     *
     * @param query         查询条件
     * @param effectiveData 销售列表
     * @return Long
     */
    private Long getCount(VisitorVisitAnalysisQuery query, List<String> effectiveData) {
        Long count = 0L;

        if (Constants.DataType.CUSTOMER_RECOMMEND == query.getDataType()) {
            List<String> list = this.getQueryListByCollection(effectiveData);
            if (CollectionUtils.isNotEmpty(list)) {
                count = getData(query, list);
            }
        } else {
            count = getData(query, effectiveData);
        }
        return count;
    }

    @Override
    public List<String> getQueryListByCollection(List<String> list) {
        QueryWrapper<BindingManager> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .in(BindingManager::getUserUuid, list)
                .eq(BindingManager::getHistoryLabel, 0);
        List<BindingManager> bindingManagers = bindingManagerMapper.selectList(queryWrapper);
        if (CollectionUtils.isNotEmpty(bindingManagers)) {
            return bindingManagers.parallelStream()
                    .map(BindingManager::getVisitorUuid)
                    .distinct()
                    .collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

}
