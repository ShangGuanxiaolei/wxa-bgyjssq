package cn.ideamake.bgyjssq.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.ideamake.bgyjssq.common.components.LogService;
import cn.ideamake.bgyjssq.common.enums.ResourceType;
import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.handler.UserInfoContext;
import cn.ideamake.bgyjssq.common.util.AliyunOssKit;
import cn.ideamake.bgyjssq.common.util.AverageDataUtil;
import cn.ideamake.bgyjssq.common.util.Constants;
import cn.ideamake.bgyjssq.dao.mapper.BindingManagerMapper;
import cn.ideamake.bgyjssq.dao.mapper.UserMapper;
import cn.ideamake.bgyjssq.pojo.bo.VisitorActivityStatistics;
import cn.ideamake.bgyjssq.pojo.dto.SaveOrChangeBindDTO;
import cn.ideamake.bgyjssq.pojo.entity.BindingManager;
import cn.ideamake.bgyjssq.pojo.entity.CollectorAllRecord;
import cn.ideamake.bgyjssq.pojo.entity.Project;
import cn.ideamake.bgyjssq.pojo.entity.Recommend;
import cn.ideamake.bgyjssq.pojo.entity.Resource;
import cn.ideamake.bgyjssq.pojo.entity.StaffVisitor;
import cn.ideamake.bgyjssq.pojo.entity.User;
import cn.ideamake.bgyjssq.pojo.entity.Visitor;
import cn.ideamake.bgyjssq.pojo.query.CustomerListQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorAttentionQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorHistoryBindQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorProjectQuery;
import cn.ideamake.bgyjssq.pojo.vo.CustomerInfoDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.CustomerListVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorAttentionVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorHistoryBindVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorProjectVO;
import cn.ideamake.bgyjssq.service.IBindingManagerService;
import cn.ideamake.bgyjssq.service.ICollectorAllRecordService;
import cn.ideamake.bgyjssq.service.IProjectService;
import cn.ideamake.bgyjssq.service.IRecommendService;
import cn.ideamake.bgyjssq.service.IResourceService;
import cn.ideamake.bgyjssq.service.IStaffVisitorService;
import cn.ideamake.bgyjssq.service.IVisitorService;
import cn.ideamake.bgyjssq.service.common.SaveAdminLogService;
import cn.ideamake.bgyjssq.service.common.SaveUserLogService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.jsonwebtoken.lang.Collections;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * <p>
 * 员工的访客管理 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-06-12
 */
@AllArgsConstructor
@Service
@Slf4j
public class BindingManagerServiceImpl extends ServiceImpl<BindingManagerMapper, BindingManager> implements IBindingManagerService {

    private final IStaffVisitorService staffVisitorService;

    private final IVisitorService visitorService;

    private final LogService logService;

    private final UserMapper userMapper;

    private final ICollectorAllRecordService collectorAllRecordService;

    private final AliyunOssKit aliyunOssKit;

    private final IProjectService projectService;

    private final SaveUserLogService saveUserLogService;

    private final IRecommendService recommendService;

    private final IResourceService resourceService;

    private final SaveAdminLogService saveAdminLogService;

    private static final List<VisitorActivityStatistics> EMPTY_LIST = new ArrayList<>();

    @Override
    public IPage<VisitorHistoryBindVO> selectVisitorHistoryBind(Page<VisitorHistoryBindVO> myPage, VisitorHistoryBindQuery visitorHistoryBindQuery) {
        if (visitorHistoryBindQuery.getUserid() == null) {
            visitorHistoryBindQuery.setUserid(UserInfoContext.get().getUserId());
        }
        return baseMapper.selectVisitorHistoryBind(myPage, visitorHistoryBindQuery, aliyunOssKit.getOssUrlPrefix());
    }

    @Override
    public IPage<VisitorAttentionVO> selectVisitorAttention(Page<VisitorAttentionVO> myPage, VisitorAttentionQuery visitorAttentionQuery) {
        return baseMapper.selectVisitorAttention(myPage, visitorAttentionQuery, aliyunOssKit.getOssUrlPrefix());
    }


    @Override
    public IPage<VisitorProjectVO> selectConditionAttention(VisitorProjectQuery visitorProjectQuery) {
        return baseMapper.selectConditionAttention(visitorProjectQuery.getPageInfo(), visitorProjectQuery);
    }

    @Override
    @Transactional(rollbackFor = BusinessException.class)
    public boolean saveOrChangeBinding(SaveOrChangeBindDTO bindInfo) {
        Project project = projectService.lambdaQuery()
                .eq(Project::getId, bindInfo.getProjectId())
                .one();
        setSellerId(bindInfo, project);
        List<String> visitorIds = bindInfo.getVisitorIds();
        List<String> sellerIds = bindInfo.getSellerIds();
        // 低效率实现
        if (!Collections.isEmpty(visitorIds) && !Collections.isEmpty(sellerIds)) {
            batchChangeBind(bindInfo, project, visitorIds, sellerIds);
        } else {
            getChangeBindingVO(bindInfo, project);
        }
        return true;
    }


    @Override
    public List<VisitorActivityStatistics> getVisitorDynamicInfoList(String visitorUuid, String sellerId) {
        if (StrUtil.isBlank(visitorUuid) || StrUtil.isBlank(sellerId)) {
            return EMPTY_LIST;
        }

        return baseMapper.getVisitorDynamicInfoList(visitorUuid, sellerId, ResourceType.ARTICLE.getCode());
    }

    @Override
    public List<VisitorActivityStatistics> getVisitorDynamicInfoListForLeader(
            String visitorUuid,
            List<String> sellerIdList) {
        if (StrUtil.isBlank(visitorUuid) || Collections.isEmpty(sellerIdList)) {
            return EMPTY_LIST;
        }

        return baseMapper.getVisitorDynamicInfoListForLeader(visitorUuid, ResourceType.ARTICLE.getCode(), sellerIdList);
    }

    @Override
    public IPage<CustomerListVO> customerListQuery(CustomerListQuery query) {
        return baseMapper.customerListQuery(new Page<>(query.getPage(), query.getLimit()), query);
    }

    @Override
    public CustomerInfoDetailVO getCustomerInfoDetailById(String id, String projectId, String sellerId) {
        return baseMapper.getCustomerInfoDetail(id, projectId, sellerId);
    }

    /**
     * 保存绑定的内容
     *
     * @param binding binding
     * @return boolean
     */
    private boolean insertCertainBind(SaveOrChangeBindDTO binding) {
        Visitor visitor = visitorService.getById(binding.getVisitorId());
        Optional.ofNullable(visitor).orElseThrow(() -> new BusinessException("访客信息不存在"));

        //添加绑定时必会产生一条员工访客记录,
        boolean result = saveOrChangeStaffVisitor(binding.getSellerId(), binding.getVisitorId(), visitor.getVisitorSource());
        if (!result) {
            log.error("添加员工访客信息时出错");
            return false;
        }

        BindingManager bindingManager = new BindingManager();
        bindingManager.setUserUuid(binding.getSellerId());
        bindingManager.setVisitorUuid(binding.getVisitorId());
        bindingManager.setProjectId(binding.getProjectId());
        //设置项目维度访客来源
        bindingManager.setSourceLabel(visitor.getVisitorSource());
        //设置初始跟办
        bindingManager.setOriginFollowUp(binding.getSellerId());

        return save(bindingManager);
    }

    /**
     * 添加员工管理的访客记录
     *
     * @return boolean
     */
    private boolean saveOrChangeStaffVisitor(String userId, String visitorUuid, Integer origin) {
        StaffVisitor staffVisitor = staffVisitorService.lambdaQuery().eq(StaffVisitor::getUserUuid, userId)
                .eq(StaffVisitor::getVisitorUuid, visitorUuid)
                .one();
        if (staffVisitor != null) {
            log.info("员工访客记录管理已经存在");
            return true;
        }
        Visitor visitor = visitorService.getById(visitorUuid);
        if (visitor == null) {
            log.error("visitor does not exist");
            throw new BusinessException("访客" + visitorUuid + "存在");
        }
        StaffVisitor sv = new StaffVisitor();

        //对于员工维护的访客信息初始化，具体内容有待进一步确认
        sv.setVisitorName(visitor.getNickName());
        sv.setVisitorPhone(visitor.getPhone());
        sv.setVisitorUuid(visitorUuid);
        sv.setVisitorSource(origin);
        sv.setUserUuid(userId);

        return staffVisitorService.saveOrUpdate(sv);
    }

    /**
     * 检查销售是否存在
     *
     * @param bindInfo 绑定对象
     * @return User
     */
    @NotNull
    private User checkUserExist(SaveOrChangeBindDTO bindInfo) {
        User user = userMapper.selectById(bindInfo.getSellerId());
        if (user == null) {
            throw new BusinessException("所绑定的销售" + bindInfo.getSellerId() + "不存在");
        }
        return user;
    }

    /**
     * 获取当前有效绑定
     *
     * @param bindInfo 绑定对象
     * @return BindingManager
     */
    private BindingManager getEffectiveBindingManager(SaveOrChangeBindDTO bindInfo) {
        return lambdaQuery()
                .eq(BindingManager::getVisitorUuid, bindInfo.getVisitorId())
                .eq(BindingManager::getProjectId, bindInfo.getProjectId())
                .eq(BindingManager::getHistoryLabel, 0)
                .one();
    }

    /**
     * 检查是否重复绑定
     *
     * @param bindInfo 绑定对象
     * @return boolean
     */
    private boolean checkRepeatBinding(SaveOrChangeBindDTO bindInfo) {
        //查询请求切换的绑定在库中是否存在,通过销售id,访客id和有效标示查询，记录要么唯一存在，要么不存在
        BindingManager bindingManager = lambdaQuery()
                .eq(BindingManager::getVisitorUuid, bindInfo.getVisitorId())
                .eq(BindingManager::getProjectId, bindInfo.getProjectId())
                .eq(BindingManager::getUserUuid, bindInfo.getVisitorId())
                .eq(BindingManager::getHistoryLabel, 0).one();
        if (bindingManager != null) {
            log.warn("重复绑定直接返回");
            return true;
        }
        return false;
    }

    /**
     * 系统切换绑定
     *
     * @param bindInfo 绑定对象
     * @param project  项目
     */
    private void systemChange(SaveOrChangeBindDTO bindInfo, Project project) {
        //检查销售是否存在
        User user = checkUserExist(bindInfo);

        //检查是否重复绑定
        if (checkRepeatBinding(bindInfo)) {
            return;
        }

        //查看访客对于该项目是不是有当前有效记录
        BindingManager checkFirst = getEffectiveBindingManager(bindInfo);
        if (checkFirst != null) {
            //系统切换绑定
            systemChangeBinding(bindInfo, user, project, checkFirst);
        } else {
            //系统新增绑定
            insertCertainBind(bindInfo);
        }
    }

    /**
     * 系统设置跟办
     *
     * @param bindInfo   绑定参数
     * @param user       员工
     * @param project    项目
     * @param checkFirst 历史绑定
     */
    private void systemChangeBinding(SaveOrChangeBindDTO bindInfo, User user, Project project, BindingManager checkFirst) {
        String sellerId = bindInfo.getSellerId();
        String visitorId = bindInfo.getVisitorId();
        String oldSellerId = checkFirst.getUserUuid();

        //更新当前有效记录为历史绑定
        updateBinding(bindInfo, checkFirst, sellerId, visitorId);

        //保存系统操作日志
        saveSystemOperationLog(user, project, visitorId, oldSellerId);
    }

    /**
     * 保存系统操作日志
     *
     * @param user        员工
     * @param project     项目
     * @param visitorId   访客id
     * @param oldSellerId 原销售id
     */
    private void saveSystemOperationLog(User user, Project project, String visitorId, String oldSellerId) {
        //采集日志添加
        CollectorAllRecord collectorAllRecord = new CollectorAllRecord();
        collectorAllRecord.setVisitorId(visitorId);
        collectorAllRecord.setVisitorContent(
                "系统将您关注的项目 " + project.getProjectTitle()
                        + " 销售切换为 " + user.getNickName());
        collectorAllRecordService.save(collectorAllRecord);

        //系统操作日志记录
        saveUserLogService.saveChangeBindLog(oldSellerId, visitorId, project, this::acceptSystemChange);

        //客户日志处理
        logService.recordVisitorLog(visitorId,
                "系统把您关注的" + project.getProjectTitle()
                        + "项目跟办员工更换为" + user.getNickName());
    }

    /**
     * 转跟办业务处理
     *
     * @param bindInfo 绑定参数
     * @param project  项目
     */
    private void getChangeBindingVO(SaveOrChangeBindDTO bindInfo, Project project) {
        //检查销售是否存在
        User user = checkUserExist(bindInfo);

        //检查是否重复绑定
        if (checkRepeatBinding(bindInfo)) {
            return;
        }

        //查看访客对于该项目是不是有当前有效记录
        BindingManager checkFirst = getEffectiveBindingManager(bindInfo);
        if (checkFirst != null) {
            //用户切换绑定
            changeBinding(bindInfo, user, project, checkFirst);
        } else {
            //用户新增绑定
            saveBinding(bindInfo, user, project);
        }
    }

    /**
     * 切换跟办
     *
     * @param bindInfo   跟办参数
     * @param user       员工
     * @param project    项目
     * @param checkFirst 是否当前有效
     */
    private void changeBinding(SaveOrChangeBindDTO bindInfo, User user, Project project, BindingManager checkFirst) {
        String sellerId = bindInfo.getSellerId();
        String visitorId = bindInfo.getVisitorId();
        updateBinding(bindInfo, checkFirst, sellerId, visitorId);

        //保存切换绑定日志
        saveChangeBindingLog(user, project, checkFirst, sellerId, visitorId);
    }

    /**
     * 保存切换绑定日志
     *
     * @param user       员工
     * @param project    项目
     * @param checkFirst 原绑定
     * @param sellerId   销售id
     * @param visitorId  访客id
     */
    private void saveChangeBindingLog(User user, Project project, BindingManager checkFirst, String sellerId, String visitorId) {
        logService.recordVisitorLog(visitorId,
                "您把 " + project.getProjectTitle()
                        + "项目的跟办员工更换为 " + user.getNickName());

        //采集日志添加
        CollectorAllRecord collectorAllRecord = new CollectorAllRecord();
        collectorAllRecord.setVisitorId(visitorId);
        collectorAllRecord.setVisitorContent("切换项目 " + project.getProjectTitle()
                + " 销售为 " + user.getNickName());
        collectorAllRecordService.save(collectorAllRecord);

        saveUserLogService.saveChangeBindLog(checkFirst.getUserUuid(), visitorId, project, this::acceptAlreadyChange);

        saveUserLogService.saveChangeBindLog(sellerId, visitorId, project, this::acceptAutoChange);
    }

    /**
     * 更新绑定
     *
     * @param bindInfo   绑定对象
     * @param checkFirst 历史绑定
     * @param sellerId   销售id
     * @param visitorId  访客id
     */
    private void updateBinding(SaveOrChangeBindDTO bindInfo,
                               BindingManager checkFirst,
                               String sellerId,
                               String visitorId) {
        //更新当前有效记录为历史绑定
        checkFirst.setHistoryLabel(checkFirst.getId());
        this.updateById(checkFirst);

        //检查该用户对于该项目对于该员工是否有历史绑定,有则置为有效，相当于切换回之前记录
        BindingManager historyRecord = lambdaQuery()
                .eq(BindingManager::getVisitorUuid, visitorId)
                .eq(BindingManager::getProjectId, bindInfo.getProjectId())
                .eq(BindingManager::getUserUuid, sellerId)
                .one();
        checkFirst.setId(historyRecord == null ? null : historyRecord.getId());
        checkFirst.setHistoryLabel(0);
        checkFirst.setUserUuid(sellerId);
        //暂时只维持最新的转办次数记录
        checkFirst.setTransfersNumber(checkFirst.getTransfersNumber() + 1);
        this.saveOrUpdate(checkFirst);

        //将访客添加到员工管理访客列表中
        saveOrChangeStaffVisitor(sellerId, visitorId, checkFirst.getSourceLabel());
    }

    /**
     * 添加绑定及日志
     *
     * @param bindInfo 绑定参数
     * @param user     员工
     * @param project  项目
     */
    private void saveBinding(SaveOrChangeBindDTO bindInfo, User user, Project project) {
        if (insertCertainBind(bindInfo)) {
            saveBindingLog(bindInfo, user, project);
        }
    }

    /**
     * 保存绑定日志
     *
     * @param bindInfo 绑定对象
     * @param user     员工
     * @param project  项目
     */
    private void saveBindingLog(SaveOrChangeBindDTO bindInfo, User user, Project project) {
        String visitorId = bindInfo.getVisitorId();
        String userUuid = user.getUuid();
        String userName = user.getNickName();
        String projectTitle = project.getProjectTitle();
        Visitor visitor = visitorService.lambdaQuery()
                .eq(Visitor::getUuid, visitorId)
                .one();
        Recommend recommend = recommendService.lambdaQuery()
                .eq(Recommend::getTargetUuid, visitorId)
                .eq(Recommend::getProjectId, bindInfo.getProjectId())
                .one();

        //访客来源
        Integer visitorSource = visitor.getVisitorSource();

        //访客日志采集
        saveVisitorLog(visitorId, userUuid, userName, projectTitle, recommend, visitorSource);

        //推荐日志采集
        saveRecommendLog(project, visitorId, userUuid, userName, projectTitle, visitorSource);
    }

    private void saveVisitorLog(String visitorId,
                                String userUuid,
                                String userName,
                                String projectTitle,
                                Recommend recommend,
                                Integer visitorSource) {
        logService.recordVisitorLog(visitorId,
                "您首次关注员工" + userName + "的"
                        + projectTitle + "项目空间（" + getOrigin(visitorSource) + "）");

        //采集日志添加
        CollectorAllRecord collectorAllRecord = new CollectorAllRecord();
        collectorAllRecord.setSellerId(userUuid);
        collectorAllRecord.setVisitorId(visitorId);
        collectorAllRecord.setOrigin(visitorSource);
        collectorAllRecord.setVisitorContent(
                "他通过" + getOrigin(visitorSource)
                        + "关注了员工 " + userName + "的"
                        + projectTitle + " 项目空间"
                        + (recommend == null ? "" : "，属" + getRecommendType(recommend.getRecommendType())));
        collectorAllRecordService.save(collectorAllRecord);
    }

    private void saveRecommendLog(Project project,
                                  String visitorId,
                                  String userUuid,
                                  String userName,
                                  String projectTitle,
                                  Integer visitorSource) {
        List<Recommend> list = recommendService.lambdaQuery()
                .eq(Recommend::getTargetUuid, visitorId)
                .eq(Recommend::getRecommendType, 0)
                .eq(Recommend::getProjectId, project.getId())
                .list();
        if (CollectionUtils.isNotEmpty(list)) {
            list.parallelStream().forEach(l -> {
                String sourceUuid = l.getSourceUuid();
                Integer promotionType = l.getPromotionType();
                String resourceTitle = Optional.ofNullable(resourceService.getById(l.getResourceId()))
                        .map(Resource::getResourceTitle)
                        .orElse("");
                String visitorName = Optional.ofNullable(visitorService.getById(visitorId))
                        .map(Visitor::getNickName)
                        .orElse("");

                String content = "您亲友" + visitorName
                        + "通过您分享的" + getOrigin(promotionType) + " " + resourceTitle + " "
                        + "关注了员工" + userName + "的 " + projectTitle + " 项目空间（"
                        + getOrigin(visitorSource) + "）";
                Optional.ofNullable(userMapper.selectById(sourceUuid))
                        .ifPresent(u -> logService.recordVisitorLog(u.getUuid(), content));

                CollectorAllRecord record = new CollectorAllRecord();
                record.setSellerId(userUuid);
                record.setVisitorId(sourceUuid);
                record.setOrigin(visitorSource);
                record.setVisitorContent(
                        "他向亲友" + visitorName
                                + "成功推荐了员工" + userName + "的"
                                + projectTitle + " 项目空间（分享"
                                + getOrigin(promotionType) + "）");
                collectorAllRecordService.save(record);
            });
        }
    }

    /**
     * 根据类型获取分享途径
     *
     * @param type 类型
     * @return 分享途径
     */
    private String getOrigin(Integer type) {
        String str;
        switch (type) {
            case 1:
                str = "微楼书";
                break;
            case 2:
                str = "微文章";
                break;
            case 3:
                str = "微海报";
                break;
            case 4:
                str = "小程序";
                break;
            case 5:
                str = "专属码";
                break;
            case 6:
                str = "其他项";
                break;
            default:
                str = "";
                break;
        }
        return str;
    }

    /**
     * 获取拓客类别
     *
     * @param type 类型
     * @return java.lang.String
     */
    private String getRecommendType(Integer type) {
        String str;
        switch (type) {
            case 0:
                str = "客户转介";
                break;
            case 1:
                str = "员工拓展";
                break;
            default:
                str = "";
                break;
        }
        return str;
    }

    /**
     * 设置销售id
     *
     * @param saveOrChangeBindDTO dto
     * @param project             项目
     */
    private void setSellerId(SaveOrChangeBindDTO saveOrChangeBindDTO, Project project) {
        if (StrUtil.isBlank(saveOrChangeBindDTO.getSellerId())) {
            Optional.ofNullable(project).map(p -> {
                saveOrChangeBindDTO.setSellerId(p.getDefaultSeller() == null
                        ? Constants.DEFAULT_SELLER_UUID
                        : p.getDefaultSeller());
                return p;
            }).orElseThrow(() -> new BusinessException("项目信息不存在"));
        }
    }

    /**
     * 批量转跟办
     *
     * @param bindInfo   对象
     * @param project    项目
     * @param visitorIds 客户列表
     * @param sellerIds  销售列表
     */
    private void batchChangeBind(SaveOrChangeBindDTO bindInfo, Project project, List<String> visitorIds, List<String> sellerIds) {
        List<Map> maps;
        synchronized (this) {
            maps = AverageDataUtil.averageData(visitorIds, sellerIds);
        }
        Optional.of(maps).ifPresent(m -> {
            m.forEach(v -> {
                for (Object o : v.entrySet()) {
                    Map.Entry next = (Map.Entry) o;
                    String key = (String) next.getKey();
                    List<String> value = (List<String>) next.getValue();
                    if (StrUtil.isNotBlank(key)) {
                        Optional.ofNullable(value).ifPresent(s -> s.forEach(i -> {
                            bindInfo.setSellerId(key);
                            bindInfo.setVisitorId(i);
                            systemChange(bindInfo, project);
                        }));
                    }
                }
            });
            //保存系统操作日志
            saveAdminLogService.saveSystemLog(getContent(m));
        });
    }

    @NotNull
    private String acceptSystemChange(Visitor visitor, User user, Project project) {
        return "您的客户" + visitor.getNickName()
                + "被系统转跟办到" + project.getProjectTitle()
                + "项目的员工" + user.getNickName() + "名下";
    }

    @NotNull
    private String acceptAlreadyChange(Visitor visitor, User user, Project project) {
        return "您的客户" + visitor.getNickName()
                + "之前已关注员工" + user.getNickName() + "的"
                + project.getProjectTitle() + "项目空间，请知悉";
    }

    @NotNull
    private String acceptAutoChange(Visitor visitor, User user, Project project) {
        return "您的客户" + visitor.getNickName()
                + "已自行转跟办到" + project.getProjectTitle()
                + "项目的员工" + user.getNickName() + "名下";
    }

    private String getContent(List<Map> maps) {
        return "批量设置了客户跟办员工，分配方案：" + maps.toString();
    }

}
