package cn.ideamake.bgyjssq.service.impl;

import cn.hutool.json.JSONObject;
import cn.ideamake.bgyjssq.dao.mapper.CollectorResourceMapper;
import cn.ideamake.bgyjssq.pojo.dto.ArticleReadPicCountDTO;
import cn.ideamake.bgyjssq.pojo.entity.CollectorResource;
import cn.ideamake.bgyjssq.pojo.query.DashBoardQuery;
import cn.ideamake.bgyjssq.pojo.vo.dashboard.ArticleDataPieVO;
import cn.ideamake.bgyjssq.pojo.vo.dashboard.ArticleDataVO;
import cn.ideamake.bgyjssq.service.ICollectorResourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 资源采集 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-31
 */
@Service
public class CollectorResourceServiceImpl extends ServiceImpl<CollectorResourceMapper, CollectorResource> implements ICollectorResourceService {

    @Override
    public List<ArticleDataVO> getArticleDataLineChart(DashBoardQuery query) {
        List<ArticleDataVO> list = baseMapper.getArticleDataLineChart(query);
        list.addAll(baseMapper.getShareDataLineChart(query));
        return list;
    }

    @Override
    public JSONObject getArticleDataPieChart(DashBoardQuery query) {
        // 查阅读次数
        List<ArticleDataPieVO> readNumber = baseMapper.getReadNumber(query);

        // 查阅读人次
        ArticleReadPicCountDTO readPicDTO = Optional.ofNullable(baseMapper.getReadCount(query))
                .orElse(new ArticleReadPicCountDTO(0, 0));
        int allCount = readPicDTO.getSellerCount() + readPicDTO.getVisitorCount();
        ArticleDataPieVO sellerPic = new ArticleDataPieVO("员工");
        ArticleDataPieVO visitorPic = new ArticleDataPieVO("客户");
        if (allCount != 0) {
            DecimalFormat df = new DecimalFormat("0.00");
            // 员工
            sellerPic = sellerPic.setCount(readPicDTO.getSellerCount())
                    .setPercent(Double.valueOf(df.format((double) readPicDTO.getSellerCount() / allCount)));
            // 客户
            visitorPic.setCount(readPicDTO.getVisitorCount())
                    .setPercent(Double.valueOf(df.format((double) readPicDTO.getVisitorCount() / allCount)));
        }

        //暂定分享次数为分享资源的次数，从im_share表中查询
        List<ArticleDataPieVO> shareNumber = baseMapper.getShareNumber(query);

        //查询分享人数
        ArticleReadPicCountDTO shareCountDTO = Optional.ofNullable(baseMapper.getShareCount(query))
                .orElse(new ArticleReadPicCountDTO(0, 0));
        int allShareCount = shareCountDTO.getSellerCount() + shareCountDTO.getVisitorCount();
        ArticleDataPieVO sellerSharePic = new ArticleDataPieVO("员工");
        ArticleDataPieVO visitorSharePic = new ArticleDataPieVO("客户");
        if (allShareCount != 0) {
            DecimalFormat df = new DecimalFormat("0.00");
            // 员工
            sellerSharePic = sellerSharePic.setCount(shareCountDTO.getSellerCount())
                    .setPercent(Double.valueOf(df.format((double) shareCountDTO.getSellerCount() / allShareCount)));
            // 客户
            visitorSharePic = visitorSharePic.setCount(shareCountDTO.getVisitorCount())
                    .setPercent(Double.valueOf(df.format((double) shareCountDTO.getVisitorCount() / allShareCount)));
        }

        ArrayList<ArticleDataPieVO> articleDataPieList = new ArrayList<>();
        articleDataPieList.add(sellerPic);
        articleDataPieList.add(visitorPic);

        ArrayList<ArticleDataPieVO> shareDataPieList = new ArrayList<>();
        shareDataPieList.add(sellerSharePic);
        shareDataPieList.add(visitorSharePic);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("readPv", readNumber);
        jsonObject.put("readUv", articleDataPieList);
        jsonObject.put("sharePv", shareNumber);
        jsonObject.put("shareUv", shareDataPieList);

        return jsonObject;
    }
}
