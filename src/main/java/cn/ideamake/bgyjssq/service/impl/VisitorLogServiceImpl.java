package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.dao.mapper.VisitorLogMapper;
import cn.ideamake.bgyjssq.pojo.entity.VisitorLog;
import cn.ideamake.bgyjssq.service.IVisitorLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户日志记录表 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-06-20
 */
@Service
public class VisitorLogServiceImpl extends ServiceImpl<VisitorLogMapper, VisitorLog> implements IVisitorLogService {

}
