package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.dao.mapper.UserPushActivityMapper;
import cn.ideamake.bgyjssq.pojo.entity.UserPushActivity;
import cn.ideamake.bgyjssq.pojo.vo.PushActivitisVO;
import cn.ideamake.bgyjssq.service.IUserPushActivityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-08-09
 */
@Service
public class UserPushActivityServiceImpl extends ServiceImpl<UserPushActivityMapper, UserPushActivity> implements IUserPushActivityService {
    @Override
    public boolean isExist(PushActivitisVO pushActivitisVO){
        List<PushActivitisVO> uerPushActivity = baseMapper.isExist(pushActivitisVO);
        return !uerPushActivity.isEmpty();
    }
}
