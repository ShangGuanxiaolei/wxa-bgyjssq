package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.pojo.entity.SignInQrCodePushUser;
import cn.ideamake.bgyjssq.dao.mapper.SignInQrCodePushUserMapper;
import cn.ideamake.bgyjssq.service.ISignInQrCodePushUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 签到二维码推送员工列表 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-09-16
 */
@Service
public class SignInQrCodePushUserServiceImpl extends ServiceImpl<SignInQrCodePushUserMapper, SignInQrCodePushUser> implements ISignInQrCodePushUserService {

}
