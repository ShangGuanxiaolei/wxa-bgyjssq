package cn.ideamake.bgyjssq.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.util.Constants;
import cn.ideamake.bgyjssq.common.util.DistanceUtils;
import cn.ideamake.bgyjssq.common.util.TenCentMapUtil;
import cn.ideamake.bgyjssq.dao.mapper.BindingManagerMapper;
import cn.ideamake.bgyjssq.dao.mapper.CollectorAllRecordMapper;
import cn.ideamake.bgyjssq.dao.mapper.UserMapper;
import cn.ideamake.bgyjssq.pojo.dto.CollectForAllParamDTO;
import cn.ideamake.bgyjssq.pojo.entity.BindingManager;
import cn.ideamake.bgyjssq.pojo.entity.CollectorAllRecord;
import cn.ideamake.bgyjssq.pojo.entity.CollectorResource;
import cn.ideamake.bgyjssq.pojo.entity.CollectorVisitor;
import cn.ideamake.bgyjssq.pojo.entity.Project;
import cn.ideamake.bgyjssq.pojo.entity.Resource;
import cn.ideamake.bgyjssq.pojo.entity.Resource720;
import cn.ideamake.bgyjssq.pojo.entity.User;
import cn.ideamake.bgyjssq.pojo.query.StaffVisitorRankQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorTrendQuery;
import cn.ideamake.bgyjssq.pojo.vo.StaffVisitorRankVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorTrendVO;
import cn.ideamake.bgyjssq.service.ICollectorAllRecordService;
import cn.ideamake.bgyjssq.service.ICollectorResourceService;
import cn.ideamake.bgyjssq.service.ICollectorVisitorService;
import cn.ideamake.bgyjssq.service.IProjectService;
import cn.ideamake.bgyjssq.service.IResource720Service;
import cn.ideamake.bgyjssq.service.IResourceService;
import cn.ideamake.bgyjssq.service.IUserGroupManagerService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 用户行为全量记录，用于销售对用户行为的观测 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@AllArgsConstructor
@Service
@Slf4j
public class CollectorAllRecordServiceImpl extends ServiceImpl<CollectorAllRecordMapper, CollectorAllRecord> implements ICollectorAllRecordService {

    private final ICollectorResourceService collectorResourceService;

    private final ICollectorVisitorService collectorVisitorService;

    private final IUserGroupManagerService userGroupManagerService;

    private final IResourceService resourceService;

    private final IResource720Service resource720Service;

    private final IProjectService projectService;

    private final UserMapper userMapper;

    private final BindingManagerMapper bindingManagerMapper;

    private static final int ALL_RECORD_LOG_LABEL = 1;

    private static final int VISITOR_RECORD_LOG_LABEL = 2;

    private static final int RESOURCE_RECORD_LOG_LABEL = 3;

    @Override
    @Async("CollectorTaskExecutor")
    public void collectData(CollectForAllParamDTO collectForAllParamDTO) {
        Optional.ofNullable(collectForAllParamDTO.getType()).orElseThrow(() -> new BusinessException("日志内容错误"));
        switch (collectForAllParamDTO.getType()) {
            case ALL_RECORD_LOG_LABEL:
                saveCollectorAllRecord(collectForAllParamDTO);
                break;
            case VISITOR_RECORD_LOG_LABEL:
                saveCollectorVisitor(collectForAllParamDTO);
                break;
            case RESOURCE_RECORD_LOG_LABEL:
                saveCollectorResource(collectForAllParamDTO);
                break;
            default:
                log.error("上报类型错误");
                break;
        }
    }

    private void saveCollectorResource(CollectForAllParamDTO collectForAllParamDTO) {
        String userId = collectForAllParamDTO.getUserId();
        String uuid = collectForAllParamDTO.getUuid();
        //拦截脏数据入库
        if (null == collectForAllParamDTO.getResourceId()) {
            log.error("访问的文章id为空");
            return;
        }
        if (null == userId || null == collectForAllParamDTO.getUserType()) {
            log.error("访问的用户id或类型为空");
            return;
        }

        CollectorResource collectorResourceDataBase = new CollectorResource();
        BeanUtils.copyProperties(collectForAllParamDTO, collectorResourceDataBase);
        collectorResourceDataBase.setRequestId(uuid);
        collectorResourceService.save(collectorResourceDataBase);
    }

    private void saveCollectorVisitor(CollectForAllParamDTO collectForAllParamDTO) {
        String uuid = collectForAllParamDTO.getUuid();
        String visitorId = collectForAllParamDTO.getVisitorId();
        //拦截脏数据入库
        Integer projectId = collectForAllParamDTO.getProjectId();
        if (null == projectId || null == uuid) {
            log.error("访问的项目id或者uuid为空:{}", collectForAllParamDTO);
            return;
        }
        if (null == visitorId || null == collectForAllParamDTO.getSellerId()) {
            log.error("访问的用户和销售id都为空{}:", collectForAllParamDTO);
            return;
        }

        CollectorVisitor collectorVisitorRep = collectorVisitorService.lambdaQuery()
                .eq(CollectorVisitor::getRequestVisitId, uuid)
                .one();
        if (null == collectorVisitorRep) {
            log.info("未找到上报记录，插入新记录,{}", visitorId);
            collectorVisitorRep = new CollectorVisitor();
            BeanUtils.copyProperties(collectForAllParamDTO, collectorVisitorRep);
            collectorVisitorRep.setRequestVisitId(uuid);
            // 有经纬度的情况,调用腾讯地图获取地址信息保存
            saveAddress(collectForAllParamDTO, collectorVisitorRep);
            collectorVisitorService.save(collectorVisitorRep);
        } else {
            log.info("有效访问{}再次提交,累加访问时间", uuid);
            Integer stayTime = Optional.ofNullable(collectForAllParamDTO.getStayTime())
                    .orElse(1);
            Integer repStayTime = Optional.ofNullable(collectorVisitorRep.getStayTime())
                    .orElse(1);
            collectorVisitorRep.setStayTime(stayTime > 1 ? stayTime : (stayTime + repStayTime));
            collectorVisitorRep.setCreateAt(LocalDateTime.now());
            // 有经纬度的情况,调用腾讯地图获取地址信息保存
            saveAddress(collectForAllParamDTO, collectorVisitorRep);
            collectorVisitorService.updateById(collectorVisitorRep);
        }

        //到访处理
        accessHandle(collectForAllParamDTO, visitorId, projectId);
    }

    /**
     * 到访次数记录逻辑处理
     *
     * @param collectForAllParamDTO dto对象
     * @param visitorId             访客id
     * @param projectId             项目id
     */
    private void accessHandle(CollectForAllParamDTO collectForAllParamDTO, String visitorId, Integer projectId) {
        String collectLongitude = collectForAllParamDTO.getLongitude();
        String collectLatitude = collectForAllParamDTO.getLatitude();
        if (StringUtils.isBlank(collectLongitude) && StringUtils.isBlank(collectLatitude)) {
            return;
        }

        Project project = projectService.lambdaQuery()
                .eq(Project::getId, projectId)
                .eq(Project::getDeleteMark, 0)
                .one();
        if (project == null) {
            return;
        }

        String projectLongitude = project.getLongitude();
        String projectLatitude = project.getLatitude();
        Double radius = project.getRadius();
        if (StringUtils.isBlank(projectLatitude)
                || StringUtils.isBlank(projectLongitude)
                || radius == null) {
            return;
        }

        DistanceUtils.Coordinate collectCoordinate = new DistanceUtils.Coordinate(
                Double.valueOf(collectLongitude), Double.valueOf(collectLatitude));
        DistanceUtils.Coordinate projectCoordinate = new DistanceUtils.Coordinate(
                Double.valueOf(projectLongitude), Double.valueOf(projectLatitude));
        double distance = DistanceUtils.getDistance(collectCoordinate, projectCoordinate);
        //在指定范围内，上报到访数据
        if (distance <= radius) {
            QueryWrapper<BindingManager> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda()
                    .eq(BindingManager::getVisitorUuid, visitorId)
                    .eq(BindingManager::getHistoryLabel, 0)
                    .eq(BindingManager::getProjectId, projectId);
            Optional.ofNullable(bindingManagerMapper.selectOne(queryWrapper)).ifPresent(b -> {
                b.setVisitorCount(b.getVisitorCount() + 1);
                b.setFirstVisitTime(LocalDateTime.now());
                bindingManagerMapper.update(b, queryWrapper);
            });
        }
    }

    /**
     * 保存地址信息
     *
     * @param collectForAllParamDTO 前端传递对象
     * @param collectorVisitorRep   数据对象
     */
    private void saveAddress(CollectForAllParamDTO collectForAllParamDTO, CollectorVisitor collectorVisitorRep) {
        String latitude = collectForAllParamDTO.getLatitude();
        String longitude = collectForAllParamDTO.getLongitude();
        if (StrUtil.isNotBlank(latitude) && StrUtil.isNotBlank(longitude)) {
            collectorVisitorRep.setLatitude(latitude);
            collectorVisitorRep.setLongitude(longitude);
            String address = TenCentMapUtil.getAddress(Double.valueOf(latitude), Double.valueOf(longitude));
            if (StrUtil.isNotBlank(address)) {
                collectorVisitorRep.setAddress(address);
            }
        }
    }

    private void saveCollectorAllRecord(CollectForAllParamDTO dto) {
        String visitorId = dto.getVisitorId();
        String uuid = dto.getUuid();
        String sellerId = dto.getSellerId();
        Integer resourceId = dto.getResourceId();

        if (StringUtils.isBlank(sellerId)) {
            log.error("销售id没有传递，资源id：{},访客id：{}, 请求id：{}",
                    resourceId, visitorId, uuid);
            return;
        }

        final StringBuilder str = getVisitorContent(dto, visitorId, uuid, sellerId, resourceId);

        saveCollectAllRecord(dto, uuid, str);
    }

    private StringBuilder getVisitorContent(
            CollectForAllParamDTO dto, String visitorId, String uuid, String sellerId, Integer resourceId) {
        String visitorContent = Optional.ofNullable(dto.getVisitorContent())
                .map(s -> dto.getVisitorContent())
                .orElse("");
        String userName = Optional.ofNullable(userMapper.selectById(sellerId))
                .map(User::getNickName)
                .orElse("");
        final StringBuilder str = new StringBuilder("他查看了员工" + userName + "的" + visitorContent);

        if (StringUtils.isBlank(visitorContent)) {
            log.error("没有传递访问内容，访客id：{}, 请求id：{}", visitorId, uuid);
            String projectName = Optional.ofNullable(projectService.lambdaQuery()
                    .eq(Project::getId, dto.getProjectId())
                    .one())
                    .map(Project::getProjectTitle)
                    .orElse("项目");
            str.append(projectName);
            Optional.ofNullable(dto.getResourceType()).ifPresent(t -> {
                String resourceTitle;
                //4代表楼书
                int type = 4;
                if (t == type) {
                    resourceTitle = Optional.ofNullable(resource720Service.lambdaQuery()
                            .eq(Resource720::getId, resourceId)
                            .one())
                            .map(Resource720::getName)
                            .orElse("楼书");

                } else {
                    resourceTitle = Optional.ofNullable(resourceService.lambdaQuery()
                            .eq(Resource::getId, resourceId)
                            .one())
                            .map(Resource::getResourceTitle)
                            .orElse("");
                }
                str.append(" ").append(resourceTitle);
            });
        }
        return str;
    }

    private void saveCollectAllRecord(CollectForAllParamDTO dto, String uuid, StringBuilder str) {
        Integer stayTime = Optional.ofNullable(dto.getStayTime()).orElse(1);
        Optional.ofNullable(this.lambdaQuery()
                .eq(CollectorAllRecord::getRequestId, uuid)
                .one())
                .map(c -> {
                    c.setStayTime(stayTime > 1 ? stayTime : c.getStayTime() + stayTime);
                    return this.updateById(c);
                })
                .orElseGet(() -> {
                    CollectorAllRecord collectorAllRecord = new CollectorAllRecord();
                    BeanUtils.copyProperties(dto, collectorAllRecord);
                    collectorAllRecord.setRequestId(uuid);
                    collectorAllRecord.setVisitorContent(str.toString());
                    if (stayTime == 1) {
                        collectorAllRecord.setStayTime(RandomUtils.nextInt(10, 60));
                    }
                    return this.save(collectorAllRecord);
                });
    }

    @Override
    public IPage<VisitorTrendVO> getVisitorTrend(VisitorTrendQuery visitorTrendQuery, List<String> sellerIdList) {
        return baseMapper.getVisitorTrend(
                new Page<>(visitorTrendQuery.getPage(), visitorTrendQuery.getLimit()),
                visitorTrendQuery, sellerIdList);
    }

    @Override
    public IPage<StaffVisitorRankVO> getStaffVisitorRank(StaffVisitorRankQuery staffVisitorRankQuery) {
        if (staffVisitorRankQuery.getSearchTime() != null && staffVisitorRankQuery.getSearchTime() >= 0) {
            LocalDate now = LocalDate.now();
            Period p = Period.ofDays(0 - staffVisitorRankQuery.getSearchTime());
            LocalDate then = now.plus(p);
            staffVisitorRankQuery.setStartTime(then.toString());
            staffVisitorRankQuery.setEndTime(LocalDateTime.of(now, LocalTime.MAX).toString());
        }

        String sellerId = staffVisitorRankQuery.getSellerId();
        log.info("{}查询访客排名", sellerId);

        //获取销售列表
        List<String> sellerIds = userGroupManagerService.getSellerIdListAddMe(sellerId);
        staffVisitorRankQuery.setSellerIds(new HashSet<>(sellerIds));

        switch (staffVisitorRankQuery.getSearchType()) {
            case Constants.VISIT_ANALYSE.STAY_TIME:
                log.info("停留时间排名查询");
                if (Constants.USER_TYPE.STAFF.equals(staffVisitorRankQuery.getSearchUserType())) {
                    return baseMapper.getStaffRankForHigherUserStayTime(staffVisitorRankQuery.getPageInfo(), staffVisitorRankQuery);
                } else if (Constants.USER_TYPE.VISITOR.equals(staffVisitorRankQuery.getSearchUserType())) {
                    return baseMapper.getStaffVisitorRankForStayTime(staffVisitorRankQuery.getPageInfo(), staffVisitorRankQuery);
                }
                break;
            case Constants.VISIT_ANALYSE.RECOMMEND_PEOPLE:
                log.info("推荐人数排名查询");
                if (Constants.USER_TYPE.STAFF.equals(staffVisitorRankQuery.getSearchUserType())) {
                    return baseMapper.getStaffRankForHigherUserRecommendNumber(staffVisitorRankQuery.getPageInfo(), staffVisitorRankQuery);
                } else if (Constants.USER_TYPE.VISITOR.equals(staffVisitorRankQuery.getSearchUserType())) {
                    return baseMapper.getStaffVisitorRankForRecommend(staffVisitorRankQuery.getPageInfo(), staffVisitorRankQuery);
                }
                break;
            case Constants.VISIT_ANALYSE.SHARE_TIMES:
                log.info("分享次数排名查询");
                if (Constants.USER_TYPE.STAFF.equals(staffVisitorRankQuery.getSearchUserType())) {
                    return baseMapper.getStaffRankForHigherUserShareTimes(staffVisitorRankQuery.getPageInfo(), staffVisitorRankQuery);
                } else if (Constants.USER_TYPE.VISITOR.equals(staffVisitorRankQuery.getSearchUserType())) {
                    return baseMapper.getStaffVisitorRankForShareTimes(staffVisitorRankQuery.getPageInfo(), staffVisitorRankQuery);
                }
                break;
            case Constants.VISIT_ANALYSE.VISITED_TIMES:
                log.info("到访次数排名查询");
                if (Constants.USER_TYPE.STAFF.equals(staffVisitorRankQuery.getSearchUserType())) {
                    return baseMapper.getStaffRankForHigherUserVisitedTimes(staffVisitorRankQuery.getPageInfo(), staffVisitorRankQuery);
                } else if (Constants.USER_TYPE.VISITOR.equals(staffVisitorRankQuery.getSearchUserType())) {
                    return baseMapper.getStaffVisitorRankForVisitedTimes(staffVisitorRankQuery.getPageInfo(), staffVisitorRankQuery);
                }
                break;
            case Constants.VISIT_ANALYSE.LEFT_PHONE:
                log.info("留电人数排名查询");
                if (Constants.USER_TYPE.STAFF.equals(staffVisitorRankQuery.getSearchUserType())) {
                    return baseMapper.getStaffRankForHigherUserLeftPhone(staffVisitorRankQuery.getPageInfo(), staffVisitorRankQuery);
                } else if (Constants.USER_TYPE.VISITOR.equals(staffVisitorRankQuery.getSearchUserType())) {
                    return baseMapper.getStaffVisitorRankForLeftPhone(staffVisitorRankQuery.getPageInfo(), staffVisitorRankQuery);
                }
                break;
            default:
                break;
        }
        throw new BusinessException("查询类型错误");
    }

}
