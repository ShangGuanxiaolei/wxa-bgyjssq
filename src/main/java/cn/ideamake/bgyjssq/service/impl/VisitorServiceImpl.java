package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.common.util.Constants;
import cn.ideamake.bgyjssq.dao.mapper.VisitorMapper;
import cn.ideamake.bgyjssq.pojo.entity.Visitor;
import cn.ideamake.bgyjssq.pojo.query.DefaultQueryPage;
import cn.ideamake.bgyjssq.pojo.query.TopTenQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorLogQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorVisitAnalysisQuery;
import cn.ideamake.bgyjssq.pojo.vo.CustomerTopTenVO;
import cn.ideamake.bgyjssq.pojo.vo.FocusProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorLogVO;
import cn.ideamake.bgyjssq.pojo.vo.analysis.DataAnalysisDetailListVO;
import cn.ideamake.bgyjssq.pojo.vo.analysis.VisitorListVO;
import cn.ideamake.bgyjssq.service.IStaffVisitorService;
import cn.ideamake.bgyjssq.service.IVisitorService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 访客表 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
@AllArgsConstructor
@Service
public class VisitorServiceImpl extends ServiceImpl<VisitorMapper, Visitor> implements IVisitorService {

    private final IStaffVisitorService staffVisitorService;

    private static final List<String> CUSTOMER_TOP_TEN_SORT_COLUMNS = new ArrayList<>();

    static {
        CUSTOMER_TOP_TEN_SORT_COLUMNS.add("recommendNum");
        CUSTOMER_TOP_TEN_SORT_COLUMNS.add("phoneCount");
        CUSTOMER_TOP_TEN_SORT_COLUMNS.add("visitNum");
        CUSTOMER_TOP_TEN_SORT_COLUMNS.add("recommendCount");
        CUSTOMER_TOP_TEN_SORT_COLUMNS.add("microBookNum");
        CUSTOMER_TOP_TEN_SORT_COLUMNS.add("microTextNum");
    }

    @Override
    public IPage<VisitorLogVO> getVisitorLog(VisitorLogQuery visitorLogQuery) {
        return baseMapper.selectVisitorLog(
                new Page<>(visitorLogQuery.getPage(), visitorLogQuery.getLimit()),
                visitorLogQuery
        );
    }

    @Override
    public List<CustomerTopTenVO> selectCustomerTopTenList(TopTenQuery query) {

        // 默认采用推介总人数
        String sort = CUSTOMER_TOP_TEN_SORT_COLUMNS.get(0);
        if (null != query.getSort() && CUSTOMER_TOP_TEN_SORT_COLUMNS.contains(query.getSort())) {
            sort = query.getSort();
        }

        //暂时遍历10条取10次，后续有更好方式再优化
        return baseMapper.selectCustomerTopTenList(query, sort);
    }

    @Override
    public List<VisitorListVO> selectVisitorByUserId(VisitorVisitAnalysisQuery query) {
        List<String> list = new ArrayList<>();
        String sellerId = query.getSellerId();
        list.add(sellerId);
        if (Constants.DataType.CUSTOMER_RECOMMEND == query.getDataType()) {
            List<String> queryList = staffVisitorService.getQueryListByCollection(list);
            return baseMapper.selectVisitorByUserId(query, queryList);
        } else {
            return baseMapper.selectVisitorByUserId(query, list);
        }
    }

    @Override
    public IPage<FocusProjectVO> selectFocusProject(DefaultQueryPage page, String userId) {
        return baseMapper.selectFocusProject(page.getPageInfo(), userId);
    }

    @Override
    public DataAnalysisDetailListVO getVisitorVO(String uuid) {
        return baseMapper.getVisitorVO(uuid);
    }
}
