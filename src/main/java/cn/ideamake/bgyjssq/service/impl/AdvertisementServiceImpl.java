package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.dao.mapper.AdvertisementMapper;
import cn.ideamake.bgyjssq.pojo.dto.AdvertisementDTO;
import cn.ideamake.bgyjssq.pojo.entity.Advertisement;
import cn.ideamake.bgyjssq.pojo.query.AdvertisementQuery;
import cn.ideamake.bgyjssq.pojo.vo.AdvertisementVO;
import cn.ideamake.bgyjssq.service.IAdvertisementService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.jsonwebtoken.lang.Collections;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 广告实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-08-07
 */
@Service
public class AdvertisementServiceImpl extends ServiceImpl<AdvertisementMapper, Advertisement>
        implements IAdvertisementService {

    /**
     * 系统广告
     */
    private static final Integer ADVERTISEMENT_SYSTEM = 1;

    /**
     * 项目广告
     */
    private static final Integer ADVERTISEMENT_PROJECT = 2;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean addAdvertisement(AdvertisementDTO dto) {
        List<Integer> projects = dto.getProjects();
        Advertisement advertisement = new Advertisement();
        BeanUtils.copyProperties(dto, advertisement);
        if (Objects.equals(dto.getAdverType(), ADVERTISEMENT_SYSTEM)) {
            return this.save(advertisement);
        } else if (Objects.equals(dto.getAdverType(), ADVERTISEMENT_PROJECT)) {
            if (Collections.isEmpty(projects)) {
                throw new BusinessException("项目不能为空");
            }
            List<Advertisement> list = this.lambdaQuery()
                    .in(Advertisement::getProjectId, projects)
                    .list();
            if (!Collections.isEmpty(list)) {
                throw new BusinessException("该项目已有广告，请删除已有广告后重试！");
            }
            projects.forEach(p -> {
                advertisement.setProjectId(p);
                this.save(advertisement);
            });
            return true;
        } else {
            return false;
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean editAdvertisement(AdvertisementDTO dto) {
        Advertisement advertisement = new Advertisement();
        BeanUtils.copyProperties(dto, advertisement);
        if (Objects.equals(dto.getAdverType(), ADVERTISEMENT_SYSTEM)) {
            return this.updateById(advertisement);
        } else if (Objects.equals(dto.getAdverType(), ADVERTISEMENT_PROJECT)) {
            List<Integer> projects = dto.getProjects();
            if (Collections.isEmpty(projects)) {
                throw new BusinessException("项目不能为空");
            }
            advertisement.setProjectId(projects.get(0));
            return this.updateById(advertisement);
        } else {
            return false;
        }
    }

    @Override
    public IPage<AdvertisementVO> selectAdver(AdvertisementQuery advertisementQuery) {
        return baseMapper.selectByAdver(advertisementQuery.getPageInfo(), advertisementQuery);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean deleteAdver(List<String> ids) {
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public AdvertisementVO selectEffectiveAdver(Integer projectId) {
        //系统通知优先
        AdvertisementVO advertisementVO = baseMapper.selectEffectiveAdver(-1, ADVERTISEMENT_SYSTEM);
        if (advertisementVO != null) {
            return advertisementVO;
        }
        return baseMapper.selectEffectiveAdver(projectId, ADVERTISEMENT_PROJECT);
    }

}
