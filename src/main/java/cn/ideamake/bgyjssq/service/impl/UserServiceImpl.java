package cn.ideamake.bgyjssq.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.ideamake.bgyjssq.common.components.LogService;
import cn.ideamake.bgyjssq.common.config.PositionConstant;
import cn.ideamake.bgyjssq.common.enums.RestEnum;
import cn.ideamake.bgyjssq.common.exception.AuthenticationException;
import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.exception.InviteCodeGeneratorException;
import cn.ideamake.bgyjssq.common.exception.UserNotFoundException;
import cn.ideamake.bgyjssq.common.handler.UserInfoContext;
import cn.ideamake.bgyjssq.common.util.AliyunOssKit;
import cn.ideamake.bgyjssq.common.util.Constants;
import cn.ideamake.bgyjssq.common.util.JwtKit;
import cn.ideamake.bgyjssq.common.util.MyCollectionUtils;
import cn.ideamake.bgyjssq.common.util.WeChatService;
import cn.ideamake.bgyjssq.dao.mapper.BindingManagerMapper;
import cn.ideamake.bgyjssq.dao.mapper.Resource720Mapper;
import cn.ideamake.bgyjssq.dao.mapper.UserMapper;
import cn.ideamake.bgyjssq.pojo.bo.Audience;
import cn.ideamake.bgyjssq.pojo.bo.ProjectInfo;
import cn.ideamake.bgyjssq.pojo.bo.RefreshToken;
import cn.ideamake.bgyjssq.pojo.bo.SellerInfo;
import cn.ideamake.bgyjssq.pojo.bo.StaffInfoBO;
import cn.ideamake.bgyjssq.pojo.bo.Token;
import cn.ideamake.bgyjssq.pojo.bo.VisitorActivityStatistics;
import cn.ideamake.bgyjssq.pojo.bo.WXUserInfo;
import cn.ideamake.bgyjssq.pojo.dto.BussinessCardHomeEditDTO;
import cn.ideamake.bgyjssq.pojo.dto.CustomerManagerBindingDTO;
import cn.ideamake.bgyjssq.pojo.dto.CustomerManagerDTO;
import cn.ideamake.bgyjssq.pojo.dto.InviteCodeGenDTO;
import cn.ideamake.bgyjssq.pojo.dto.ProjectQRCodeDTO;
import cn.ideamake.bgyjssq.pojo.dto.ScanInviteCodeDTO;
import cn.ideamake.bgyjssq.pojo.dto.UserEditDTO;
import cn.ideamake.bgyjssq.pojo.dto.UserSaveDTO;
import cn.ideamake.bgyjssq.pojo.dto.VisitorSubmitPhoneDTO;
import cn.ideamake.bgyjssq.pojo.entity.BindingManager;
import cn.ideamake.bgyjssq.pojo.entity.CollectorAllRecord;
import cn.ideamake.bgyjssq.pojo.entity.CollectorResource;
import cn.ideamake.bgyjssq.pojo.entity.CollectorVisitor;
import cn.ideamake.bgyjssq.pojo.entity.OauthWechat;
import cn.ideamake.bgyjssq.pojo.entity.Project;
import cn.ideamake.bgyjssq.pojo.entity.ProjectManager;
import cn.ideamake.bgyjssq.pojo.entity.Recommend;
import cn.ideamake.bgyjssq.pojo.entity.Resource720;
import cn.ideamake.bgyjssq.pojo.entity.StaffVisitor;
import cn.ideamake.bgyjssq.pojo.entity.User;
import cn.ideamake.bgyjssq.pojo.entity.UserInfo;
import cn.ideamake.bgyjssq.pojo.entity.UserRegister;
import cn.ideamake.bgyjssq.pojo.entity.Visitor;
import cn.ideamake.bgyjssq.pojo.query.BusinessQRCodeQuery;
import cn.ideamake.bgyjssq.pojo.query.CustomerManagerQuery;
import cn.ideamake.bgyjssq.pojo.query.DigDataQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffKpiQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffListQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffVisitorQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffVisitorRankQuery;
import cn.ideamake.bgyjssq.pojo.query.UserProjectQuery;
import cn.ideamake.bgyjssq.pojo.query.UserVisitorProjectQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorInfoDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorTrendQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorVisitAnalysisQuery;
import cn.ideamake.bgyjssq.pojo.query.WeChatQRQuery;
import cn.ideamake.bgyjssq.pojo.vo.BusinessQRcodeVO;
import cn.ideamake.bgyjssq.pojo.vo.BussinessCardHomeVO;
import cn.ideamake.bgyjssq.pojo.vo.CustomerManagerVO;
import cn.ideamake.bgyjssq.pojo.vo.DigDataVO;
import cn.ideamake.bgyjssq.pojo.vo.InviteRegisterVO;
import cn.ideamake.bgyjssq.pojo.vo.ProjectQRCodeVO;
import cn.ideamake.bgyjssq.pojo.vo.ProjectSellerInfoVO;
import cn.ideamake.bgyjssq.pojo.vo.PushedActivityVO;
import cn.ideamake.bgyjssq.pojo.vo.SellerActivitisVO;
import cn.ideamake.bgyjssq.pojo.vo.SellerVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffKpiVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffListVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffNameVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffVisitorRankVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffVisitorVO;
import cn.ideamake.bgyjssq.pojo.vo.UserDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.UserProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.UserSessionVO;
import cn.ideamake.bgyjssq.pojo.vo.UserVisitorProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorInfoDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorTrendVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorVisitAnalysisVO;
import cn.ideamake.bgyjssq.pojo.vo.analysis.DataAnalysisDetailListVO;
import cn.ideamake.bgyjssq.pojo.vo.analysis.DataAnalysisDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.OriginCountCell;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.TotalStatistics;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.VisitPeriod;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.VisitTrend;
import cn.ideamake.bgyjssq.service.IBindingManagerService;
import cn.ideamake.bgyjssq.service.IClickRecordService;
import cn.ideamake.bgyjssq.service.ICollectorAllRecordService;
import cn.ideamake.bgyjssq.service.ICollectorResourceService;
import cn.ideamake.bgyjssq.service.ICollectorVisitorService;
import cn.ideamake.bgyjssq.service.IOauthWechatService;
import cn.ideamake.bgyjssq.service.IProjectManagerService;
import cn.ideamake.bgyjssq.service.IProjectService;
import cn.ideamake.bgyjssq.service.IRecommendService;
import cn.ideamake.bgyjssq.service.IShareService;
import cn.ideamake.bgyjssq.service.IStaffVisitorService;
import cn.ideamake.bgyjssq.service.IUserGroupManagerService;
import cn.ideamake.bgyjssq.service.IUserInfoService;
import cn.ideamake.bgyjssq.service.IUserRegisterService;
import cn.ideamake.bgyjssq.service.IUserService;
import cn.ideamake.bgyjssq.service.IVisitorService;
import cn.ideamake.bgyjssq.service.common.SaveUserLogService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static cn.ideamake.bgyjssq.common.util.Constants.HasGroup.HAS_GROUP;
import static cn.ideamake.bgyjssq.common.util.Constants.HasGroup.HAS_NOT_GROUP;
import static cn.ideamake.bgyjssq.common.util.Constants.REDIS_CACHE.REFRESH_TOKEN;
import static cn.ideamake.bgyjssq.common.util.Constants.REFRESH_TOKEN_VALIDITY_PERIOD;
import static cn.ideamake.bgyjssq.common.util.Constants.TOURIST_UUID;
import static cn.ideamake.bgyjssq.common.util.Constants.USER_TYPE.ADMIN;
import static cn.ideamake.bgyjssq.common.util.Constants.USER_TYPE.STAFF;
import static cn.ideamake.bgyjssq.common.util.Constants.USER_TYPE.VISITOR;
import static java.lang.Boolean.FALSE;
import static java.util.concurrent.TimeUnit.MINUTES;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
@AllArgsConstructor
@Service
@Slf4j
public class UserServiceImpl extends com.baomidou.mybatisplus.extension.service.impl.ServiceImpl<UserMapper, User> implements IUserService {

    private final IUserInfoService userInfoService;

    private final IVisitorService visitorService;

    private final IOauthWechatService oauthWeChatService;

    private final IProjectService projectService;

    private final IProjectManagerService projectManagerService;

    private final LogService logService;

    private final IUserRegisterService userRegisterService;

    private final Audience audience;

    private final BindingManagerMapper bindingManagerMapper;

    private final RedisTemplate<String, Object> redisTemplate;

    private final AliyunOssKit aliyunOssKit;

    private final WeChatService weChatService;

    private final ICollectorVisitorService collectorVisitorService;

    private final IRecommendService recommendService;

    private final IBindingManagerService bindingManagerService;

    private final IStaffVisitorService staffVisitorService;

    private final ICollectorAllRecordService collectorAllRecordService;

    private final IClickRecordService clickRecordService;

    private final IShareService shareService;

    private final ICollectorResourceService collectorResourceService;

    private final IUserGroupManagerService userGroupManagerService;

    private final SaveUserLogService saveUserLogService;

    private final Resource720Mapper resource720Mapper;

    /**
     * http前缀
     */
    private final static String HTTP = "http";

    /**
     * 手机号字段
     */
    private final static String PHONE = "phone";

    /**
     * 员工top10排序字段对应数据库表字段
     */
    private static final List<String> STAFF_TOP_TEN_SORT_COLUMNS = new ArrayList<>();

    static {
        STAFF_TOP_TEN_SORT_COLUMNS.add("phoneCount");
        STAFF_TOP_TEN_SORT_COLUMNS.add("recommendNum");
        STAFF_TOP_TEN_SORT_COLUMNS.add("microBookNum");
        STAFF_TOP_TEN_SORT_COLUMNS.add("codeNum");
        STAFF_TOP_TEN_SORT_COLUMNS.add("microTextNum");
        STAFF_TOP_TEN_SORT_COLUMNS.add("microPosterNum");
    }

    @Override
    public InviteRegisterVO getInviteRegisterCode(InviteCodeGenDTO inviteCodeGenDTO) {
        InviteRegisterVO inviteRegisterVO = new InviteRegisterVO();
        UserRegister userRegister = new UserRegister();
        userRegister.setOperatorUuid(UserInfoContext.get().getUserId());
        userRegister.setProjectList(inviteCodeGenDTO.getProjects().toString());
        userRegister.setValidateCode(randomCode());

        if (userRegisterService.save(userRegister)) {

            inviteRegisterVO.setInviteId(userRegister.getId());
            inviteRegisterVO.setValidateCode(userRegister.getValidateCode());

            WeChatQRQuery qrQuery = WeChatQRQuery.of().setScene("inviteId:" + userRegister.getId());
            inviteRegisterVO.setAppletQrCode("data:image/jpeg;base64,"
                    + Base64.getEncoder().encodeToString(weChatService.getAppletsCode(qrQuery)));

            return inviteRegisterVO;
        } else {
            throw new InviteCodeGeneratorException();
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean scanInviteRegisterCode(ScanInviteCodeDTO scanInviteCodeDTO) {
        String visitorId = UserInfoContext.get().getUserId();
        if (visitorId == null) {
            throw new AuthenticationException(RestEnum.TOKEN_EXPIRED);
        }
        //游客身份校验
        if (TOURIST_UUID.equals(visitorId)) {
            throw new BusinessException("游客不能注册为销售");
        }
        UserRegister userRegister = userRegisterService.getById(scanInviteCodeDTO.getInviteId());

        if (userRegister == null) {
            throw new BusinessException("邀请记录不存在");
        }

        log.info("通过邀请记录#{}#注册成功销售", visitorId);

        String phone = weChatService.decodeUserPhone(scanInviteCodeDTO.getPhone());
        User checkPhoneUser = lambdaQuery().eq(User::getPhone, phone).one();

        if (checkPhoneUser != null) {
            log.info("{}手机号验证通过", checkPhoneUser.getPhone());

            //登录必须经过微信授权，默认不为空，将微信信息身份做修改
            OauthWechat oauthWechat = oauthWeChatService.getUserWeChatInfoByCache(visitorId);

            //进入请求的前提是微信信息不为空，此处判断待定删除
            if (oauthWechat == null) {
                log.error("邀请注册获取用户{}微信信息，用户不存在", visitorId);
                throw new UserNotFoundException(visitorId);
            }
            if (changeUserType(userRegister, visitorId, phone, checkPhoneUser, oauthWechat)) {
                return true;
            }
        } else {
            log.error("验证手机号失败");
            throw new BusinessException("验证手机号失败");
        }
        return true;
    }

    @Override
    public boolean scanInviteRegisterCodeBackDoor(Integer inviteId) {
        String visitorId = UserInfoContext.get().getUserId();
        if (visitorId == null) {
            throw new AuthenticationException(RestEnum.TOKEN_EXPIRED);
        }

        UserRegister userRegister = userRegisterService.getById(inviteId);

        if (userRegister == null) {
            throw new BusinessException("邀请记录不存在");
        }

        log.info("通过邀请记录#{}#注册成功销售", visitorId);

        User checkPhoneUser = lambdaQuery().eq(User::getUuid, visitorId).one();

        //用户不是销售，处理逻辑
        if (checkPhoneUser == null) {

            //登录必须经过微信授权，默认不为空，将微信信息身份做修改
            OauthWechat oauthWechat = oauthWeChatService.getUserWeChatInfoByCache(visitorId);

            //进入请求的前提是微信信息不为空，此处判断待定删除
            if (oauthWechat == null) {
                log.error("邀请注册获取用户{}微信信息，用户不存在", visitorId);
                throw new UserNotFoundException(visitorId);
            }
            checkPhoneUser = new User();
            UserInfo userInfo = new UserInfo();
            checkPhoneUser.setNickName(oauthWechat.getNickName());
            checkPhoneUser.setAvatar(oauthWechat.getAvatarUrl());
            checkPhoneUser.setUuid(visitorId);
            checkPhoneUser.setName(oauthWechat.getNickName());
            userInfo.setUserUuid(visitorId);
            //保存员工信息
            if (this.save(checkPhoneUser)) {
                userInfoService.save(userInfo);
            }
            //更新微信用户类型
            oauthWechat.setUserType(STAFF);
            if (oauthWeChatService.updateUserWeChatInfoByIdByCache(oauthWechat) == null) {
                log.error("更新用户身份{}信息失败", oauthWechat);
                throw new BusinessException("更新微信信息失败");
            }
            //移除访客数据
            removeVisitorData(visitorId);
            //添加项目到对应的销售
            bindProjectToUser(visitorId, userRegister);
        }
        return true;
    }

    private void bindProjectToUser(String visitorId, UserRegister userRegister) {
        List<ProjectManager> list = new ArrayList<>();
        JSONArray jsonArray = JSON.parseArray(userRegister.getProjectList());
        jsonArray.forEach(projectId -> {
            ProjectManager pm = new ProjectManager();
            pm.setUserUuid(visitorId);
            pm.setProjectId((Integer) projectId);
            list.add(pm);
        });
        if (!projectManagerService.saveBatch(list)) {
            throw new BusinessException("添加分配项目失败");
        }
    }

    private void removeVisitorData(String visitorId) {
        visitorService.removeById(visitorId);
        removeCollectorAllRecord(visitorId);
        removeCollectorVisitor(visitorId);
        removeCollectorResource(visitorId);
        removeStaffVisitor(visitorId);
        removeBindManager(visitorId);
        removeRecommend(visitorId);
    }

    private void removeRecommend(String visitorId) {
        QueryWrapper<Recommend> queryWrapper6 = new QueryWrapper<>();
        queryWrapper6.lambda().eq(Recommend::getTargetUuid, visitorId);
        recommendService.remove(queryWrapper6);
    }

    private void removeBindManager(String visitorId) {
        QueryWrapper<BindingManager> queryWrapper5 = new QueryWrapper<>();
        queryWrapper5.lambda().eq(BindingManager::getVisitorUuid, visitorId);
        bindingManagerService.remove(queryWrapper5);
    }

    private void removeStaffVisitor(String visitorId) {
        QueryWrapper<StaffVisitor> queryWrapper4 = new QueryWrapper<>();
        queryWrapper4.lambda().eq(StaffVisitor::getVisitorUuid, visitorId);
        staffVisitorService.remove(queryWrapper4);
    }

    private void removeCollectorResource(String visitorId) {
        QueryWrapper<CollectorResource> queryWrapper3 = new QueryWrapper<>();
        queryWrapper3.lambda().eq(CollectorResource::getVisitorId, visitorId);
        collectorResourceService.remove(queryWrapper3);
    }

    private void removeCollectorVisitor(String visitorId) {
        QueryWrapper<CollectorVisitor> queryWrapper2 = new QueryWrapper<>();
        queryWrapper2.lambda().eq(CollectorVisitor::getVisitorId, visitorId);
        collectorVisitorService.remove(queryWrapper2);
    }

    private void removeCollectorAllRecord(String visitorId) {
        QueryWrapper<CollectorAllRecord> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.lambda().eq(CollectorAllRecord::getVisitorId, visitorId);
        collectorAllRecordService.remove(queryWrapper1);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean removeSellerById(String sellerId) {

        log.info("删除销售{}", sellerId);

        if (null == sellerId) {
            log.error("切换的销售id不能为空");
            throw new BusinessException("销售id为空");
        }

        //删除用户表信息
        if (!removeById(sellerId)) {
            log.error("删除销售表信息");
        }

        //删除用户信息表
        if (!userInfoService.removeById(sellerId)) {
            log.error("删除用户信息表");
        }
        //删除销售管理访客信息
        Map<String, Object> condition = new HashMap<>(1);
        condition.put("user_uuid", sellerId);
        if (!staffVisitorService.removeByMap(condition)) {
            log.error("删除员工管理访客信息失败");
        }

        //删除销售管理项目
        if (!projectManagerService.removeByMap(condition)) {
            log.error("删除员工管理访客信息失败");
        }

        //删除绑定信息
        if (!bindingManagerService.removeByMap(condition)) {
            log.error("删除该员工绑定表信息失败");
        }
        //删除微信表信息
        oauthWeChatService.cleanWeChatUserInfo(sellerId);

        //删除分享信息
        recommendService.deleteVisitorShareRecord(sellerId);

        //删除分享点击表记录

        return deleteUser(sellerId, "seller_id");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean removeVisitorById(String visitorId) {

        String s = "删除访客";
        log.info(s, visitorId);

        if (null == visitorId) {
            log.error("切换的销售id不能为空");
            throw new BusinessException("销售id为空");
        }

        //删除访客表信息
        if (!visitorService.removeById(visitorId)) {
            log.error("删除访客表信息");
        }

        //删除销售管理访客信息
        Map<String, Object> condition = new HashMap<>(1);
        condition.put("visitor_uuid", visitorId);
        if (!staffVisitorService.removeByMap(condition)) {
            log.error("删除员工管理访客表信息失败");
        }
        //删除分享信息
        recommendService.deleteVisitorShareRecord(visitorId);

        //删除绑定信息
        if (!bindingManagerService.removeByMap(condition)) {
            log.error("删除员工绑定表信息失败");
        }

        //删除微信表信息
        oauthWeChatService.cleanWeChatUserInfo(visitorId);

        return deleteUser(visitorId, "visitor_id");
    }

    @Override
    @Transactional(rollbackFor = BusinessException.class)
    public boolean visitorSubmitPhone(VisitorSubmitPhoneDTO visitorSubmitPhoneDTO) {
        //更新访客本身手机号
        Visitor visitor = visitorService.getById(visitorSubmitPhoneDTO.getVisitorId());
        if (null == visitor) {
            throw new BusinessException("上报信息访客不存在");
        }
        String phone = weChatService.decodeUserPhone(visitorSubmitPhoneDTO.getPhone());

        visitor.setPhone(phone);
        if (!visitorService.updateById(visitor)) {
            throw new BusinessException("更新用户手机号失败");
        }

        //更新销售访客管理表信息,此处通用处理，如果没传sellerId则只更新访客信息
        if (StringUtils.isNotBlank(visitorSubmitPhoneDTO.getSellerId())) {
            boolean result = staffVisitorService.lambdaUpdate()
                    .eq(StaffVisitor::getUserUuid, visitorSubmitPhoneDTO.getSellerId())
                    .eq(StaffVisitor::getVisitorUuid, visitorSubmitPhoneDTO.getVisitorId())
                    .set(StaffVisitor::getVisitorPhone, phone)
                    .update();

            if (!result) {
                log.error("更新员工{}访客{}手机号失败", visitorSubmitPhoneDTO.getSellerId(), visitorSubmitPhoneDTO.getSellerId());
                throw new BusinessException("更新销售管理访客信息失败");
            }
            //更新绑定表对应的手机号信息
            bindingManagerService.lambdaUpdate()
                    .eq(BindingManager::getUserUuid, visitorSubmitPhoneDTO.getSellerId())
                    .eq(BindingManager::getVisitorUuid, visitorSubmitPhoneDTO.getVisitorId())
                    .eq(BindingManager::getHistoryLabel, 0)
                    .set(BindingManager::getPhone, phone)
                    .update();
        }

        return true;
    }

    @Override
    public UserDetailVO getUserInfo(String userId) {
        OauthWechat oauthWechat = oauthWeChatService.getUserWeChatInfoByCache(userId);
        if (oauthWechat != null) {
            log.info("用户获取微信信息:" + oauthWechat);
            if (STAFF.equals(oauthWechat.getUserType())) {
                log.info("用户是销售");
                return getUserInfoDetail(userId);

            } else if (VISITOR.equals(oauthWechat.getUserType())) {
                log.debug("用户是访客");
                return getVisitorInfoDetail(userId);
            }
        }
        return getVisitorInfoDetail(userId);
    }

    @Override
    public BussinessCardHomeVO getBusinessCardHome(String userId) {
        User user = getById(userId);
        if (user != null) {
            UserInfo userInfo = userInfoService.getById(userId);
            if (userInfo != null) {
                String phone = user.getPhone();
                BussinessCardHomeVO bc = new BussinessCardHomeVO();
                bc.setPopularityCounts(getPopularityCounts(userId));
                bc.setCompanyLogo(checkReturnUrl(userInfo.getCompanyLogo()));
                bc.setCoverPicture(checkReturnUrl(userInfo.getCoverPicture()));
                bc.setAvatarUrl(checkReturnUrl(user.getAvatar()));
                bc.setProjectName(getProjectName(userInfo.getProjectId()));
                bc.setName(user.getNickName());
                bc.setPosition(userInfo.getPositionName());
                bc.setCompany(userInfo.getCompanyName());
                bc.setAddress(userInfo.getAddress());
                bc.setEmail(userInfo.getEmail());
                bc.setWeChatId(userInfo.getWechatId());
                bc.setWeChatQrcode(checkReturnUrl(userInfo.getQrCode()));
                bc.setPhone(phone);
                bc.setSellerPhone(phone);
                //设置隐藏属性
                setHideProperties(userInfo, bc);
                return bc;
            } else {
                throw new UserNotFoundException(userId);
            }
        }
        throw new UserNotFoundException(userId);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public BussinessCardHomeVO updateBusinessCardHome(BussinessCardHomeEditDTO bc) {
        log.info("更新用户信息");
        String userId = bc.getUserId();
        User user = this.lambdaQuery().eq(User::getUuid, userId).one();
        Optional.ofNullable(user).orElseThrow(() -> new BusinessException("用户信息不存在"));
        UserInfo userInfo = userInfoService.lambdaQuery().eq(UserInfo::getUserUuid, userId).one();
        Optional.ofNullable(userInfo).orElseThrow(() -> new BusinessException("用户信息不存在"));

        String userName = bc.getName();
        String phone = bc.getPhone();
        String weChatId = bc.getWeChatId();

        if (this.removeById(userId)) {
            MultipartFile file = bc.getAvatarUrl();
            user.setUuid(userId);
            user.setNickName(userName);
            user.setPhone(phone);
            if (file != null) {
                String avatar = user.getAvatar();
                String key = null;
                if (StringUtils.isNotBlank(avatar)) {
                    String[] split = avatar.split("/");
                    key = split[split.length - 1];
                }
                if (!Objects.deepEquals(file.getOriginalFilename(), key)) {
                    user.setAvatar(aliyunOssKit.upload(file, user.getAvatar()));
                }
            }
            if (!this.save(user)) {
                throw new BusinessException("保存用户信息失败");
            }
        }
        if (userInfoService.removeById(userId)) {
            MultipartFile file = bc.getWeChatQrcode();
            MultipartFile picture = bc.getCoverPicture();
            userInfo.setUserUuid(userId);
            if (file != null) {
                if (!Objects.deepEquals(file.getOriginalFilename(), userInfo.getQrCode())) {
                    userInfo.setQrCode(aliyunOssKit.upload(file, userInfo.getQrCode()));
                }
            }
            if (picture != null) {
                if (!Objects.deepEquals(picture.getOriginalFilename(), userInfo.getCoverPicture())) {
                    userInfo.setCoverPicture(aliyunOssKit.upload(picture, userInfo.getCoverPicture()));
                }
            }
            userInfo.setWechatId(weChatId);
            userInfo.setHideColumn(bc.getHideColumn());
            if (!userInfoService.save(userInfo)) {
                throw new BusinessException("保存用户信息失败");
            }
        }

        BussinessCardHomeVO homeVO = new BussinessCardHomeVO();
        homeVO.setName(userName);
        homeVO.setPhone(phone);
        homeVO.setSellerPhone(phone);
        homeVO.setAvatarUrl(user.getAvatar());
        homeVO.setWeChatId(weChatId);
        homeVO.setPosition(userInfo.getPositionName());
        homeVO.setCompany(userInfo.getCompanyName());
        homeVO.setAddress(userInfo.getAddress());
        homeVO.setEmail(userInfo.getEmail());
        homeVO.setWeChatQrcode(userInfo.getQrCode());
        homeVO.setCompanyLogo(userInfo.getCompanyLogo());
        homeVO.setCoverPicture(userInfo.getCoverPicture());
        homeVO.setPopularityCounts(getPopularityCounts(userId));
        homeVO.setProjectName(getProjectName(userInfo.getProjectId()));
        //设置隐藏属性
        setHideProperties(userInfo, homeVO);

        return homeVO;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Token initLoginByCode(WxMaJscode2SessionResult result,
                                 HttpServletRequest request,
                                 Integer origin) {
        String openId = result.getOpenid() == null ? "" : result.getOpenid();
        String unionId = result.getUnionid() == null ? "" : result.getUnionid();
        String sessionKey = result.getSessionKey();
        OauthWechat oauthWechat;
        //openId为空表示是游客登录，理论上不存在此情况，此处为了兼容
        if (StringUtils.isBlank(openId)) {
            oauthWechat = oauthWeChatService.getUserWeChatInfoByOpenId(openId);
        } else {
            if (StringUtils.isBlank(unionId)) {
                //unionId为空
                oauthWechat = oauthWeChatService.getUserWeChatInfoByOpenId(openId);
            } else {
                oauthWechat = oauthWeChatService.getUserWeChatInfoByUnionId(unionId);
            }
        }
        UserSessionVO userSessionVO = getUserSessionVO(origin, openId, unionId, oauthWechat, sessionKey, null);
        //更新用户openId为小程序openId,游客身份不更新
        updateOpenId(openId, unionId, userSessionVO.getUserId());
        return this.createToken(userSessionVO, request);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Token initLoginByAccessToken(WxMpUser wxMpUser, HttpServletRequest request, Integer origin) {
        String openId = wxMpUser.getOpenId() == null ? "" : wxMpUser.getOpenId();
        String unionId = wxMpUser.getUnionId() == null ? "" : wxMpUser.getUnionId();
        OauthWechat oauthWechat = oauthWeChatService.getUserWeChatInfoByUnionId(unionId);
        UserSessionVO userSessionVO = getUserSessionVO(origin, openId, unionId, oauthWechat, null, wxMpUser);
        return this.createToken(userSessionVO, request);
    }

    @NotNull
    private UserSessionVO getUserSessionVO(Integer origin,
                                           String openId,
                                           String unionId,
                                           OauthWechat oauthWechat,
                                           String sessionKey, WxMpUser wxMpUser) {
        return Optional.ofNullable(oauthWechat)
                .map(s -> updateOauthWeChat(origin, openId, sessionKey, s))
                .orElseGet(() -> saveOauthWeChat(origin, openId, unionId, sessionKey, wxMpUser));
    }

    private UserSessionVO updateOauthWeChat(Integer origin,
                                            String openId,
                                            String sessionKey,
                                            OauthWechat s) {
        return getUserSessionByUpdateOauthWeChat(openId, sessionKey, s, origin);
    }

    private void updateOpenId(String openId, String unionId, String userId) {
        if (TOURIST_UUID.equals(userId)) {
            return;
        }
        if (StringUtils.isNotBlank(unionId)) {
            OauthWechat oauthWechat = oauthWeChatService.lambdaQuery()
                    .eq(OauthWechat::getUnionid, unionId)
                    .one();
            if (openId.equals(oauthWechat.getOpenid())) {
                return;
            }
            oauthWeChatService.lambdaUpdate()
                    .set(OauthWechat::getOpenid, openId)
                    .eq(OauthWechat::getUnionid, unionId)
                    .update();
        }
    }

    private UserSessionVO saveOauthWeChat(Integer origin, String openId, String unionId, String sessionKey, WxMpUser wxMpUser) {
        //保存访客信息
        Visitor visitor = saveVisitor(origin, wxMpUser);
        //插入微信表数据
        String visitorUuid = visitor.getUuid();
        OauthWechat o = new OauthWechat();
        o.setNickName(wxMpUser == null ? null : wxMpUser.getNickname());
        o.setAvatarUrl(wxMpUser == null ? null : wxMpUser.getHeadImgUrl());
        o.setOpenid(openId);
        o.setUnionid(unionId);
        o.setUserUuid(visitorUuid);
        o.setUserType(VISITOR);
        return getUserSessionBySaveOauthWeChat(origin, sessionKey, o, unionId, openId, visitorUuid);
    }

    private Visitor saveVisitor(Integer origin, WxMpUser wxMpUser) {
        Visitor visitor = new Visitor();
        visitor.setNickName(wxMpUser == null ? null : wxMpUser.getNickname());
        visitor.setAvatar(wxMpUser == null ? null : wxMpUser.getHeadImgUrl());
        visitor.setVisitorSource(origin);
        if (!visitorService.save(visitor)) {
            throw new BusinessException("用户注册失败");
        }
        return visitor;
    }

    private UserSessionVO getUserSessionByUpdateOauthWeChat(String openId,
                                                            String sessionKey,
                                                            OauthWechat oauthWechat,
                                                            Integer origin) {
        String unionId = oauthWechat.getUnionid();
        String userUuid = oauthWechat.getUserUuid();
        Integer userType = oauthWechat.getUserType();

        //用户不为空，默认是老用户,创建用户会话对象
        log.info("not first login by wechat code");
        UserSessionVO userSessionVO = new UserSessionVO();
        userSessionVO.setUnionId(unionId);
        userSessionVO.setUserId(userUuid);
        userSessionVO.setOpenId(openId);
        userSessionVO.setSessionKey(sessionKey);

        String str = sessionKey == null ? "授权公众号" : "授权登录小程序";
        String message = "用户数据错误，请联系管理员";
        if (VISITOR.equals(userType)) {
            log.info("old visitor");
            userSessionVO.setUserType(VISITOR);
            logService.recordVisitorLog(userUuid, str);
            //采集日志添加
            CollectorAllRecord collectorAllRecord = new CollectorAllRecord();
            collectorAllRecord.setVisitorId(userUuid);
            collectorAllRecord.setOrigin(origin);
            collectorAllRecord.setVisitorContent(sessionKey == null ? "首次授权公众号" : "首次登录小程序");
            collectorAllRecordService.save(collectorAllRecord);
        } else if (STAFF.equals(userType)) {
            log.warn("old user");
            userSessionVO.setUserType(STAFF);
            logService.recordUserLog(userUuid, str, false);
        } else if (ADMIN.equals(userType)) {
            log.error("admin login");
            userSessionVO.setUserType(ADMIN);
        } else {
            log.error("illegal user label");
            throw new BusinessException(message);
        }


        return userSessionVO;
    }

    private UserSessionVO getUserSessionBySaveOauthWeChat(Integer origin,
                                                          String sessionKey,
                                                          OauthWechat oauthWechat,
                                                          String unionId,
                                                          String openid,
                                                          String visitorUuid) {
        return saveOauthWeChat(origin, sessionKey, oauthWechat, unionId, openid, visitorUuid);
    }

    private UserSessionVO saveOauthWeChat(Integer origin,
                                          String sessionKey,
                                          OauthWechat oauthWechat,
                                          String unionId,
                                          String openid,
                                          String visitorUuid) {
        OauthWechat o = oauthWeChatService.lambdaQuery().eq(OauthWechat::getOpenid, openid).one();
        if (o != null) {
            if (oauthWeChatService.lambdaUpdate().set(OauthWechat::getUnionid, unionId).eq(OauthWechat::getOpenid, openid).update()) {
                return saveUserLoginLogAndGetUserSessionVO(origin, sessionKey, o, unionId, openid, visitorUuid);
            } else {
                return registerFail(visitorUuid);
            }
        }
        if (oauthWeChatService.save(oauthWechat)) {
            return saveUserLoginLogAndGetUserSessionVO(
                    origin, sessionKey, oauthWechat, unionId, openid, visitorUuid);
        } else {
            return registerFail(visitorUuid);
        }
    }

    @NotNull
    private UserSessionVO registerFail(String visitorUuid) {
        log.error("wechat info insert error");
        //业务回滚
        visitorService.removeById(visitorUuid);
        throw new BusinessException("用户注册失败");
    }

    private UserSessionVO saveUserLoginLogAndGetUserSessionVO(Integer origin,
                                                              String sessionKey,
                                                              OauthWechat oauthWechat,
                                                              String unionId,
                                                              String openid,
                                                              String visitorUuid) {
        UserSessionVO userSessionVO = new UserSessionVO();
        userSessionVO.setSessionKey(sessionKey);
        userSessionVO.setOpenId(openid);
        userSessionVO.setUserType(VISITOR);
        userSessionVO.setUnionId(unionId);
        userSessionVO.setUserId(visitorUuid);

        //缓存用户微信信息
        redisTemplate.opsForValue().set(
                Constants.CACHE_PREFIX_LABEL.WECHAT + visitorUuid,
                oauthWechat,
                Duration.ofMinutes(10));
        String str = sessionKey == null ? "首次授权公众号" : "首次登录小程序";
        //访客日志记录
        logService.recordVisitorLog(visitorUuid, str);
        //采集日志添加
        CollectorAllRecord collectorAllRecord = new CollectorAllRecord();
        collectorAllRecord.setVisitorId(visitorUuid);
        //默认来源是小程序
        collectorAllRecord.setOrigin(null != origin ? origin : 4);
        collectorAllRecord.setVisitorContent(str);
        collectorAllRecordService.save(collectorAllRecord);

        return userSessionVO;
    }

    @Override
    public boolean saveWxUserInfo(WXUserInfo wxUserInfo) {
        if (wxUserInfo != null) {
            OauthWechat wechat = new OauthWechat();
            BeanUtils.copyProperties(wxUserInfo, wechat);
            OauthWechat updateWeChatInfo = oauthWeChatService.updateUserWeChatInfoByUserIdByCache(wxUserInfo.getUserId(), wechat);
            if (updateWeChatInfo != null) {

                //用户信息是访客
                if (updateWeChatInfo.getUserType().equals(VISITOR)) {
                    Visitor visitor = new Visitor();
                    visitor.setNickName(wxUserInfo.getNickName());
                    visitor.setAvatar(wxUserInfo.getAvatarUrl());
                    visitor.setUuid(updateWeChatInfo.getUserUuid());
                    visitor.setAddress(wxUserInfo.getCountry() + " " + wxUserInfo.getProvince() + " " + wechat.getCity());
                    if (!visitorService.updateById(visitor)) {
                        throw new BusinessException("同步更新访客表失败");
                    }
                    return true;
                    //其他信息有待进一步确定
                    //用户是员工
                } else if (updateWeChatInfo.getUserType().equals(Constants.USER_TYPE.STAFF)) {
                    //用户是员工,更新员工相关表
                    User user = new User();
                    UserInfo userInfo = new UserInfo();

                    user.setNickName(wxUserInfo.getNickName());
                    user.setAvatar(wxUserInfo.getAvatarUrl());
                    user.setUuid(updateWeChatInfo.getUserUuid());

                    updateById(user);


                    //用户存储信息有待进一步确认
                    userInfo.setGender(wxUserInfo.getGender());
                    userInfo.setUserUuid(updateWeChatInfo.getUserUuid());
                    userInfo.setAddress(updateWeChatInfo.spliceAddress());
                    if (!userInfoService.updateById(userInfo)) {
                        throw new BusinessException("同步更新用户表失败");
                    }

                    return true;
                }
            } else {
                throw new BusinessException("更新微信信息失败");
            }
        }
        return false;
    }

    @Override
    public boolean saveUser(UserSaveDTO userSaveDTO) {
        //添加用户
        User user = new User();
        UserInfo userInfo = new UserInfo();
        BeanUtils.copyProperties(userSaveDTO, user);
        BeanUtils.copyProperties(userSaveDTO, userInfo);
        save(user);

        //添加用户详情
        userInfo.setUserUuid(user.getUuid());
        userInfo.setAddress(userSaveDTO.getAddress());
        userInfoService.save(userInfo);

        //添加微信用户
        OauthWechat wechat = new OauthWechat();
        wechat.setUserType(STAFF);
        wechat.setUserUuid(user.getUuid());
        wechat.setUnionid("a" + System.currentTimeMillis());
        wechat.setOpenid("asdfasdfewrqwer" + System.currentTimeMillis());
        wechat.setAvatarUrl(user.getAvatar());
        wechat.setNickName(user.getNickName());
        wechat.setCity("深圳");
        wechat.setCountry("中国");
        wechat.setProvince("广东");
        oauthWeChatService.save(wechat);

        //添加项目
        List<ProjectManager> lists = new ArrayList<>();
        int data = 6;
        for (int i = 1; i < data; i++) {
            ProjectManager pm = new ProjectManager();
            pm.setUserUuid(user.getUuid());
            pm.setProjectId(i);
            lists.add(pm);
        }
        projectManagerService.saveBatch(lists);

        return true;
    }

    @Override
    public BusinessQRcodeVO getBusinessQrCode(BusinessQRCodeQuery query) {
        BusinessQRcodeVO businessQrCodeVO = new BusinessQRcodeVO();

        UserInfo userInfo = userInfoService.getById(query.getUserid());
        User user = getById(query.getUserid());

        if (null == user) {
            log.error("用户{}在用户表不存在", query.getUserid());
            throw new BusinessException("所传的id的用户在用户表不存在");
        }

        if (null == userInfo) {
            log.error("用户{}在用户详情表不存在", query.getUserid());
            throw new BusinessException("所传的id的用户在用户详情表不存在");
        }

        //设置返回属性
        businessQrCodeVO.setAvatar(checkReturnUrl(user.getAvatar()));
        businessQrCodeVO.setCompany(userInfo.getCompanyName());
        businessQrCodeVO.setName(user.getNickName());
        businessQrCodeVO.setPhone(user.getPhone());
        businessQrCodeVO.setPosition(PositionConstant.positionTranslate(userInfo.getPosition()));
        businessQrCodeVO.setQrcode("data:image/jpeg;base64," + Base64.getEncoder().encodeToString(weChatService.getAppletsCode(query.getWeChatQRQuery())));

        return businessQrCodeVO;
    }


    @Override
    public ProjectQRCodeVO getProjectQrCode(ProjectQRCodeDTO dto) {
        ProjectQRCodeVO vo = new ProjectQRCodeVO();
        //获取工程的信息
        Project project = projectService.getById(dto.getProjectId());
        Optional.ofNullable(project).orElseThrow(() -> new BusinessException("项目信息不存在"));
        vo.setImg(project.getCoverPicture());
        vo.setName(project.getProjectTitle());
        vo.setQrcode("data:image/jpeg;base64," + Base64.getEncoder()
                .encodeToString(weChatService.getAppletsCode(dto.getWeChatQRQuery())));
        return vo;
    }

    @Override
    public ProjectQRCodeVO getVrQrCode(ProjectQRCodeDTO dto) {
        ProjectQRCodeVO vo = new ProjectQRCodeVO();
        String image = Optional.ofNullable(resource720Mapper
                .selectById(dto.getFloorBookId()))
                .map(Resource720::getVrPoster)
                .orElse("https://bgyjssq.ideamake.cn/wxa-bgyjssq/81cad030-fb09-4c9b-9c2d-6228a81190b9.jpg");
        vo.setImg(checkReturnUrl(image));
        vo.setQrcode("data:image/jpeg;base64," + Base64.getEncoder()
                .encodeToString(weChatService.getAppletsCode(dto.getWeChatQRQuery())));
        log.info("生成vr720二维码的参数:{}", dto.getWeChatQRQuery().getScene());
        log.info("生成vr720二维码的路径:{}", dto.getWeChatQRQuery().getPage());
        return vo;
    }

    @Override
    public ProjectQRCodeVO getProfileQrCode(ProjectQRCodeDTO dto) {
        ProjectQRCodeVO vo = new ProjectQRCodeVO();
        String profilePoster = Optional.ofNullable(projectService
                .getById(dto.getProjectId()))
                .map(Project::getProfilePoster)
                .orElse("https://bgyjssq.ideamake.cn/wxa-bgyjssq/81cad030-fb09-4c9b-9c2d-6228a81190b9.jpg");
        vo.setImg(profilePoster);
        vo.setQrcode("data:image/jpeg;base64," + Base64.getEncoder()
                .encodeToString(weChatService.getAppletsCode(dto.getWeChatQRQuery())));
        log.info("生成vr720二维码的参数:{}", dto.getWeChatQRQuery().getScene());
        log.info("生成vr720二维码的路径:{}", dto.getWeChatQRQuery().getPage());
        return vo;
    }

    @Override
    public boolean updateUserInfo(UserEditDTO userEditDTO) {
        if (userEditDTO.getUserId() == null) {
            log.error("no user id found");
            return false;
        }
        User user = getById(userEditDTO.getUserId());
        if (user == null) {
            log.error("user not exist");
            return false;
        }
        if (userEditDTO.getPhone() != null) {
            user.setUuid(userEditDTO.getUserId());
            user.setNickName(userEditDTO.getName());
            user.setPhone(userEditDTO.getPhone());
            updateById(user);
        }
        UserInfo userInfo = new UserInfo();
        //修改扩展表信息
        userInfo.setUserUuid(userEditDTO.getUserId());
        userInfo.setCompanyName(userEditDTO.getCompany());
        userInfo.setAddress(userEditDTO.getAddress());
        userInfo.setEmail(userEditDTO.getEmail());
        userInfo.setBusinessCardCode(userEditDTO.getBusinessCardImg());

        //判断前端传来的参数二维码文件是否存在,存在则更新二维码地址
        if (userEditDTO.getWeChatQrcode() != null) {
            String fileName = aliyunOssKit.upload(userEditDTO.getWeChatQrcode());
            userInfo.setQrCode(fileName);
        }
        return userInfoService.updateById(userInfo);
    }

    @Override
    public IPage<UserProjectVO> getStaffProject(UserProjectQuery userProjectQuery) {
        return baseMapper.selectProjectForStaff(userProjectQuery.getPageInfo(), userProjectQuery, aliyunOssKit.getOssUrlPrefix());
    }

    @Override
    public VisitorInfoDetailVO getVisitorInfoDetail(VisitorInfoDetailQuery query) {

        // 先查询访客基本信息
        VisitorInfoDetailVO visitorInfoDetailVO = baseMapper.selectVisitorBaseInfo(query);
        if (visitorInfoDetailVO != null) {
            List<VisitorActivityStatistics> activityList =
                    bindingManagerService.getVisitorDynamicInfoList(query.getUserid(), query.getSellerId());
            visitorInfoDetailVO.setActivityInfo(activityList);
            return visitorInfoDetailVO;
        }

        log.error("未发现访客信息");
        return null;
    }

    @Override
    public VisitorInfoDetailVO getVisitorInfoDetailForLeader(VisitorInfoDetailQuery query) {
        String sellerId = query.getSellerId();
        String userId = query.getUserid();

        List<String> sellerList = getSellerIdList(sellerId, userId);

        VisitorInfoDetailVO visitorInfoDetailVO = baseMapper.selectVisitorBaseInfoForLeader(query, sellerList);

        Optional.ofNullable(visitorInfoDetailVO).map(v -> {
            List<VisitorActivityStatistics> activityList =
                    bindingManagerService.getVisitorDynamicInfoListForLeader(userId, sellerList);
            v.setActivityInfo(activityList);
            return v;
        }).orElseGet(() -> {
            log.error("未发现访客信息");
            return null;
        });

        return visitorInfoDetailVO;
    }

    @Override
    public IPage<StaffVisitorVO> getStaffVisitorByFilter(StaffVisitorQuery staffVisitorQuery) {
        if (staffVisitorQuery.getSellerId() == null) {
            staffVisitorQuery.setSellerId(UserInfoContext.get().getUserId());
        }
        //动态统计信息需要从这里汇总获取
        return baseMapper.selectStaffVisitorByFilter(new Page<>(staffVisitorQuery.getPage(), staffVisitorQuery.getLimit()), staffVisitorQuery);
    }

    /**
     * 员工获取访客动态数据
     *
     * @param visitorTrendQuery visitorTrendQuery
     * @return IPage<VisitorTrendVO>
     */
    @Override
    public IPage<VisitorTrendVO> getVisitorTrend(VisitorTrendQuery visitorTrendQuery) {
        String sellerId = visitorTrendQuery.getSellerId();
        String userId = visitorTrendQuery.getVisitorId();

        List<String> sellerIdList = getSellerIdList(sellerId, userId);

        return collectorAllRecordService.getVisitorTrend(visitorTrendQuery, sellerIdList);
    }

    @Override
    public IPage<StaffVisitorRankVO> getStaffVisitorRank(StaffVisitorRankQuery staffVisitorRankQuery) {
        return collectorAllRecordService.getStaffVisitorRank(staffVisitorRankQuery);
    }

    @Override
    public VisitorVisitAnalysisVO getVisitorVisitAnalysis(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery) {
        //临时调整接口
        Integer searchTime = visitorVisitAnalysisQuery.getSearchTime();
        if (searchTime != null && searchTime >= 0) {
            LocalDate now = LocalDate.now();
            Period p = Period.ofDays(0 - searchTime);
            LocalDate then = now.plus(p);
            visitorVisitAnalysisQuery.setStartTime(then.toString());
            visitorVisitAnalysisQuery.setEndTime(LocalDateTime.of(now, LocalTime.MAX).toString());
        }

        return getVisitorVisitAnalysisVO(visitorVisitAnalysisQuery);
    }

    @Override
    public DataAnalysisDetailVO getVisitorList(VisitorVisitAnalysisQuery query) {

        String sellerId = getSellerId(query);

        List<Integer> groupIdList =
                userGroupManagerService.getGroupIdList(sellerId, new ArrayList<>(query.getProjects()));

        //不包含组即返回单个销售对应的客户信息
        if (CollectionUtils.isEmpty(groupIdList)) {
            return getCustomerList(query);
        }

        //调用统一的组查询接口
        List<DataAnalysisDetailListVO> list = staffVisitorService.getUserGroupAnalyse(query);

        DataAnalysisDetailVO dataAnalysisDetailVO = new DataAnalysisDetailVO();

        //查询高等级销售数据
        setHigherSellerData(dataAnalysisDetailVO, query, sellerId);

        //返回组数据
        dataAnalysisDetailVO.setHasGroup(HAS_GROUP);
        dataAnalysisDetailVO.setDataAnalysisDetailList(list);
        return dataAnalysisDetailVO;
    }

    @Override
    public UserVisitorProjectVO getVisitorProjectUserInfo(UserVisitorProjectQuery userVisitorProjectQuery) {
        UserSessionVO uv = UserInfoContext.get();
        if (uv != null && uv.getUserId() != null) {
            userVisitorProjectQuery.setVisitorId(uv.getUserId());
            return baseMapper.selectProjectUserInfo(userVisitorProjectQuery);
        }
        throw new AuthenticationException(RestEnum.NEED_LOGIN);
    }

    @Override
    public ProjectSellerInfoVO getProjectSellerInfo(Integer projectId) {
        String visitorId = UserInfoContext.get().getUserId();
        Project project = projectService.getById(projectId);
        log.info(visitorId + "####" + project);
        BindingManager bindingManager = bindingManagerService.lambdaQuery()
                .eq(BindingManager::getVisitorUuid, visitorId)
                .eq(BindingManager::getHistoryLabel, 0)
                .eq(BindingManager::getProjectId, projectId)
                .one();
        if (null == bindingManager) {
            log.error("visitor({}) not binding for the project({})", visitorId, project);
            throw new BusinessException(RestEnum.NOT_BINDING);
        }

        log.info(bindingManager.toString());
        User user = getById(bindingManager.getUserUuid());
        UserInfo userInfo = userInfoService.getById(bindingManager.getUserUuid());

        String userUuid = user.getUuid();
        String userName = user.getNickName();

        SellerInfo sellerInfo = new SellerInfo();
        sellerInfo.setAvatar(checkReturnUrl(user.getAvatar()));
        sellerInfo.setName(userName);
        String[] hideColumn = this.getHideColumn(userInfo.getHideColumn());
        sellerInfo.setHideColumn(hideColumn);
        //隐藏手机号
        if (Arrays.asList(hideColumn).contains(PHONE)) {
            sellerInfo.setPhone("");
        } else {
            sellerInfo.setPhone(user.getPhone());
        }
        sellerInfo.setUserid(userUuid);
        sellerInfo.setWeChatQrUrl(checkReturnUrl(userInfo.getQrCode()));

        ProjectInfo projectInfo = new ProjectInfo();
        projectInfo.setProjectName(project.getProjectTitle());
        projectInfo.setFloorBook(project.getFloorBookId());
        projectInfo.setProjectImg(checkReturnUrl(project.getCoverPicture()));
        projectInfo.setProjectId(projectId);

        ProjectSellerInfoVO projectSellerInfoVO = new ProjectSellerInfoVO();
        projectSellerInfoVO.setProjectInfo(projectInfo);
        projectSellerInfoVO.setSellerInfo(sellerInfo);

        //保存日志
        saveCollectorLog(visitorId, project, userName);
        return projectSellerInfoVO;
    }

    @Override
    public Token createToken(UserSessionVO userSessionVO, HttpServletRequest request) {
        Token token = JwtKit.createJwt(userSessionVO, audience, request);
        cacheRefreshToken(token, userSessionVO);
        return token;
    }

    @Override
    public Token refreshToken(String refreshToken, HttpServletRequest request) {
        // 1. 通过用户传入的 refreshToken, 获取缓存在redis中的信息
        String key = REFRESH_TOKEN + refreshToken;
        Boolean hasKey = redisTemplate.hasKey(key);
        if (hasKey == null || !hasKey) {
            // 超过刷新令牌的时间. 需要重新登录获取了.
            throw new AuthenticationException(RestEnum.REFRESH_TOKEN_EXPIRED);
        }
        Gson gson = new Gson();
        RefreshToken refreshTokenObj =
                gson.fromJson((String) redisTemplate.opsForValue().get(key), RefreshToken.class);

        redisTemplate.delete(key);

        // 2. 从缓存中获取了用户信息, 重新签发 accessToken
        UserSessionVO userSessionVO = refreshTokenObj.getUserSessionVO();
        Token token = JwtKit.createJwt(userSessionVO, audience, request);

        // 3. 重新装载 RefreshToken , 准备存入redis中,
        cacheRefreshToken(token, userSessionVO);

        return token;
    }

    @Override
    public List<CustomerManagerVO> customerListQuery(CustomerManagerQuery query) {

        // 查出客户
        List<CustomerManagerDTO> customerManagerDTO = bindingManagerMapper.selectCustomerList(query, new Page<>(query.getPage(), query.getLimit()));

        if (null != customerManagerDTO) {
            ArrayList<CustomerManagerVO> result = new ArrayList<>();

            customerManagerDTO.stream()
                    .collect(Collectors.collectingAndThen(
                            Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(CustomerManagerDTO::getUuid))), ArrayList::new))
                    .forEach(customerManager1 -> {

                        // 基本信息
                        CustomerManagerVO customer = new CustomerManagerVO();
                        customer.setNickName(customerManager1.getNickName())
                                .setPhone(customerManager1.getPhone())
                                .setUuid(customerManager1.getUuid())
                                .setVisitorSource(customerManager1.getVisitorSource());
                        ArrayList<CustomerManagerBindingDTO> dtoArrayList = new ArrayList<>();

                        customerManagerDTO.forEach(customerManager2 -> {
                            if (StrUtil.equals(customerManager2.getUuid(), customerManager1.getUuid())) {
                                // 明细
                                CustomerManagerBindingDTO customerManagerBindingDTO = new CustomerManagerBindingDTO();
                                customerManagerBindingDTO.setName(customerManager2.getName())
                                        .setProjectTitle(customerManager2.getProjectTitle())
                                        .setSelfLabel(customerManager2.getSelfLabel());

                                dtoArrayList.add(customerManagerBindingDTO);
                            }
                        });
                        customer.setBindingList(dtoArrayList);
                        result.add(customer);
                    });
            return result;
        }
        return Collections.emptyList();
    }

    @Override
    public List<String> getStaffProjectNameListByInviteId(Integer inviteId) {

        UserRegister userRegister = userRegisterService.lambdaQuery().select(UserRegister::getProjectList).eq(UserRegister::getId, inviteId).one();
        if (null == userRegister) {
            // 邀请记录不存在
            throw new BusinessException(RestEnum.INVITE_RECORD_NOT_FOUND);
        }
        JSONArray jsonArray = JSON.parseArray(userRegister.getProjectList());
        List<Integer> projectIdList = jsonArray.toJavaList(Integer.class);
        List<Project> projectList = projectService.lambdaQuery().select(Project::getProjectTitle).in(Project::getId, projectIdList).list();
        if (CollUtil.isNotEmpty(projectList)) {
            return projectList.stream().map(Project::getProjectTitle).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    @Override
    public IPage<StaffListVO> getStaffInfoList(StaffListQuery query) {
        Page<StaffListVO> page = new Page<>(query.getPage(), query.getLimit());
        List<StaffListVO> list = baseMapper.getStaffInfoList(query, page);
        list.forEach(l -> l.setBindProject(projectService.selectProjectNameByUserId(l.getId())));
        return page.setRecords(list);
    }

    @Override
    public List<StaffKpiVO> selectStaffTopTenList(StaffKpiQuery query) {

        // 默认用留电次数排序
        String sort = STAFF_TOP_TEN_SORT_COLUMNS.get(1);
        // 查询语句采用的 ${}, 必须经过此步骤使用后端定死的列名, 防止SQL注入风险
        if (null != query.getSort() || !STAFF_TOP_TEN_SORT_COLUMNS.contains(query.getSort())) {
            query.setSort(sort);
        }

        // 查询出姓名,组名,留电统计, 推广汇总
        return baseMapper.selectStaffKpiList(query, new Page<>(1, 10));
    }

    @Override
    public IPage<DigDataVO> selectStaffDigData(DigDataQuery query) {
        if (Constants.USER_TYPE.STAFF.equals(query.getUserType())) {
            return baseMapper.selectDigDataForStaffAchievement(query.getPageInfo(), query);
        }
        if (Constants.USER_TYPE.VISITOR.equals(query.getUserType())) {
            return baseMapper.selectDigDataForCustomerAchievement(query.getPageInfo(), query);
        }
        throw new BusinessException("查询类型错误");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean importStaff(MultipartFile file) {
        try {
            // 读取Excel文件
            List<StaffInfoBO> staffInfoList = ExcelImportUtil.importExcel(file.getInputStream(), StaffInfoBO.class, new ImportParams());

            if (CollUtil.isNotEmpty(staffInfoList)) {

                List<User> userList = new ArrayList<>();
                List<UserInfo> userInfoList = new ArrayList<>();

                //查询已存在员工数据
                List<User> users = this.lambdaQuery().list().parallelStream()
                        .filter(s -> null != s.getPhone())
                        .collect(Collectors.toList());

                DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

                //保存用户基本信息
                boolean userSave = saveUser(staffInfoList, userList, users, df);

                //保存用户扩展信息
                boolean userInfoSave = saveUserInfo(staffInfoList, userList, userInfoList, df);

                log.info("userSave = {}, userInfoSave = {}", userSave, userInfoSave);
                return userInfoSave && userSave;
            } else {
                throw new BusinessException("表格数据为空");
            }
        } catch (Exception e) {
            log.error("导入失败", e);
            throw new BusinessException("导入失败,请检查数据正确性");
        }
    }

    @Override
    public IPage<StaffKpiVO> selectStaffKpiList(StaffKpiQuery query) {
        Page<StaffKpiVO> page = query.getPageInfo();
        List<StaffKpiVO> list = baseMapper.selectStaffKpiList(query, page);
        return page.setRecords(list);
    }

    @Override
    public List<DataAnalysisDetailListVO> getSellerDetailList(VisitorVisitAnalysisQuery query) {
        return this.getSellerList(query, query.getGroupId(), new DataAnalysisDetailVO()).getDataAnalysisDetailList();
    }

    @Override
    public DataAnalysisDetailVO getVisitorDetailList(VisitorVisitAnalysisQuery query) {
        DataAnalysisDetailVO dataAnalysisDetailVO = new DataAnalysisDetailVO();
        dataAnalysisDetailVO.setHasGroup(HAS_NOT_GROUP);
        dataAnalysisDetailVO.setStaffVisitorList(visitorService.selectVisitorByUserId(query));
        return dataAnalysisDetailVO;
    }

    /**
     * 保存访问日志
     *
     * @param visitorId 访客id
     * @param project   项目
     * @param userName  员工名
     */
    private void saveCollectorLog(String visitorId, Project project, String userName) {
        logService.recordVisitorLog(visitorId, "您访问了员工" + userName
                + "的" + project.getProjectTitle() + "项目空间");

        List<BindingManager> list = bindingManagerService.lambdaQuery()
                .eq(BindingManager::getVisitorUuid, visitorId)
                .eq(BindingManager::getHistoryLabel, 0)
                .list();
        Optional.ofNullable(list).ifPresent(s -> s.parallelStream()
                .map(BindingManager::getUserUuid)
                .distinct()
                .forEach(v -> saveUserLogService.saveChangeBindLog(v, visitorId, project, this::accept, FALSE)));
    }

    private String accept(Visitor visitor, User user, Project project) {
        return "您的客户" + visitor.getNickName()
                + "查看了员工" + user.getNickName() + "的"
                + project.getProjectTitle() + "项目空间";
    }

    /**
     * 抽离的公共方法, 用于将签发的 刷新令牌 以及 访问令牌存入缓存中
     *
     * @param token         accessToken, refreshToken
     * @param userSessionVO 用户信息
     */
    private void cacheRefreshToken(Token token, UserSessionVO userSessionVO) {

        RefreshToken refreshTokenObj = new RefreshToken(token, userSessionVO);

        String key = REFRESH_TOKEN + token.getRefreshToken();

        redisTemplate.opsForValue().set(key, JSON.toJSONString(refreshTokenObj), REFRESH_TOKEN_VALIDITY_PERIOD, MINUTES);
    }

    private UserDetailVO getUserInfoDetail(String userId) {
        User user = getById(userId);
        if (user != null) {
            UserInfo userInfo = userInfoService.getById(userId);
            if (userInfo != null) {

                UserDetailVO userDetailVO = new UserDetailVO();
                userDetailVO.setUserId(userId);
                userDetailVO.setName(user.getNickName());
                userDetailVO.setAddress(userInfo.getAddress());
                userDetailVO.setAvatarUrl(user.getAvatar());
                if (user.getAvatar() != null && !user.getAvatar().contains(HTTP)) {
                    userDetailVO.setAvatarUrl(aliyunOssKit.getUrlByFileName(user.getAvatar()));
                }
                Project project = projectService.getById(userInfo.getProjectId());
                if (project == null) {
                    userDetailVO.setCompany("");
                } else {
                    userDetailVO.setCompany(project.getProjectTitle());
                }
                String[] hideColumn = this.getHideColumn(userInfo.getHideColumn());
                userDetailVO.setHideColumn(hideColumn);
                //隐藏手机号
                if (Arrays.asList(hideColumn).contains(PHONE)) {
                    userDetailVO.setPhone("");
                } else {
                    userDetailVO.setPhone(user.getPhone() == null ? "" : user.getPhone());
                }
                userDetailVO.setEmail(userInfo.getEmail() == null ? "" : userInfo.getEmail());
                userDetailVO.setCompany(userInfo.getCompanyName() == null ? "" : userInfo.getCompanyName());
                userDetailVO.setWeChatQrcode(checkReturnUrl(userInfo.getQrCode()));
                userDetailVO.setBusinessCardCode(userInfo.getBusinessCardCode());
                userDetailVO.setPosition(userInfo.getPositionName());
                userDetailVO.setRole(1);
                if (userGroupManagerService.isHigherLevelRole(userId)) {
                    userDetailVO.setHigherRole(1);
                }
                return userDetailVO;
            }
        }
        return null;
    }

    private UserDetailVO getVisitorInfoDetail(String userId) {
        Visitor visitor = visitorService.getById(userId);
        if (visitor != null) {
            UserDetailVO userDetailVO = new UserDetailVO();
            userDetailVO.setName(visitor.getNickName());
            userDetailVO.setAvatarUrl(visitor.getAvatar());
            List<CollectorVisitor> collectorVisitorList = collectorVisitorService.lambdaQuery()
                    .eq(CollectorVisitor::getVisitorId, userId)
                    .gt(CollectorVisitor::getProjectId, 0)
                    .orderByDesc(CollectorVisitor::getId)
                    .list();
            Integer projectId = null;
            if (CollUtil.isNotEmpty(collectorVisitorList)) {
                CollectorVisitor collectorVisitor = collectorVisitorList.get(0);
                projectId = collectorVisitor.getProjectId();

                Optional<CollectorResource> collectorResource = getProjectIdByResource(userId);
                if (collectorResource.isPresent()) {
                    if (collectorResource.get().getCreateAt().isAfter(collectorVisitor.getCreateAt())) {
                        projectId = collectorResource.get().getProjectId();
                    }
                }
            } else {
                Optional<CollectorResource> collectorResource = getProjectIdByResource(userId);
                if (collectorResource.isPresent()) {
                    projectId = collectorResource.get().getProjectId();
                }
            }

            userDetailVO.setLastProjectId(projectId);
            userDetailVO.setPhone(visitor.getPhone());
            userDetailVO.setWeChatQrcode(visitor.getQrCode());
            userDetailVO.setRole(0);
            return userDetailVO;
        }
        return null;
    }

    private Optional<CollectorResource> getProjectIdByResource(String userId) {
        List<CollectorResource> collectorResourceList = collectorResourceService.lambdaQuery()
                .eq(CollectorResource::getVisitorId, userId)
                .gt(CollectorResource::getProjectId, 0)
                .orderByDesc(CollectorResource::getId)
                .list();
        if (CollUtil.isNotEmpty(collectorResourceList)) {
            return Optional.of(collectorResourceList.get(0));
        }
        return Optional.empty();
    }

    private static String randomCode() {
        return String.valueOf((int) ((Math.random() * 9 + 1) * 100000));
    }

    private String checkReturnUrl(String url) {
        if (null == url) {
            return null;
        }
        if (url.contains(HTTP)) {
            return url;
        } else {
            return aliyunOssKit.getUrlByFileName(url);
        }
    }

    /**
     * 抽出的访客数据钻取方法
     *
     * @param query 查询条件
     * @return VisitorVisitAnalysisVO
     */
    private VisitorVisitAnalysisVO getVisitorVisitAnalysisVO(VisitorVisitAnalysisQuery query) {
        Set<Integer> projects = query.getProjects();
        String sellerId = getSellerId(query);

        //获取员工列表
        List<String> list = userGroupManagerService.getSellerIdListAddMe(sellerId);

        //只取出绑定项目的数据
        List<String> temp = projectManagerService.getEffectiveData(projects, list);

        TotalStatistics totalStatistics =
                collectorVisitorService.getVisitorVisitTotalData(query, temp);
        log.info("汇总统计:" + totalStatistics);

        //设置客户转介数据
        setCustomerRecommend(query, temp, totalStatistics);

        List<OriginCountCell> originStatistics =
                collectorVisitorService.getVisitorVisitOriginData(query, temp);
        log.info("来源统计:" + originStatistics);
        VisitTrend visitTrend =
                collectorVisitorService.getVisitorVisitTrendData(query, temp);
        log.info("访问趋势:" + visitTrend);
        List<VisitPeriod> visitPeriod =
                collectorVisitorService.getVisitorVisitPeriodData(query, temp);
        log.info("访问活跃时间段:" + visitPeriod);
        return new VisitorVisitAnalysisVO(totalStatistics, originStatistics, visitTrend, visitPeriod);
    }

    /**
     * 设置客户转介数据
     *
     * @param query           查询条件
     * @param temp            销售列表
     * @param totalStatistics 访客数据
     */
    private void setCustomerRecommend(VisitorVisitAnalysisQuery query, List<String> temp, TotalStatistics totalStatistics) {
        List<String> queryList = staffVisitorService.getQueryListByCollection(temp);
        if (CollectionUtils.isEmpty(queryList)) {
            totalStatistics.setCustomerRecommend(0);
        } else {
            totalStatistics.setCustomerRecommend(collectorVisitorService
                    .getCustomerRecommend(query, queryList)
                    .intValue());
        }
    }

    /**
     * 抽出的钻取员工数据列表
     *
     * @param query 查询条件
     * @return List<AccessStatistics>
     */
    private DataAnalysisDetailVO getSellerList(VisitorVisitAnalysisQuery query,
                                               Integer groupId,
                                               DataAnalysisDetailVO dataAnalysisDetailVO) {

        Optional.ofNullable(groupId).orElseThrow(() -> new BusinessException("分组id不能为空"));

        List<DataAnalysisDetailListVO> dataAnalysisDetailList = new ArrayList<>();

        List<String> list = new ArrayList<>();

        List<SellerVO> userInfoList = userInfoService.getUserInfoList(groupId);

        Optional.ofNullable(userInfoList).ifPresent(l -> l.forEach(v -> {
            String userUuid = v.getId();
            if (query.getSellerId().equals(v.getId())) {
                return;
            }
            list.add(userUuid);

            Long data = getData(query, list, userUuid);

            dataAnalysisDetailList.add(new DataAnalysisDetailListVO(
                    v.getId(),
                    v.getName(),
                    v.getAvatar(),
                    data,
                    v.getProjectName()));

            list.remove(0);
        }));

        dataAnalysisDetailList.sort(Comparator.reverseOrder());

        dataAnalysisDetailVO.setHasGroup(HAS_GROUP);
        dataAnalysisDetailVO.setDataAnalysisDetailList(dataAnalysisDetailList);
        return dataAnalysisDetailVO;
    }

    /**
     * 获取销售对应的客户信息
     *
     * @param query 查询条件
     * @return DataAnalysisDetailVO
     */
    private DataAnalysisDetailVO getCustomerList(VisitorVisitAnalysisQuery query) {
        DataAnalysisDetailVO dataAnalysisDetailVO = new DataAnalysisDetailVO();
        dataAnalysisDetailVO.setHasGroup(HAS_NOT_GROUP);
        dataAnalysisDetailVO.setStaffVisitorList(visitorService.selectVisitorByUserId(query));
        return dataAnalysisDetailVO;
    }

    /**
     * 隐藏指定信息
     *
     * @param hideColumn 隐藏字段标识
     * @param bc         BushinessCardHomeVO
     */
    private void hideSaleManMessage(String hideColumn, BussinessCardHomeVO bc) {
        try {
            if (StringUtils.isNotBlank(hideColumn)) {
                bc.setHideColumn(getHideColumn(hideColumn));
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("json格式转换错误");
        }
    }

    /**
     * 设置隐藏属性
     *
     * @param userInfo 用户扩展信息
     * @param homeVO   返回VO
     */
    private void setHideProperties(UserInfo userInfo, BussinessCardHomeVO homeVO) {
        hideSaleManMessage(userInfo.getHideColumn(), homeVO);
        String[] hideColumn = homeVO.getHideColumn();
        Optional.ofNullable(hideColumn).ifPresent(h -> {
            //隐藏手机号
            if (Arrays.asList(h).contains(PHONE)) {
                homeVO.setPhone("");
            }
        });
    }

    /**
     * 获取项目名
     *
     * @param projectId 项目id
     * @return String
     */
    private String getProjectName(Integer projectId) {
        assert projectId != null;
        return Optional.ofNullable(projectService.lambdaQuery()
                .eq(Project::getId, projectId)
                .one()).map(Project::getProjectTitle)
                .orElse("");
    }

    /**
     * 获取活跃度
     *
     * @param userId 销售id
     * @return int
     */
    private int getPopularityCounts(String userId) {
        return collectorVisitorService.lambdaQuery()
                .eq(CollectorVisitor::getSellerId, userId)
                .count();
    }

    /**
     * 获取隐藏字段
     *
     * @param hideColumn 隐藏字段
     * @return String[]
     */
    private String[] getHideColumn(String hideColumn) {
        try {
            if (StringUtils.isNotBlank(hideColumn)) {
                return hideColumn.split(",");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("json格式转换错误,data:{}", hideColumn);
        }
        return new String[]{};
    }

    /**
     * 获取销售id
     *
     * @param visitorVisitAnalysisQuery query对象
     * @return java.lang.String
     */
    private String getSellerId(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery) {
        String sellerId = visitorVisitAnalysisQuery.getSellerId();

        if (StringUtils.isBlank(sellerId)) {
            sellerId = UserInfoContext.get().getUserId();
        }
        return sellerId;
    }

    /**
     * 获取销售列表
     *
     * @param sellerId 员工id
     * @param userId   客户id
     * @return List<String>
     */
    private List<String> getSellerIdList(String sellerId, String userId) {
        List<String> userIdList = userGroupManagerService.getSellerIdListAddMe(sellerId);

        List<BindingManager> list = bindingManagerService.lambdaQuery()
                .select(BindingManager::getUserUuid)
                .eq(BindingManager::getVisitorUuid, userId)
                .list();

        List<String> sellerList = new ArrayList<>();

        Optional.ofNullable(list).ifPresent(l ->
                l.forEach(v -> Optional.ofNullable(userIdList).ifPresent(u ->
                        u.forEach(i -> {
                            if (i.equals(v.getUserUuid())) {
                                sellerList.add(i);
                            }
                        }))));

        MyCollectionUtils.removeDuplicate(sellerList);

        return sellerList;
    }

    @Override
    public List<SellerActivitisVO> getSellerActivitiesList(String sellerId) {
        List<SellerActivitisVO> sellerActivities = baseMapper.getSellerActivities(sellerId);
        sellerActivities.parallelStream().forEach(s -> {
            LocalDateTime startAt = s.getStartAt();
            LocalDateTime stopAt = s.getStopAt();
            LocalDateTime now = LocalDateTime.now();
            if (now.isBefore(startAt)) {
                //活动未开始
                s.setStatus(0);
            } else if (now.isAfter(stopAt)) {
                //活动已结束
                s.setStatus(2);
            } else {
                //活动进行中
                s.setStatus(1);
            }
        });
        return sellerActivities;
    }

    @Override
    public List<StaffNameVO> getSellerPushStaff(Integer projectId) {
        return baseMapper.getSellerName(projectId);
    }

    @Override
    public List<PushedActivityVO> getPushActivity(String userUuid) {
        return baseMapper.getPushActivity(userUuid);
    }

    @Override
    public boolean checkDeletedGroupIncludeProject(Integer groupId) {
        return projectService.lambdaQuery().eq(Project::getProjectGroupId, groupId).list().isEmpty();
    }

    /**
     * 切换身份
     *
     * @param userRegister   注册用户
     * @param visitorId      访客id
     * @param phone          手机号
     * @param checkPhoneUser 用户手机
     * @param oauthWeChat    微信信息
     * @return boolean
     */
    private boolean changeUserType(UserRegister userRegister, String visitorId, String phone, User checkPhoneUser, OauthWechat oauthWeChat) {
        if (oauthWeChat.getUserType().equals(VISITOR)) {
            //用户切换身份后用户的id仍然使用之前
            lambdaUpdate().eq(User::getPhone, phone)
                    .set(User::getUuid, oauthWeChat.getUserUuid())
                    .set(User::getAvatar, oauthWeChat.getAvatarUrl())
                    .set(User::getNickName, oauthWeChat.getNickName())
                    .update();

            //转换信息待定,此处用于后续用户切换身份的逻辑处理
            userInfoService.lambdaUpdate().eq(UserInfo::getUserUuid, checkPhoneUser.getUuid())
                    .set(UserInfo::getUserUuid, oauthWeChat.getUserUuid())
                    .set(UserInfo::getAddress, oauthWeChat.spliceAddress())
                    .update();

            /*
              修改记录:  切换身份直接删除访客记录,重新生成
              时间: 2019-07-10 20:06:14
             */
            if (!visitorService.removeById(visitorId)) {
                log.error("切换身份，删除原先访客{}记录信息失败,准备业务回滚", visitorId);
                throw new BusinessException("删除访客信息失败");
            }
            // 删除访客bindingManager表
            HashMap<String, Object> map = new HashMap<>(1);
            map.put("visitor_uuid", visitorId);
            boolean removeBindManager = bindingManagerService.removeByMap(map);
            log.info("删除访客bindingManager记录: {}, visitorId: {}", removeBindManager, visitorId);

            // 删除访客staff_visitor表
            boolean removeStaffVisitor = staffVisitorService.removeByMap(map);
            log.info("删除访客staffVisitor记录: {}, visitorId: {}", removeStaffVisitor, visitorId);

            // 删除分享表信息
            recommendService.deleteVisitorShareRecord(visitorId);

            //更新微信表信息
            oauthWeChat.setUserType(STAFF);
            if (oauthWeChatService.updateUserWeChatInfoByIdByCache(oauthWeChat) == null) {
                log.error("更新用户身份{}信息失败", oauthWeChat);
                throw new BusinessException("更新微信信息失败");
            }
            log.info("删除销售相关的采集数据");

            HashMap<String, Object> removeCondition = new HashMap<>(1);
            removeCondition.put("visitor_id", visitorId);
            collectorVisitorService.removeByMap(removeCondition);
            collectorAllRecordService.removeByMap(removeCondition);

            //添加项目到对应的销售
            bindProjectToUser(oauthWeChat.getUserUuid(), userRegister);

            return true;
        } else if (oauthWeChat.getUserType().equals(STAFF)) {
            JSONArray jsonArray = JSON.parseArray(userRegister.getProjectList());

            List<ProjectManager> addPro = new ArrayList<>();
            List<Integer> list = projectManagerService.lambdaQuery().select(ProjectManager::getProjectId)
                    .eq(ProjectManager::getUserUuid, oauthWeChat.getUserUuid()).list().stream().map(ProjectManager::getProjectId).collect(Collectors.toList());

            jsonArray.stream().filter(proId -> !list.contains(Integer.parseInt(proId.toString()))).forEach(proId -> {
                ProjectManager pm = new ProjectManager();
                pm.setUserUuid(oauthWeChat.getUserUuid());
                pm.setProjectId((Integer) proId);
                addPro.add(pm);
            });
            if (!addPro.isEmpty()) {
                projectManagerService.saveBatch(addPro);
            }
        }
        return false;
    }

    /**
     * 删除用户信息
     *
     * @param userId       用户id
     * @param userIdFromDb 用户id
     * @return boolean
     */
    private boolean deleteUser(String userId, String userIdFromDb) {
        log.info("删除销售相关的采集数据");
        Map<String, Object> removeCondition = new HashMap<>(1);
        removeCondition.put(userIdFromDb, userId);
        collectorVisitorService.removeByMap(removeCondition);
        collectorAllRecordService.removeByMap(removeCondition);
        tmpClean(userId);
        return true;
    }

    /**
     * 清除数据
     *
     * @param userId 用户id
     */
    private void tmpClean(String userId) {
        Map<String, Object> condition = new HashMap<>(1);
        condition.put("source_uuid", userId);
        clickRecordService.removeByMap(condition);
        condition.clear();
        condition.put("target_uuid", userId);
        clickRecordService.removeByMap(condition);

        condition.clear();
        condition.put("share_user", userId);
        shareService.removeByMap(condition);
        condition.clear();
        condition.put("user_id", userId);
        if (!collectorResourceService.removeByMap(condition)) {
            log.error("删除该员工资源采集表信息失败");
        }
    }

    /**
     * 保存用户扩展信息
     *
     * @param staffInfoList 导入数据
     * @param userList      用户基本数据
     * @param userInfoList  用户扩展数据
     * @param df            时间转换格式
     * @return boolean
     */
    private boolean saveUserInfo(List<StaffInfoBO> staffInfoList, List<User> userList, List<UserInfo> userInfoList, DateTimeFormatter df) {
        userList.forEach(user -> staffInfoList.forEach(staff -> {
            if (Objects.equals(staff.getPhone(), user.getPhone())) {
                String userUuid = user.getUuid();
                UserInfo userInfo = new UserInfo(
                        userUuid,
                        staff.getCompanyName(),
                        staff.getWechat(),
                        staff.getBip(),
                        staff.getJobName(),
                        staff.getCategory(),
                        StringUtils.isBlank(staff.getRegisterDate())
                                ? null
                                : LocalDateTime.parse(staff.getRegisterDate(), df),
                        staff.getDepartment(),
                        staff.getGroup()
                );

                //添加绑定项目
                saveBindProject(staff, userUuid);

                userInfoList.add(userInfo);
            }
        }));

        // 保存用户补充信息
        return userInfoService.saveOrUpdateBatch(userInfoList);
    }

    /**
     * 保存员工基本信息
     *
     * @param staffInfoList 导入数据
     * @param userList      用户基本数据
     * @param users         db数据
     * @param df            时间转换格式
     * @return boolean
     */
    private boolean saveUser(List<StaffInfoBO> staffInfoList, List<User> userList, List<User> users, DateTimeFormatter df) {
        staffInfoList.forEach(staff -> {
            User user = new User();

            //设置已存在用户的uuid用于更新操作
            users.parallelStream()
                    .filter(u -> u.getPhone().equals(staff.getPhone()))
                    .forEach(u -> user.setUuid(u.getUuid()));

            user.setPhone(staff.getPhone());
            user.setNickName(staff.getNickName());
            user.setIsActive(staff.getStatus());
            user.setCreateAt(StringUtils.isBlank(staff.getRegisterDate())
                    ? null
                    : LocalDateTime.parse(staff.getRegisterDate(), df));

            userList.add(user);
        });

        // 保存用户
        return this.saveOrUpdateBatch(userList);
    }

    /**
     * 保存绑定项目
     *
     * @param staff    导入员工信息
     * @param userUuid 员工id
     */
    private void saveBindProject(StaffInfoBO staff, String userUuid) {
        String projectIdsString = staff.getBindProject();
        if (StringUtils.isBlank(projectIdsString)) {
            return;
        }
        List<ProjectManager> projectManagerList = new ArrayList<>();
        String[] projectIds = projectIdsString.split(",");
        Arrays.stream(projectIds)
                .filter(p -> CollectionUtils.isEmpty(projectManagerService.lambdaQuery()
                        .eq(ProjectManager::getUserUuid, userUuid)
                        .eq(ProjectManager::getProjectId, Integer.valueOf(p))
                        .list()))
                .forEach(v -> projectManagerList.add(new ProjectManager(Integer.valueOf(v), userUuid)));

        projectManagerService.saveBatch(projectManagerList);
    }

    /**
     * 设置高等级角色数据
     *
     * @param dataAnalysisDetailVO 返回对象
     * @param query                查询条件
     * @param sellerId             销售id
     */
    private void setHigherSellerData(DataAnalysisDetailVO dataAnalysisDetailVO, VisitorVisitAnalysisQuery query, String sellerId) {
        SellerVO userInfoVO = userInfoService.getUserInfoVO(sellerId);
        DataAnalysisDetailListVO vo = new DataAnalysisDetailListVO();
        List<String> userList = new ArrayList<>();
        userList.add(sellerId);
        vo.setAvatar(userInfoVO.getAvatar());
        vo.setId(userInfoVO.getId());
        vo.setName(userInfoVO.getName());
        vo.setProjectName(userInfoVO.getProjectName());
        vo.setCount(getData(query, userList, sellerId));
        dataAnalysisDetailVO.setStaffAnalysisVO(vo);
    }

    /**
     * 获取总数
     *
     * @param query    查询条件
     * @param list     销售列表
     * @param userUuid 销售id
     * @return Long
     */
    private Long getData(VisitorVisitAnalysisQuery query, List<String> list, String userUuid) {
        Long data = 0L;
        List<String> queryList = getQueryList(query, list, userUuid);
        if (CollectionUtils.isNotEmpty(queryList)) {
            data = staffVisitorService.getData(query, queryList);
        }
        return data;
    }

    /**
     * 获取查询列表
     *
     * @param query    查询条件
     * @param list     销售列表
     * @param userUuid 销售id
     * @return List<String>
     */
    private List<String> getQueryList(VisitorVisitAnalysisQuery query, List<String> list, String userUuid) {
        if (Constants.DataType.CUSTOMER_RECOMMEND == query.getDataType()) {
            List<BindingManager> bindingManagers = bindingManagerService.lambdaQuery()
                    .eq(BindingManager::getUserUuid, userUuid)
                    .eq(BindingManager::getHistoryLabel, 0)
                    .list();
            if (CollectionUtils.isNotEmpty(bindingManagers)) {
                return bindingManagers.parallelStream()
                        .map(BindingManager::getVisitorUuid)
                        .distinct()
                        .collect(Collectors.toList());
            } else {
                return new ArrayList<>();
            }
        }
        return list;
    }

}