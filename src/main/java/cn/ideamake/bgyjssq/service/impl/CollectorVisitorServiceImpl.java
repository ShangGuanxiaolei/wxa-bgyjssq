package cn.ideamake.bgyjssq.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONObject;
import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.dao.mapper.CollectorVisitorMapper;
import cn.ideamake.bgyjssq.pojo.entity.CollectorVisitor;
import cn.ideamake.bgyjssq.pojo.query.DashBoardQuery;
import cn.ideamake.bgyjssq.pojo.query.DistributionMapQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorVisitAnalysisQuery;
import cn.ideamake.bgyjssq.pojo.vo.CustomerDistributionMapVO;
import cn.ideamake.bgyjssq.pojo.vo.RecommendAnalysisVO;
import cn.ideamake.bgyjssq.pojo.vo.dashboard.CustomerVisitData;
import cn.ideamake.bgyjssq.pojo.vo.dashboard.MiniProgramDataVO;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.OriginCountCell;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.TotalStatistics;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.TotalVisitTrend;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.VisitPeriod;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.VisitTrend;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.VisitTrendCell;
import cn.ideamake.bgyjssq.service.ICollectorVisitorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * <p>
 * 项目访问采集 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Service
public class CollectorVisitorServiceImpl extends ServiceImpl<CollectorVisitorMapper, CollectorVisitor> implements ICollectorVisitorService {

    /**
     * 客户访问数据-客户单次访问时长统计规则引擎
     */
    @Value("classpath:rule/customer-visit-data.json")
    private Resource customerVisitDataRule;

    @Override
    public LocalDateTime getVisitorLastVisitTime(String visitorUuid) {

        if (null == visitorUuid) {
            throw new BusinessException("访客ID必传");
        }

        // 写在此处,是为了后期加缓存
        CollectorVisitor visitor = lambdaQuery()
                .select(CollectorVisitor::getCreateAt)
                .eq(CollectorVisitor::getVisitorId, visitorUuid)
                .orderByDesc(CollectorVisitor::getCreateAt)
                .last("limit 1")
                .one();

        if (null != visitor) {
            return visitor.getCreateAt();
        }
        return null;
    }

    @Override
    public VisitTrend getVisitTrendForPc(DashBoardQuery dashBoardQuery) {
        VisitTrend visitTrend = new VisitTrend();
        List<VisitTrendCell> visitorNum = baseMapper.getPcPvForProjects(dashBoardQuery);
        List<VisitTrendCell> numberOfPeople = baseMapper.getPcUvForProjects(dashBoardQuery);
        visitTrend.setNumberOfPeople(numberOfPeople);
        visitTrend.setVisitorNum(visitorNum);
        return visitTrend;
    }

    @Override
    public TotalVisitTrend getTotalSummaryDataForPc(DashBoardQuery dashBoardQuery) {
        TotalVisitTrend totalVisitTrend = new TotalVisitTrend();
        List<VisitTrendCell> visitorNum = baseMapper.getPcPvForProjects(dashBoardQuery);
        List<VisitTrendCell> numberOfPeople = baseMapper.getPcUvForProjects(dashBoardQuery);
        List<VisitTrendCell> peopleOfRemainPhone =baseMapper.getPcRemainPhoneForProjects(dashBoardQuery);
        List<VisitTrendCell> peopleOfAccess = baseMapper.getPcVisitedForProjects(dashBoardQuery);
        totalVisitTrend.setVisitorNum(visitorNum);
        totalVisitTrend.setNumberOfPeople(numberOfPeople);
        totalVisitTrend.setPeopleOfRemainPhone(peopleOfRemainPhone);
        totalVisitTrend.setPeopleOfAccess(peopleOfAccess);
        return totalVisitTrend;
    }

    @Override
    public TotalStatistics getVisitorVisitTotalData(
            VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            List<String> userIdList) {
        return baseMapper.getVisitorVisitTotalDataDao(visitorVisitAnalysisQuery, userIdList);
    }

    @Override
    public Long getNumberOfVisits(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList) {
        return baseMapper.getNumberOfVisits(visitorVisitAnalysisQuery, userIdList);
    }

    @Override
    public Long getNumberOfPeople(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList) {
        return baseMapper.getNumberOfPeople(visitorVisitAnalysisQuery, userIdList);
    }

    @Override
    public Long getPeopleOfRemainPhone(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList) {
        return baseMapper.getPeopleOfRemainPhone(visitorVisitAnalysisQuery, userIdList);
    }

    @Override
    public Long getPeopleOfAccess(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList) {
        return baseMapper.getPeopleOfAccess(visitorVisitAnalysisQuery, userIdList);
    }

    @Override
    public Long getShareCount(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList) {
        return baseMapper.getShareCount(visitorVisitAnalysisQuery, userIdList);
    }

    @Override
    public Long getSharePeople(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList) {
        return baseMapper.getSharePeople(visitorVisitAnalysisQuery, userIdList);
    }

    @Override
    public Long getReadCount(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList) {
        return baseMapper.getReadCount(visitorVisitAnalysisQuery, userIdList);
    }

    @Override
    public Long getReadPeople(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList) {
        return baseMapper.getReadPeople(visitorVisitAnalysisQuery, userIdList);
    }

    @Override
    public Long getSellerRecommend(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList) {
        return baseMapper.getSellerRecommend(visitorVisitAnalysisQuery, userIdList);
    }

    @Override
    public Long getCustomerRecommend(VisitorVisitAnalysisQuery visitorVisitAnalysisQuery, List<String> userIdList) {
        return baseMapper.getCustomerRecommend(visitorVisitAnalysisQuery, userIdList);
    }

    @Override
    public List<OriginCountCell> getVisitorVisitOriginData(
            VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            List<String> userIdList) {
        return baseMapper.getVisitorVisitOriginDataDao(visitorVisitAnalysisQuery, userIdList);
    }

    @Override
    public VisitTrend getVisitorVisitTrendData(
            VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            List<String> userIdList) {
        VisitTrend visitTrend = new VisitTrend();
        List<VisitTrendCell> visitorNum =
                baseMapper.getVisitorVisitTrendVisitTimesDataDao(visitorVisitAnalysisQuery, userIdList);
        List<VisitTrendCell> numberOfPeople =
                baseMapper.getVisitorVisitTrendPeopleNumberDataDao(visitorVisitAnalysisQuery, userIdList);
        visitTrend.setNumberOfPeople(numberOfPeople);
        visitTrend.setVisitorNum(visitorNum);
        return visitTrend;
    }

    @Override
    public List<VisitPeriod> getVisitorVisitPeriodData(
            VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            List<String> userIdList) {
        return baseMapper.getVisitorVisitPeriodDataDao(visitorVisitAnalysisQuery, userIdList);
    }

    @Override
    public JSONObject getCustomerVisitData(DashBoardQuery query) {

        // 1. 查客户单次访问时长, 已经是根据时间段从小到大汇总
        int[] visitors = new int[7];
        AtomicBoolean change = new AtomicBoolean(false);
        lambdaQuery().select(CollectorVisitor::getStayTime)
                .ge(null != query.getStartTime(), CollectorVisitor::getCreateAt, query.getStartTime())
                .le(null != query.getEndTime(), CollectorVisitor::getCreateAt, query.getEndTime())
                .in(CollUtil.isNotEmpty(query.getProjectIds()), CollectorVisitor::getProjectId, query.getProjectIds())
                .orderByAsc(CollectorVisitor::getStayTime)
                .list()


                .parallelStream()
                .map(CollectorVisitor::getStayTime)
                .collect(Collectors.toList())
                .parallelStream()
                .forEach(time -> {
                    change.set(true);
                    if (time > 0 && time <= 10) {
                        visitors[0] += 1;
                    } else if (time <= 30) {
                        visitors[1] += 1;
                    } else if (time <= 60) {
                        visitors[2] += 1;
                    } else if (time <= 180) {
                        visitors[3] += 1;
                    } else if (time <= 600) {
                        visitors[4] += 1;
                    } else if (time <= 1800) {
                        visitors[5] += 1;
                    } else {
                        visitors[6] += 1;
                    }
                });
        ArrayList<CustomerVisitData> visitDataArrayList = new ArrayList<>(7);
        if (change.get()) {
            for (int i = 0, visitorsLength = visitors.length; i < visitorsLength; i++) {
                int visitor = visitors[i];
                CustomerVisitData customerVisitData = new CustomerVisitData();
                customerVisitData.setValue(visitor).setDate(i);
                visitDataArrayList.add(customerVisitData);
            }
        }

        // 2. 查全天活跃时段长
        List<CustomerVisitData> fullTimeActiveTimeList = baseMapper.getFullTimeActiveTime(query);

        JSONObject result = new JSONObject();
        result.put("activeTime", fullTimeActiveTimeList);
        result.put("singleTime", visitDataArrayList);
        return result;
    }

    @Override
    public MiniProgramDataVO getVisitData(DashBoardQuery query) {
        return new MiniProgramDataVO(baseMapper.selectNumberOfVisits(query),
                baseMapper.selectNumberOfPeople(query),
                baseMapper.selectPeopleOfRemainPhone(query),
                baseMapper.selectPeopleOfAccess(query));
    }

    @Override
    public RecommendAnalysisVO selectRecommendAllTotal(String sourceId) {
        return baseMapper.selectRecommendAllTotal(sourceId);
    }

    @Override
    public List<CustomerDistributionMapVO> getCustomerDistributionMap(DistributionMapQuery query) {
        return baseMapper.selectCustomerDistributionMap(query);
    }

}
