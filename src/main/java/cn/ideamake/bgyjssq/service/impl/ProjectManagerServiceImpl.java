package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.dao.mapper.ProjectManagerMapper;
import cn.ideamake.bgyjssq.pojo.dto.StaffHideProjectDTO;
import cn.ideamake.bgyjssq.pojo.entity.ProjectManager;
import cn.ideamake.bgyjssq.service.IProjectManagerService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 员工项目管理表 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
@Service
@Slf4j
public class ProjectManagerServiceImpl extends ServiceImpl<ProjectManagerMapper, ProjectManager> implements IProjectManagerService {

    @Override
    public boolean hideProjectForStaff(StaffHideProjectDTO staffHideProjectDTO) {

        if (staffHideProjectDTO.getIsRecommended() == 1) {
            log.info("通过销售id:{}显示项目id:{}", staffHideProjectDTO.getSellerId(), staffHideProjectDTO.getProjectId());
            return lambdaUpdate().set(ProjectManager::getStatus, 1).eq(ProjectManager::getUserUuid, staffHideProjectDTO.getSellerId()).eq(ProjectManager::getProjectId, staffHideProjectDTO.getProjectId()).update();
        } else if (staffHideProjectDTO.getIsRecommended() == 0) {
            log.info("通过销售id:{}隐藏项目id:{}", staffHideProjectDTO.getSellerId(), staffHideProjectDTO.getProjectId());
            return lambdaUpdate().set(ProjectManager::getStatus, 0).eq(ProjectManager::getUserUuid, staffHideProjectDTO.getSellerId()).eq(ProjectManager::getProjectId, staffHideProjectDTO.getProjectId()).update();

        } else {
            throw new BusinessException("错误标识");
        }
    }

    @Override
    public String tmpTest(String userId, String proIds) {
        List<Integer> list = lambdaQuery().select(ProjectManager::getProjectId)
                .eq(ProjectManager::getUserUuid, userId).list().stream().map(ProjectManager::getProjectId).collect(Collectors.toList());
        JSONArray jsonArray = JSON.parseArray(proIds);
        List<ProjectManager> addPro = new ArrayList<>();
        jsonArray.stream().filter(proId -> !list.contains(Integer.parseInt(proId.toString()))).forEach(proId -> {
            ProjectManager pm = new ProjectManager();
            pm.setUserUuid(userId);
            pm.setProjectId((Integer) proId);
            addPro.add(pm);
        });
        return addPro.toString();
    }

    @Override
    public List<String> getEffectiveData(Set<Integer> projects, List<String> list) {
        //项目为空的情况下，处理异常问题
        if (CollectionUtils.isEmpty(projects)) {
            return list;
        }
        List<String> temp = new ArrayList<>();
        List<ProjectManager> projectManagerList = this.lambdaQuery().in(ProjectManager::getProjectId, projects).list();
        Optional.ofNullable(projectManagerList).ifPresent(p -> temp.addAll(
                projectManagerList.stream()
                        .map(ProjectManager::getUserUuid)
                        .collect(Collectors.toList())
                        .stream()
                        .filter(list::contains)
                        .distinct()
                        .collect(Collectors.toList())));
        return temp;
    }
}
