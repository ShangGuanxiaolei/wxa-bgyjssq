package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.dao.mapper.UserRegisterMapper;
import cn.ideamake.bgyjssq.pojo.entity.UserRegister;
import cn.ideamake.bgyjssq.service.IUserRegisterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 管理员工邀请注册的接口 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-06-21
 */
@Service
public class UserRegisterServiceImpl extends ServiceImpl<UserRegisterMapper, UserRegister> implements IUserRegisterService {


}
