package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.pojo.entity.Resource720;
import cn.ideamake.bgyjssq.dao.mapper.Resource720Mapper;
import cn.ideamake.bgyjssq.pojo.vo.floorbook.FloorBook720VO;
import cn.ideamake.bgyjssq.service.IResource720Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 720楼书 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-09-03
 */
@Service
public class Resource720ServiceImpl extends ServiceImpl<Resource720Mapper, Resource720> implements IResource720Service {

    @Override
    public List<FloorBook720VO> selectFloorBook(Integer projectId, Integer status) {
        return baseMapper.selectFloorBook(projectId, status);
    }
}
