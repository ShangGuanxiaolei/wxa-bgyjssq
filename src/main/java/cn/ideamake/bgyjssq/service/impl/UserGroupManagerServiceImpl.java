package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.common.enums.DictType;
import cn.ideamake.bgyjssq.common.handler.UserInfoContext;
import cn.ideamake.bgyjssq.dao.mapper.UserGroupManagerMapper;
import cn.ideamake.bgyjssq.pojo.dto.UserGroupEditDTO;
import cn.ideamake.bgyjssq.pojo.entity.Dict;
import cn.ideamake.bgyjssq.pojo.entity.SysAdminProject;
import cn.ideamake.bgyjssq.pojo.entity.UserGroupManager;
import cn.ideamake.bgyjssq.pojo.vo.UserBaseVO;
import cn.ideamake.bgyjssq.pojo.vo.UserGroupEditVO;
import cn.ideamake.bgyjssq.pojo.vo.UserGroupVO;
import cn.ideamake.bgyjssq.pojo.vo.UserListVO;
import cn.ideamake.bgyjssq.service.IDictService;
import cn.ideamake.bgyjssq.service.IUserGroupManagerService;
import cn.ideamake.bgyjssq.service.permission.ISysAdminProjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

/**
 * <p>
 * 员工分组管理表 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-08-13
 */
@AllArgsConstructor
@Service
@Slf4j
public class UserGroupManagerServiceImpl extends ServiceImpl<UserGroupManagerMapper, UserGroupManager> implements IUserGroupManagerService {

    private static final String NO_GROUP = "未分组";

    private final IDictService dictService;

    private final ISysAdminProjectService sysAdminProjectService;

    @Override
    public List<String> getSellerIdList(String sellerId) {
        return baseMapper.getSellerIdListByUserId(sellerId);
    }

    @Override
    public List<String> getSellerIdListAddMe(String sellerId) {
        List<String> list = baseMapper.getSellerIdListByUserId(sellerId);
        list.add(sellerId);
        return list;
    }

    @Override
    public List<Integer> getGroupIdList(String sellerId, List<Integer> projectIds) {
        return baseMapper.getGroupIdList(sellerId, projectIds);
    }

    @Override
    public List<String> getSellerIdListByGroupIds(List<Integer> groupIds) {
        return baseMapper.getSellerIdListByGroupIds(groupIds);
    }

    @Override
    public boolean isHigherLevelRole(String sellerId) {
        return !lambdaQuery().eq(UserGroupManager::getUserUuid, sellerId).list().isEmpty();
    }

    @Override
    public List<UserGroupVO> getGroup(List<Integer> projectIds) {
        return baseMapper.getGroup(projectIds);
    }

    @Override
    public List<UserListVO> getUser(Integer groupId) {
        return baseMapper.getUser(groupId);
    }

    @Override
    public List<UserGroupVO> getUserGroup() {
        List<UserGroupVO> group;
        Integer adminId = UserInfoContext.getAdminId();
        List<SysAdminProject> list = sysAdminProjectService.lambdaQuery()
                .eq(SysAdminProject::getSysAdminId, adminId)
                .list();
        if (CollectionUtils.isNotEmpty(list)) {
            List<Integer> projectIds = list.stream()
                    .map(SysAdminProject::getProjectId)
                    .collect(toList());
            group = this.getGroup(projectIds);
        } else {
            group = this.getGroup(null);
        }
        setUserGroup(group);
        return group;
    }

    @Override
    public List<UserGroupVO> getGroupByProjectId(Integer projectId) {
        List<UserGroupVO> group = baseMapper.getGroupByProjectId(projectId);
        setUserGroup(group);
        return group;
    }

    @Override
    public List<UserListVO> getUserNoGroup() {
        return baseMapper.getUserNoGroup();
    }

    @Override
    public UserGroupEditVO selectUserAndManagerByGroupId(Integer groupId) {
        UserGroupEditVO userGroupEditVO = new UserGroupEditVO();
        userGroupEditVO.setManagers(baseMapper.selectManagerByGroupId(groupId));
        userGroupEditVO.setUsers(baseMapper.selectUserByGroupId(groupId));
        return userGroupEditVO;
    }

    @Override
    public List<UserBaseVO> selectUsers() {
        return baseMapper.selectUsers();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateGroupManagerInfo(UserGroupEditDTO userGroupEditDTO) {
        StringBuilder stringBuilder = new StringBuilder();
        List<String> userIds = userGroupEditDTO.getUserIds();
        if (userIds != null) {
            userIds.forEach(u -> {
                stringBuilder.append(u);
                stringBuilder.append(",");
            });
        }
        if (null != userGroupEditDTO.getGroupId()) {
            //先更新组信息
            dictService.lambdaUpdate()
                    .eq(Dict::getId, userGroupEditDTO.getGroupId())
                    .set(Dict::getName, userGroupEditDTO.getName())
                    .set(Dict::getProjectId, userGroupEditDTO.getProjectId())
                    .set(Dict::getResources, stringBuilder.toString())
                    .update();
            //列表不为空时处理逻辑
            if (!userGroupEditDTO.getUserIds().isEmpty()) {
                List<UserGroupManager> collect = lambdaQuery().eq(UserGroupManager::getGroupId, userGroupEditDTO.getGroupId()).list();
                if (collect.isEmpty()) {
                    log.info("组{}无管理人员", userGroupEditDTO.getGroupId());
                    List<UserGroupManager> finalAddList = new ArrayList<>();
                    userGroupEditDTO.getUserIds().forEach(u -> finalAddList.add(new UserGroupManager().setUserUuid(u).setGroupId(userGroupEditDTO.getGroupId())));
                    return saveBatch(finalAddList);
                }
                //获取新增列表

                List<String> collectTmp = collect.stream().map(UserGroupManager::getUserUuid).collect(toList());
                List<UserGroupManager> addList = new ArrayList<>();
                userGroupEditDTO.getUserIds().stream().filter(s -> !collectTmp.contains(s)
                ).forEach(s ->
                        addList.add(new UserGroupManager().setGroupId(userGroupEditDTO.getGroupId()).setUserUuid(s))
                );
                if (!addList.isEmpty()) {
                    saveBatch(addList);
                }


                //获取删除列表
                List<Integer> deleteList = collect.stream().filter(s -> !userGroupEditDTO.getUserIds().contains(s.getUserUuid())).map(UserGroupManager::getId).collect(toList());
                if (!deleteList.isEmpty()) {
                    removeByIds(deleteList);
                }
                return true;
            } else {
                Map<String, Object> map = new HashMap<>(2);
                map.put("group_id", userGroupEditDTO.getGroupId());
                return removeByMap(map);

            }
        } else {
            Dict dict = new Dict();
            dict.setName(userGroupEditDTO.getName());
            dict.setType(DictType.STAFF);
            dict.setProjectId(userGroupEditDTO.getProjectId());
            dict.setResources(stringBuilder.toString());
            dictService.save(dict);
            if (CollectionUtils.isNotEmpty(userGroupEditDTO.getUserIds())) {
                List<UserGroupManager> userGroupManagers = new ArrayList<>();
                userGroupEditDTO.getUserIds().forEach(s -> userGroupManagers.add(new UserGroupManager().setGroupId(dict.getId()).setUserUuid(s)));
                if (!userGroupManagers.isEmpty()) {
                    saveBatch(userGroupManagers);
                }
            }


        }
        return true;
    }

    /**
     * 设置分组员工列表
     *
     * @param group 分组
     */
    private void setUseList(List<UserGroupVO> group) {
        Optional.ofNullable(group).ifPresent(g -> g.forEach(v -> {
            Integer id = v.getId();
            Optional.ofNullable(id).ifPresent(i -> {
                List<UserListVO> user = this.getUser(i);
                v.setUserList(user);
            });
        }));
    }

    /**
     * 设置员工分组
     *
     * @param group 组
     */
    private void setUserGroup(List<UserGroupVO> group) {
        setUseList(group);
        List<UserListVO> userNoGroup = this.getUserNoGroup();
        if (CollectionUtils.isNotEmpty(userNoGroup)) {
            UserGroupVO userGroupVO = new UserGroupVO();
            userGroupVO.setUserList(userNoGroup);
            userGroupVO.setGroupName(NO_GROUP);
            userGroupVO.setId(0);
            Optional.ofNullable(group).ifPresent(g -> g.add(userGroupVO));
        }
    }


}
