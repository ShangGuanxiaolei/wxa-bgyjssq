package cn.ideamake.bgyjssq.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.ideamake.bgyjssq.common.enums.DictType;
import cn.ideamake.bgyjssq.common.enums.PosterCategory;
import cn.ideamake.bgyjssq.common.enums.ResourceType;
import cn.ideamake.bgyjssq.common.enums.StaffNotificationType;
import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.handler.UserInfoContext;
import cn.ideamake.bgyjssq.common.util.Constants;
import cn.ideamake.bgyjssq.dao.mapper.ResourceMapper;
import cn.ideamake.bgyjssq.pojo.dto.PublishResourceDTO;
import cn.ideamake.bgyjssq.pojo.dto.ResourceBulkOperation;
import cn.ideamake.bgyjssq.pojo.entity.Dict;
import cn.ideamake.bgyjssq.pojo.entity.ProjectManager;
import cn.ideamake.bgyjssq.pojo.entity.Resource;
import cn.ideamake.bgyjssq.pojo.query.QueryPage;
import cn.ideamake.bgyjssq.pojo.query.ResourceListQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffNotificationDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffNotificationQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffNotifyListQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorNotificationQuery;
import cn.ideamake.bgyjssq.pojo.vo.ArticleDetailVisitorListVO;
import cn.ideamake.bgyjssq.pojo.vo.NotifyDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.PVAndUvVO;
import cn.ideamake.bgyjssq.pojo.vo.ResourceBaseVO;
import cn.ideamake.bgyjssq.pojo.vo.ResourceDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.ResourceDigDataQuery;
import cn.ideamake.bgyjssq.pojo.vo.ResourceDigDataVO;
import cn.ideamake.bgyjssq.pojo.vo.ResourceListVO;
import cn.ideamake.bgyjssq.pojo.vo.SellerVO;
import cn.ideamake.bgyjssq.pojo.vo.UserRoleListVO;
import cn.ideamake.bgyjssq.pojo.vo.analysis.VisitorListVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.StaffNotificationGroupVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.StaffNotificationResourceVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.StaffNotificationVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.VisitorNotificationGroupVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.VisitorNotificationOldVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.VisitorNotificationResourceVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.VisitorNotificationVO;
import cn.ideamake.bgyjssq.service.IDictService;
import cn.ideamake.bgyjssq.service.IProjectManagerService;
import cn.ideamake.bgyjssq.service.IResourceService;
import cn.ideamake.bgyjssq.service.IUserGroupManagerService;
import cn.ideamake.bgyjssq.service.IUserInfoService;
import cn.ideamake.bgyjssq.service.IUserRoleService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.additional.update.impl.LambdaUpdateChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static cn.ideamake.bgyjssq.common.enums.ContentPostStatus.PENDING_RELEASE;
import static cn.ideamake.bgyjssq.common.enums.ContentPostStatus.PUBLISHED;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
@AllArgsConstructor
@Service
@Slf4j
public class ResourceServiceImpl
        extends ServiceImpl<ResourceMapper, Resource>
        implements IResourceService {

    private final RedisTemplate<String, String> redisTemplate;

    private final IUserRoleService userRoleService;

    private final IProjectManagerService projectManagerService;

    private final IUserGroupManagerService userGroupManagerService;

    private final IUserInfoService userInfoService;

    private final IDictService dictService;

    private final ResourceMapper resourceMapper;

    /**
     * 项目通知
     */
    private final static int PROJECT_LEVEL = 3;

    /**
     * 系统通知
     */
    private final static int SYSTEM_LEVEL = 2;

    @Override
    public IPage<ResourceListVO> selectResourceList(
            ResourceListQuery query, ResourceType resourceType, DictType dictType) {
        return baseMapper.selectResourceVoList(
                query.getPageInfo(), query, resourceType.getCode(), dictType.name());
    }

    @Override
    public IPage<ResourceBaseVO> selectResourcesByProjectId(
            QueryPage query, Integer resourceType, Integer projectId, Integer grouping) {
        return baseMapper.selectResourcesByProjectId(
                query.getPageInfo(), resourceType, projectId, -1);
    }

    @Override
    public boolean publishResource(PublishResourceDTO resourceDTO, ResourceType resourceType) {
        //增加海报添加的校验
        if (ResourceType.POSTER == resourceType) {
            if (CollectionUtils.isEmpty(resourceDTO.getProjectId())) {
                throw new BusinessException("海报必须为项目通知");
            }
        }
        Resource resource = new Resource();
        BeanUtils.copyProperties(resourceDTO, resource);
        // 设置资源类型
        resource.setResourceType(resourceType.getCode());

        // 判断redis中是否存在图片, 保存图片后半部分(不包含oss地址的部分)
        if (StrUtil.isNotBlank(resource.getResourceBg())) {
            String fileName = (String) redisTemplate.opsForHash().get(Constants.REDIS_CACHE.UPLOAD_IMAGE, resource.getResourceBg());
            redisTemplate.opsForHash().delete(Constants.REDIS_CACHE.UPLOAD_IMAGE, resource.getResourceBg());
            if (null != fileName) {
                resource.setResourceBg(fileName);
            } else {
                resource.setResourceBg(null);
            }
        }

        // 设置资源状态
        if (resource.getPublishTime() == null || "".equals(resource.getPublishTime().toString())) {
            resource.setPublishTime(LocalDateTime.now());
        }
        if (Objects.nonNull(resourceDTO.getPublishTime())) {
            LocalDateTime publishTime = resourceDTO.getPublishTime();
            if (LocalDateTime.now().isAfter(publishTime) || LocalDateTime.now().isEqual(publishTime)) {
                resource.setStatus(PUBLISHED.getCode());
            }
        } else {
            resource.setStatus(PUBLISHED.getCode());
        }

        if (PosterCategory.SYSTEM.getCode().equals(resourceDTO.getResourceCategory())) {
            // 系统
            resource.setProjectId(-1);
            return saveOrUpdate(resource);
        } else {

            if (resourceDTO.getPushTargets() != null && !resourceDTO.getPushTargets().isEmpty()) {
                ArrayList<Resource> resources = new ArrayList<>();
                resourceDTO.getPushTargets().forEach(singlePushVO -> {
                    Resource resourceBean = new Resource();
                    BeanUtils.copyProperties(resource, resourceBean);
                    resourceBean.setProjectId(singlePushVO.getProjectId());
                    resourceBean.setVisitorTypes(singlePushVO.getVisitorType());
                    resources.add(resourceBean);

                });
                return saveOrUpdateBatch(resources);
            }


            // 项目,此处兼容老版接口，等先接口调试完毕删除
            List<Integer> projectIdList = resourceDTO.getProjectId();
            if (CollectionUtils.isNotEmpty(projectIdList)) {
                // 每个projectId保存一篇海报
                ArrayList<Resource> resources = new ArrayList<>();
                projectIdList.forEach(projectId -> {
                    Resource resourceBean = new Resource();
                    resource.setProjectId(projectId);
                    BeanUtils.copyProperties(resource, resourceBean);
                    resources.add(resourceBean);
                });
                return saveOrUpdateBatch(resources);
            }
            return true;
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean editReleaseStatus(ResourceBulkOperation operation) {

        LambdaUpdateChainWrapper<Resource> lambdaUpdate = lambdaUpdate()
                .set(Resource::getStatus, operation.getStatus())
                .eq(Resource::getResourceType, operation.getResourceType())
                .in(Resource::getId, operation.getIds());
        // 设置为发布中,将推送时间设置为当前时间
        if (PUBLISHED.getCode().equals(operation.getStatus())) {
            lambdaUpdate.set(Resource::getPublishTime, LocalDate.now());
        } else if (PENDING_RELEASE.getCode().equals(operation.getStatus())) {
            // 设置为待发布,将推送时间设置为当前时间+1天
            lambdaUpdate.set(Resource::getPublishTime, LocalDate.now().plusDays(1));
        }
        boolean update = lambdaUpdate.update();
        log.info("edit release status, status = {}, customerIds = {}", operation.getStatus(), operation.getIds());
        return update;
    }

    @Override
    public boolean topping(ResourceBulkOperation operation) {
        boolean update = lambdaUpdate().set(Resource::getTopStatus, operation.getTop())
                .eq(Resource::getResourceType, operation.getResourceType())
                .in(Resource::getId, operation.getIds())
                .update();
        log.error("topping, top = {}, articleList = {}", operation.getTop(), operation.getIds());
        return update;
    }

    @Override
    public StaffNotificationVO getStaffNotification(StaffNotificationQuery query) {
        //项目校验
        setProjects(query);

        StaffNotificationVO staffNotificationVO = new StaffNotificationVO();
        Page<StaffNotificationResourceVO> page = query.getPageInfo();
        Page<StaffNotificationGroupVO> pageGroup = query.getPageInfo();
        int type = query.getType();

        if (StaffNotificationType.ARTICLE.getCode() == type) {
            // 文章加入系统通知项目类型，海报必须为项目通知
            query.getProjects().add(-1);
            String name = DictType.ARTICLE.name();
            setStaffNotificationVO(query, staffNotificationVO, page, pageGroup, name);
            return staffNotificationVO;
        } else if (StaffNotificationType.POSTER.getCode() == type) {
            String name = DictType.POSTER.name();
            setStaffNotificationVO(query, staffNotificationVO, page, pageGroup, name);
            return staffNotificationVO;
        } else {
            log.error("staffNotificationType参数无效,传入type={}", type);
            throw new BusinessException("员工通知类型参数无效");
        }
    }


    @Override
    public IPage<StaffNotificationResourceVO> getResourcesByGroupId(StaffNotificationQuery query) {
        //如果projects没传,或者多传了projectId,则抛出了异常
        if (null == query.getProjects() || query.getProjects().size() != 1) {
            throw new BusinessException("无效的项目id");
        }
        query.setSearchString(null);
        int type = query.getType();
        Page<StaffNotificationResourceVO> page = new Page<>(query.getPage(), query.getLimit());
        if (StaffNotificationType.ARTICLE.getCode().equals(type)) {
            //查询文章内容
            page.setRecords(baseMapper.getStaffResource(query, page, PUBLISHED.getCode(), DictType.ARTICLE.name(), new ArrayList<>()));
            //查询组内容
            return page;
        } else if (StaffNotificationType.POSTER.getCode().equals(type)) {
            page.setRecords(baseMapper.getStaffResource(query, page, PUBLISHED.getCode(), DictType.POSTER.name(), new ArrayList<>()));
            return page;
        } else {
            log.error("staffNotificationType参数无效,传入type={}", type);
            throw new BusinessException("员工通知类型参数无效");
        }
    }

    @Override
    public IPage<VisitorNotificationOldVO> getVisitorNotification(VisitorNotificationQuery vnf) {
        //等级为3表示为项目通知
        int level = 3;
        if (level == vnf.getLevel() && null == vnf.getProjectId()) {
            throw new BusinessException("项目id不能为空");
        }
        return baseMapper.getVisitorNotification(vnf.getPageInfo(), vnf, PUBLISHED.getCode(), DictType.ARTICLE.name());
    }

    @Override
    public VisitorNotificationVO getVisitorNotificationNew(VisitorNotificationQuery query) {
        VisitorNotificationVO visitorNotificationVO = new VisitorNotificationVO();
        //设置未分组资源列表
        visitorNotificationVO.setResources(this.getVisitorNotificationResource(query).getResources());

        if (SYSTEM_LEVEL == query.getLevel()) {
            visitorNotificationVO.setGroups(new Page<>());
            return visitorNotificationVO;
        }

        //设置分组资源列表
        visitorNotificationVO.setGroups(getVisitorNotificationGroup(query));

        return visitorNotificationVO;
    }

    @Override
    public VisitorNotificationVO getVisitorNotificationResource(VisitorNotificationQuery query) {
        if (PROJECT_LEVEL == query.getLevel() && null == query.getProjectId()) {
            throw new BusinessException("项目id不能为空");
        }
        IPage<VisitorNotificationResourceVO> visitorNotificationNew = baseMapper.getVisitorNotificationNew(
                query.getPageInfo(),
                query,
                PUBLISHED.getCode(),
                DictType.ARTICLE.name()
        );
        return new VisitorNotificationVO(new Page<>(), visitorNotificationNew);
    }


    @Override
    public ArticleDetailVisitorListVO selectArticleDetailByArticleId(StaffNotificationDetailQuery query) {
        List<VisitorListVO> visitorList = baseMapper.selectArticleDetailByArticleId(query);
        List<String> list = new ArrayList<>();
        list.add(query.getUserId());
        PVAndUvVO pvAndUv = baseMapper.selectPvAndUv(query.getResourceId(), list);
        return new ArticleDetailVisitorListVO(visitorList,
                pvAndUv == null ? visitorList.size() : pvAndUv.getPageViews(),
                pvAndUv == null ? visitorList.size() : pvAndUv.getUniqueVisitors());
    }

    @Override
    public List<UserRoleListVO> selectResourceDetail(StaffNotificationDetailQuery query) {
        List<UserRoleListVO> list = new ArrayList<>();
        Optional.ofNullable(userRoleService.selectUserIdListByParentId(query.getUserId()))
                .ifPresent(u -> u.forEach(v -> {
                    List<String> userList = userRoleService.selectAllUserIdListByParentId(v.getUserUuid());
                    PVAndUvVO pvAndUvVO = baseMapper.selectPvAndUv(query.getResourceId(), userList);
                    list.add(new UserRoleListVO(
                            v.getUserUuid(),
                            v.getName(),
                            pvAndUvVO.getPageViews(),
                            pvAndUvVO.getUniqueVisitors()
                    ));
                }));
        return list;
    }

    @Override
    public List<UserRoleListVO> getUserGroup(StaffNotificationDetailQuery query) {
        List<UserRoleListVO> list = new ArrayList<>();
        String userId = query.getUserId();
        Integer resourceId = query.getResourceId();

        List<Integer> projectIds = new ArrayList<>();
        Optional.ofNullable(resourceMapper.selectById(resourceId))
                .ifPresent(r -> projectIds.add(r.getProjectId()));

        List<Integer> groupIdList = userGroupManagerService.getGroupIdList(userId, projectIds);

        Optional.ofNullable(groupIdList).ifPresent(u -> u.forEach(g -> {

            List<String> userIdList = userInfoService.getUserIdList(g);

            List<String> collect = userIdList.stream()
                    .filter(s -> !s.equals(UserInfoContext.userInfo().getUserId()))
                    .collect(Collectors.toList());

            PVAndUvVO pvAndUvVO = baseMapper.selectPvAndUv(resourceId, collect);

            Dict dict = dictService.lambdaQuery().eq(Dict::getId, g).one();

            Optional.ofNullable(dict).orElseThrow(() -> new BusinessException("分组数据不存在"));

            list.add(new UserRoleListVO(
                    dict.getId().toString(),
                    dict.getName(),
                    pvAndUvVO.getPageViews(),
                    pvAndUvVO.getUniqueVisitors()
            ));

        }));
        return list;
    }

    @Override
    public void getHigherSellerData(NotifyDetailVO notifyDetailVO, StaffNotificationDetailQuery query) {
        String uuid = query.getUserId();
        SellerVO userInfoVO = userInfoService.getUserInfoVO(uuid);
        List<String> userIdList = new ArrayList<>();
        userIdList.add(uuid);
        PVAndUvVO pvAndUvVO = baseMapper.selectPvAndUv(query.getResourceId(), userIdList);
        UserRoleListVO vo = new UserRoleListVO();
        vo.setAvatar(userInfoVO.getAvatar());
        vo.setId(userInfoVO.getId());
        vo.setName(userInfoVO.getName());
        vo.setPageViews(pvAndUvVO.getPageViews());
        vo.setUniqueVisitors(pvAndUvVO.getUniqueVisitors());
        notifyDetailVO.setUserRoleListVO(vo);
    }

    @Override
    public List<UserRoleListVO> getUserList(StaffNotifyListQuery query) {
        List<UserRoleListVO> list = new ArrayList<>();

        List<String> userList = new ArrayList<>();

        List<SellerVO> userInfoList = userInfoService.getUserInfoList(query.getGroupId());
        Optional.ofNullable(userInfoList).ifPresent(l -> l.stream()
                .filter(s -> !s.getId().equals(UserInfoContext.userInfo().getUserId()))
                .forEach(v -> {
                    userList.add(v.getId());
                    PVAndUvVO pvAndUvVO = baseMapper.selectPvAndUv(query.getResourceId(), userList);
                    list.add(new UserRoleListVO(
                            v.getId(),
                            v.getName(),
                            v.getAvatar(),
                            pvAndUvVO.getPageViews(),
                            pvAndUvVO.getUniqueVisitors()));
                    userList.remove(0);
                }));

        return list;
    }

    @Override
    public ResourceDetailVO getResourceByTypeAndId(Integer id, ResourceType resourceType) {
        return baseMapper.getResourceByTypeAndId(id, resourceType.getCode());
    }

    @Override
    public ResourceDetailVO getResourceByTypeAndProjectId(Integer projectId, ResourceType resourceType) {
        return baseMapper.getResourceByTypeProjectId(projectId, resourceType.getCode());
    }

    @Override
    public boolean updateGroupIdByResourceIdsBatch(List<Integer> resourceIds, Integer groupId) {

        //此处判断是否是增量，如果是减量则将减量的资源置为未分配
        List<Integer> preResourceIds = lambdaQuery().eq(Resource::getGroupId, groupId).list().stream().map(Resource::getId)
                .collect(Collectors.toList());

        List<Resource> resources = new ArrayList<>();
        if (null != resourceIds && !resourceIds.isEmpty()) {
            resourceIds.forEach(id -> {
                resources.add(new Resource().setId(id).setGroupId(groupId));
                //移除变更后仍在选择列表中的资源id
                if (!preResourceIds.isEmpty()) {
                    preResourceIds.remove(id);
                }
            });
        }
        if (!resources.isEmpty()) {
            //更新资源表信息
            updateBatchById(resources);

        }
        resources.clear();
        //此时preResourceIds只包含变更后比变更前少的资源id
        if (!preResourceIds.isEmpty()) {
            //生成变更减少的对象列表，设置成未分组状态
            preResourceIds.forEach(resourceId -> resources.add(new Resource().setId(resourceId).setGroupId(-1)));
        }

        if (!resources.isEmpty()) {
            //将变更减少的资源设置成未分组
            return updateBatchById(resources);
        }
        return true;
    }

    @Override
    public boolean verifyResourceInDeleteGroup(Integer groupId) {
        return lambdaQuery().eq(Resource::getGroupId, groupId).list().isEmpty();
    }

    @Override
    public IPage<ResourceDigDataVO> digResourceBrowseData(ResourceDigDataQuery query) {
        return baseMapper.digDataForResource(query.getPageInfo(), query);
    }

    @Override
    public boolean editArticleGroup(ResourceBulkOperation operation) {
        boolean update = lambdaUpdate().set(Resource::getGroupId, operation.getGroupId())
                .in(Resource::getId, operation.getIds())
                .eq(Resource::getResourceType, operation.getResourceType())
                .update();
        log.error("edit article group, groupId = {}, articleList = {}", operation.getGroupId(), operation.getIds());
        return update;
    }

    /**
     * 项目为空的校验处理
     *
     * @param query query
     */
    private void setProjects(StaffNotificationQuery query) {
        List<Integer> projects = query.getProjects();
        //如果projects没传，则取该销售对应的所有项目和项目组
        if (CollectionUtils.isEmpty(projects)) {
            List<Integer> list = projectManagerService.lambdaQuery()
                    .eq(ProjectManager::getUserUuid, query.getUserid())
                    .list()
                    .stream()
                    .map(ProjectManager::getProjectId)
                    .collect(Collectors.toList());
            if (CollectionUtils.isEmpty(list)) {
                throw new BusinessException("该销售无管理项目,请进行脏数据检查");
            }
            query.setProjects(list);
        }
    }

    /**
     * 设置通知返回对象属性
     *
     * @param query               查询条件
     * @param staffNotificationVO VO对象
     * @param page                分页
     * @param pageGroup           分页
     * @param name                资源类型
     */
    private void setStaffNotificationVO(StaffNotificationQuery query,
                                        StaffNotificationVO staffNotificationVO,
                                        Page<StaffNotificationResourceVO> page,
                                        Page<StaffNotificationGroupVO> pageGroup, String name) {

        String userId = query.getUserid();
        List<String> userIdList = userGroupManagerService.getSellerIdListAddMe(userId);

        List<Integer> groups =
                baseMapper.getGroupsByProjects(query.getProjects(), name);
        query.setGroups(groups);

        // 已发布状态
        Integer code = PUBLISHED.getCode();

        page.setRecords(baseMapper.getStaffResource(query, page, code, name, userIdList));
        staffNotificationVO.setResources(page);

        pageGroup.setRecords(baseMapper.getStaffResourceGroup(query, pageGroup, code, name, userIdList));
        staffNotificationVO.setGroups(pageGroup);
    }

    /**
     * 访客查询分组资源列表
     *
     * @param query 查询条件
     * @return IPage<VisitorNotificationGroupVO>
     */
    private IPage<VisitorNotificationGroupVO> getVisitorNotificationGroup(VisitorNotificationQuery query) {
        List<Integer> groups =
                baseMapper.getGroupsByProjects(new ArrayList<>(query.getProjectId()), DictType.ARTICLE.name());
        query.setGroups(groups);
        return baseMapper.getVisitorResourceGroup(query.getPageInfo(), query, DictType.ARTICLE.name());
    }


}
