package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.dao.mapper.UserInfoMapper;
import cn.ideamake.bgyjssq.pojo.entity.UserInfo;
import cn.ideamake.bgyjssq.pojo.vo.SellerVO;
import cn.ideamake.bgyjssq.pojo.vo.UserInfoVO;
import cn.ideamake.bgyjssq.service.IUserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements IUserInfoService {

    @Override
    public List<SellerVO> getUserInfoList(Integer groupId) {
        return baseMapper.getUserInfoList(groupId);
    }

    @Override
    public SellerVO getUserInfoVO(String uuid) {
        return baseMapper.getUserInfoVO(uuid);
    }

    @Override
    public List<String> getUserIdList(Integer groupId) {
        return baseMapper.getUserIdList(groupId);
    }

    @Override
    public List<UserInfoVO> getUserList(Integer projectId) {
        return baseMapper.getUserList(projectId);
    }

    @Override
    public List<SellerVO> getUserIdListByGroupIds(List<Integer> groupIds) {
        return baseMapper.getUserIdListByGroupIds(groupIds);
    }
}
