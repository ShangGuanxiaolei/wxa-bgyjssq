package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.util.Constants;
import cn.ideamake.bgyjssq.dao.mapper.OauthWechatMapper;
import cn.ideamake.bgyjssq.pojo.entity.OauthWechat;
import cn.ideamake.bgyjssq.service.IOauthWechatService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nullable;
import java.time.Duration;
import java.util.Optional;

/**
 * <p>
 * 用户微信登录表 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-06-11
 */
@AllArgsConstructor
@Service
public class OauthWechatServiceImpl extends ServiceImpl<OauthWechatMapper, OauthWechat> implements IOauthWechatService {

    private final RedisTemplate<String, Object> redisTemplate;

    @Override
    public OauthWechat getUserWeChatInfoByCache(String userId) {
        //先从缓存中读取用户信息，不存在则读取表中数据
        OauthWechat oauthWechat = (OauthWechat) redisTemplate.opsForValue().get(Constants.CACHE_PREFIX_LABEL.WECHAT + userId);
        if (oauthWechat != null && oauthWechat.getUserType() == null) {
            oauthWechat = null;
        }
        if (oauthWechat == null) {
            oauthWechat = lambdaQuery().eq(OauthWechat::getUserUuid, userId).one();
            if (oauthWechat != null) {
                redisTemplate.opsForValue().set(Constants.CACHE_PREFIX_LABEL.WECHAT + oauthWechat.getUserUuid(), oauthWechat, Duration.ofMinutes(10));
            }
            return oauthWechat;
        }
        return oauthWechat;
    }

    @Nullable
    @Override
    public OauthWechat getUserWeChatInfoByOpenId(String openId) {
        //查询用户信息，无则返回null，外层调用方处理逻辑
        return Optional.ofNullable(this.lambdaQuery().eq(OauthWechat::getOpenid, openId).one())
                .map(o -> {
                    redisTemplate.opsForValue().set(
                            Constants.CACHE_PREFIX_LABEL.WECHAT + o.getUserUuid(),
                            o,
                            Duration.ofMinutes(10)
                    );
                    return o;
                }).orElse(null);
    }

    @Nullable
    @Override
    public OauthWechat getUserWeChatInfoByUnionId(String unionId) {
        //查询用户信息，无则返回null，外层调用方处理逻辑,处理多账号特殊情况
        return Optional.ofNullable(this.lambdaQuery().eq(OauthWechat::getUnionid, unionId).list())
                .map(l -> l.stream().findFirst().map(o -> {
                    redisTemplate.opsForValue().set(
                            Constants.CACHE_PREFIX_LABEL.WECHAT + o.getUserUuid(), o, Duration.ofMinutes(10));
                    return o;
                })).get().orElse(null);
    }

    @Nullable
    @Override
    public OauthWechat getUserWeChatInfoByUnionIdAndOpenId(String unionId, String openId) {
        //查询用户信息，无则返回null，外层调用方处理逻辑
        return Optional.ofNullable(this.lambdaQuery()
                .eq(OauthWechat::getUnionid, unionId)
                .eq(OauthWechat::getOpenid, openId)
                .one())
                .map(o -> {
                    redisTemplate.opsForValue().set(
                            Constants.CACHE_PREFIX_LABEL.WECHAT + o.getUserUuid(),
                            o,
                            Duration.ofMinutes(10)
                    );
                    return o;
                }).orElse(null);
    }

    @Override
    @Transactional(rollbackFor = BusinessException.class)
    public OauthWechat updateUserWeChatInfoByIdByCache(OauthWechat oauthWechat) {
        if (updateById(oauthWechat)) {
            redisTemplate.delete(Constants.CACHE_PREFIX_LABEL.WECHAT + oauthWechat.getUserUuid());
            return oauthWechat;
        } else {
            throw new BusinessException("更新微信信息异常");
        }
    }

    @Override
    @Transactional(rollbackFor = BusinessException.class)
    public OauthWechat updateUserWeChatInfoByOpenIdByCache(String openId, OauthWechat newData) {
        if (openId == null) {
            log.error("openId  is a must");
        }
        //后续补充
        OauthWechat oldData = lambdaQuery().eq(OauthWechat::getOpenid, openId).one();
        if (null == oldData) {
            throw new BusinessException("更新的用户信息不存在");
        }
        newData.setId(oldData.getId());


        return updateUserWeChatInfoByIdByCache(newData);
    }

    @Override
    @Transactional(rollbackFor = BusinessException.class)
    public OauthWechat updateUserWeChatInfoByUserIdByCache(String userId, OauthWechat newData) {
        if (userId == null) {
            log.error("userId  is a must");
        }
        //后续补充
        OauthWechat oldData = lambdaQuery().eq(OauthWechat::getUserUuid, userId).one();
        if (null == oldData) {
            throw new BusinessException("更新的用户信息不存在");
        }
        newData.setId(oldData.getId());
        newData.setUserType(oldData.getUserType());
        newData.setUserUuid(oldData.getUserUuid());
        return updateUserWeChatInfoByIdByCache(newData);
    }

    @Override
    public OauthWechat cleanWeChatUserInfo(String userId) {
        OauthWechat oauthWechat = lambdaQuery().eq(OauthWechat::getUserUuid, userId).one();
        redisTemplate.delete(Constants.CACHE_PREFIX_LABEL.WECHAT + userId);
        if (null == oauthWechat) {
            return null;
        }
        removeById(oauthWechat);
        return oauthWechat;
    }

    @Override
    public boolean saveOrUpdateFormId(String userId, String fromId) {
        redisTemplate.opsForHash().put(Constants.CACHE_PREFIX_LABEL.WECHAT_FORM_ID, userId, fromId);
        return true;
    }

    @Override
    public String getFormId(String userId) {
        return (String) redisTemplate.opsForHash().get(Constants.CACHE_PREFIX_LABEL.WECHAT_FORM_ID, userId);
    }

}
