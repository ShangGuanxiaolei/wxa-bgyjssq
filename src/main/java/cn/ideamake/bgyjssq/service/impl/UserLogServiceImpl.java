package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.dao.mapper.UserLogMapper;
import cn.ideamake.bgyjssq.pojo.entity.UserLog;
import cn.ideamake.bgyjssq.pojo.query.user.UserLogQuery;
import cn.ideamake.bgyjssq.pojo.vo.user.UserLogVO;
import cn.ideamake.bgyjssq.service.IUserLogService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
@Service
public class UserLogServiceImpl extends ServiceImpl<UserLogMapper, UserLog> implements IUserLogService {

    @Override
    public IPage<UserLogVO> getUserLog(UserLogQuery query) {
        return baseMapper.selectUserLog(query.getPageInfo(), query);
    }
}
