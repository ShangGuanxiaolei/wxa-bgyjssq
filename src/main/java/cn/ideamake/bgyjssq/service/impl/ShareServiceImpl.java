package cn.ideamake.bgyjssq.service.impl;

import cn.ideamake.bgyjssq.dao.mapper.ShareMapper;
import cn.ideamake.bgyjssq.pojo.entity.Share;
import cn.ideamake.bgyjssq.service.IShareService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 分享表，用户记录销售或者访客的分享行为 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-23
 */
@Service
public class ShareServiceImpl extends ServiceImpl<ShareMapper, Share> implements IShareService {

}
