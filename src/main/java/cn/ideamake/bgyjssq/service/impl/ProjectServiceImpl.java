package cn.ideamake.bgyjssq.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.ideamake.bgyjssq.common.enums.DictType;
import cn.ideamake.bgyjssq.common.enums.ResourceType;
import cn.ideamake.bgyjssq.common.util.Constants;
import cn.ideamake.bgyjssq.dao.mapper.ProjectMapper;
import cn.ideamake.bgyjssq.dao.mapper.SysAdminMapper;
import cn.ideamake.bgyjssq.dao.mapper.SysAdminProjectMapper;
import cn.ideamake.bgyjssq.pojo.dto.ProjectBaseInfoDTO;
import cn.ideamake.bgyjssq.pojo.dto.ProjectGroupListDTO;
import cn.ideamake.bgyjssq.pojo.dto.SaveOrChangeProjectDTO;
import cn.ideamake.bgyjssq.pojo.entity.Dict;
import cn.ideamake.bgyjssq.pojo.entity.Project;
import cn.ideamake.bgyjssq.pojo.entity.Resource;
import cn.ideamake.bgyjssq.pojo.entity.SysAdmin;
import cn.ideamake.bgyjssq.pojo.entity.SysAdminProject;
import cn.ideamake.bgyjssq.pojo.query.DefaultQueryPage;
import cn.ideamake.bgyjssq.pojo.query.VisitorProjectQuery;
import cn.ideamake.bgyjssq.pojo.vo.ProjectGroupListVO;
import cn.ideamake.bgyjssq.pojo.vo.ResourceBaseVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.floorbook.ProjectFloorBook720VO;
import cn.ideamake.bgyjssq.service.IDictService;
import cn.ideamake.bgyjssq.service.IProjectService;
import cn.ideamake.bgyjssq.service.IResourceService;
import cn.ideamake.bgyjssq.service.IUserRoleService;
import cn.ideamake.bgyjssq.service.common.SaveAdminLogService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.jsonwebtoken.lang.Collections;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static cn.ideamake.bgyjssq.common.util.Constants.ROOT;

/**
 * <p>
 * 项目表 服务实现类
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
@AllArgsConstructor
@Service
public class ProjectServiceImpl extends ServiceImpl<ProjectMapper, Project> implements IProjectService {

    private final RedisTemplate<String, String> redisTemplate;

    private final IDictService dictService;

    private final IResourceService resourceService;

    private final IUserRoleService userRoleService;

    private final SaveAdminLogService saveAdminLogService;

    private final SysAdminProjectMapper sysAdminProjectMapper;

    private final SysAdminMapper sysAdminMapper;

    @Override
    public IPage<VisitorProjectVO> selectProjectList(VisitorProjectQuery query) {
        return baseMapper.selectProjectList(query.getPageInfo(), query, DictType.PROJECT.name());
    }

    @Override
    public IPage<ProjectFloorBook720VO> selectProjectFloorBook720List(VisitorProjectQuery query) {
        return baseMapper.selectProjectFloorBook720List(query.getPageInfo(), query, DictType.PROJECT.name());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveOrUpdateProject(SaveOrChangeProjectDTO projectDTO) {

        Project project = new Project();
        BeanUtils.copyProperties(projectDTO, project);

        // 判断redis中是否存在图片, 保存图片后半部分(不包含oss地址的部分)
        if (StrUtil.isNotBlank(projectDTO.getCoverPicture())) {
            String fileName = (String) redisTemplate.opsForHash().get(Constants.REDIS_CACHE.UPLOAD_IMAGE, projectDTO.getCoverPicture());
            redisTemplate.opsForHash().delete(Constants.REDIS_CACHE.UPLOAD_IMAGE, projectDTO.getCoverPicture());
            if (null != fileName) {
                project.setCoverPicture(fileName);
            } else {
                project.setCoverPicture(null);
            }
        }
        if (StrUtil.isNotBlank(projectDTO.getProjectLogo())) {
            String fileName = (String) redisTemplate.opsForHash().get(Constants.REDIS_CACHE.UPLOAD_IMAGE, projectDTO.getProjectLogo());
            redisTemplate.opsForHash().delete(Constants.REDIS_CACHE.UPLOAD_IMAGE, projectDTO.getProjectLogo());
            if (null != fileName) {
                project.setProjectLogo(fileName);
            } else {
                project.setProjectLogo(null);
            }
        }

        boolean saveOrUpdate = saveOrUpdate(project);

        // 保存完项目, 生成一个对应的首页
        Integer projectId = project.getId();
        if (Objects.isNull(projectDTO.getId())) {
            Resource homeResource = new Resource();
            homeResource.setResourceTitle("默认首页名称")
                    .setProjectId(projectId)
                    .setGroupId(project.getProjectGroupId())
                    .setResourceType(ResourceType.HOME.getCode());

            Resource floorBookResource = new Resource();
            floorBookResource.setResourceTitle("默认楼书名称")
                    .setProjectId(projectId)
                    .setGroupId(project.getProjectGroupId())
                    .setResourceType(ResourceType.FLOOR_BOOK.getCode());
            ArrayList<Resource> resources = new ArrayList<>();
            resources.add(homeResource);
            resources.add(floorBookResource);
            resourceService.saveBatch(resources);
        }
        if (project.getProjectArea() != null) {
            String content = "设置项目" + project.getProjectTitle() + "区域为" + project.getProjectArea();
            saveAdminLogService.saveSystemLog(content);
        }

        //保存成功处理项目权限
        if (saveOrUpdate) {
            QueryWrapper<SysAdminProject> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda().eq(SysAdminProject::getProjectId, projectId);
            Optional.ofNullable(sysAdminProjectMapper.selectOne(queryWrapper)).orElseGet(() -> {
                QueryWrapper<SysAdmin> wrapper = new QueryWrapper<>();
                wrapper.lambda().eq(SysAdmin::getRoleId, ROOT);
                Optional.ofNullable(sysAdminMapper.selectOne(wrapper)).ifPresent(a -> {
                    SysAdminProject adminProject = new SysAdminProject();
                    adminProject.setSysAdminId(a.getId());
                    adminProject.setProjectId(projectId);
                    sysAdminProjectMapper.insert(adminProject);
                });
                return null;
            });
        }

        return saveOrUpdate;
    }

    @Override
    public IPage<ProjectGroupListVO> selectProjectGroupList(DefaultQueryPage query) {
        return baseMapper.selectProjectGroupList(query.getPageInfo(), DictType.PROJECT.name());
    }

    @Override
    public List<ResourceBaseVO> projectBaseByGroupId(Integer groupId) {

        return null;
    }

    @Override
    public List<ProjectGroupListDTO> selectProjectGroupTree(String sellerId) {
        // 从项目管理表中通过销售id获取销售对应的项目
        //临时添加兼容pc后台获取，没有传sellerId时返回全部项目分组和项目
        List<ProjectBaseInfoDTO> baseInfoList = baseMapper.selectProjectBaseInfoListBySellerId(sellerId);
        // 查字典表中type=project, 拿到所有楼盘组
        List<ProjectGroupListDTO> groupList = new ArrayList<>();
        dictService.lambdaQuery()
                .eq(Dict::getType, DictType.PROJECT.name())
                .list()
                .forEach(pg -> {
                    // 拿到项目列表中对应组id的内容
                    List<ProjectBaseInfoDTO> projectList = baseInfoList.stream()
                            .filter(v -> pg.getId().equals(v.getGroupId()))
                            .collect(Collectors.toList());

                    // 过滤掉不属于销售的项目组(list为空)
                    if (CollUtil.isNotEmpty(projectList)) {
                        groupList.add(new ProjectGroupListDTO(pg, projectList));
                    }
                });
        return groupList;
    }

    @Override
    public List<Integer> selectProjectIdListByUserId(String userId) {
        List<String> userIdList = userRoleService.selectAllUserIdListByParentId(userId);
        if (Collections.isEmpty(userIdList)) {
            return new ArrayList<>();
        }
        return baseMapper.selectProjectIdListByUserId(userIdList);
    }

    @Override
    public List<String> selectProjectNameByUserId(String userId) {
        return baseMapper.selectProjectNameByUserId(userId);
    }

}
