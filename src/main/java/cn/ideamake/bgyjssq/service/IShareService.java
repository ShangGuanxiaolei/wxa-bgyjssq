package cn.ideamake.bgyjssq.service;


import cn.ideamake.bgyjssq.pojo.entity.Share;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 分享表，用户记录销售或者访客的分享行为 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-23
 */
public interface IShareService extends IService<Share> {

}
