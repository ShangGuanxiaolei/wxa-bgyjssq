package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.entity.UserRegister;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 管理员工邀请注册的接口 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-06-21
 */
public interface IUserRegisterService extends IService<UserRegister> {

}
