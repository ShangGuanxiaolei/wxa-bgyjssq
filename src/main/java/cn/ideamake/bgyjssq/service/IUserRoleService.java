package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户角色绑定表，记录用户和角色的绑定关系 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-06-06
 */
public interface IUserRoleService extends IService<UserRole> {

    /**
     * 根据上级id查询下级用户id列表
     *
     * @param userId 上级id
     * @return List<UserRole>
     */
    List<UserRole> selectUserIdListByParentId(String userId);

    /**
     * 根据上级id查询所有下级用户id列表
     *
     * @param userId 上级id
     * @return List<UserRole>
     */
    List<String> selectAllUserIdListByParentId(String userId);

}
