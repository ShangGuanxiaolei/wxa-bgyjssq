package cn.ideamake.bgyjssq.service;

import cn.hutool.json.JSONObject;
import cn.ideamake.bgyjssq.pojo.entity.CollectorResource;
import cn.ideamake.bgyjssq.pojo.query.DashBoardQuery;
import cn.ideamake.bgyjssq.pojo.vo.dashboard.ArticleDataVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 资源采集 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-07-31
 */
public interface ICollectorResourceService extends IService<CollectorResource> {

    /**
     * pc后台-dashboard-获取微文章数据
     *
     * @param query 参数
     * @return 列表
     */
    List<ArticleDataVO> getArticleDataLineChart(DashBoardQuery query);

    /**
     * PC后台-dashboard-获取微文章数据饼图
     *
     * @param query 参数
     * @return 列表
     */
    JSONObject getArticleDataPieChart(DashBoardQuery query);

}
