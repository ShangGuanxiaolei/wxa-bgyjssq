package cn.ideamake.bgyjssq.service;

import cn.ideamake.bgyjssq.pojo.dto.UserGroupEditDTO;
import cn.ideamake.bgyjssq.pojo.entity.UserGroupManager;
import cn.ideamake.bgyjssq.pojo.vo.UserBaseVO;
import cn.ideamake.bgyjssq.pojo.vo.UserGroupEditVO;
import cn.ideamake.bgyjssq.pojo.vo.UserGroupVO;
import cn.ideamake.bgyjssq.pojo.vo.UserListVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 员工分组管理表 服务类
 * </p>
 *
 * @author ideamake
 * @since 2019-08-13
 */
public interface IUserGroupManagerService extends IService<UserGroupManager> {

    /**
     * 高等级角色获取管理的员工id列表
     *
     * @param sellerId 用户id
     * @return List<String>
     */
    List<String> getSellerIdList(String sellerId);

    /**
     * 高等级角色获取管理的员工id列表
     *
     * @param sellerId 用户id
     * @return List<String>
     */
    List<String> getSellerIdListAddMe(String sellerId);

    /**
     * 高等级角色获取管理的组id列表
     *
     * @param sellerId   用户id
     * @param projectIds 项目id列表
     * @return List<Integer>
     */
    List<Integer> getGroupIdList(String sellerId, List<Integer> projectIds);

    /**
     * 高等级角色获取管理的员工id列表
     *
     * @param groupIds 组id列表
     * @return List<String>
     */
    List<String> getSellerIdListByGroupIds(List<Integer> groupIds);

    /**
     * 检查员工是否是高等级角色
     *
     * @param sellerId 用户id
     * @return boolean
     */
    boolean isHigherLevelRole(String sellerId);

    /**
     * 查询所有员工分组
     *
     * @param projectIds 项目id列表
     * @return List<UserGroupVO>
     */
    List<UserGroupVO> getGroup(List<Integer> projectIds);

    /**
     * 根据组id查询员工
     *
     * @param groupId 组id
     * @return List<UserListVO>
     */
    List<UserListVO> getUser(Integer groupId);

    /**
     * 分组查询所有员工
     *
     * @return List<UserGroupVO>
     */
    List<UserGroupVO> getUserGroup();

    /**
     * 分组查询项目下的所有员工
     *
     * @param projectId 项目id
     * @return List<UserGroupVO>
     */
    List<UserGroupVO> getGroupByProjectId(Integer projectId);

    /**
     * 查询未分组员工
     *
     * @return List<UserListVO>
     */
    List<UserListVO> getUserNoGroup();

    /**
     * 通过组id获取组管理员和组成员
     *
     * @return
     */
    UserGroupEditVO selectUserAndManagerByGroupId(Integer groupId);

    List<UserBaseVO> selectUsers();

    /**
     * 通过组id和用户id列表来更新组管理员信息
     *
     * @param userGroupEditDTO
     * @return
     */
    boolean updateGroupManagerInfo(UserGroupEditDTO userGroupEditDTO);

}
