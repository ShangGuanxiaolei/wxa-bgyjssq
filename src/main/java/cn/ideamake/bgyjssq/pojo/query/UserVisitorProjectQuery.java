package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-20 14:09
 * @Version: 1.0
 * 获取访客对应项目的销售信息
 */
@Data
public class UserVisitorProjectQuery {

    /**
     * 访客id
     */
    private String visitorId;
    /**
     * 销售id
     */
    private String userid;

    /**
     * 项目id
     */
    private Integer projectId;
}
