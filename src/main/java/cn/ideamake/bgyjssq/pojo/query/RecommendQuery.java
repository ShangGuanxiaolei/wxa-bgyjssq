package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @Author: Walt
 * @Data: 2019-06-12 16:45
 * @Version: 1.0
 * 我的推荐查询参数
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RecommendQuery extends QueryPage {
    /**
     * 开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    /**
     * 用户id
     */
    @UserId
    private String userid;

}
