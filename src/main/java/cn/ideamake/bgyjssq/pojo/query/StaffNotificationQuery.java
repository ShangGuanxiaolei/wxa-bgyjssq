package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import cn.ideamake.bgyjssq.pojo.bo.CollationRule;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author: Walt
 * @Data: 2019-06-18 15:40
 * @Version: 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class StaffNotificationQuery extends DefaultQueryPage {

    /**
     * 类别： 海报 2，文章 1
     */
    @NotNull(message = "通知类别为必填项")
    private Integer type;

    /**
     * 用户id
     */
    @UserId
    private String userid;

    /**
     * 按文章、海报、文章组、海报组标题
     */
    private String searchString;

    /**
     * 起始时间
     */
    private String startTime;

    /**
     * 截止时间
     */
    private String endTime;

    /**
     * 项目id列表,系统通知的项目id为-1,默认返回
     */
    private List<Integer> projects;

    /**
     * 查询临时存储字段，与前端无关
     */
    private List<Integer> groups;

    /**
     * 项目组id
     */
    private Integer groupId;

    /**
     * 排序规则
     */
    private CollationRule collation;

    /**
     * 是否过滤在组中的资源0-不过滤，1-过滤
     */
    private Integer condition = 0;

}
