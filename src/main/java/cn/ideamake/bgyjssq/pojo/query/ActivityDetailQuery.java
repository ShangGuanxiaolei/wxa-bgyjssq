package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * @author gxl
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ActivityDetailQuery extends DefaultQueryPage {

    @NotNull(message = "活动ID未传")
    private Integer id;
}
