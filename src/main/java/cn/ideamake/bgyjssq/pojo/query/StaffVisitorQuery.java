package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import cn.ideamake.bgyjssq.pojo.bo.FilterOptions;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @Author: Walt
 * @Data: 2019-06-19 11:45
 * @Version: 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class StaffVisitorQuery  extends QueryPage {
    /**
     * 销售id
     */
    @UserId
    private String sellerId;

    /**
     * 项目列表
     */
    private List<String> projects;

    /**
     * 项目组id列表
     */
    private List<String> projectGroupIds;

    /**
     * 过滤属性
     */
    private FilterOptions filterOptions;

    /**
     * 排序规则, eg： 首次访问 时间倒序： ['1', '2']
     */
    private List<String> sortOptions;

    /**
     * 根据姓名查询字段
     */
    private String search;

    /**
     * 员工分组id列表
     */
    private List<Integer> groupIds;

    /**
     * 销售id列表
     */
    private List<String> sellerIds;
}
