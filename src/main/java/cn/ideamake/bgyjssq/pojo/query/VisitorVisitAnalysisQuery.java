package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * @Author: Walt
 * @Data: 2019-07-09 10:14
 * @Version: 1.0
 * 针对员工查看客户访问分析的查询接口，查询中包含数组，暂定用post接口
 */
@Data
public class VisitorVisitAnalysisQuery {

    /**
     * 查询的项目列表
     */
    @NotNull(message = "项目id不能为空")
    private Set<Integer> projects;

    /**
     *  暂定-1或空-全部，0-今天, 1-昨天，7-7天，30-30天，不传默认全部
     */
    private Integer searchTime=-1;

    /**
     * 查询的销售Id,目前暂时通过访问token获取填充
     */
    @UserId
    private String sellerId;

    /**
     * 开始时间
     */
    private String startTime;

    /**
     * 结束时间
     */
    private String endTime;

    /**
     * 组id
     */
    private Integer groupId;

    /**
     * 数据类型
     */
    private int dataType;
}
