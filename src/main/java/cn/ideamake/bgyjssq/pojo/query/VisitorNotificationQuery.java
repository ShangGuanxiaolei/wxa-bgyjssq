package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author: Walt
 * @Data: 2019-06-14 14:40
 * @Version: 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class VisitorNotificationQuery extends DefaultQueryPage {

    /**
     * 用户的id
     */
    @UserId
    private String userid;
    /**
     * 通知的类型 1-全部,2-系统通知，3-项目通知
     */
    @NotNull(message = "项目等级不能为空")
    private Integer level;

    /**
     * 项目id,level in (1,3) 时,projectId != null
     */
    private Integer projectId;

    /**
     * 用户类型
     */
    private Integer userType = 0;

    /**
     * 查询临时存储字段，与前端无关
     */
    private List<Integer> groups;

    /**
     * 组id
     */
    private Integer groupId;

}
