package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * @Author: gxl
 * @Data: 2019-08-13 18:40
 * @Version: 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class StaffNotifyListQuery extends QueryPage{

    /**
     * 用户id
     */
    @NotNull(message = "分组id不能为空")
    private Integer groupId;

    /**
     * 资源id
     */
    @NotNull(message = "资源id不能为空")
    private Integer resourceId;

}
