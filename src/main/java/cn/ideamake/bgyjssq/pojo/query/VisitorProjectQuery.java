package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @Author: Walt
 * @Data: 2019-06-17 16:52
 * @Version: 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class VisitorProjectQuery  extends DefaultQueryPage {

    /**
     * 搜索范围
     */
    private String scope;

    /**
     * 搜索内容
     */
    private String search;

    /**
     * 访客id
     */
    @UserId
    private String visitorId;

    /**
     * 项目组
     */
    private List<Integer> projectGroups;

    /**
     * 发布状态
     */
    private Integer status;

    /**
     * 项目列表
     */
    private List<Integer> projectIds;

}
