package cn.ideamake.bgyjssq.pojo.query;

import cn.binarywang.wx.miniapp.bean.WxMaCodeLineColor;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Author: Walt
 * @Data: 2019-06-11 15:55
 * @Version: 1.0
 */
@Data
@Accessors(chain = true)
@RequiredArgsConstructor(staticName = "of")
public class WeChatQRQuery implements Serializable {

    /**
     * 最大32个可见字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~，
     * 其它字符请自行编码为合法字符（因不支持%，中文无法使用 urlencode 处理，请使用其他编码方式）
     * 必填
     */
    private String scene = "a=b";

    /**
     * 必须是已经发布的小程序存在的页面（否则报错），
     * 例如 pages/index/index, 根路径前不要填加 /,不能携带参数（参数请放在scene字段里），如果不填写这个字段，默认跳主页面
     * 非必填，默认主页
     */
    private String page;

    /**
     * 二维码的宽度，单位 px，最小 280px，最大 1280px
     * 非必填，默认430
     */
    @JsonProperty("width")
    private int width = 340;

    /**
     * 自动配置线条颜色，如果颜色依然是黑色，则说明不建议配置主色调
     * 非必填，默认 false
     */
    @JsonProperty("auto_color")
    private boolean autoColor;

    /**
     * auto_color 为 false 时生效，使用 rgb 设置颜色
     * 例如 {"r":"xxx","g":"xxx","b":"xxx"} 十进制表示
     */
    @JsonProperty("line_color")
    private WxMaCodeLineColor lineColor = new WxMaCodeLineColor("0", "0", "0");

    /**
     * 是否需要透明底色，为 true 时，生成透明底色的小程序
     */
    @JsonProperty("is_hyaline")
    private boolean isHyaline;

}
