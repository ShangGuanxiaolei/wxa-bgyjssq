package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * @Author: Walt
 * @Data: 2019-06-18 15:40
 * @Version: 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class StaffNotificationDetailQuery extends QueryPage{

    /**
     * 用户id
     */
    @NotNull(message = "用户id不能为空")
    @UserId
    private String userId;

    /**
     * 资源id
     */
    @NotNull(message = "资源id不能为空")
    private Integer resourceId;

}
