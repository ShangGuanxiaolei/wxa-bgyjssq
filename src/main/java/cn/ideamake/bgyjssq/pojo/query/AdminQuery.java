package cn.ideamake.bgyjssq.pojo.query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author gxl
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminQuery extends QueryPage {

    /**
     * 管理员名称或电话
     */
    private String nameOrPhone;

    /**
     * 角色id
     */
    private Integer roleId;

    /**
     * 登录时间
     */
    private String loginAt;

}
