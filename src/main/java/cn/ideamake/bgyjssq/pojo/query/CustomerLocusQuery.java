package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.pojo.bo.BindRecordBO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/07/10
 * @description 客户运动轨迹
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CustomerLocusQuery extends BindRecordBO implements Serializable {

    private String startTime;

    private String endTime;

}
