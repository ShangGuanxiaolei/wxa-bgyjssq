package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;

/**
 * @Author: Walt
 * @Data: 2019-07-08 14:21
 * @Version: 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class StaffVisitorRankQuery extends QueryPage {

    /**
     * 查询的项目列表
     */
    private Set<Integer> projects;

    /**
     * 查看类型为停留时间具体看，time-停留时间，share-分享次数，recommend-推荐次数
     */
    private String searchType;

    /**
     * 查询时间段，暂定-1-全部，0-今天, 1-昨天，7-7天，30-30天
     */
    private Integer searchTime;

    /**
     * 销售的id
     */
    @UserId
    private String sellerId;

    /**
     * 开始时间
     */
    private String startTime;

    /**
     * 结束时间
     */
    private String endTime;

    /**
     * 搜索用户类型0-访客，1-销售
     */
    private Integer searchUserType;

    /**
     * 销售id列表
     */
    private Set<String> sellerIds;

}
