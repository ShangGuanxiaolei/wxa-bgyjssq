package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author imyzt
 * @date 2019/07/08
 * @description 客户管理列表查询对象(PC后台)
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class CustomerListQuery extends DefaultQueryPage implements Serializable {

    /**
     * 客户姓名/手机
     */
    private String key;

    /**
     * 项目组别
     */
    private List<Integer> projectGroupIds;

    /**
     * 项目列表
     */
    private List<Integer> projectIds;

    /**
     * 员工组别
     */
    private List<Integer> userGroupIds;

    /**
     * 员工列表
     */
    private List<String> sellerIds;

    /**
     * 访问时间（开始）
     */
    private String visitStartTime;

    /**
     * 访问时间（结束）
     */
    private String visitEndTime;

    /**
     * 客户类别
     */
    private List<Integer> type;

    /**
     * 客户属性
     */
    private Integer customerAttribute;

    /**
     * 联系方式
     */
    private Integer contact;

    /**
     * 定位状态（0-未授权，1-已授权）
     */
    private Integer positioningStatus;

    /**
     * 拓客渠道
     */
    private List<Integer> sourceLabel;

    /**
     * 拓客类别
     */
    private List<Integer> promotionLabel;

    /**
     * 跟办类别
     */
    private Integer category;

    /**
     * 转办次数min
     */
    private Integer minTransfersNumber;

    /**
     * 转办次数max
     */
    private Integer maxTransfersNumber;

    /**
     * 排序字段，转手跟办次数（0倒叙，1正序）
     */
    private Integer sortKey;

    /**
     * 最后登录时间（开始）
     */
    private String startTime;

    /**
     * 最后登录时间（结束）
     */
    private String endTime;

    /**
     * 曾今到访（0-否，1-是）
     */
    private Integer beforeVisit;

}
