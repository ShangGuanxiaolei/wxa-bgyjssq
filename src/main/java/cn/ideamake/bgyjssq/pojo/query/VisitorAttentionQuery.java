package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: Walt
 * @Data: 2019-06-13 14:58
 * @Version: 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class VisitorAttentionQuery extends QueryPage{

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @UserId
    private String userid;

}
