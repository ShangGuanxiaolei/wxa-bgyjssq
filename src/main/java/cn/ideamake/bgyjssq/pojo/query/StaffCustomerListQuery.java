package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * @author imyzt
 * @date 2019/07/03
 * @description 员工客户列表查询对象
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class StaffCustomerListQuery extends QueryPage implements Serializable {

    /**
     * 项目列表
     */
    @NotEmpty(message = "至少选择一个项目")
    private List<Integer> projectId;

    private String userid;

    /**
     * 手机号/姓名
     */
    private String condition;

    /**
     * 客户属性
     */
    private List<Integer> attribute;

    /**
     * 动态类别
     */
    private List<Integer> category;

    private String startTime;

    private String endTime;

    /**
     * 是否有手机号
     */
    private Boolean phone;

    /**
     * 客户来源
     */
    private List<Integer> source;

}
