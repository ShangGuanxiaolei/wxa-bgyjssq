package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author: Walt
 * @Data: 2019-08-16 10:34
 * @Version: 1.0
 */
@Data
public class DigDataQuery extends DefaultQueryPage{
    /**
     * 用户id
     */
    @NotNull(message = "查询用户id不能为空")
    private String userId;

    /**
     * 钻取类型
     */
    private Integer digDataType =-1;

    /**
     * 开始时间
     */
    private String startTime;

    /**
     * 结束时间
     */
    private String endTime;

    /**
     * 查询项目列表
     */
    private List<Integer> projectIds;

    /**
     * 查询的用户类型
     */
    @NotNull(message = "查询的用户类型不能为空")
    private Integer userType;

}
