package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * @Author: Walt
 * @Data: 2019-07-05 17:00
 * @Version: 1.0
 * 访客动态查询参数
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class VisitorTrendQuery extends QueryPage {

    /**
     * 访客id
     */
    @NotNull(message = "访客id不能为空")
    private String visitorId;

    /**
     * 销售id
     */
    @UserId
    private String sellerId;

}
