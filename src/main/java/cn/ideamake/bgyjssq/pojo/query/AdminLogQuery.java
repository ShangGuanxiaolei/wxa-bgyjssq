package cn.ideamake.bgyjssq.pojo.query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author gxl
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminLogQuery extends DefaultQueryPage {

    /**
     * 管理员名称
     */
    private String adminName;

    /**
     * 角色id
     */
    private Integer roleId;

    /**
     * 操作时间
     */
    private String createAt;

}
