package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import cn.ideamake.bgyjssq.pojo.bo.WeChatPhone;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author gxl
 */
@Data
public class SignInQuery {


    /**
     * 活动id
     */
    @NotNull(message = "活动id不能为空")
    Integer id;

    /**
     * visitor uuid
     */
    @UserId
    private String uuid;

    /**
     * 用户手机号
     */
    private WeChatPhone phone;

    /**
     * 经度
     */
    private String latitude;

    /**
     * 纬度
     */
    private String longitude;
}
