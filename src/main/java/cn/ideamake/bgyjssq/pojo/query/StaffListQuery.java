package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Set;

/**
 * @author imyzt
 * @date 2019/07/15
 * @description 员工列表查询对象-PC后台
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class StaffListQuery extends DefaultQueryPage implements Serializable {

    /**
     * 姓名/电话/小组/职务/公司名
     */
    private String condition;

    private String startTime;

    private String endTime;

    private String companyName;

    private Integer groupId;

    private Integer staffType;

    /**
     * 履职状态
     */
    private Integer active;

    /**
     * 项目列表
     */
    private Set<Integer> projects;
}
