package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author: Walt
 * @Data: 2019-07-15 17:50
 * @Version: 1.0
 */
@Data
public class BusinessQRCodeQuery {

    /**
     * 获取用的id
     */
    @NotNull(message = "用户ID不能为空")
    private String userid;

    /**
     * 微信二维码参数
     */
    private WeChatQRQuery weChatQRQuery = WeChatQRQuery.of();
}
