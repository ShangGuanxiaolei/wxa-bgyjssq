package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/07/01
 * @description 客户管理列表查询对象(小程序端)
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class CustomerManagerQuery extends QueryPage implements Serializable {

    /**
     * 项目/姓名/电话
     */
    private String condition;

    /**
     * 项目名称
     */
    private String projectTitle;

    /**
     * 客户类别
     */
    private Integer selfLabel;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 拓客类别
     */
    private String visitorSource;

    // TODO: 2019-07-01 客户属性,拓客渠道,跟办类别,最近登录
}
