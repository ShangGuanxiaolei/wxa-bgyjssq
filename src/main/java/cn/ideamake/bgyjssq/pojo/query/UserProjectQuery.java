package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: Walt
 * @Data: 2019-06-18 17:12
 * @Version: 1.0
 * 获取员工的项目
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserProjectQuery  extends QueryPage {
    /**
     * 用户id
     */
    @UserId
    private String userid;

    /**
     * 销售id
     */
    private String sellerId;

    /**
     * 根据楼盘名称或区域搜索
     */
    private String search;

}
