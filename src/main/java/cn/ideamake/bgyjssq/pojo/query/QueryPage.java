package cn.ideamake.bgyjssq.pojo.query;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Walt
 * @Data: 2019-06-26 09:43
 * @Version: 1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class QueryPage {

    /**
     * 每页多少记录
     */
    private Integer limit=300;

    /**
     * 当前第几页
     */
    private Integer page=1;

    public <T> Page<T> getPageInfo() {
        return new Page<>(this.page, this.limit);
    }

}
