package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;

@Data
public class VisitorCountQuery {
    /**
     * visitor uuid
     */
    private String uuid;

    /**
     *  activity id
     */
    private Integer id;
}
