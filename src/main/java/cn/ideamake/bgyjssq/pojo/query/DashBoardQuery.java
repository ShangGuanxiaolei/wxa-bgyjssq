package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;

import java.util.List;

/**
 * @author imyzt
 * @date 2019/07/17
 * @description 数据统计页面查询参数
 */
@Data
public class DashBoardQuery {

    /**
     * 顶框项目过滤
     */
    private List<Integer> projectIds;

    /**
     * 时间查询参数
     */
    private String startTime;
    private String endTime;

    /**
     * 微文章-数据-来源,0销售，1访客
     */
    private Integer userType;

    /**
     * 访客类型
     *  {name: '一般访客', value: '1' },
     *  { name: '意向客户', value: '2' },
     *  { name: '诚意客户', value: '3' },
     *  { name: '项目业主', value: '4' }
     */
    private Integer type;
}
