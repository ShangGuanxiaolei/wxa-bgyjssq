package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author imyzt
 * @date 2019/08/01
 * @description 员工业绩管理查询参数
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class StaffKpiQuery extends DefaultQueryPage implements Serializable {

    /**
     * 员工姓名/职务
     */
    private String staffName;

    private String startTime;

    private String endTime;


    /**
     * 筛选的小组
     */
    private List<Integer> groupIds;

    /**
     * 排序字段
     */
    private String sort;

    /**
     * 是否倒序
     */
    private Integer desc = 1;

    /**
     * 职务id
     */
    private Integer position;

    /**
     * 员工类别
     */
    private Integer staffType;

    /**
     * 顶框项目过滤,项目就是部门
     */
    private List<Integer> projectIds;

    /**
     * 项目组
     */
    private List<Integer> projectGroups;

}
