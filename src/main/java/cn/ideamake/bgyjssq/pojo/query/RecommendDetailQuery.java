package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: Walt
 * @Data: 2019-05-17 17:26
 * @Version: 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RecommendDetailQuery extends QueryPage {

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 用户id,推荐来源
     */
    @UserId
    private String userid;

}
