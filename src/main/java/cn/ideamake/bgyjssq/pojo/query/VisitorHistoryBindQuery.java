package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @Author: Walt
 * @Data: 2019-06-13 11:23
 * @Version: 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class VisitorHistoryBindQuery extends QueryPage implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userid;

}
