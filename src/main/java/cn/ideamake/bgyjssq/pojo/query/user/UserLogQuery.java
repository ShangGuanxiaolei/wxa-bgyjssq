package cn.ideamake.bgyjssq.pojo.query.user;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import cn.ideamake.bgyjssq.pojo.query.DefaultQueryPage;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: gxl
 * @Data: 2019-09-11 15:29
 * @Version: 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserLogQuery extends DefaultQueryPage {

    /**
     * 用户的id
     */
    @UserId
    private String userId;

}
