package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: Walt
 * @Data: 2019-08-10 13:45
 * @Version: 1.0
 * 备用-待删除
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class GroupProjectsQuery extends QueryPage {

    /**
     * 查询开始时间
     */
    private String startTime;

    /**
     * 查询结束时间
     */
    private String endTime;

    /**
     * 项目组id
     */
    private Integer groupId;

    /**
     * 销售id
     */
    @UserId
    private String sellerId;
}
