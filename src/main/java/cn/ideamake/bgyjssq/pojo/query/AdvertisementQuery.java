package cn.ideamake.bgyjssq.pojo.query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Yusi Zhang
 * @Created 2019-08-07 19:27
 * TODO()
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdvertisementQuery extends DefaultQueryPage {

    /**
     * 项目组别
     */
    private Integer projectGroup;

    /**
     * 项目列表
     */
    private List<Integer> projectIds;

    /**
     * 广告标题
     */
    private String adverTitle;

    /**
     * 广告类别
     */
    private Integer adverType;

    /**
     * 广告状态
     */
    private Integer status;

    /**
     * 开始时间
     */
    private String startTime;

    /**
     * 结束时间
     */
    private String endTime;
}
