package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.common.enums.ContentPostStatus;
import cn.ideamake.bgyjssq.common.enums.ContentTopStatus;
import cn.ideamake.bgyjssq.common.enums.PosterCategory;
import cn.ideamake.bgyjssq.common.validate.EnumVerify;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author imyzt
 * @date 2019/07/02
 * @description 资源列表查询对象
 * 文章
 * 海报
 * 楼书
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class ResourceListQuery extends DefaultQueryPage implements Serializable {

    /**
     * 查询关键字
     */
    private String condition;

    /**
     * 资源类别
     */
    @EnumVerify(message = "请选择正确的资源类别", enumClass = PosterCategory.class)
    private Integer category;

    /**
     * 资源组别
     */
    private Integer groupId;

    /**
     * 项目ID
     */
    private List<Integer> projectIds;

    /**
     * 发布时间
     */
    private String startTime;
    private String endTime;

    /**
     * 发布状态
     */
    @EnumVerify(message = "请选择正确的发布状态", enumClass = ContentPostStatus.class)
    private Integer status;

    /**
     * 是否置顶
     */
    @EnumVerify(message = "请选择正确的置顶状态", enumClass = ContentTopStatus.class)
    private Integer top;

    /**
     * 项目组别
     */
    private Integer projectGroup;


}
