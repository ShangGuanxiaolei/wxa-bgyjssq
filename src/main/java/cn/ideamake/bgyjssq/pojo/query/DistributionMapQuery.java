package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author imyzt
 * @date 2019/07/22
 * @description PC后台-客户分布地图查询参数
 */
@Data
@Accessors(chain = true)
public class DistributionMapQuery implements Serializable {

    /**
     * 查看时段开始
     */
    private String startTime;

    /**
     * 查看时段结束
     */
    private String endTime;

    /**
     * 项目组
     */
    private List<Integer> projectGroupIds;

    /**
     * 项目
     */
    private List<Integer> projectIds;

    /**
     * 员工组
     */
    private List<Integer> userGroupIds;

    /**
     * 员工类别（普通员工、特殊员工。。。）
     */
    private List<Integer> sellerType;

    /**
     * 客户类别（一般访客、意向客户。。。）
     */
    private List<Integer> customerType;

    /**
     * 客户属性（0老客户/1新客户）
     */
    private Integer customerAttributes;

    /**
     * 客户来源（小程序、微楼书。。。）
     */
    private List<Integer> visitorSource;

    /**
     * 联系方式（0无，1有）
     */
    private Integer hasPhone;

    /**
     * 拓客类别0-访客 1-员工
     */
    private List<Integer> promotionLabel;

}
