package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author imyzt
 * @date 2019/07/29
 * @description 员工/客户 top10查询对象
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TopTenQuery extends DashBoardQuery {

    /**
     * 排序字段
     */
    private String sort;

    /**
     * 是否倒序
     */
    private Integer desc = 1;

    /**
     * 用户类型
     */
    private Integer staffType;
}
