package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author gxl
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class QrCodeQuery extends DefaultQueryPage {

    /**
     * 项目组别ID
     */
    private Integer projectGroupId;

    /**
     * 项目ID
     */
    private Integer projectId;

    /**
     * 活动组别id
     */
    private Integer activityGroupId;

    /**
     * 活动ID
     */
    private Integer activityId;

    /**
     * 启用时间1
     */
    private String startTime;

    /**
     * 启用时间2
     */
    private String endTime;

    /**
     * 使用状态
     */
    private Integer status;

    /**
     * 销售员姓名
     */
    private String sellerName;

    /**
     * 活动名称
     */
    private String activityName;

    /**
     * 项目列表
     */
    private List<Integer> projectIds;

}
