package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/07/01
 * @description 楼书查询对象
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class FloorBookQuery extends QueryPage implements Serializable {

    /**
     * 姓名,电话,小组,职务
     */
    private String condition;

    /**
     * 分组
     */
    private String group;

    /**
     * 发布状态
     */
    private String status;
}
