package cn.ideamake.bgyjssq.pojo.query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author gxl
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StaffGroupQuery extends DefaultQueryPage {

    /**
     * 项目分组id
     */
    private Integer projectGroupId;

    /**
     * 项目名
     */
    private String projectName;

    /**
     * 项目列表
     */
    private List<Integer> projectIds;

}
