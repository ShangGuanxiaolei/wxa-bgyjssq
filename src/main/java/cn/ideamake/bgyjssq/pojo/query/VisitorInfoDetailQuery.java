package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author: Walt
 * @Data: 2019-06-18 19:42
 * @Version: 1.0
 */
@Data
public class VisitorInfoDetailQuery {
    /**
     * 访客id
     */
    @NotBlank(message = "客户ID未传")
    private String userid;

    /**
     * 销售id
     */
    @UserId
    private String sellerId;
}
