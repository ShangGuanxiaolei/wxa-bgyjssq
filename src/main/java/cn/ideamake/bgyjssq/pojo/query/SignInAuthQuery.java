package cn.ideamake.bgyjssq.pojo.query;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SignInAuthQuery {
    /**
     * 二维码失效时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    LocalDateTime endTime;

    /**
     * 活动id
     */
    Integer id;
}
