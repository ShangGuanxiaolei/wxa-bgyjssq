package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author gxl
 */
@Data
public class BatchActivityQuery {

    /**
     * 活动IDs
     */
    @NotNull(message = "请选择活动id")
    private List<Integer> ids;
}
