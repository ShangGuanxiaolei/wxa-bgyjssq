package cn.ideamake.bgyjssq.pojo.query;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: Walt
 * @Data: 2019-06-14 14:40
 * @Version: 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class VisitorLogQuery extends QueryPage {

    /**
     * 用户的id
     */
    @UserId
    private String userid;

}
