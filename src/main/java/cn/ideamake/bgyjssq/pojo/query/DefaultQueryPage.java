package cn.ideamake.bgyjssq.pojo.query;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: Walt
 * @Data: 2019-06-26 09:43
 * @Version: 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DefaultQueryPage extends QueryPage {

    public DefaultQueryPage() {
        setLimit(10);
    }
}
