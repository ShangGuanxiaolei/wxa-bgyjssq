package cn.ideamake.bgyjssq.pojo.bo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author imyzt
 * @date 2019/07/11
 * @description 项目ID列表
 */
@Data
@Accessors(chain = true)
public class ProjectIdSetBO implements Serializable {

    private List<Integer> ids;
}
