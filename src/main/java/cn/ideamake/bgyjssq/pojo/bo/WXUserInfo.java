package cn.ideamake.bgyjssq.pojo.bo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import cn.ideamake.bgyjssq.common.annotition.UserId;
import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-06 17:11
 * @Version: 1.0
 */
@Data
public class WXUserInfo {

    @UserId
    private String userId;

    private String nickName;

    @OssPrefixAutoFill
    private String avatarUrl;

    private String city;

    private String country;

    private Integer gender;

    private String language;

    private String province;

}
