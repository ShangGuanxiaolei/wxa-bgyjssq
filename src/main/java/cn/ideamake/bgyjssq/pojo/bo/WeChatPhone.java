package cn.ideamake.bgyjssq.pojo.bo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author: Walt
 * @Data: 2019-07-18 13:46
 * @Version: 1.0
 * 微信手机号解析
 */
@Data
public class WeChatPhone  implements Serializable {
    /**
     * 包括敏感数据在内的完整用户信息的加密数据
     */
    private String encryptedData;

    /**
     * 加密算法的初始向量
     */
    private String iv;

    /**
     * 用户sessin_key
     */
    private String sessionKey;

    /**
     * 微信授权码
     */
    @NotNull(message = "微信授权码不能为空")
    private String code;
}
