package cn.ideamake.bgyjssq.pojo.bo;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-08-28 11:16
 * @Version: 1.0
 */
@Data
public class SinglePushVO {
    /**
     * 项目
     */
    private Integer projectId;

    /**
     * 推送访客类型
     */
    private String visitorType;
}
