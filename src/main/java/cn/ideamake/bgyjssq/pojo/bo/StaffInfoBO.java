package cn.ideamake.bgyjssq.pojo.bo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.Data;

import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/07/29
 * @description 批量导入员工信息, Excel模板中员工信息的格式
 */
@Data
@ExcelTarget("staffInfoBO")
public class StaffInfoBO implements Serializable {

    @Excel(name = "注册日期")
    private String registerDate;

    @Excel(name = "姓名")
    private String nickName;

    @Excel(name = "员工类别")
    private Integer category;

    @Excel(name = "BIP账号")
    private String bip;

    @Excel(name = "微信账号")
    private String wechat;

    @Excel(name = "手机号码")
    private String phone;

    @Excel(name = "公司名称")
    private String companyName;

    @Excel(name = "职务名称")
    private String jobName;

    @Excel(name = "履职状态")
    private Integer status;

    @Excel(name = "绑定项目")
    private String bindProject;

    @Excel(name = "所属部门")
    private Integer department;

    @Excel(name = "小组名称")
    private Integer group;


}
