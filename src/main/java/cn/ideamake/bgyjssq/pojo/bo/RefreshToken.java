package cn.ideamake.bgyjssq.pojo.bo;


import cn.ideamake.bgyjssq.pojo.vo.UserSessionVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/1/12 14:51
 * @description RefreshToken
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RefreshToken implements Serializable{

    private Token token;

    private UserSessionVO userSessionVO;
}
