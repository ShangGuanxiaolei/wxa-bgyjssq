package cn.ideamake.bgyjssq.pojo.bo;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-25 17:10
 * @Version: 1.0
 */
@Data
public class ProjectInfo {

    private Integer projectId;

    private String projectName;

    private String projectImg;

    private String floorBook;

}
