package cn.ideamake.bgyjssq.pojo.bo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/1/12 13:37
 * @description Token
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Token implements Serializable{

    @JsonProperty("access_token")
    private String accessToken;

    @JsonProperty("refresh_token")
    private String refreshToken;
}
