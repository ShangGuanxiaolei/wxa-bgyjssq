package cn.ideamake.bgyjssq.pojo.bo;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-08-09 09:37
 * @Version: 1.0
 */
@Data
public class CollationRule {

    /**
     * 发布时间
     */
    private Integer publishTime;

    /**
     * 阅读数量
     */
    private Integer pageViews;

    /**
     * 阅读人数
     */
    private Integer uniqueVisitors;
}
