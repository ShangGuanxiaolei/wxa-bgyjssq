package cn.ideamake.bgyjssq.pojo.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author imyzt
 * @date 2019/1/15 9:32
 * @description AccessToken
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccessToken {

    private String accessToken;

    private LocalDateTime expiredTime;
}
