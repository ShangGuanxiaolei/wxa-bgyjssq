package cn.ideamake.bgyjssq.pojo.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: Walt
 * @Data: 2019-06-18 19:30
 * @Version: 1.0
 */
@Data
public class VisitorActivityStatistics {

    /**
     * bindManagerId
     */
    private Integer id;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 项目名
     */
    private String projectName;

    /**
     * 首次关注项目日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime firstFollowDate;

    /**
     * 首次营销中心日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime firstMarketingDate;

    /**
     * 累计登录空间次数
     */
    private Integer totalLoginCount;

    /**
     * 累计驻留空间时间
     */
    private Integer totalStayDuration;

    /**
     * 累计观看文章篇数
     */
    private Integer totalReadArticleCount;

    /**
     * 累计扫码海报数量
     */
    private Integer totalReadPosterCount;

    /**
     * 累计推介项目人次
     */
    private Integer totalIntroProjectCount;

    /**
     * 累计贡献留电数量
     */
    private Integer phoneCount;

    /**
     * 客户类别
     */
    private Integer visitorType;

    /**
     * 拓客类别
     */
    private Integer promotionType;
}
