package cn.ideamake.bgyjssq.pojo.bo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-25 17:10
 * @Version: 1.0
 */
@Data
public class SellerInfo {

    /**
     * 销售名称
     */
    private String name;

    /**
     * 销售头像地址
     */
    @OssPrefixAutoFill
    private String avatar;

    /**
     * 销售手机号
     */
    private String phone;

    /**
     * 销售id
     */
    private String userid;

    /**
     * 销售二维码链接
     */
    @OssPrefixAutoFill
    private String weChatQrUrl;

    /**
     * 隐藏字段
     */
    private String[] hideColumn;
}
