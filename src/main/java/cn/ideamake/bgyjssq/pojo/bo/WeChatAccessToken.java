package cn.ideamake.bgyjssq.pojo.bo;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-11 15:44
 * @Version: 1.0
 */
@Data
public class WeChatAccessToken {
    private String accessToken;
    private String expiresIn;
}
