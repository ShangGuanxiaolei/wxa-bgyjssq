package cn.ideamake.bgyjssq.pojo.bo;

import cn.ideamake.bgyjssq.common.enums.VisitorOrigin;
import lombok.Data;

/**
 * @author imyzt
 * @date 2019/07/09
 * @description 客户信息
 */
@Data
public class CustomerInfoBO {

    private String name;

    private String phone;

    /**
     * 拓客类别
     */
    private String visitorSource;

    /**
     * 拓客渠道
     */
    private VisitorOrigin origin;

    /**
     * 备注
     */
    private String remark;
}
