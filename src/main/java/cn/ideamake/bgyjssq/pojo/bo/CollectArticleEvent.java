package cn.ideamake.bgyjssq.pojo.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: Walt
 * @Data: 2019-07-02 14:48
 * @Version: 1.0
 * 访客访问文章内容采集
 * type
 * 1-全量访问记录，用于给销售观看访客行为，
 * 2-项目访问记录，用户统计访客对于某项目的访问
 * 3-文章阅读记录，用于记录访客对于文章的记录
 */
@Data
public class CollectArticleEvent {

    /**
     * 访客id
     */
    private String visitorId;

    /**
     * 销售id
     */
    private String sellerId;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 文章id
     */
    private Integer articleId;

    /**
     * 访问开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createAt;

    /**
     * 停留时长（秒）
     */
    private Integer stayTime;

    /**
     * 访问类型
     */
    private Integer type;

    /**
     * 用户访问的ip地址
     */
    private String ip;
}
