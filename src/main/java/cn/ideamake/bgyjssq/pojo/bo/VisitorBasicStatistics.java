package cn.ideamake.bgyjssq.pojo.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: Walt
 * @Data: 2019-06-18 19:23
 * @Version: 1.0
 * 访客基础统计model
 */
@Data
public class VisitorBasicStatistics {

    /**
     * 累计驻留时长
     */
    private Integer stayTime;

    /**
     * 最近访问时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastVisitTime;

    /**
     * 最近访问项目
     */
    private String lastVisitProject;

    /**
     * 累计关注项目
     */
    private Integer followProjNum;

    /**
     * 累计访问次数
     */
    private Integer visitCount;

    /**
     * 累计观看文章
     */
    private Integer articleCount;

    /**
     * 累计扫码海报
     */
    private Integer posterCount;

    /**
     * 累计推荐人数
     */
    private Integer totalIntroCount;

    /**
     * 累计贡献留电
     */
    private Integer phoneCount;
}
