package cn.ideamake.bgyjssq.pojo.bo;

import lombok.Data;

import java.util.List;

/**
 * @Author: Walt
 * @Data: 2019-06-19 11:46
 * @Version: 1.0
 * 客户管理访客顾虑规则
 */
@Data
public class FilterOptions {

    /**
     * 客户属性
     */
    private List<String> properties;

    /**
     * 动态类别
     */
    private List<String> types;

    /**
     *  联系方式： 有手机号、无手机号
     */
    private Integer contactWays;

    /**
     * 客户来源： 微传单、微楼书等
     */
    private List<String> origins;

    /**
     * 最近访问时间
     */
    private List<String> dateRange;

    /**
     * 拓客类别
     * 0-访客拓展
     * 1-销售转介
     */
    private Integer introType;


}
