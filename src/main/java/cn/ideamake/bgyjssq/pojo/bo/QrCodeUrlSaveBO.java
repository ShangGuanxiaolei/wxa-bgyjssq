package cn.ideamake.bgyjssq.pojo.bo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author: Walt
 * @Data: 2019-08-23 16:56
 * @Version: 1.0
 */
@Data
public class QrCodeUrlSaveBO implements Serializable {
    @NotNull(message = "key值不能为空")
    private String key;

    @NotNull(message = "内容不能为空")
    private String value;
}
