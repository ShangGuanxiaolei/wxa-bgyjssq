package cn.ideamake.bgyjssq.pojo.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: Walt
 * @Data: 2019-07-02 19:49
 * @Version: 1.0
 * 访客访问项目采集
 * type
 * 1-全量访问记录，用于给销售观看访客行为，
 * 2-项目访问记录，用户统计访客对于某项目的访问
 * 3-文章阅读记录，用于记录访客对于文章的记录
 */
@Data
public class CollectVisitEvent {
    /**
     * 访客id
     */
    private String visitorId;
    /**
     * 销售id
     */
    private String sellerId;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 访问开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createAt;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 维度
     */
    private String latitude;

    /**
     * 停留时长（秒）
     */
    private Integer stayTime;

    /**
     * 来源标记，1，2，3，4，(小程序，老带新等)
     */
    private Integer origin;

    /**
     * 采集类型
     */
    private Integer type;

    /**
     * 用户访问的ip地址
     */
    private String ip;
}
