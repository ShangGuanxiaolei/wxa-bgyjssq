package cn.ideamake.bgyjssq.pojo.bo;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author imyzt
 * @date 2019/1/12 13:31
 * @description Audience
 */
@Data
@Component
@ConfigurationProperties(prefix = "audience")
public class Audience {
    private String clientId;
    private String base64Secret;
    private String name;
    private int expiresSecond;
}
