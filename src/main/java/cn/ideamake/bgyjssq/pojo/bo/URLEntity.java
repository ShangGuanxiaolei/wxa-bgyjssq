package cn.ideamake.bgyjssq.pojo.bo;

import lombok.Data;

import java.util.Map;

/**
 * @Author: Walt
 * @Data: 2019-08-29 16:02
 * @Version: 1.0
 */
@Data
public class URLEntity {
    /**
     * 基础url
     */
    public String baseUrl;
    /**
     * url参数
     */
    public Map<String, String> params;

}
