package cn.ideamake.bgyjssq.pojo.bo;

import cn.ideamake.bgyjssq.pojo.query.DefaultQueryPage;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/07/10
 * @description 销售id-客户id-项目id绑定一条记录
 * 此处统一三个参数的参数名,需要时直接继承该对象
 * 继承queryPage是因为Java是单继承,避免子类需要page参数,不使用时可以直接忽略
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BindRecordBO extends DefaultQueryPage implements Serializable {

    /**
     * 访客的id
     */
    @NotBlank(message = "访客ID必填")
    private String visitorId;

    /**
     * 销售的id
     */
    @NotBlank(message = "销售ID必填")
    private String sellerId;

    /**
     * 项目id
     */
    @NotNull(message = "项目ID必填")
    private Integer projectId;
}
