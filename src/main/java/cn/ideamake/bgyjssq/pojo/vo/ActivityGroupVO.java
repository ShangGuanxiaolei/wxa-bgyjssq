package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

/**
 * @author ideamake
 */
@Data
public class ActivityGroupVO {
    /**
     * 活动组ID
     */
    private Integer groupId;

    /**
     * 活动组名
     */
    private String groupName;
}
