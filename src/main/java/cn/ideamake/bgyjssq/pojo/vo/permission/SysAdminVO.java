package cn.ideamake.bgyjssq.pojo.vo.permission;

import cn.ideamake.bgyjssq.pojo.vo.ProjectVO;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 系统管理员列表
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysAdminVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 管理员id
     */
    private Integer id;

    /**
     * 管理员名称
     */
    private String name;

    /**
     * 角色id
     */
    private Integer roleId;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 用户状态0禁用，1启用
     */
    private Integer status;

    /**
     * 上次登录时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime previousLoginAt;

    /**
     * 项目信息
     */
    private List<ProjectVO> projectVO;

}
