package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BatchActivityVO {
    /**
     * 活动ID
     */
    private Integer id;

    /**
     * 活动二维码
     */
    private String qrCode;
}
