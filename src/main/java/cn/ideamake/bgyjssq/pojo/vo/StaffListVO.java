package cn.ideamake.bgyjssq.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author imyzt
 * @date 2019/07/15
 * @description 员工列表-PC后台
 */
@Data
public class StaffListVO implements Serializable {

    /**
     * 用户id
     */
    private String id;

    /**
     * 注册时间
     */
    @JsonFormat( pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime registerDate;

    /**
     * 用户名称
     */
    private String name;

    /**
     * bip账号
     */
    private String bip;

    /**
     * 微信账号
     */
    private String wechatId;

    /**
     * 用户手机号
     */
    private String phone;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 所属项目
     */
    private String projectName;

    /**
     * 小组名称
     */
    private String groupName;

    /**
     * 职务名称
     */
    private String positionName;

    /**
     * 职业状态
     */
    private Integer active;

    /**
     * 绑定项目
     */
    private List<String> bindProject;

    /**
     * 员工类别
     */
    private String staffType;

    /**
     * 销售组所属项目
     */
    private String projectGroupName;

}
