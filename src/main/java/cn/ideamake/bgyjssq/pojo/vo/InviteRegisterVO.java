package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-21 12:15
 * @Version: 1.0
 */
@Data
public class InviteRegisterVO {

    /**
     * 邀请记录的id
     */
    private Integer inviteId;

    /**
     * 邀请验证码
     */
    private String validateCode;

    /**
     * 小程序太阳码
     */
    private String appletQrCode;

}
