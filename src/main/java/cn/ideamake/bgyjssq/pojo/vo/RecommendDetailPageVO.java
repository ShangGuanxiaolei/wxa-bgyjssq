package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: Walt
 * @Data: 2019-05-17 17:20
 * @Version: 1.0
 * 访客推荐详情页信息
 */
@Data
public class RecommendDetailPageVO {

    /**
     * 头像url
     */
    @OssPrefixAutoFill
    private String avatarUrl;

    /**
     * 姓名
     */
    private String name;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 推介时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime introDate;

    /**
     * 是否已到访: 0 到访， 1 未到访
     */
    private Integer visitState;

    /**
     * 推广类型
     */
    private Integer promotionType;
}
