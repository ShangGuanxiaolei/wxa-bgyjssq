package cn.ideamake.bgyjssq.pojo.vo.permission;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author gxl
 * @version 1.0
 * @description TODO
 * @date 2019-07-25 14:57
 */
@Data
public class AdminLoginVO {

    /**
     * 用户名
     */
    @NotNull(message = "用户名不能为空")
    private String username;

    /**
     * 密码
     */
    @NotNull(message = "密码不能为空")
    private String password;
}
