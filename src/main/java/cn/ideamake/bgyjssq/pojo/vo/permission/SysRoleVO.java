package cn.ideamake.bgyjssq.pojo.vo.permission;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 系统管理员列表
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysRoleVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色唯一记录
     */
    private Integer id;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色成员数
     */
    private Long member;

    /**
     * 权限id列表
     */
    private List<Integer> permissionIds;

}
