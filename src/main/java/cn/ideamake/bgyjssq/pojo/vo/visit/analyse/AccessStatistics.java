package cn.ideamake.bgyjssq.pojo.vo.visit.analyse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Walt
 * @Data: 2019-07-09 11:17
 * @Version: 1.0
 * 访客访问数据总揽，用于小程序员工查看访客
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AccessStatistics {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 访问数量
     */
    private Integer numberOfVisits;

    /**
     * 访问人数
     */
    private Integer numberOfPeople;
}
