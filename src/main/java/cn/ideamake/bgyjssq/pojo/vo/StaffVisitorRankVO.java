package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-07-08 14:27
 * @Version: 1.0
 */
@Data
public class StaffVisitorRankVO {

    /**
     * 访客头像
     */
    @OssPrefixAutoFill
    private String avatar;

    /**
     * 用户姓名
     */
    private String userName;

    /**
     * 用户手机号
     */
    private String userPhone;

    /**
     * 返回的统计数值，是否差成三个待定
     */
    private String count;

    /**
     * 部门项目名称
     */
    private String projectName;

    /**
     * 组名
     */
    private String groupName;
}
