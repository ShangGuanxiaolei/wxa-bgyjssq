package cn.ideamake.bgyjssq.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Yusi Zhang
 * @Created 2019-08-07 20:28
 * TODO()
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class AdvertisementVO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 广告内容，广告网址
     */
    private String adverContent;

    /**
     * 广告类别
     */
    private Integer adverType;

    /**
     * 广告标题
     */
    private String adverTitle;

    /**
     * 广告发布时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime adverDate;

    /**
     * 广告过期时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime invalidDate;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 广告状态
     */
    private Integer status;

    /**
     * 是否授权电话
     */
    private Boolean isAuthorizedPhone;

}
