package cn.ideamake.bgyjssq.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author ideamake
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SignInQrCodeVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 项目id
     */
    @NotNull(message = "项目不能为空")
    private Integer projectId;

    /**
     * 员工id
     */
    @NotNull(message = "销售员不能为空")
    private String userUuid;

    /**
     * 活动名称
     */
    @NotNull(message = "活动名称不能为空")
    private String activityName;

    /**
     * 活动ID
     */
    @NotNull(message = "活动组ID不能为空")
    private Integer activityGroupId;

    /**
     * 二维码使用场景
     */
    private String scene;

    /**
     * 二维码跳转页面
     */
    private String page;

    /**
     * 开始时间
     */

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startAt;

    /**
     * 结束时间
     */

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime stopAt;

    /**
     * 活动人数累计上限
     */
    private Integer activityNumTop;

    /**
     * 活动人数每日上限
     */
    private Integer activityNumDayTop;

    /**
     * 单人扫码累计上限
     */
    private Integer singleNumTop;

    /**
     * 单人扫码每日上限
     */
    private Integer singleNumDayTop;

    /**
     * 强制要求授权电话
     */
    private Integer pushAuthPhone;
    /**
     * 强制要求授权定位
     */
    private Integer pushAuthAddress;

    /**
     * 签到成功播放音乐
     */
    private Integer musicAfterSignIn;

    /**
     * 是否显示客户当天签到次数
     */
    private Integer showDaySignInNum;

    /**
     * 是否显示客户累计签到次数
     */
    private Integer showAllSignInNum;

    /**
     * 是否显示客户累计到访次数
     */
    private Integer showVisitNum;

}
