package cn.ideamake.bgyjssq.pojo.vo.visit.analyse;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-07-09 10:22
 * @Version: 1.0
 * 员工查看访客数据分析 客户来源统计BO对象
 */
@Data
public class OriginStatistics {

    /**
     * 累计新增来访
     */
    private Integer newAddition;

    /**
     * 累计老带新
     */
    private Integer oldWithNew;

    /**
     * 累计朋友圈
     */
    private Integer circleOfFriends;

    /**
     * 累计项目码
     */
    private Integer projectCode;

    /**
     * 累计小程序
     */
    private Integer applets;

    /**
     * 累计小程序
     */
    private Integer others;
}
