package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author imyzt
 * @date 2019/07/29
 * @description 客户top10列表返回对象
 */
@Data
public class CustomerTopTenVO implements Serializable {

    private String uuid;

    private String customerName;

    /**
     * 客户类别
     */
    private Integer customerType;

    /**
     * 推介总人数
     */
    private Integer recommendNum;

    /**
     * 贡献留电人数
     */
    private Integer phoneCount;

    /**
     * 贡献来访人次
     */
    private Integer visitNum;

    /**
     * 推介总人次
     */
    private Integer recommendCount;

    /**
     * 微楼书
     */
    private Integer microBookNum;

    /**
     * 微海报
     */
    private Integer microPosterNum;

    /**
     * 微图文
     */
    private Integer microTextNum;

    /**
     * 推广项目个数
     */
    private Integer recommendProjectNum;

    /**
     * 访客推介过的项目的名称
     */
    private String recommendProjects;


}
