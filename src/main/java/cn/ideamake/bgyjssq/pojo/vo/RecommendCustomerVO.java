package cn.ideamake.bgyjssq.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: gxl
 * @Data: 2019-08-08 11:16
 * @Version: 1.0
 * 访客推荐客户列表
 */
@Data
public class RecommendCustomerVO {

    /**
     * 姓名
     */
    private String name;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 推介时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime introDate;

    /**
     * 推荐类型
     */
    private Integer promotionType;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 到访时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime visitTime;
}
