package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: Walt
 * @Data: 2019-08-16 10:40
 * @Version: 1.0
 */
@Data
public class DigDataVO {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 项目id
     */
    private Integer projectId;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 用户头像
     */
    @OssPrefixAutoFill
    private String avatar;
    /**
     * 用户手机号
     */
    private String userPhone;
    /**
     * 拓客类别
     */
    private Integer promotionType;
    /**
     * 拓客渠道
     */
    private Integer visitorSource;
    /**
     * 跟办类别
     */
    private Integer transfersNumber;

    /**
     * 跟办员工
     */
    private String staffName;

    /**
     * 当前跟办bip
     */
    private String staffNameBip;

    /**
     * 客户类别
     */
    private String customerType;

    /**
     * 初始跟办
     */
    private String originFollowUp;

    /**
     * 初始跟办bip
     */
    private String originFollowUpBip;

    /**
     * 首次访问小程序时间（注意：非到访营销中心时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime firstVisitTime;

    /**
     * 最近访问小程序时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastVisitTime;



}
