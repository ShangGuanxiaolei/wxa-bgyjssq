package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.Data;

/**
 * @author gxl
 * @version 1.0
 * @description 脑图VO
 * @date 2019-09-19 17:37
 */
@Data
public class BrainMappingVO {

    /**
     * 头像
     */
    @OssPrefixAutoFill
    private String avatar;

    /**
     * 名称
     */
    private String nickname;

    /**
     * id
     */
    private String uuid;

    /**
     * 父id
     */
    private String parentUuid;

}
