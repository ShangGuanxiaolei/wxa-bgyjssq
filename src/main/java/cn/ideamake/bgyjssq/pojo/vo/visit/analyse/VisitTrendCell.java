package cn.ideamake.bgyjssq.pojo.vo.visit.analyse;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-07-09 10:30
 * @Version: 1.0
 * 访问分析的最小单位
 */
@Data
public class VisitTrendCell {
    /**
     * 访问日期
     */
    private String visitorDate;

    /**
     * 访问次数
     */
    private Integer visitorCount;
}
