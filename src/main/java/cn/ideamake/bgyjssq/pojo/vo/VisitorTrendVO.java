package cn.ideamake.bgyjssq.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: Walt
 * @Data: 2019-07-05 16:47
 * @Version: 1.0
 */

@Data
public class VisitorTrendVO {
    /**
     * 事件时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime eventTime;

    /**
     * 事件内容
     */
    private String eventContent;

    /**
     * 停留时间
     */
    private Integer stayTime;

    /**
     * 销售名称
     */
    private String sellerName;

    /**
     * 项目名称
     */
    private String projectName;

}
