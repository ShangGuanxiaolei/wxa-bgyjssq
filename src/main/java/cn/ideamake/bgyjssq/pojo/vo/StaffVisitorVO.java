package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: Walt
 * @Data: 2019-06-19 14:26
 * @Version: 1.0
 */
@Data
public class StaffVisitorVO {

    /**
     * 头像url
     */
    @OssPrefixAutoFill
    private String avatar;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 用户id
     */
    private String userid;

    /**
     * 姓名
     */
    private String name;

    /**
     * 性别
     */
    private Integer gender;

    /**
     * 客户来源
     */
    private Integer origin;

    /**
     * 访问次数
     */
    private Integer visitCount;

    /**
     * 最近访问日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime visitDatetime;

    /**
     * 最近访问记录： 访问项目、名片等记录
     */
    private String visitRecord;

    /**
     * 推荐人数
     */
    private Integer recomNum;

    /**
     * 关注项目数量
     */
    private Integer followProjNum;

    /**
     * 身份： 项目业主、一般访客、意向客户等
     */
    @JsonProperty("identity")
    private Integer selfLabel;

    /**
     * 客户是否转到其他员工名下
     */
    private Boolean isTransfer;
}
