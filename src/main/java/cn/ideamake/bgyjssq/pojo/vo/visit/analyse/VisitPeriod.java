package cn.ideamake.bgyjssq.pojo.vo.visit.analyse;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-07-09 10:34
 * @Version: 1.0
 */
@Data
public class VisitPeriod {

    /**
     * 时间段 0-0~2  1-2~4 ....
     */
    private Integer period;

    /**
     * 访问次数
     */
    private Integer visitCount;
}
