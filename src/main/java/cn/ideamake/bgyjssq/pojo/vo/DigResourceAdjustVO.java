package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-08-28 16:14
 * @Version: 1.0
 */
@Data
public class DigResourceAdjustVO {

    private String visitorId;
    private String visitorName;
    private String visitorPhone;
    private String attentionProjectNames;
    private String brewTime;
//    private
}
