package cn.ideamake.bgyjssq.pojo.vo.notify;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-08-08 14:26
 * @Version: 1.0
 */
@Data
public class StaffNotificationVO {
    /**
     * 搜索返回的通知组
     */
    private IPage<StaffNotificationGroupVO> groups;

    /**
     * 搜索返回的资源
     */
    private IPage<StaffNotificationResourceVO> resources;
}
