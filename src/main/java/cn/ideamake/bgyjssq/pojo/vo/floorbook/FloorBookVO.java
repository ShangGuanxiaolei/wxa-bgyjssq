package cn.ideamake.bgyjssq.pojo.vo.floorbook;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author gxl
 * @date 2019/09/11
 * @description 楼书数据查询返回对象
 */
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Data
public class FloorBookVO {

    /**
     * 楼书
     */
    private String floorBook;

    /**
     * 720楼书
     */
    private List<FloorBook720VO> floorBook720List;

}
