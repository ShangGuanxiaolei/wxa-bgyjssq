package cn.ideamake.bgyjssq.pojo.vo.visit.analyse;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-07-15 17:46
 * @Version: 1.0
 */
@Data
public class OriginCountCell {
    /**
     * 来源标志
     */
    private String originLabel;

    /**
     * 统计数量
     */
    private Integer countNum;
}
