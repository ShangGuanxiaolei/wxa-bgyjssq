package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/07/25
 * @description 楼盘分组列表展示项
 */
@Data
public class ProjectGroupListVO implements Serializable {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 分组名
     */
    private String name;

    /**
     * 使用分组的个数
     */
    private Integer count;
}
