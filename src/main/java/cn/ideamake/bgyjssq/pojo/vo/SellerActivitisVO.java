package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author gxl
 */
@Data
public class SellerActivitisVO {

    /**
     * 活动Id
     */
    private Integer activityId;
    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 项目id
     */
    private String projectId;

    /**
     * 项目码
     */
    private String weChatQrcode;

    /**
     * 活动名称
     */
    private String activityName;

    /**
     * 项目特点标签
     */
    private String tags;

    /**
     * 项目地址
     */
    private String address;

    /**
     * 访问次数
     */
    private Integer visitCounts;

    /**
     * 开始时间
     */
    private LocalDateTime startAt;

    /**
     * 结束时间
     */
    private LocalDateTime stopAt;

    /**
     * 活动状态
     */
    private Integer status;
}
