package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/07/29
 * @description 员工业绩管理
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class StaffKpiVO extends StaffTopTenVO implements Serializable {

    /**
     * bip账号
     */
    private String bip;

    /**
     * 部门名称
     */
    private String projectName;

    /**
     * 员工姓名
     */
    private String staffName;

    /**
     * 项目组
     */
    private String groupName;

    /**
     * 累计留电
     */
    private Integer phoneCount;

    /**
     * 累计来访
     */
    private Integer visitNum;

    /**
     * 推广总人数
     */
    private Integer recommendNum;

    /**
     * 推广总人次
     */
    private Integer recommendCount;

    /**
     * 微楼书
     */
    private Integer microBookNum;

    /**
     * 专属码
     */
    private Integer codeNum;

    /**
     * 微文章
     */
    private Integer microTextNum;

    /**
     * 微海报
     */
    private Integer microPosterNum;

    /**
     * 小程序
     */
    private Integer applet;




}
