package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.Data;

/**
 * @author gxl
 */
@Data
public class PreSignInProjectVO {

    /**
     * 活动Id
     */
    private Integer activityId;

    /**
     * 项目Id
     */
    private Integer projectId;

    /**
     * 项目名
     */
    private String projectName;

    /**
     * 活动图片
     */
    @OssPrefixAutoFill
    private String image;

    /**
     * 活动名
     */
    private String activityName;

    /**
     * 签到成功是否播放音乐
     */
    private Integer musicAfterSignIn;

    /**
     * 是否授权电话
     */
    private Integer authPhone;

    /**
     * 是否授权地址
     */
    private Integer authAddress;

    /**
     * 强制要求指定区域签到
     */
    private Integer designatedArea;

    /**
     * 是否显示客户当天签到次数
     */
    private Integer showDaySignInNum;

    /**
     * 是否显示客户累计签到次数
     */
    private Integer showAllSignInNum;

    /**
     * 是否显示客户累计到访次数
     */
    private Integer showVisitNum;

}
