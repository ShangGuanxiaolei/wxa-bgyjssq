package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.enums.VisitorOrigin;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author imyzt
 * @date 2019/07/08
 * @description 客户列表(PC后台)
 */
@Data
public class CustomerListVO implements Serializable {

    private String id;

    private String projectId;

    private String sellerId;

    @JsonProperty("phone")
    private String visitorPhone;

    @JsonProperty("name")
    private String visitorName;

    /**
     * 客户类别
     * 一般访客,项目业主...
     */
    @JsonProperty("type")
    private String selfLabel;

    @JsonProperty("focusProjects")
    private String projectTitle;

    /**
     * 拓客渠道
     * 朋友圈,项目码...
     */
    @JsonProperty("sourceChannel")
    private VisitorOrigin sourceLabel;

    /**
     * 当前跟办
     */
    @JsonProperty("currentHandle")
    private String staffName;

    /**
     * 转手跟办次数
     */
    private Integer transfersNumber;

    /**
     * 初始跟办
     */
    @JsonProperty("firstHandle")
    private String originFollowUp;

    /**
     * 首次访问小程序时间（注意：非到访营销中心时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime firstVisitTime;

    /**
     * 最近访问小程序时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastVisitTime;

    /**
     * 当前跟办bip
     */
    private String staffNameBip;

    /**
     * 初始跟办bip
     */
    private String originFollowUpBip;

    /**
     * 拓客类别（员工拓展/客户转介）
     */
    private Integer sourceType;

    /**
     * 客户属性（新老客户）
     */
    private Integer customerAttribute;

    /**
     * 备注
     */
    private String remark;
}
