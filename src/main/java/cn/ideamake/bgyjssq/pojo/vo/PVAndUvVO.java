package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

/**
 * @author gxl
 * @version 1.0
 * @description TODO
 * @date 2019-08-02 12:15
 */
@Data
public class PVAndUvVO {

    private Long pageViews;

    private Long uniqueVisitors;
}
