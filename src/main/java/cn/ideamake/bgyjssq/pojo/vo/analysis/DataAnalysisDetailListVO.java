package cn.ideamake.bgyjssq.pojo.vo.analysis;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

/**
 * 文章详情列表
 * @author gxl
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DataAnalysisDetailListVO implements Serializable, Comparable<DataAnalysisDetailListVO> {

    /**
     * id
     */
    private String id;

    /**
     * 名称
     */
    private String name;

    /**
     * 员工头像地址
     */
    @OssPrefixAutoFill
    private String avatar;

    /**
     * 数量
     */
    private Long count;

    /**
     * 项目名称
     */
    private String projectName;

    public DataAnalysisDetailListVO(String id, String name, Long count, String projectName) {
        this.id = id;
        this.name = name;
        this.count = count;
        this.projectName = projectName;
    }

    @Override
    public int compareTo(@NotNull DataAnalysisDetailListVO o) {
        return this.count.compareTo(o.getCount());
    }
}
