package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author imyzt
 * @date 2019/07/31
 * @description 资源表PC后台展示对象
 */
@Data
public class ResourceListVO implements Serializable {

    private Integer id;

    @OssPrefixAutoFill
    private String cover;

    private String title;

    /**
     * 资源类别
     * @see cn.ideamake.bgyjssq.common.enums.ResourceCategory
     */
    private Integer category;

    private Integer projectId;

    private String projectTitle;

    /**
     * 组id(详情对应下拉框使用)
     */
    private Integer groupId;

    /**
     * 组名称(列表使用)
     */
    private String groupName;

    private Integer status;

    private Integer top;

    @JsonFormat( pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime publishTime;

    /**
     * 推送人群类别
     */
    private String visitorType;

    /**
     * 浏览文章次数
     */
    private Long pageView;

    /**
     * 浏览文章人数
     */
    private Long uniqueVisitor;

    /**
     * 新增用户人数
     */
    private Long newPeopleCount;

    /**
     * 新增留电用户人数
     */
    private Long newRemainPhoneCount;
}