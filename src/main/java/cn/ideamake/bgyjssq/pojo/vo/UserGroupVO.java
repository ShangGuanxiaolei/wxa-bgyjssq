package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

import java.util.List;

/**
 * @author gxl
 * @version 1.0
 * @description 员工分组VO
 * @date 2019-09-02 17:15
 */
@Data
public class UserGroupVO {

    /**
     * 组id
     */
    private Integer id;

    /**
     * 组名
     */
    private String groupName;

    /**
     * 员工列表
     */
    private List<UserListVO> userList;
}
