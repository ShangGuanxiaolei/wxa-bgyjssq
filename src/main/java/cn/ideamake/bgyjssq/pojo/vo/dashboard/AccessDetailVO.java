package cn.ideamake.bgyjssq.pojo.vo.dashboard;

import lombok.Data;

/**
 * @author gxl
 */
@Data
public class AccessDetailVO {

    /**
     * 来源
     */
    private Integer item;

    /**
     * 访问数
     */
    private Integer count;

    /**
     * 占比
     */
    private Double percent;
}
