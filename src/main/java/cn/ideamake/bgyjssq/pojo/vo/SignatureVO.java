package cn.ideamake.bgyjssq.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 微信签名类
 * @author gxl
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SignatureVO {

    /**
     * 公众号appId
     */
    private String appId;

    /**
     * 时间戳
     */
    private String timestamp;

    /**
     * 随机字符串
     */
    private String nonceStr;

    /**
     * 签名串
     */
    private String signature;
}
