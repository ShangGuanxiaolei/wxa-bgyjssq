package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.response.Rest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author imyzt
 * @date 2019/07/01
 * @description 分页对象
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class SimplePageResponse<T extends List<T>> extends Rest implements Serializable {

    private Long total;
    private Long current;

    public <T> SimplePageResponse(IPage<T> page, Rest rest) {
        this.current = page.getCurrent();
        this.total = page.getTotal();
        this.setCode(rest.getCode());
        this.setMsg(rest.getMsg());
        this.setData(page.getRecords());
    }


}
