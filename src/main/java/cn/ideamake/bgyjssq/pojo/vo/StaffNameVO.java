package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

@Data
public class StaffNameVO {
    /**
     * user_uuid
     */
    private String staffUuId;

    /**
     * 员工名
     */
    private String staffName;
}
