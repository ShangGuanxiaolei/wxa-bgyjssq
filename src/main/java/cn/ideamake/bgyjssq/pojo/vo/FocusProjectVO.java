package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

/**
 * @author gxl
 * @version 1.0
 * @description 关注项目VO
 * @date 2019-08-27 14:51
 */
@Data
public class FocusProjectVO {

    /**
     * projectName : 项目名称
     * customerType : 客户类别
     * customerFocusTime : 客户关注时间
     * firstHandle : 初始跟办
     * currentHandle : 当前跟办
     * sourceType : 拓客类别
     * sourceChannel : 拓客渠道
     * totalVisitCounts : 累计访问次数
     * totalVisitTimes : 累计访问时长
     */
    private String projectName;
    private String customerType;
    private String customerFocusTime;
    private String firstHandle;
    private String currentHandle;
    private String sourceType;
    private String sourceChannel;
    private String totalVisitCounts;
    private String totalVisitTimes;
}
