package cn.ideamake.bgyjssq.pojo.vo.floorbook;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author gxl
 * @date 2019/09/06
 * @description 720楼书视图
 */
@Data
@Accessors(chain = true)
public class FloorBook720VO implements Serializable {

    /**
     * id
     */
    private Integer id;

    /**
     * 标题
     */
    private String title;

    /**
     * 楼书图片
     */
    private String url;

    /**
     * 资源背景图
     */
    private String image;

}
