package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-12 10:00
 * @Version: 1.0
 * 后台生成项目码返回到前台的响应内容
 */
@Data
public class ProjectQRCodeVO {

    /**
     * 名称
     */
    private String name;

    /**
     * 图片地址
     */
    private String img;

    /**
     * Base64的二维码字符串
     */
    private String qrcode;
}
