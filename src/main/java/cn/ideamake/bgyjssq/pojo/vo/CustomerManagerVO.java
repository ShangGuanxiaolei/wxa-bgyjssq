package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.pojo.dto.CustomerManagerBindingDTO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author imyzt
 * @date 2019/07/01
 * @description 客户列表返回对象
 */
@Data
@Accessors(chain = true)
public class CustomerManagerVO implements Serializable {

    private String uuid;
    /**
     * 客户昵称
     */
    private String nickName;
    private String phone;
    /**
     * 拓客类别
     */
    private String visitorSource;
    private List<CustomerManagerBindingDTO> bindingList;
}
