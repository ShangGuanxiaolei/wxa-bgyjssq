package cn.ideamake.bgyjssq.pojo.vo.notify;

import lombok.Data;

/**
 * @Author: gxl
 * @Data: 2019-09-20 13:58
 * @Version: 1.0
 * @description 访客通知组对象
 */
@Data
public class VisitorNotificationGroupVO {

    /**
     * 文章组id
     */
    private Integer groupId;

    /**
     * 文章组名称
     */
    private String groupName;

    /**
     * 文章组id
     */
    private Integer projectId;

    /**
     * 文章组名称
     */
    private String projectName;

    /**
     * 资源数量
     */
    private Integer resourceNums;

}
