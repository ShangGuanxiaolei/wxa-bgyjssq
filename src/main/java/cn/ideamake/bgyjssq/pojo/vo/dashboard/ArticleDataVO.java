package cn.ideamake.bgyjssq.pojo.vo.dashboard;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author imyzt
 * @date 2019/07/17
 * @description pc后台-微文章数据
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class ArticleDataVO {

    /**
     * 次
     */
    private Integer value = 0;

    /**
     * 日期
     */
    private String date;

    private String type;

    public ArticleDataVO(String date) {
        this.date = date;
    }
}
