package cn.ideamake.bgyjssq.pojo.vo.visit.analyse;

import lombok.Data;

import java.util.List;

/**
 * @Author: Walt
 * @Data: 2019-07-11 17:37
 * @Version: 1.0
 * 员工查看访客数据分析 客户访问趋势按日期统计的BO对象
 */
@Data
public class VisitTrend {
    /**
     * 访问次数
     */
    private List<VisitTrendCell> visitorNum;

    /**
     * 访问人数
     */
    private List<VisitTrendCell> numberOfPeople;
}
