package cn.ideamake.bgyjssq.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author imyzt
 * @date 2019/07/02
 * @description 文章列表查询返回对象描述信息
 */
@Data
@Accessors(chain = true)
public class ArticleDetailVO implements Serializable {

    private Integer id;

    @JsonProperty("type")
    private Integer resourceType;

    @JsonProperty("title")
    private String resourceTitle;

    private Integer projectId;

    @JsonProperty("cover")
    private String resourceBg;

    @JsonFormat( pattern = "yyyy-MM-dd")
    private LocalDate publishTime;

    /**
     * 发布状态
     */
    private Integer status;

    private Integer groupId;

    @JsonProperty("top")
    private Integer topStatus;

    @JsonProperty("content")
    private String resourceContent;

}
