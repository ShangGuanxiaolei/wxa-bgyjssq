package cn.ideamake.bgyjssq.pojo.vo.notify;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: gxl
 * @Data: 2019-09-20 14:14
 * @Version: 1.0
 * @description 访客查看文章列表
 */
@Data
public class VisitorNotificationResourceVO {

    private Integer id;

    /**
     * 通知等级，2项目通知，1系统通知
     */
    private Integer level;
    /**
     * 通知标题
     */
    private String resourceName;
    /**
     * 通知图像地址
     */
    @OssPrefixAutoFill
    private String img;

    /**
     * h5地址
     */
    private String url;

    /**
     * 文章组别
     */
    private String groupName;

    /**
     * 部门组别
     */
    private String projectName;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 发布时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime releaseTime;
}
