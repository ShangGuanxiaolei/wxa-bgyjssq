package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

@Data
public class PreSignInVO {
    /**
     * 项目信息
     */
    private PreSignInProjectVO preSignInProjectVO;

    /**
     * 销售信息
     */
    private SignInStaffVO signInStaffVO;
}
