package cn.ideamake.bgyjssq.pojo.vo.permission;

import lombok.Data;

import java.util.List;

/**
 * @author gxl
 * @version 1.0
 * @description TODO
 * @date 2019-09-12 15:28
 */
@Data
public class NewMenuListVO {
    /**
     * path : /dataOverview
     * name : dataOverview
     * redirect : /dataOverview/dashboard
     * component : () => import('@/views/dataOverview/Index')
     * meta : {"title":"数据概览","requireAuth":false,"icon":"pie-chart"}
     * children : [{"path":"/dataOverview","name":"dataOverview","redirect":"/dataOverview/dashboard","component":"() => import('@/views/dataOverview/Index')","meta":{"title":"数据概览","requireAuth":false}}]
     */
    private Integer id;
    private String path;
    private String name;
    private String redirect;
    private String component;
    private MetaBean meta;
    private List<NewMenuListVO> children;

    @Data
    public static class MetaBean {
        /**
         * title : 数据概览
         * requireAuth : false
         * icon : pie-chart
         */
        private String title;
        private boolean requireAuth;
        private String icon;
    }

}
