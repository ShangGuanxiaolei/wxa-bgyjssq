package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

/**
 * @author gxl
 * @version 1.0
 * @description 推荐分析VO
 * @date 2019-08-01 16:19
 */
@Data
public class RecommendAnalysisVO {

    /**
     * 推荐总人数
     */
    private Integer totalPeople;

    /**
     * 留电人数
     */
    private Integer remainPhoneTotalPeople;

    /**
     * 来访人数
     */
    private Integer accessTotalPeople;

    /**
     * 推荐总人次
     */
    private Integer totalNumber;

    /**
     * 推荐微楼书人次
     */
    private Integer totalMicroBook;

    /**
     * 推荐文章人次
     */
    private Integer totalArticle;

    /**
     * 推荐小程序人次
     */
    private Integer totalMiniProgram;
}
