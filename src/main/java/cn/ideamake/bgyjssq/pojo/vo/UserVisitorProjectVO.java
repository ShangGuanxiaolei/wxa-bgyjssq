package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-20 14:12
 * @Version: 1.0
 */
@Data
public class UserVisitorProjectVO {

    /**
     * 名称
     */
    private String name;

    /**
     * 头像url
     */
    @OssPrefixAutoFill
    private String avatar;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 销售id
     */
    private String userid;

}
