package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-08-15 15:58
 * @Version: 1.0
 */
@Data
public class UserBaseVO {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * bip账号
     */
    private String bip;
}
