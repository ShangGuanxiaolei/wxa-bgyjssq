package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-05-17 10:37
 * @Version: 1.0
 */
@Data
public class ChangeBindingVO {

    /**
     * 员工微信名称
     */
    private String name;

    /**
     * 员工微信二维码URL
     */
    @OssPrefixAutoFill
    private String avatar;

    /**
     * 员工手机号
     */
    private String phone;

    /**
     * 用户id
     */
    private String userid;
}
