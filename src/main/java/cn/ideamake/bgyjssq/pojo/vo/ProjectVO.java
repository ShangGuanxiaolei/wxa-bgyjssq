package cn.ideamake.bgyjssq.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author gxl
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProjectVO {

    /**
     * 项目ID
     */
    private Integer projectId;

    /**
     * 项目名
     */
    private String projectName;
}
