package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

/**
 * @author imyzt
 * @date 2019/07/29
 * @description 员工推广业绩TOP10视图
 */
@Data
public class StaffTopTenVO {

    private String uuid;

    private String staffName;

    private String groupName;

    /**
     * 累计留电
     */
    private Integer phoneCount;

    /**
     * 推广总人次
     */
    private Integer recommendNum;

    /**
     * 微楼书
     */
    private Integer microBookNum;

    /**
     * 专属码
     */
    private Integer codeNum;

    /**
     * 微图文
     */
    private Integer microTextNum;

    /**
     * 微海报
     */
    private Integer microPosterNum;

}
