package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-05-16 15:47
 * @Version: 1.0
 * 获取员工详情页返回信息
 */
@Data
public class UserDetailVO {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 员工微信头像地址
     */
    @OssPrefixAutoFill
    private String avatarUrl;

    /**
     * 用户显示名称，默认微信名
     */
    private String name;

    /**
     * 用户显手机号
     */
    private String phone;

    /**
     * 员工所在公司
     */
    private String company;

    /**
     * 员工岗位
     */
    private String position;

    /**
     * 员工居住地址
     */
    private String address;

    /**
     * 员工邮箱
     */
    private String email;

    /**
     * 员工微信账号
     */
    private String weChatId;

    /**
     * 员工微信二维码URL
     */
    @OssPrefixAutoFill
    private String weChatQrcode;

    /**
     * 名片码URL
     */
    private String businessCardCode;

    /**
     * 是否为员工
     */
    private Integer role;

    /**
     * 员工最近访问项目id
     */
    private Integer lastProjectId;

    /**
     * 是否是高等级角色0-不是，1-是,默认不是
     */
    private Integer higherRole = 0;

    /**
     * 隐藏字段
     */
    private String[] hideColumn;
}
