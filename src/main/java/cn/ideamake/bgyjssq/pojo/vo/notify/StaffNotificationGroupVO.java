package cn.ideamake.bgyjssq.pojo.vo.notify;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-18 14:56
 * @Version: 1.0
 * 员工通知查询返回信息
 */
@Data
public class StaffNotificationGroupVO {

    /**
     * 文章组id
     */
    private Integer groupId;

    /**
     * 文章组名称
     */
    private String groupName;

    /**
     * 文章组id
     */
    private Integer projectId;

    /**
     * 文章组名称
     */
    private String projectName;

    /**
     * 浏览量pv
     */
    private Long pageViews;

    /**
     * 阅读量vu
     */
    private Long uniqueVisitors;

    /**
     * 资源数量
     */
    private Integer resourceNums;

}
