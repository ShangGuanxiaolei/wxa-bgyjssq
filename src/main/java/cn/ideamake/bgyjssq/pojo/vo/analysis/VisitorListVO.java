package cn.ideamake.bgyjssq.pojo.vo.analysis;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 文章详情列表
 * @author gxl
 */
@Data
public class VisitorListVO {

    /**
     * 用户id
     */
    private String visitorUuid;

    /**
     * 头像地址
     */
    @OssPrefixAutoFill
    private String avatar;

    /**
     * 名称
     */
    private String name;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 访问时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime viewsDatetime;

    /**
     * 停留时长
     */
    private String stayDuration;
}
