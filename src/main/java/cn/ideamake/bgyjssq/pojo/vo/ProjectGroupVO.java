package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

@Data
public class ProjectGroupVO {
    /**
     * 项目组ID
     */
    private Integer projectGroupId;

    /**
     * 项目组名
     */
    private String projectGroupName;
}
