package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.Data;

@Data
public class SignInStaffVO {
    /**
     * 销售uuid
     */
   private String staffUuid;

    /**
     * 销售电话
     */
    private String phone;

    /**
     * 销售name
     */
    private String name;

    /**
     * 员工职务
     */
    private String position;

    /**
     * 销售头像
     */
    @OssPrefixAutoFill
    private String icon;
}
