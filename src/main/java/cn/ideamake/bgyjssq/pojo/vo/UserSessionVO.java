package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author gxl
 */
@EqualsAndHashCode(callSuper = true)
@ToString
@Data
public class UserSessionVO extends BaseVO implements Serializable {

    /**
     * 主键ID
     */
    private String userId;

    /**
     * 用户名openid
     */
    private String openId;

    /**
     * 用户unionid
     */
    private String unionId;

    /**
     * 用户nickName
     */
    private String nickName;

    /**
     * 用户类型，0代表访客，1代表
     */
    private Integer userType;

    /**
     * 头像地址
     */
    @OssPrefixAutoFill
    private String avatarUrl;

    /**
     * 用户session_key,暂时存储在此处
     */
    private String sessionKey;

    /**
     * 角色id，后台接口使用
     */
    private Integer roleId;

    /**
     * 系统用户id，后台接口使用
     */
    private Integer adminId;
}
