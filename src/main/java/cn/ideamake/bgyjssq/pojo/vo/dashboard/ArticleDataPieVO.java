package cn.ideamake.bgyjssq.pojo.vo.dashboard;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/07/18
 * @description dashboard-文章数据-饼图
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ArticleDataPieVO implements Serializable {

    private String item;

    /**
     * 数量
     */
    private Integer count = 0;

    /**
     *  百分比
     */
    private Double percent = 0.00D;

    public ArticleDataPieVO(String item) {
        this.item = item;
    }

    public ArticleDataPieVO(Integer count, Double percent) {
        this.count = count;
        this.percent = percent;
    }
}
