package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 文章详情列表
 * @author gxl
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserRoleListVO implements Serializable {

    /**
     * id
     */
    private String id;

    /**
     * 名称
     */
    private String name;

    /**
     * 员工头像地址
     */
    @OssPrefixAutoFill
    private String avatar;

    /**
     * 浏览量pv
     */
    private Long pageViews;

    /**
     * 阅读量vu
     */
    private Long uniqueVisitors;

    public UserRoleListVO(String id, String name, Long pageViews, Long uniqueVisitors) {
        this.id = id;
        this.name = name;
        this.pageViews = pageViews;
        this.uniqueVisitors = uniqueVisitors;
    }
}
