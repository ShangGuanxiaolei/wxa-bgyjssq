package cn.ideamake.bgyjssq.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author ideamake
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class QrCodeVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 活动ID
     */
    private Long id;

    /**
     * 项目组名
     */
    private String projectGroupName;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 活动组名
     */
    private String activityGroupName;

    /**
     * 活动名称
     */
    private String activityName;

    /**
     * 开启时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime stopTime;

    /**
     * 使用状态1启用，0禁用
     */
    private Integer status;

    /**
     * 销售员
     */
    private String salePersonName;

    /**
     * 活动签到客户总数
     */
    private Long visitorCount;

    /**
     * 签到码地址
     */
    private String qrCodeUrl;

}
