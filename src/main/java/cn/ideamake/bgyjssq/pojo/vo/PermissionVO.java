package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 权限资源封装对象
 *
 * @author gxl
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PermissionVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 资源id
     */
    private Integer id;

    /**
     * 资源名称
     */
    private String title;

    /**
     * 父类id
     */
    private Integer parentId;

    /**
     * 权限资源列表
     */
    private List<PermissionVO> children;

}
