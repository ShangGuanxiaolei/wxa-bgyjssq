package cn.ideamake.bgyjssq.pojo.vo.permission;

import lombok.Data;

import java.util.List;

/**
 * @author gxl
 * @version 1.0
 * @description TODO
 * @date 2019-07-29 14:59
 */
@Data
public class MenuListAllVO {

    private Integer id;

    private String name;

    private String title;

    private String path;

    private Integer level;

    private String icon;

    private Integer parentId;

    private List<MenuListAllVO> children;

}
