package cn.ideamake.bgyjssq.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 文章详情列表
 * @author gxl
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserInfoVO implements Serializable {

    /**
     * id
     */
    private String userUuid;

    /**
     * 名称
     */
    private String name;

    /**
     * 用户bip
     */
    private String bip;

}
