package cn.ideamake.bgyjssq.pojo.vo.analysis;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 文章详情列表
 * @author gxl
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DataAnalysisDetailVO {

    /**
     * 组数据
     */
    private List<DataAnalysisDetailListVO> dataAnalysisDetailList;

    /**
     * 客户数据
     */
    private List<VisitorListVO> staffVisitorList;

    /**
     * 高级角色判断标识（1是，0否）
     */
    private Integer hasGroup;

    /**
     * 高等级销售数据
     */
    private DataAnalysisDetailListVO staffAnalysisVO;

}
