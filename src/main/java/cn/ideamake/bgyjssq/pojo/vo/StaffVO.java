package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/07/15
 * @description 员工列表-PC后台
 */
@Data
public class StaffVO implements Serializable {

    /**
     * 用户名称
     */
    private String name;

    /**
     * bip账号
     */
    private String bip;

    /**
     * 微信账号
     */
    private String wechatId;

    /**
     * 用户手机号
     */
    private String phone;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 分组id
     */
    private Integer groupId;

    /**
     * 职务名称
     */
    private String positionName;

    /**
     * 职业状态
     */
    private Integer isActive;

}
