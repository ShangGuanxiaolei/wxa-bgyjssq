package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author imyzt
 * @date 2019/08/01
 * @description 资源详情视图对象
 */
@Data
@Accessors(chain = true)
public class ResourceDetailVO implements Serializable {

    private Integer id;

    /**
     * 资源类别
     * @see cn.ideamake.bgyjssq.common.enums.ResourceCategory
     */
    private Integer category;

    private String title;

    private Integer projectId;

    /**
     * 资源列表背景图
     */
    @OssPrefixAutoFill
    private String cover;

    @JsonFormat( pattern = "yyyy-MM-dd")
    private LocalDate publishTime;

    private Integer status;

    private Integer top;

    private Integer groupId;

    /**
     * 对应文章, 首页, 楼书的正文
     */
    private String content;

    /**
     * 对应海报, 海报链接
     */
    private String url;

    /**
     * 推送的用户类型
     */
    private String visitorType;

}
