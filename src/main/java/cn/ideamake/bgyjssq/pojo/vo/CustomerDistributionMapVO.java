package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/07/22
 * @description PC后台-客户分布地图
 */
@Data
@Accessors(chain = true)
public class CustomerDistributionMapVO implements Serializable {

    private Integer id;

    private String name;

    /**
     * 纬度(为了不丢失小数点后面的0)
     */
    private String latitude;

    /**
     * 经度
     */
    private String longitude;
}
