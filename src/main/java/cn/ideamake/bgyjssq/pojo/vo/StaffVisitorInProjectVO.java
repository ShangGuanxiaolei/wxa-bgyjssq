package cn.ideamake.bgyjssq.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author gxl
 */
@Data
public class StaffVisitorInProjectVO {

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 项目名
     */
    private String projectName;

    /**
     * 活动名
     */
    private String activityName;

    /**
     * 客户id
     */
    private String visitorId;

    /**
     * 客户名称
     */
    private String visitorName;

    /**
     * 电话号码
     */
    private String phoneNum;

    /**
     * 销售id
     */
    private String sellerId;

    /**
     * 跟办员工
     */
    private String staff;

    /**
     * 签到状态
     */
    private Boolean signInStatus;

    /**
     * 首次签到日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime firstSignInDate;

    /**
     * 最近签到日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastSignInDate;

    /**
     * 累计签到天数
     */
    private Integer totalDays;

    /**
     * 累计签到次数
     */
    private Integer totalCount;

}
