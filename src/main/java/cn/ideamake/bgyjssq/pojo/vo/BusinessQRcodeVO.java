package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-12 15:12
 * @Version: 1.0
 * 用户获取名片二维码返回结果
 */
@Data
public class BusinessQRcodeVO {
    /**
     * 用户名称
     */
    private String name;
    /**
     * 用户头像地址
     */
    @OssPrefixAutoFill
    private String avatar;
    /**
     * 用户所在公司
     */
    private String company;
    /**
     * 用户手机号
     */
    private String phone;
    /**
     * 用户岗位
     */
    private String position;
    /**
     * 用户二维码Base64字符串
     */
    private String qrcode;
}
