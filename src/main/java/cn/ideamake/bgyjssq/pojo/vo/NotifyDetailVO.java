package cn.ideamake.bgyjssq.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 文章详情列表
 * @author gxl
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class NotifyDetailVO {

    /**
     * 组列表
     */
    private List<UserRoleListVO> userGroupList;

    /**
     * 客户列表
     */
    private ArticleDetailVisitorListVO articleDetail;

    /**
     * 是否管理组（1是，0否）
     */
    private Integer hasGroup;

    /**
     * 高等级销售信息
     */
    UserRoleListVO userRoleListVO;
}
