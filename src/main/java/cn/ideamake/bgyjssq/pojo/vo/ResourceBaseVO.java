package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-08-10 10:26
 * @Version: 1.0
 */
@Data
public class ResourceBaseVO {
    /**
     * 资源名称
     */
    private String title;

    /**
     * 资源id
     */
    private Integer resourceId;

    /**
     * 资源名称和组名分隔
     */
    private String groupName;
}
