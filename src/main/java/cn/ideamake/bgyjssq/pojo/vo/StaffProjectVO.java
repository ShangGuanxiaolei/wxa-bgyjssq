package cn.ideamake.bgyjssq.pojo.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: Walt
 * @Data: 2019-06-18 14:58
 * @Version: 1.0
 * 员工管理项目
 */
@Data
public class StaffProjectVO {

    /**
     * 项目背景图
     */
    private String projectImg;

    /**
     * 项目名称
     */

    private String projectName;

    /**
     * 项目id
     */
    private String projectId;

    /**
     * 是否在我的推荐中隐藏： 0 隐藏， 1 展示
     */
    private String isRecommend;

    /**
     * 项目码
     */
    private String weChatQrcode;

    /**
     * 项目特点标签
     */
    private String tags;

    /**
     * 项目地址
     */
    private String address;

    /**
     * 访问次数
     */
    private String visitCounts;

    /**
     * 项目logo
     */
    private String projectLogo;

    /**
     * 销售绑定项目的时间， 该列表按这个字段倒序排列
     */
    @JSONField(format="yyyy-MM-dd HH:mm")
    private LocalDateTime bindDate;
}
