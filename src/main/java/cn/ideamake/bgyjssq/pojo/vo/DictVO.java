package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分组VO
 *
 * @author gxl
 */
@Data
public class DictVO implements Serializable {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 分组名
     */
    private String name;

    /**
     * 使用分组的个数
     */
    private Integer count;

    /**
     * 项目Id
     */
    private String projectId;

    /**
     * 项目名
     */
    private String projectName;

    /**
     * 项目组名
     */
    private String projectGroupName;

    /**
     * 资源id
     */
    private String resourceIds;

    /**
     * 管理用户列表
     */
    private List<String> userList;
}
