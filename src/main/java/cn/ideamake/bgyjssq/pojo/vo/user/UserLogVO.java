package cn.ideamake.bgyjssq.pojo.vo.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: gxl
 * @Data: 2019-09-11 15:27
 * @Version: 1.0
 */
@Data
public class UserLogVO {

    /**
     * 日志内容
     */
    private String info;

    /**
     * 日志时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime datetime;

    /**
     * 是否标红
     */
    private Integer isShowRed;
}
