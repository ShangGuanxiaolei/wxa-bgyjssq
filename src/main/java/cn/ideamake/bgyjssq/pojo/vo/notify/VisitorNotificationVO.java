package cn.ideamake.bgyjssq.pojo.vo.notify;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: gxl
 * @Data: 2019-09-20 14:04
 * @Version: 1.0
 * @description 访客查看通知
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class VisitorNotificationVO {

    /**
     * 分组资源列表
     */
    private IPage<VisitorNotificationGroupVO> groups;

    /**
     * 未分组资源列表
     */
    private IPage<VisitorNotificationResourceVO> resources;

}
