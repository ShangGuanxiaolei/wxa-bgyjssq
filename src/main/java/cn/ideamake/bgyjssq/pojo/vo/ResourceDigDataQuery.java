package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.pojo.query.DefaultQueryPage;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * @Author: Walt
 * @Data: 2019-08-16 17:13
 * @Version: 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ResourceDigDataQuery extends DefaultQueryPage {
    /**
     * 资源id
     */
    @NotNull(message = "资源id不能为空")
    private Integer resourceId;

    /**
     * 资源类型
     */
    @NotNull(message = "资源类型不能为空")
    private Integer resourceType;

    /**
     * 钻取的数据类型
     */
    @NotNull(message = "资源类型不能为空")
    private Integer digDataType;
}
