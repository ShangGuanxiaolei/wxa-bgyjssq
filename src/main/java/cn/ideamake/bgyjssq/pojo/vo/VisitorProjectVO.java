package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-17 16:56
 * @Version: 1.0
 */
@Data
public class VisitorProjectVO {
    /**
     * 项目背景图片
     */
    @OssPrefixAutoFill
    private String projectImg;
    /**
     * 项目logo的URL地址
     */
    @OssPrefixAutoFill
    private String projectLogo;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 项目id
     */
    private String projectId;
    /**
     * 项目标签
     */
    private String tags;
    /**
     * 项目地址
     */
    private String address;

    /**
     * 项目区域
     */
    private String area;

    private String groupName;

    private Integer groupId;

    /**
     * 楼书列表展示时,
     */
    private Integer homeId;

    /**
     * 楼书id
     */
    private Integer floorBookId;

    /**
     * 默认销售
     */
    private String defaultSeller;

    /**
     * 默认销售id
     */
    private String defaultSellerId;

    /**
     * 售楼处经度
     */
    private String longitude;

    /**
     * 售楼处纬度
     */
    private String latitude;

    /**
     * 半径
     */
    private Double radius;

    /**
     * 项目图片的URL地址
     */
    @OssPrefixAutoFill
    private String profilePoster;

}
