package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

@Data
public class PushedActivityVO {

    /**
     * 活动ID
     */
    private Integer activityId;

    /**
     * 项目ID
     */
    private Integer projectId;

    /**
     * 活动名
     */
    private String activityName;

    /**
     * 项目名
     */
    private String projectName;

    /**
     * qr二维码
     */
    private String qrCode;
}
