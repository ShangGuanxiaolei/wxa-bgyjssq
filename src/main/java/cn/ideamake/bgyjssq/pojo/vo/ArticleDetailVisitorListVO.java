package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.pojo.vo.analysis.VisitorListVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 文章详情列表
 * @author gxl
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ArticleDetailVisitorListVO {

    /**
     * 客户列表
     */
    private List<VisitorListVO> visitorList;

    /**
     * 浏览量pv
     */
    private Long pageViews;

    /**
     * 阅读量vu
     */
    private Long uniqueVisitors;
}
