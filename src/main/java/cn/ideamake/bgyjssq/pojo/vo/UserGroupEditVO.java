package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author: Walt
 * @Data: 2019-08-15 16:00
 * @Version: 1.0
 */
@Data
public class UserGroupEditVO {

    /**
     * 组内成员
     */
    private List<UserBaseVO> users;

    /**
     * 组的管理员信息
     */
    private List<UserBaseVO> managers;
}
