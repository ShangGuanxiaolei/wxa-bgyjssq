package cn.ideamake.bgyjssq.pojo.vo.permission;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 角色权限封装类
 *
 * @author ideamkae
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysRolePermissionVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色唯一记录
     */
    private Integer id;

    /**
     * 角色名称
     */
    @NotNull(message = "角色名称不能为空")
    private String name;

    /**
     * 权限索引
     */
    @NotNull(message = "角色权限不能为空")
    private List<Integer> permissionIds;

}
