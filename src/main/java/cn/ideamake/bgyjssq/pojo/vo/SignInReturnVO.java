package cn.ideamake.bgyjssq.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SignInReturnVO {
    /**
     * 签到状态
     */
    private Integer status;

    /**
     * 今日签到次数
     */
    private Integer countToday;

    /**
     * 签到总次数
     */
    private Integer totalCount;

    /**
     * 累计登录天数
     */
    private Integer totalDays;

    /**
     * 签到时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime signInTime;

    /**
     * 活动是否结束
     */
    private Integer isActivityEnd;

    /**
     * 活动是否名额已满
     */
    private Integer isActivityFull;

    /**
     * 用户是否今日已满
     */
    private Integer isTodayFull;
}
