package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import cn.ideamake.bgyjssq.pojo.bo.CustomerInfoBO;
import cn.ideamake.bgyjssq.pojo.dto.CustomerFollowProjectInfoDTO;
import lombok.Data;

/**
 * @author imyzt
 * @date 2019/07/09
 * @description 客户信息详情
 */
@Data
public class CustomerInfoDetailVO {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 头像地址
     */
    @OssPrefixAutoFill
    private String avatar;

    /**
     * 客户姓名
     */
    private String name;

    /**
     * 用户手机号
     */
    private String phone;

    private CustomerInfoBO customerInfo;

    private CustomerFollowProjectInfoDTO projectInfo;

    /**
     * 项目名（客户为业主时返回）
     */
    private String projectName;

}
