package cn.ideamake.bgyjssq.pojo.vo.notify;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: Walt
 * @Data: 2019-06-18 14:56
 * @Version: 1.0
 * 员工通知查询返回信息
 */
@Data
public class StaffNotificationResourceVO {

    private Integer id;

    /**
     * 文章或者海报缩略图
     */
    @OssPrefixAutoFill
    private String imgUrl;

    /**
     * 标题
     */
    private String title;

    /**
     * 发布时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime releaseTime;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 文章组名称
     */
    private String resourceGroupName;

    /**
     * 浏览量pv
     */
    private Long pageViews;

    /**
     * 阅读量vu
     */
    private Long uniqueVisitors;

    /**
     * h5地址,海报url地址
     */
    @OssPrefixAutoFill
    private String resourceUrl;

    /**
     * 资源名称
     */
    private String resourceName;

    /**
     * 项目名
     */
    private String projectName;

}
