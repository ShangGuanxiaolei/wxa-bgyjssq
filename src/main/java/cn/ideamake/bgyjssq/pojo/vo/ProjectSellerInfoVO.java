package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.pojo.bo.ProjectInfo;
import cn.ideamake.bgyjssq.pojo.bo.SellerInfo;
import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-25 17:08
 * @Version: 1.0
 */
@Data
public class ProjectSellerInfoVO {

    private SellerInfo sellerInfo;

    private ProjectInfo projectInfo;

}
