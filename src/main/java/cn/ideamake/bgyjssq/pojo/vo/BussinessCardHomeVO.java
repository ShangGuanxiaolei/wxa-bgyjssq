package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-17 15:24
 * @Version: 1.0
 * 用户首页名片获取返回信息
 */
@Data
public class BussinessCardHomeVO {

    /**
     * 头像url
     */
    @OssPrefixAutoFill
    private String avatarUrl;

    /**
     * 用户名， 默认为微信名称
     */
    private String name;

    /**
     * 背景图片
     */
    private String companyLogo;

    /**
     * 公司logo
     */
    private String coverPicture;

    /**
     * 电话号码
     */
    private String phone;

    /**
     * 公司职务
     */
    private String position;

    /**
     * 项目名
     */
    private String projectName;

    /**
     * 公司
     */
    private String company;

    /**
     * 地址
     */
    private String address;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 微信id
     */
    private String weChatId;

    /**
     * 微信二维码url
     */
    private String weChatQrcode;

    /**
     * 活跃度
     */
    private Integer popularityCounts;

    /**
     * 需要隐藏的属性名
     */
    private String[] hideColumn;

    /**
     * 销售手机号
     */
    private String sellerPhone;
}
