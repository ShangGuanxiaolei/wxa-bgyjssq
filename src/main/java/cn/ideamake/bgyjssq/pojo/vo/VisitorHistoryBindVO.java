package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: Walt
 * @Data: 2019-06-13 11:16
 * @Version: 1.0
 * 访客历史绑定返回信息结构
 */
@Data
public class VisitorHistoryBindVO {

    /**
     * 绑定员工头像地址
     */
    @OssPrefixAutoFill
    private String avatar;

    /**
     * 绑定员工姓名
     */
    private String name;

    /**
     * 绑定项目名称
     */
    private String projectName;

    /**
     * 绑定日志
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime bindDate;

    /**
     * 绑定员工id
     */
    private String sellerId;

    /**
     * 是否是历史绑定，如有销售切换，则之前的数据为历史数据，不可变更
     */
    private Integer historyLabel;
}
