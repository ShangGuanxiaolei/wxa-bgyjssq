package cn.ideamake.bgyjssq.pojo.vo.visit.analyse;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-07-09 11:17
 * @Version: 1.0
 * 访客访问数据总揽，用于小程序员工查看访客
 */
@Data
public class TotalStatistics {

    /**
     * 访问数量
     */
    private Integer numberOfVisits;

    /**
     * 访问人数
     */
    private Integer numberOfPeople;

    /**
     * 留电人数
     */
    private Integer peopleOfRemainPhone;

    /**
     * 到访人数
     */
    private Integer peopleOfAccess;

    /**
     * 分享次数
     */
    private Integer shareCount;

    /**
     * 分享人数
     */
    private Integer sharePeople;

    /**
     * 阅读次数
     */
    private Integer readCount;

    /**
     * 阅读人数
     */
    private Integer readPeople;

    /**
     * 员工拓展
     */
    private Integer sellerRecommend;

    /**
     * 客户推介
     */
    private Integer customerRecommend;


}
