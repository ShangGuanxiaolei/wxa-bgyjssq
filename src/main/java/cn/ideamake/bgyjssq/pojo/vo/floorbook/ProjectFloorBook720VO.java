package cn.ideamake.bgyjssq.pojo.vo.floorbook;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.Data;

/**
 * @Author: gxl
 * @Data: 2019-09-06 10:47
 * @Version: 1.0
 */
@Data
public class ProjectFloorBook720VO {

    /**
     * 720楼书id
     */
    private Integer id;

    /**
     * 项目背景图片
     */
    @OssPrefixAutoFill
    private String projectImg;
    /**
     * 项目logo的URL地址
     */
    @OssPrefixAutoFill
    private String projectLogo;
    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 项目标签
     */
    private String tags;

    /**
     * 项目地址
     */
    private String address;

    /**
     * 项目区域
     */
    private String area;

    /**
     * 720楼书链接
     */
    private String vrUrl;

    /**
     * 组名
     */
    private String groupName;

    /**
     * 720楼书状态
     */
    private Integer status;

    /**
     * 720楼书标题
     */
    private String name;

    /**
     * 720资源图片
     */
    @OssPrefixAutoFill
    private String image;

    /**
     * 720楼书归属项目组id
     */
    private Integer groupId;

    /**
     * vr720分享海报
     */
    private String vrPoster;

}
