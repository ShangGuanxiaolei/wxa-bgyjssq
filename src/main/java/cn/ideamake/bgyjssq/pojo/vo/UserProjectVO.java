package cn.ideamake.bgyjssq.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: Walt
 * @Data: 2019-06-13 14:18
 * @Version: 1.0
 * 用户我的项目返回结构
 */
@Data
public class UserProjectVO {


    /**
     * 项目背景图片
     */
    private String projectImg;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 项目id
     */
    private Integer projectId;
    /**
     * 是否在我的推荐中隐藏： 0 隐藏， 1 展示
     */
    private Integer isRecommend;
    /**
     * 项目码
     */
    private String weChatQrcode;
    /**
     * 1-3: ["湖景资源"],   项目特点标签
     */
    private String tags;
    /**
     * 项目地址
     */
    private String address;
    /**
     * 项目访问次数
     */
    private Integer visitCounts=100;
    /**
     * 项目logo
     */
    private String projectLogo;
    /**
     * 销售绑定项目的时间， 该列表按这个字段倒序排列
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime bindDatetime;
}
