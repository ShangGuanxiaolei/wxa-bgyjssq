package cn.ideamake.bgyjssq.pojo.vo.permission;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 系统管理员列表
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class AdminLogVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 管理员操作记录标志
     */
    private Integer id;

    /**
     * 管理员名称
     */
    private String adminName;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 操作内容
     */
    private String operatorContent;

    /**
     * 操作时间
     */
    @JsonFormat( pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createAt;

    /**
     * IP地址
     */
    private String ipAddress;

    /**
     * 操作账号
     */
    private String phone;

}
