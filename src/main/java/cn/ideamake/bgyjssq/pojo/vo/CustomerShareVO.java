package cn.ideamake.bgyjssq.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: gxl
 * @Data: 2019-08-08 15:11
 * @Version: 1.0
 * 访客分享数据
 */
@Data
public class CustomerShareVO {

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 分享时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createAt;

    /**
     * 分享内容
     */
    private String resourceType;

    /**
     * 浏览次数
     */
    private Long pageViews;

    /**
     * 浏览人数
     */
    private Long uniqueVisitors;

    /**
     * 关注小程序人数
     */
    private Long focusMiniProgramTotal;

    /**
     * 留电人数
     */
    private Long remainPhoneTotal;
}
