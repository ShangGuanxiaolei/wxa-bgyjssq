package cn.ideamake.bgyjssq.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ExpireVO {
    /**
     * 二维码失效时间
     */

    LocalDateTime endTime;

    /**
     * now
     */
    LocalDateTime nowTime;

    /**
     * 活动id
     */
    Integer id;
}
