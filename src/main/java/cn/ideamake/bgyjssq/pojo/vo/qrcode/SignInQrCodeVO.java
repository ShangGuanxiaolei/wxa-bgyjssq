package cn.ideamake.bgyjssq.pojo.vo.qrcode;

import cn.ideamake.bgyjssq.pojo.dto.qrcode.SignInQrCodeDTO;
import lombok.ToString;

/**
 * @author gxl
 * @version 1.0
 * @description 签到码VO对象
 * @date 2019-09-17 14:55
 */
@ToString
public class SignInQrCodeVO extends SignInQrCodeDTO {

}
