package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-13 14:38
 * @Version: 1.0
 * 访客我的关注返回信息结构
 */

@Data
public class VisitorAttentionVO {

    /**
     * 项目图片
     */
    @OssPrefixAutoFill
    private String projectImg;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 项目id
     */
    private String projectId;
    /**
     * 项目logo
     */
    @OssPrefixAutoFill
    private String projectLogo;
    /**
     * 项目特点标签
      */
    private String tags;
    /**
     * 项目地址
     */
    private String address;
    /**
     * 销售名称
     */
    private String sellerName;
    /**
     * 销售手机号
     */
    private String sellerPhone;
    /**
     * 销售头像
     */
    @OssPrefixAutoFill
    private String sellerAvatar;
    /**
     * 销售id
     */
    private String sellerId;

}
