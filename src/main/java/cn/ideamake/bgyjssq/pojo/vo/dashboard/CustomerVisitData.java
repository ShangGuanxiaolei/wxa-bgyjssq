package cn.ideamake.bgyjssq.pojo.vo.dashboard;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/07/17
 * @description 客户访问数据
 */
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class CustomerVisitData implements Serializable {

    /**
     * 时长
     */
    private Integer value = 0;

    /**
     * 时间
     */
    private Integer date;

    private String type;

    public CustomerVisitData(String type) {
        this.type = type;
    }
}
