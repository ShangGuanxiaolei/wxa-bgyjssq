package cn.ideamake.bgyjssq.pojo.vo.visit.analyse;

import lombok.Data;

import java.util.List;

/**
 * @Author: Walt
 * @Data: 2019-07-11 17:37
 * @Version: 1.0
 * 访问次数，访问人数，留电次数，留电人数 按日期数量展示for pc后台
 */
@Data
public class TotalVisitTrend {
    /**
     * 访问次数
     */
    private List<VisitTrendCell> visitorNum;

    /**
     * 访问人数
     */
    private List<VisitTrendCell> numberOfPeople;

    /**
     * 留电人数
     */
    private List<VisitTrendCell> peopleOfRemainPhone;

    /**
     * 到访人数
     */
    private List<VisitTrendCell> peopleOfAccess;
}
