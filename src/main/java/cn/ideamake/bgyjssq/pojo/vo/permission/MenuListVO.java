package cn.ideamake.bgyjssq.pojo.vo.permission;

import lombok.Data;

import java.util.List;

/**
 * @author gxl
 * @version 1.0
 * @description 菜单列表
 * @date 2019-07-29 14:25
 */
@Data
public class MenuListVO {

    private Integer id;

    private String name;

    private String title;

    private String icon;

    private String path;

    private List<MenuListChildrenVO> children;
}
