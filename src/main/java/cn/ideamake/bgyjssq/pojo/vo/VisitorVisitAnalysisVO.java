package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author: Walt
 * @Data: 2019-07-09 10:17
 * @Version: 1.0
 * 客户访问分析接口返回对象
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class VisitorVisitAnalysisVO {

    /**
     * 访问总揽
     */
    private TotalStatistics totalStatistics;

    /**
     * 访问来源统计图表
     */
    private List<OriginCountCell> originStatistics;

    /**
     * 访问趋势统计
     */
    private VisitTrend visitTrend;

    /**
     * 访问时间段统计
     */
    private List<VisitPeriod> visitPeriod;
}
