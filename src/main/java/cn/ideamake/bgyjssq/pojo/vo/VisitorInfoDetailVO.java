package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import cn.ideamake.bgyjssq.pojo.bo.VisitorActivityStatistics;
import cn.ideamake.bgyjssq.pojo.bo.VisitorBasicStatistics;
import lombok.Data;

import java.util.List;

/**
 * @Author: Walt
 * @Data: 2019-06-18 19:20
 * @Version: 1.0
 */

@Data
public class VisitorInfoDetailVO {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 销售id
     */
    private String sellerId;

    /**
     * 头像地址
     */
    @OssPrefixAutoFill
    private String avatar;

    /**
     * 客户姓名
     */
    private String name;

    /**
     * 客户来源: 微楼书、微传单、小程序、专属码、自搜索
     */
    private String origin;

    /**
     * 销售备注的用户手机号
     */
    private String phone;

    /**
     * 用户上报的手机号
     */
    private String customerPhone;

    /**
     * 地址
     */
    private String address;

    /**
     * 是否碧桂园业主
     */
    private Integer isBgyOwner;

    /**
     * 备注
     */
    private String remark;

    /**
     * 用户基本统计
     */
    private VisitorBasicStatistics basicInfo;

    /**
     * 用户活动数据
     */
    private List<VisitorActivityStatistics> activityInfo;

}
