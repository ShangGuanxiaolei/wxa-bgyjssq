package cn.ideamake.bgyjssq.pojo.vo.notify;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: gxl
 * @Data: 2019-09-20 14:04
 * @Version: 1.0
 * @description 访客查看通知
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class VisitorNotificationOldVO {

    private Integer id;

    /**
     * 通知等级，2项目通知，1系统通知
     */
    private Integer level;
    /**
     * 通知标题
     */
    private String resourceName;
    /**
     * 通知图像地址
     */
    @OssPrefixAutoFill
    private String img;

    /**
     * h5地址
     */
    private String url;

    /**
     * 文章组别
     */
    private String groupName;

    /**
     * 部门组别
     */
    private String projectName;

    /**
     * 项目id
     */
    private Integer projectId;
}
