package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

@Data
public class PushActivitisVO {
    /**
     * 活动ID
     */
    private Integer activityId;

    /**
     * user_id
     */
    private String userUuid;
}
