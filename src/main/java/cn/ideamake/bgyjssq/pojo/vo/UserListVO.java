package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

/**
 * @author gxl
 * @version 1.0
 * @description 员工
 * @date 2019-09-02 17:17
 */
@Data
public class UserListVO {

    /**
     * 员工id
     */
    private String uuid;

    /**
     * 员工名称
     */
    private String name;

    /**
     * bip账号
     */
    private String bip;
}
