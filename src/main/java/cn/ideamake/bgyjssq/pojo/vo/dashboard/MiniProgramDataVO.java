package cn.ideamake.bgyjssq.pojo.vo.dashboard;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author gxl
 */
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Data
@Accessors(chain = true)
public class MiniProgramDataVO {

    /**
     * 访问数量
     */
    private List<AccessDetailVO> numberOfVisits;

    /**
     * 访问人数
     */
    private List<AccessDetailVO> numberOfPeople;

    /**
     * 留电人数
     */
    private List<AccessDetailVO> peopleOfRemainPhone;

    /**
     * 到访人数
     */
    private List<AccessDetailVO> peopleOfAccess;

}
