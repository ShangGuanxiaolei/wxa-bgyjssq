package cn.ideamake.bgyjssq.pojo.vo;

import lombok.Data;

@Data
public class ActivityTidyVO {

    /**
     * 活动ID
     */
    private Integer activityId;

    /**
     * 活动名
     */
    private String activityName;
}
