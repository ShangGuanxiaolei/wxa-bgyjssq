package cn.ideamake.bgyjssq.pojo.vo;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-05-17 14:08
 * @Version: 1.0
 * 访客推荐页信息
 */
@Data
public class RecommendPageVO {

    private static final long serialVersionUID = 5194933845448697148L;

    /**
     * 项目图片
     */
    @OssPrefixAutoFill
    private String avatar;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 累计推荐次数
     */
    private Integer totalIntroCount;

    /**
     * 累计推荐到访
     */
    private Integer totalIntroVisited;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 项目地址
     */
    private String address;


}
