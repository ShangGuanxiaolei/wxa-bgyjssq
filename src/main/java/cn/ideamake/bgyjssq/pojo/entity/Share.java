package cn.ideamake.bgyjssq.pojo.entity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import  java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 分享表，用户记录销售或者访客的分享行为
 * </p>
 *
 * @author ideamake
 * @since 2019-07-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_share")
public class Share implements Serializable {

private static final long serialVersionUID=1L;

    /**
     * 分享记录唯一索引
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 分享者id,销售或者访客
     */
    private String shareUser;

    /**
     * 分享类型，分享内容的类型，暂定1-文章，2-海报，其他待定
     */
    private Integer shareType;

    /**
     * 分享资源的id
     */
    private Integer resourceId;

    /**
     * 分享者的类型，0-访客，1-销售
     */
    private Integer userType;

    /**
     * 分享时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 分享资源的项目id
     */
    private Integer projectId;

    /**
     * 前端生成的分享id
     */
    private String shareId;

    /**
     * 销售id
     */
    private String sellerId;

}
