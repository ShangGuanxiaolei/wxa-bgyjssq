package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户员工对于绑定自己的客户的管理
 * </p>
 *
 * @author ideamake
 * @since 2019-06-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_staff_visitor")
public class StaffVisitor implements Serializable {

private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 访客的全局id
     */
    private String visitorUuid;

    /**
     * 访客姓名
     */
    private String visitorName;

    /**
     * 员工的全局id
     */
    private String userUuid;

    /**
     * 客户手机号
     */
    private String visitorPhone;

    /**
     * 客户地址
     */
    private String visitorAddress;

    /**
     * 客户首次访问类型
     */
    private String visitorFirstVisit;

    /**
     * 客户种类，包括1-一般访客，2-意向客户，3-项目业主，默认一般访客
     */
    private Integer visitorType;

    /**
     * 员工对于访客的备注信息
     */
    private String remark;

    /**
     * 记录创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 记录更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateAt;

    /**
     * 客户来源, 1-微楼书,2-微图文,3-微海报,4-小程序,5-专属码,6-自搜索,7-其他项 ,默认小程序
     */
    private Integer visitorSource;

    /**
     * 单个销售，单个访客的到访次数记录
     */
    private Integer visitorTimes;

    /**
     * 暂时记录单个访客对于单个销售的绑定项目数量，后续看业务需求调整
     */
    private String bindings;

}
