package cn.ideamake.bgyjssq.pojo.entity;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户基本表
 * </p>
 *
 * @author ideamake
 * @since 2019-06-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Accessors(chain = true)
@TableName("im_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户全局唯一标志
     */
    @TableId(value = "uuid", type = IdType.ID_WORKER_STR)
    private String uuid;

    /**
     * 记录创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 记录更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateAt;

    /**
     * 用户手机号
     */
    private String phone;

    /**
     * 用户登录密码，备用字段
     */
    private String password;

    /**
     * 用户名
     */
    private String name;

    /**
     * 履职状态1在职，2离职
     */
    private Integer isActive;

    /**
     * 员工头像地址
     */
    @OssPrefixAutoFill
    private String avatar;

    /**
     * 员工显示昵称
     */
    private String nickName;

}
