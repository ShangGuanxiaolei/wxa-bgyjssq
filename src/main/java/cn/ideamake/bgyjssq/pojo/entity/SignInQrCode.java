package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 签到二维码
 * </p>
 *
 * @author ideamake
 * @since 2019-07-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_sign_in_qr_code")
public class SignInQrCode implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 活动ID
     */
    private Integer groupId;

    /**
     * 活动名称
     */
    private String activityName;

    /**
     * 开始时间
     */
    private LocalDateTime startAt;

    /**
     * 结束时间
     */
    private LocalDateTime stopAt;

    /**
     * 员工id
     */
    private String userUuid;

    /**
     * 活动人数累计上限
     */
    private Integer activityNumTop;

    /**
     * 活动人数每日上限
     */
    private Integer activityNumDayTop;

    /**
     * 单人扫码累计上限
     */
    private Integer singleNumTop;

    /**
     * 单人扫码每日上限
     */
    private Integer singleNumDayTop;

    /**
     * 强制要求授权电话
     */
    private Boolean pushAuthPhone;

    /**
     * 强制要求授权定位
     */
    private Boolean pushAuthAddress;

    /**
     * 强制要求指定区域签到
     */
    private Boolean designatedArea;

    /**
     * 签到成功播放音乐
     */
    private Boolean musicAfterSignIn;

    /**
     * 是否显示客户当天签到次数
     */
    private Boolean showDaySignInNum;

    /**
     * 是否显示客户累计签到次数
     */
    private Boolean showAllSignInNum;

    /**
     * 是否显示客户累计到访次数
     */
    private Boolean showVisitNum;

    /**
     * 签到界面图片
     */
    private String image;

    /**
     * 刷新频率（单位：秒）
     */
    private Integer refreshFrequency;

    /**
     * 二维码过期时间
     */
    private LocalDateTime endAt;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 纬度
     */
    private String latitude;

    /**
     * 签到半径（单位：米）
     */
    private Integer radius;

    /**
     * 是否有效，默认1有效，0无效
     */
    private Boolean isActive;

    /**
     * 签到码地址
     */
    private String qrCodeUrl;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateAt;

}
