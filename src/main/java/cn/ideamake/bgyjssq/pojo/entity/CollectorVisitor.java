package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 项目访问采集
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_collector_visitor")
public class CollectorVisitor implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 访客id
     */
    private String visitorId;

    /**
     * 销售id
     */
    private String sellerId;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 文章id
     */
    private Integer articleId;

    /**
     * 记录生成时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createAt;

    /**
     * 用户停留时间
     */
    private Integer stayTime;

    /**
     * 经度数据
     */
    private String longitude;

    /**
     * 纬度数据
     */
    private String latitude;

    /**
     * 来源标记
     */
    private Integer origin;

    /**
     * 地址
     */
    private String address;

    /**
     * 访问请求的id,用于标记一次有效的访问记录
     */
    private String requestVisitId;


}
