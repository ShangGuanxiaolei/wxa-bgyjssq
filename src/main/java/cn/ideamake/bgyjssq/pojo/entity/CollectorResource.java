package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 资源采集
 * </p>
 *
 * @author ideamake
 * @since 2019-07-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_collector_resource")
public class CollectorResource implements Serializable {

private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 资源id
     */
    private Integer resourceId;

    /**
     * 资源类型，1-文章,2-海报,3-楼书
     */
    private Integer resourceType;

    /**
     * 用户类型，1-访客，2-员工
     */
    private Integer userType;

    /**
     * 记录生成事件
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 用户停留时间
     */
    private Integer stayTime;

    /**
     * 访问请求id
     */
    private String requestId;

    /**
     * 初始跟办销售的id,
     */
    private String originSellerId;

    /**
     * 带来访问的访客id
     */
    private String visitorId;

    /**
     * 销售id
     */
    private String sellerId;
}
