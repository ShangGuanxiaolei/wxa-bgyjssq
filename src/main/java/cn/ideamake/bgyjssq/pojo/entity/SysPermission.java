package cn.ideamake.bgyjssq.pojo.entity;

import cn.ideamake.bgyjssq.common.enums.PermissionTypeEnum;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 系统权限资源表，记录系统的权限资源信息
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName("im_sys_permission")
public class SysPermission implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 权限唯一记录
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 权限名称
     */
    @NotNull(message = "权限名称不能为空")
    private String name;

    /**
     * 权限职责描述
     */
    private String description;

    /**
     * 权限资源的url
     */
    @NotNull(message = "权限资源url不能为空")
    private String url;

    /**
     * 权限类型
     */
    @NotNull(message = "权限类型不能为空")
    private PermissionTypeEnum type;

    /**
     * 父类id
     */
    private Integer parentId;

    /**
     * 图标
     */
    private String icon;

    /**
     * 路由
     */
    private String component;

    /**
     * 路径
     */
    private String redirect;

    /**
     * 是否校验token
     */
    private boolean requireAuth;

    /**
     * 接口地址
     */
    private String interfaceUrl;

}
