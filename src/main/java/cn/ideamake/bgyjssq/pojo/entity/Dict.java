package cn.ideamake.bgyjssq.pojo.entity;

import cn.ideamake.bgyjssq.common.enums.DictType;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 字典表，用于记录相同字段结构
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_dict")
public class Dict implements Serializable {

private static final long serialVersionUID=1L;

    /**
     * 部门唯一索引
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 资源类别，1-销售组，2-部门组，3-项目组
     */
    @NotNull(message = "分组类型不能为空")
    private DictType type;

    /**
     * 资源名称
     */
    @NotNull(message = "分组名称不能为空")
    private String name;

    /**
     * 资源组所在项目
     */
    private Integer projectId;

    /**
     * 资源内容描述
     */
    private String description;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createAt;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateAt;

    /**
     * 资源组列表
     */
    private String  resources;

}
