package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 720楼书
 * </p>
 *
 * @author ideamake
 * @since 2019-09-03
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_resource_720")
public class Resource720 implements Serializable {

private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 楼书名称
     */
    private String name;

    /**
     * 楼书路径
     */
    private String path;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 楼书状态0未发布，1已发布
     */
    private Integer status;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateAt;

    /**
     * 逻辑删除
     */
    private Integer archive;

    /**
     * 720图片
     */
    private String image;

    /**
     * vr720分享海报
     */
    private String vrPoster;

}
