package cn.ideamake.bgyjssq.pojo.entity;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 访客详情表
 * </p>
 *
 * @author ideamake
 * @since 2019-06-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_visitor")
public class Visitor implements Serializable {

private static final long serialVersionUID=1L;

    /**
     * 关联访客的唯一ID
     */
    @TableId(value = "uuid", type = IdType.ID_WORKER_STR)
    private String uuid;

    /**
     * 记录创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 记录更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateAt;

    /**
     * 访客真实姓名
     */
    private String realName;

    /**
     * 访客来源，看需求定义
     */
    private Integer visitorSource;

    /**
     * 访客二维码url
     */
    private String qrCode;

    /**
     * 访客昵称
     */
    private String nickName;

    /**
     * 访客状态，1表示正常，0表示删除
     */
    private Integer isActive;

    /**
     * 访客自定义头像
     */
    @OssPrefixAutoFill
    private String avatar;

    /**
     * 访客手机号
     */
    private String phone;

    /**
     * 访客地址
     */
    private String address;

    /**
     * 访客岗位
     */
    private String position;

    /**
     * 是否碧桂园业主（0否，1是）
     */
    private Integer isBgyOwner;

    /**
     * 销售备注手机号（最新）
     */
    private String commentPhone;


}
