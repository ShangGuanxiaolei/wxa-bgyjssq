package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 签到二维码推送员工列表
 * </p>
 *
 * @author ideamake
 * @since 2019-09-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_sign_in_qr_code_push_user")
public class SignInQrCodePushUser implements Serializable {

private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 签到二维码id
     */
    private Integer signQrCodeId;

    /**
     * 推送员工id
     */
    private String userUuid;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;


}
