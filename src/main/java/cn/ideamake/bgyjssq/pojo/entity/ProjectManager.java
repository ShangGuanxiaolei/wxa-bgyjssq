package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 员工项目管理表
 * </p>
 *
 * @author ideamake
 * @since 2019-06-12
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_project_manager")
public class ProjectManager implements Serializable {

private static final long serialVersionUID=1L;

    /**
     * 记录唯一索引
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 员工设定的项目状态，0代表隐藏，1代表显示
     */
    private Integer status;

    /**
     * 所属的员工的uuid
     */
    private String userUuid;

    /**
     * 项目二维码
     */
    private String qrCodeUrl;

    public ProjectManager(Integer projectId, String userUuid) {
        this.projectId = projectId;
        this.userUuid = userUuid;
    }
}
