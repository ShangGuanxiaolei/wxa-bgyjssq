package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 签到码签到客户
 * </p>
 *
 * @author ideamake
 * @since 2019-07-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_qr_code_visitor")
public class QrCodeVisitor implements Serializable {

private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 签到访客uuid
     */
    private String visitorUuid;

    /**
     * 签到二维码 活动id
     */
    private Integer signInQrCodeId;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 签到状态
     */
    private Boolean status;

}
