package cn.ideamake.bgyjssq.pojo.entity;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户微信登录表
 * </p>
 *
 * @author ideamake
 * @since 2019-06-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_oauth_wechat")
public class OauthWechat implements Serializable {

private static final long serialVersionUID=1L;

    /**
     * 登录授权唯一记录
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String userUuid;

    /**
     * 微信openid
     */
    private String openid;

    /**
     * 微信unionid
     */
    private String unionid;

    /**
     * 微信头像地址
     */
    @OssPrefixAutoFill
    private String avatarUrl;

    /**
     * 微信所在城市
     */
    private String city;

    /**
     * 微信所在国家
     */
    private String country;

    /**
     * 微信性别
     */
    private Integer gender;

    /**
     * 微信所使用语言
     */
    private String language;

    /**
     * 微信名称
     */
    private String nickName;

    /**
     * 微信所在省份
     */
    private String province;

    /**
     * 微信用户的类型，1代表客户，2代表员工
     */
    private Integer userType;

    public String spliceAddress(){
        return this.country+" "+ this.province+" "+this.city;
    }


}
