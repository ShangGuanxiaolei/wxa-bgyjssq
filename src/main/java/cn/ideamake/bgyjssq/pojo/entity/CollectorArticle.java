package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 文章采集
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_collector_article")
public class CollectorArticle implements Serializable {

private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 访客id
     */
    private String visitorId;

    /**
     * 销售id
     */
    private String sellerId;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 文章id
     */
    private Integer articleId;

    /**
     * 记录生成事件
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 用户停留时间
     */
    private Integer stayTime;


}
