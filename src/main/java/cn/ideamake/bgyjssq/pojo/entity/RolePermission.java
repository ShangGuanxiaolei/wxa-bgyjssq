package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 角色权限绑定表，记录角色和权限的绑定关系
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_role_permission")
public class RolePermission implements Serializable {

private static final long serialVersionUID=1L;

    /**
     * 角色权限绑定记录
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 角色索引
     */
    private Integer roleId;

    /**
     * 权限索引
     */
    private Integer permissionId;

}
