package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 资源表，内容为H5模版的资源统一存到该表中
 * </p>
 *
 * @author ideamake
 * @since 2019-06-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_resource")
public class Resource extends Model<Resource> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 资源唯一索引
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 资源显示名称标题
     */
    private String resourceTitle;

    /**
     * 资源创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 推送时间
     */
    private LocalDateTime publishTime;

    /**
     * 状态 {@link cn.ideamake.bgyjssq.common.enums.ContentPostStatus}
     */
    private Integer status;

    /**
     * 是否置顶，1-置顶，0-未置顶
     */
    private Integer topStatus;

    /**
     * 组id
     */
    private Integer groupId;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 资源更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateAt;

    /**
     * 资源所属用户
     */
    private String userUuid;

    /**
     * 资源类型，{@link cn.ideamake.bgyjssq.common.enums.ResourceType}
     */
    private Integer resourceType;

    /**
     * 资源列表背景图
     */
    private String resourceBg;


    /**
     * 文章, 楼书, 首页
     */
    @TableField(value = "resource_content")
    private String resourceContent;

    /**
     * 海报使用, 海报链接地址
     */
    private String resourceUrl;

    /**
     * 资源推送目标用户
     */
    @JsonProperty("visitorType")
    private String visitorTypes;

}
