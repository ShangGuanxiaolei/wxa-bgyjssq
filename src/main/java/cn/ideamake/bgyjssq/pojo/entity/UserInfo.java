package cn.ideamake.bgyjssq.pojo.entity;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户详情介绍
 * </p>
 *
 * @author ideamake
 * @since 2019-06-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@NoArgsConstructor
@TableName("im_user_info")
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 关联的用户的唯一ID
     */
    @TableId
    private String userUuid;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateAt;

    /**
     * 系统角色，暂定0代表管理员，1代表普通用户
     */
    private Integer roleSysId;

    /**
     * 用户真实姓名
     */
    private String realName;

    /**
     * 用户性别,0代表女性，1代表男性，2代表未知
     */
    private Integer gender;

    /**
     * 微信二维码
     */
    @OssPrefixAutoFill
    private String qrCode;

    /**
     * 用户备注信息
     */
    private String description;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 用户名片二维码链接
     */
    private String businessCardCode;

    /**
     * 用户所属公司id
     */
    private Integer groupId;

    /**
     * 员工背景图片地址
     */
    @OssPrefixAutoFill
    private String coverPicture;

    /**
     * 岗位id，0表示销售组长，1表示普通销售人员
     */
    private Integer position;

    /**
     * 员工地址
     */
    private String address;

    /**
     * 员工公司logo图片地址
     */
    @OssPrefixAutoFill
    private String companyLogo;

    /**
     * 员工公司名称
     */
    private String companyName;

    /**
     * 用户微信id
     */
    private String wechatId;

    /**
     * 销售信息显示/隐藏标识
     */
    private String hideColumn;

    /**
     * 角色id
     */
    private Integer roleId;

    /**
     * 所属项目
     */
    private Integer projectId;

    /**
     * 职务名称
     */
    private String positionName;

    /**
     * 用户类型，1-普通销售，2-特殊销售
     */
    private Integer staffType;

    /**
     * 用户碧桂园账号
     */
    private String bip;

    public UserInfo(String uuid,
                    String companyName,
                    String weChatId,
                    String bip,
                    String jobName,
                    Integer staffType,
                    LocalDateTime createAt,
                    Integer projectId,
                    Integer groupId) {
        this.userUuid = uuid;
        this.companyName = companyName;
        this.wechatId = weChatId;
        this.bip = bip;
        this.positionName = jobName;
        this.staffType = staffType;
        this.createAt = createAt;
        this.projectId = projectId;
        this.groupId = groupId;
    }
}
