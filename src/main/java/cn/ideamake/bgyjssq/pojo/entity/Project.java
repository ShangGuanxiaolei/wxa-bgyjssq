package cn.ideamake.bgyjssq.pojo.entity;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 项目表
 * </p>
 *
 * @author ideamake
 * @since 2019-06-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_project")
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 项目id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 项目内容介绍
     */
    private String projectContent;

    /**
     * 项目标题
     */
    private String projectTitle;

    /**
     * 项目标签
     */
    private String projectTag;

    /**
     * 项目logo的URL地址
     */
    private String projectLogo;

    /**
     * 项目图片的URL地址
     */
    @OssPrefixAutoFill
    private String coverPicture;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateAt;

    /**
     * 项目所属集团编号
     */
    private Integer projectGroupId;

    /**
     * 对应资源id的一条记录
     */
    private String floorBookId;

    /**
     * 项目是否有效,1表示有效，0表示无效
     */
    private Integer isActive;

    /**
     * 项目所在地址
     */
    private String projectArea;

    /**
     * 项目地址
     */
    private String address;

    /**
     * 删除标示
     */
    @TableLogic
    private Integer deleteMark;

    /**
     * 默认销售
     */
    private String defaultSeller;

    /**
     * 微楼书链接
     */
    private String tinyFloorBook;

    /**
     * 售楼处经度
     */
    private String longitude;

    /**
     * 售楼处纬度
     */
    private String latitude;

    /**
     * 半径
     */
    private Double radius;

    /**
     * 项目图片的URL地址
     */
    @OssPrefixAutoFill
    private String profilePoster;

}
