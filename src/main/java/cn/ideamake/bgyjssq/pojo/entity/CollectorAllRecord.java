package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户行为全量记录，用于销售对用户行为的观测
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_collector_all_record")
public class CollectorAllRecord implements Serializable {

private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 访客id
     */
    private String visitorId;

    /**
     * 销售id
     */
    private String sellerId;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 文章id
     */
    private Integer articleId;

    /**
     * 记录生成时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createAt;

    /**
     * 用户停留时间
     */
    private Integer stayTime;

    /**
     * 经度数据
     */
    private String longitude;

    /**
     * 纬度数据
     */
    private String latitude;

    /**
     * 来源标记
     */
    private Integer origin;

    /**
     * 请求ip
     */
    private String ip;

    /**
     * 访问内容
     */
    private String visitorContent;

    /**
     * 动作
     */
    private Integer action;

    /**
     * 前台约束参数
     */
    private String params;
    /**
     * 访问请求id
     */
    private String requestId;

    /**
     * 资源id
     */
    private Integer resourceId;

}
