package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 绑定关系管理,项目，员工，访客确定一条记录，员工自定义访客的信息回记录在这里
 * </p>
 *
 * @author ideamake
 * @since 2019-06-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_binding_manager")
public class BindingManager implements Serializable {

private static final long serialVersionUID=1L;

    /**
     * 记录标志
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 员工id
     */
    private String userUuid;

    /**
     * 访客id
     */
    private String visitorUuid;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 0表示非历史数据，1表示是历史数据,主要针对客户切换绑定
     */
    private Integer historyLabel;

    /**
     * 员工给客户自定义标签，1代表一般访客，2代表项目业主，3代表意向客户
     */
    private Integer selfLabel;

    /**
     * 首访营销中心日期
     */
    private LocalDateTime firstVisitTime;

    /**
     * 访问次数统计
     */
    private Integer visitorCount;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateAt;

    /**
     * 客户来源,0代表朋友圈，1代表老带新，2代表名片码，3代表小程序，是对于特定项目员工对于访客的来源记录，目前属于备注字段
     */
    private Integer sourceLabel;

    /**
     * 员工记录的访客的电话，是对于特定项目的访客的手机记录，目前属于备注字段
     */
    private String phone;

    /**
     * 员工记录的访客的位置信息，是对于特定项目的客户位置记录，目前属于备注字段
     */
    private String location;

    /**
     * 员工自定义的访客的名称，是对于特定项目的用户名称记录，目前属于备注字段
     */
    private String name;

    /**
     * 员工自定义访客的首次访问地址，是对于特定项目的访问记录，目前属于备注字段
     */
    private String firstVisit;

    /**
     * { name: '一般访客', value: '1' },\n { name: '意向客户', value: '2' },\n { name: '诚意客户', value: '3' },\n { name: '项目业主', value: '4' } 访客对于项目的身份
     */
    private String remark;

    /**
     * 转办次数，用户记录访客对应项目的转换销售的次数
     */
    private Integer transfersNumber;

    /**
     * 初始跟办销售
     */
    private String originFollowUp;

    /**
     * 拓客类别0访客，1销售
     */
    private Integer promotionLabel;


}
