package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 推荐记录管理
 * </p>
 *
 * @author ideamake
 * @since 2019-06-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_recommend")
public class Recommend implements Serializable {

private static final long serialVersionUID=1L;

    /**
     * 一条推荐标志
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 推荐人的uuid
     */
    private String sourceUuid;

    /**
     * 推荐的项目
     */
    private Integer projectId;

    /**
     * 推介时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 推介的目标访客
     */
    private String targetUuid;

    /**
     * 是否到访,0表示未到访，1表示已经到访
     */
    private Integer visitedLabel;

    /**
     * 推介所属人身份，0代表访客，1代表员工
     */
    private Integer recommendType;

    /**
     * 推荐类型
     */
    private Integer promotionType;

    /**
     * 推荐资源id
     */
    private Integer resourceId;

}
