package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户日志记录表
 * </p>
 *
 * @author ideamake
 * @since 2019-06-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_user_log")
public class UserLog implements Serializable {

private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 操作类型
     */
    private String operatorType;

    /**
     * 日志内容
     */
    private String operatorContent;

    /**
     * 记录时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 操作用户id
     */
    private String userUuid;

    /**
     * 0代表访客，1代表员工 员工和访客日志分开 ，该字段备用
     */
    private Integer userType;


}
