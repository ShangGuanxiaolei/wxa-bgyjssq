package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 活动推送员工列表
 * </p>
 *
 * @author ideamake
 * @since 2019-07-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_user_push_activity")
public class UserPushActivity {
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 活动id
     */
    @NotNull(message = "活动不能为空")
    private Integer activityId;

    /**
     * 员工id
     */
    @NotNull(message = "销售员不能为空")
    private String userUuid;
}

