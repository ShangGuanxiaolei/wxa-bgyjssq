package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 点击行为记录统计表
 * </p>
 *
 * @author ideamake
 * @since 2019-08-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_click_record")
public class ClickRecord implements Serializable {

private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 点击的分享id
     */
    private String shareId;

    /**
     * 分享者id,冗余记录用于统计
     */
    private String sourceUuid;

    /**
     * 点击者uuid，冗余记录，做统计使用
     */
    private String targetUuid;

    /**
     * 分享者用户类型，0-访客，1-销售
     */
    private Integer sourceUserType;

    /**
     * 点击人的用户类型，0-访客，1-销售
     */
    private Integer targetUserType;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateAt;

    /**
     * 分享资源的项目id,冗余记录，做统计使用
     */
    private Integer projectId;

    /**
     * 资源id，冗余记录，做统计使用
     */
    private Integer resourceId;

    /**
     * 资源类型，冗余记录，做统计使用
     */
    private Integer resourceType;


}
