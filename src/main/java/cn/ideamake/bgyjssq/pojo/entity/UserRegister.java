package cn.ideamake.bgyjssq.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 管理员工邀请注册的接口
 * </p>
 *
 * @author ideamake
 * @since 2019-06-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("im_user_register")
public class UserRegister implements Serializable {

private static final long serialVersionUID=1L;

    /**
     * 记录唯一索引
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 邀请注册的项目列表，此字段唯一，项目相同的邀请码认为是同一条记录
     */
    private String projectList;

    /**
     * 验证码
     */
    private String validateCode;

    /**
     * 小程序码url地址
     */
    private String qrcode;

    /**
     * 是否有效，默认1代表有效
     */
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createAt;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateAt;

    /**
     * 操作人的唯一id
     */
    private String operatorUuid;


}
