package cn.ideamake.bgyjssq.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author imyzt
 * @date 2019/07/23
 * @description PC后台-文章阅读人数饼图
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleReadPicCountDTO {

    private Integer visitorCount;

    private Integer sellerCount;
}
