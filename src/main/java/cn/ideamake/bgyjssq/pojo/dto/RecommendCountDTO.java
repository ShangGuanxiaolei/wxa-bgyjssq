package cn.ideamake.bgyjssq.pojo.dto;

import lombok.Data;

/**
 * @author imyzt
 * @date 2019/07/29
 * @description 推广类型分别汇总
 */
@Data
public class RecommendCountDTO {

    private String uuid;

    /**
     * 推广类型
     */
    private Integer type;

    private Integer count;
}
