package cn.ideamake.bgyjssq.pojo.dto;

import cn.ideamake.bgyjssq.common.enums.DictType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author: Walt
 * @Data: 2019-08-12 20:20
 * @Version: 1.0
 */
@Data
public class DictDTO {
    /**
     * 部门唯一索引
     */
    private Integer id;

    /**
     * 资源类别，1-销售组，2-部门组，3-项目组
     */
    @NotNull(message = "组类别不能为空")
    private DictType type;

    /**
     * 资源名称
     */
    private String name;

    /**
     * 资源组所在项目
     */
    private Integer projectId;

    /**
     * 资源内容描述
     */
    private String description;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createAt;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateAt;

    /**
     * 资源组列表
     */
    private List<Integer> resourceIds;
}
