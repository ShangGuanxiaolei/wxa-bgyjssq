package cn.ideamake.bgyjssq.pojo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author imyzt
 * @date 2019/07/03
 * @description 访客推荐记录
 */
@Data
@Accessors(chain = true)
public class VisitorRecommendDTO {

    /**
     * 访客id
     */
    private String visitorUuid;

    /**
     * 访客推荐总数
     */
    private Integer total;
}
