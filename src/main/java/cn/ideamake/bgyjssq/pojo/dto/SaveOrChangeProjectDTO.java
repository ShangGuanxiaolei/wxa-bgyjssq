package cn.ideamake.bgyjssq.pojo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/07/08
 * @description 项目保存或更新
 */
@Data
public class SaveOrChangeProjectDTO implements Serializable {

    private Integer id;

    @JsonProperty("projectName")
    private String projectTitle;

    @JsonProperty("projectImg")
    private String coverPicture;

    private String projectLogo;

    @JsonProperty("tags")
    private String projectTag;

    @JsonProperty("address")
    private String address;

    @JsonProperty("area")
    private String projectArea;

    @JsonProperty("content")
    private String projectContent;

    /**
     * 项目所属集团编号
     */
    @JsonProperty("groupId")
    private Integer projectGroupId;
}
