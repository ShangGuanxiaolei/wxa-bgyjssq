package cn.ideamake.bgyjssq.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @author gxl
 * @date 2019/08/07
 * @description PC后台编辑客户信息实体
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDTO {

    /**
     * 客户id
     */
    @NotNull(message = "客户id不能为空")
    private String customerId;

    /**
     * 项目id
     */
    @NotNull(message = "项目id不能为空")
    private Integer projectId;

    /**
     * 客户名称
     */
    private String name;

    /**
     * 客户手机
     */
    private String phone;

    /**
     * 拓客类别1-员工拓展,2-客户推介
     */
    private Integer sourceType;

    /**
     * 客户备注
     */
    private String remark;
}
