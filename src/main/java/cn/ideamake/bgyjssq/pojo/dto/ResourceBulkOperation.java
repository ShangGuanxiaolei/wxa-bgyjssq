package cn.ideamake.bgyjssq.pojo.dto;

import cn.ideamake.bgyjssq.common.enums.ContentPostStatus;
import cn.ideamake.bgyjssq.common.enums.ContentTopStatus;
import cn.ideamake.bgyjssq.common.validate.EnumVerify;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * @author imyzt
 * @date 2019/07/02
 * @description 资源批量操作表单参数
 */
@Data
@Accessors(chain = true)
public class ResourceBulkOperation implements Serializable {

    @NotEmpty(message = "请选择文章")
    private List<Integer> ids;

    /**
     * 资源类型
     * 文章,海报,楼书
     */
    private Integer resourceType;

    @EnumVerify(message = "请正确选择发布状态", enumClass = ContentPostStatus.class)
    private Integer status;

    @EnumVerify(message = "请正确选择指定状态", enumClass = ContentTopStatus.class)
    private Integer top;

    private Integer groupId;
}