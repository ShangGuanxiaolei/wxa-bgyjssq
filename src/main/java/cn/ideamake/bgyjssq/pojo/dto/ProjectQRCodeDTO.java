package cn.ideamake.bgyjssq.pojo.dto;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import cn.ideamake.bgyjssq.pojo.query.WeChatQRQuery;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author: Walt
 * @Data: 2019-06-20 15:10
 * @Version: 1.0
 */
@Data
public class ProjectQRCodeDTO {

    /**
     * 销售id
     */
    @UserId
    private String sellerId;

    /**
     * 项目id
     */
    @NotNull(message = "项目id不能为空")
    private Integer projectId;

    /**
     * vr720Id
     */
    private Integer floorBookId;

    /**
     * 生成项目码
     */
    private WeChatQRQuery weChatQRQuery;
}
