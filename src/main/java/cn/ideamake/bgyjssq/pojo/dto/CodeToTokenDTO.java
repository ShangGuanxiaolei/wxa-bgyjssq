package cn.ideamake.bgyjssq.pojo.dto;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-07-18 15:19
 * @Version: 1.0
 * 访客注册接口
 */
@Data
public class CodeToTokenDTO {
    /**
     * 微信授权码
     */
    private String code;

    /**
     * 包括敏感数据在内的完整用户信息的加密数据
     */
    private String encryptedData;

    /**
     * 加密算法的初始向量
     */
    private String iv;
    /**
     * 访客来源
     */
    private String origin = "4";
}
