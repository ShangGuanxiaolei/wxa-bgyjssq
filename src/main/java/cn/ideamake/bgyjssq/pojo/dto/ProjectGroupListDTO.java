package cn.ideamake.bgyjssq.pojo.dto;

import cn.ideamake.bgyjssq.pojo.entity.Dict;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author imyzt
 * @date 2019/07/26
 * @description 小程序-客户列表-筛选条件 项目组对应项目列表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ProjectGroupListDTO implements Serializable {

    private Integer groupId;

    private String groupName;

    private List<ProjectBaseInfoDTO> projectList;

    public ProjectGroupListDTO(Dict dict, List<ProjectBaseInfoDTO> list) {
        this.groupId = dict.getId();
        this.groupName = dict.getName();
        this.projectList = list;
    }
}
