package cn.ideamake.bgyjssq.pojo.dto;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-05-17 10:41
 * @Version: 1.0
 * 绑定信息修改
 */
@Data
public class BindingEditDTO {
    private static final long serialVersionUID=1L;
    /**
     * 改变前的销售id
     */
    private Integer sellerIdPre;

    /**
     * 改变后的销售id
     */
    private Integer sellerIdAft;

    /**
     * 改变的项目id
     */
    private Integer projectId;

    /**
     * 改变的用户id
     */
    private Integer visitorId;
}
