package cn.ideamake.bgyjssq.pojo.dto;

import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-13 15:42
 * @Version: 1.0
 * 添加员工所需的必备信息
 */
@Data
public class UserSaveDTO {

    /**
     * 用户手机号
     */
    private String phone;

    /**
     * 用户密码，可选，为之后登录准备
     */
    private String password;

    /**
     * 用户登录账号，为之后账号登录准备
     */
    private String name;

    /**
     * 用户头像地址，与用户微信头像分开，用户可以自定义
     * 此处只存储url地址
     */
    @OssPrefixAutoFill
    private String avatar;

    /**
     * 用户昵称，默认是微信昵称
     */
    private String nickName;

    /**
     * 员工真实姓名
     */
    private String realName;

    /**
     * 员工性别，0男性，1女性，默认2未知
     */
    private Integer gender;

    /**
     * 员工微信二维码，存储内容待定
     */
    private String qrcode;

    /**
     * 员工备注描述
     */
    private String description;

    /**
     * 员工邮箱地址
     */
    private String email;

    /**
     * 员工二维码
     */
    private String businessCardCode;

    /**
     * 员工首页图片
     */
    private String coverPicture;

    /**
     * 员工职位，具体内容待定
     */
    private String position;

    /**
     * 员工
     */
    private String address;

}
