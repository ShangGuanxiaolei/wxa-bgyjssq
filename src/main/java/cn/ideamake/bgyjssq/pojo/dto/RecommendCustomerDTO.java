
package cn.ideamake.bgyjssq.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author: Walt
 * @Data: 2019-07-23 16:51
 * @Version: 1.0
 * 用户拓客接口，用于新拓客时调用
 */
@Data
public class RecommendCustomerDTO {
    /**
     * 推荐人
     */
    @NotNull(message = "推荐人id不能为空")
    private String sourceUuid;

    /**
     * 被推荐人
     */
    @NotNull(message = "目标id不能为空")
    private String targetUuid;

    /**
     * 推荐项目id
     */
    @NotNull(message = "项目id不能为空")
    private Integer projectId;

    /**
     * 点击的分享id
     */
    private String shareId;

    /**
     * 分享的资源id ,如果是资源表中的信息，则带上资源id,不是则默认-1
     */
    private Integer resourceId = -1;

    /**
     * 资源类型 1-文章，2-海报，3-首页，4-楼书
     */
    private Integer resourceType;

    /**
     * 推广类型
     * {
     * 1: "微楼书",
     * 2: "微文章",
     * 3: "微海报",
     * 4: "小程序",
     * 5: "专属码",
     * 6: "其他项"
     * }
     */
    private Integer promotionType;

    /**
     * 推广人身份
     */
    private Integer recommendType;
}

