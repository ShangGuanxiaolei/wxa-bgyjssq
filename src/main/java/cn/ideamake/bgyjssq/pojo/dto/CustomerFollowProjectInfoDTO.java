package cn.ideamake.bgyjssq.pojo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author imyzt
 * @date 2019/07/09
 * @description 客户关注项目信息(PC端)
 */
@Data
public class CustomerFollowProjectInfoDTO {

    /**
     * 项目名称
     */
    private String projectTitle;

    /**
     * 客户类别
     */
    private String selfLabel;

    /**
     * 跟办类别
     */
    private String category;

    /**
     * 当前跟办
     */
    private String currentStaff;

    /**
     * 初始跟办
     */
    private String firstStaff;

    /**
     * 转办次数
     */
    private Integer count;

    /**
     * 首次访问时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime firstVisitTime;

    /**
     * 最近访问时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastVisitTime;

    /**
     * 累计访问项目次数
     */
    private Integer visitCount;

    /**
     * 驻留时间
     */
    private Integer dwellTime;

    /**
     * 浏览文章次数
     */
    private Integer browseArticles;

    /**
     * 浏览海报次数
     */
    private Integer browsePoster;
}
