package cn.ideamake.bgyjssq.pojo.dto;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-17 17:34
 * @Version: 1.0
 */

@Data
public class ScanProjectQrCodeDTO {
    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 访客id
     */
    private String visitorId;
}
