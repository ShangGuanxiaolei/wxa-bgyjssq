package cn.ideamake.bgyjssq.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class TestDTO {

    @NotNull(message = "id不可以为空")
    private String id;
}
