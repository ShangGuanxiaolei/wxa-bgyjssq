package cn.ideamake.bgyjssq.pojo.dto;

import cn.ideamake.bgyjssq.common.enums.ContentTopStatus;
import cn.ideamake.bgyjssq.common.enums.PosterCategory;
import cn.ideamake.bgyjssq.common.validate.EnumVerify;
import cn.ideamake.bgyjssq.pojo.bo.SinglePushVO;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author imyzt
 * @date 2019/07/11
 * @description 资源发布对象
 */
@Data
@Accessors(chain = true)
public class PublishResourceDTO {

    private Integer id;

    @Length(min = 5, message = "标题不能少于5个字")
    @JsonProperty("title")
    private String resourceTitle;

    /**
     * 封面
     */
    @JsonProperty("cover")
    private String resourceBg;

    /**
     * 项目id
     */
    private List<Integer> projectId;

    /**
     * 置顶, 默认不置顶
     */
    @EnumVerify(message = "请选择正确的置顶状态", enumClass = ContentTopStatus.class)
    @JsonProperty("top")
    private Integer topStatus = 0;

    /**
     * 推送时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime publishTime;

    /**
     * 类别,适配楼书发布，不校验
     */
    @EnumVerify(message = "请选择正确的类别", enumClass = PosterCategory.class, unVerifyNull = false)
    @JsonProperty("category")
    private Integer resourceCategory = 2;

    /**
     * 海报对应的内容
     */
    private String resourceUrl;

    /**
     * 文章对应的文章内容
     */
    @JsonProperty("content")
    private String resourceContent;

    /**
     * 推送的访客类型
     */
    @JsonProperty("visitorType")
    private String visitorTypes;

    /**
     * 推送项目和访客类型
     */
    private List<SinglePushVO> pushTargets;
}
