package cn.ideamake.bgyjssq.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author: Walt
 * @Data: 2019-07-23 16:51
 * @Version: 1.0
 * 访客通过点击分享链接进入系统，对于项目来说用户是新用户
 * 目前这个操作由前台判断是否是新用户
 */
@Data
public class RecordByClickLinkDTO {
    /**
     * 推荐人
     */
    @NotNull(message = "推荐人id不能为空")
    private String sourceUuid;
    /**
     * 被推荐人
     */
    @NotNull(message = "目标id不能为空")
    private String targetUuid;
    /**
     * 推荐项目id
     */
    @NotNull(message = "项目id不能为空")
    private Integer projectId;
    /**
     * 点击的分享id
     */
    @NotNull(message = "分享id不能为空")
    private String shareId;

    /**
     * 分享的资源id ,如果是资源表中的信息，则带上资源id,不是则默认-1
     */
    private Integer resourceId = -1;

    /**
     * 资源类型 1-文章，2-海报，3-首页，4-楼书
     */
    @NotNull(message = "分享类型不能为空")
    private Integer resourceType;
    /**
     * 推广类型，0-访客分享，1-员工分享
     */
    private Integer promotionType;
}


