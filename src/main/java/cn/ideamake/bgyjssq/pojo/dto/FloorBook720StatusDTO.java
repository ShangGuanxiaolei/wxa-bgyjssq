package cn.ideamake.bgyjssq.pojo.dto;

import cn.ideamake.bgyjssq.common.enums.FloorBook720Status;
import cn.ideamake.bgyjssq.common.validate.EnumVerify;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author gxl
 * @date 2019/09/09
 * @description 720楼书管理-批量操作
 */
@Data
public class FloorBook720StatusDTO implements Serializable {

    /**
     * 楼书id列表
     */
    @NotNull(message = "请选择楼书")
    private List<Integer> ids;


    /**
     * 楼书状态,0-关闭,1-开启
     */
    @EnumVerify(message = "楼书状态设置错误", enumClass = FloorBook720Status.class)
    private Integer status;
}
