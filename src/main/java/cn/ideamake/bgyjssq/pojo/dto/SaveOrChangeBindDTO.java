package cn.ideamake.bgyjssq.pojo.dto;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author: Walt
 * @Data: 2019-06-17 19:28
 * @Version: 1.0
 */
@Data
public class SaveOrChangeBindDTO {

    /**
     * 访客的id
     */
    @JsonProperty("userid")
    @UserId
    private String visitorId;

    /**
     * 更换后销售的id
     */
    private String sellerId;

    /**
     * 绑定的项目id
     */
    private Integer projectId;

    /**
     * 员工列表
     */
    private List<String> sellerIds;

    /**
     * 客户列表
     */
    private List<String> visitorIds;
}
