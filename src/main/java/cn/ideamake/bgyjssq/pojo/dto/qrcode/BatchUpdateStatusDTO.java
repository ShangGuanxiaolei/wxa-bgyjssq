package cn.ideamake.bgyjssq.pojo.dto.qrcode;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author gxl
 */
@Data
public class BatchUpdateStatusDTO {

    /**
     * 活动状态
     */
    @NotNull(message = "状态不能为空")
    private Integer status;

    /**
     * 活动ids
     */
    @NotNull(message = "活动id不能为空")
    private List<Integer> ids;

}
