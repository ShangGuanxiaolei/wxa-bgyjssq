package cn.ideamake.bgyjssq.pojo.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;

/**
 * @Author: Walt
 * @Data: 2019-05-17 10:52
 * @Version: 1.0
 * 员工信息修改前台传入参数
 */
@Data
public class UserEditDTO {

    private static final long serialVersionUID=1L;
    /**
     * 用户id
     */
    @NotBlank(message = "用户id不能为空")
    private String userId;

    /**
     * 用户显示名称，默认微信名
     */
    private String name;

    /**
     * 用户显手机号
     */
    private String phone;

    /**
     * 员工所在公司
     */
    private String company;

    /**
     * 员工岗位
     */
    private String position;

    /**
     * 员工居住地址
     */
    private String address;

    /**
     * 员工邮箱
     */
    private String email;

    /**
     * 员工微信账号
     */
    private String weChatId;

    /**
     * 员工微信二维码
     */
    private MultipartFile weChatQrcode;

    /**
     * 名片封面图片
     */
    private String businessCardImg;
}
