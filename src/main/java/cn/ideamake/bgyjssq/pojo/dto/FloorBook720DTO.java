package cn.ideamake.bgyjssq.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author gxl
 * @date 2019/09/10
 * @description 720楼书
 */
@Data
public class FloorBook720DTO implements Serializable {

    /**
     * 720资源id
     */
    private Integer id;

    /**
     * 720资源标题
     */
    private String name;

    /**
     * 720资源路径
     */
    @NotNull(message = "楼书链接不能为空")
    private String path;

    /**
     * 720图片链接
     */
    private String image;

    /**
     * 项目id
     */
    @NotNull(message = "项目不能为空")
    private Integer projectId;

    /**
     * vr720分享海报
     */
    @NotNull(message = "海报图不能为空")
    private String vrPoster;
}
