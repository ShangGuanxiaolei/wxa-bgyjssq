package cn.ideamake.bgyjssq.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/07/26
 * @description 项目(楼盘)基本信息对应
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProjectBaseInfoDTO implements Serializable {

    private Integer projectId;

    private String projectName;

    private Integer groupId;

}
