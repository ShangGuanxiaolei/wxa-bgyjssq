package cn.ideamake.bgyjssq.pojo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author imyzt
 * @date 2019/07/09
 * @description 客户列表-批量操作参数(PC后台)
 */
@Data
public class CustomerBulkOperation {

    /**
     * 客户id列表
     */
    @NotEmpty(message = "请选择客户")
    @JsonProperty("ids")
    private List<String> customerIds;

    /**
     * 销售id
     */
    private String sellerId;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 客户类别
     */
    private Integer selfLabel;

    /**
     * 员工列表
     */
    private List<String> sellerIds;

}
