package cn.ideamake.bgyjssq.pojo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Yusi Zhang
 * @Created 2019-08-07 14:06
 * TODO()
 */
@Data
public class AdvertisementDTO {

    private Integer id;

    /**
     * 广告标题
     */
    @NotNull(message = "广告标题不能为空")
    private String adverTitle;

    /**
     * 广告类别0系统，1项目
     */
    @NotNull(message = "广告类别不能为空")
    private Integer adverType;

    /**
     * 广告网址
     */
    @NotNull(message = "广告图片不能为空")
    private String adverContent;

    /**
     * 广告发布时间
     */
    @NotNull(message = "广告发布时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime adverDate;

    /**
     * 失效时间
     */
    @NotNull(message = "广告失效时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime invalidDate;

    /**
     * 广告项目列表
     */
    private List<Integer> projects;

    /**
     * 是否授权电话
     */
    private Boolean isAuthorizedPhone;

}