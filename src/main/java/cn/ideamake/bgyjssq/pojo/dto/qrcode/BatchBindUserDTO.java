package cn.ideamake.bgyjssq.pojo.dto.qrcode;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author gxl
 */
@Data
public class BatchBindUserDTO {

    /**
     * 活动ids
     */
    @NotNull(message = "活动id不能为空")
    private List<Integer> ids;

    /**
     * 项目id
     */
    @NotNull(message = "项目id不能为空")
    private Integer projectId;

    /**
     * 销售id
     */
    @NotNull(message = "销售id不能为空")
    private String sellerId;

}
