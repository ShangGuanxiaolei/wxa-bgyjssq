package cn.ideamake.bgyjssq.pojo.dto;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author: Walt
 * @Data: 2019-08-29 09:49
 * @Version: 1.0
 */
@Data
public class FormIdDTO {

    @UserId
    private String userId;
    /**
     * 用户formId
     */
    @NotNull(message = "formId不能为空")
    private String formId;
}
