package cn.ideamake.bgyjssq.pojo.dto.project;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 项目
 * </p>
 *
 * @author gxl
 * @since 2019-10-21
 */
@Data
public class ProjectDTO implements Serializable {

    /**
     * 项目id
     */
    @NotNull(message = "项目id不能为空")
    private Integer id;

    /**
     * 售楼处经度
     */
    @NotNull(message = "经度不能为空")
    private String longitude;

    /**
     * 售楼处纬度
     */
    @NotNull(message = "纬度不能为空")
    private String latitude;

    /**
     * 半径
     */
    @NotNull(message = "半径不能为空")
    private Double radius;

}
