package cn.ideamake.bgyjssq.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author: Walt
 * @Data: 2019-08-15 16:24
 * @Version: 1.0
 */
@Data
public class UserGroupEditDTO {
    /**
     * 更新的组id
     */
    private Integer groupId;
    /**
     * 新增管理员的用户列表
     */
    private List<String> userIds;
    /**
     * 组名
     */
    @NotNull(message = "组名不能为空")
    private String name;

    /**
     * 项目id
     */
    private Integer projectId;
}
