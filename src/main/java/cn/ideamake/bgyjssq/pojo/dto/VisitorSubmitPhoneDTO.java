package cn.ideamake.bgyjssq.pojo.dto;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import cn.ideamake.bgyjssq.pojo.bo.WeChatPhone;
import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-07-17 10:33
 * @Version: 1.0
 * 用户上报手机号数据结构内容
 */

@Data
public class VisitorSubmitPhoneDTO {
    /**
     * 用户手机号
     */
    private WeChatPhone phone;

    /**
     * 销售id
     */
    private String sellerId;

    /**
     * 访客id
     */
    @UserId
    private String visitorId;
}
