package cn.ideamake.bgyjssq.pojo.dto;

import cn.ideamake.bgyjssq.common.enums.ContentTopStatus;
import cn.ideamake.bgyjssq.common.validate.EnumVerify;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * @author imyzt
 * @date 2019/07/02
 * @description 发布文章文章内容
 */
@Data
@Accessors(chain = true)
public class PublishArticleDTO implements Serializable {


    @NotBlank(message = "文章内容不能为空")
    @JsonProperty("content")
    private String resourceContent;



    /**
     * 推送时间
     */
    @NotNull(message = "请选择发布时间")
    private LocalDate publishTime;

    /**
     * 文章置顶
     */
    @EnumVerify(message = "请选择正确的置顶状态", enumClass = ContentTopStatus.class)
    @JsonProperty("top")
    private Integer topStatus;

    /**
     * 项目id
     */
    private List<Integer> projectId;
}
