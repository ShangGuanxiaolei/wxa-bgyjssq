package cn.ideamake.bgyjssq.pojo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * @author imyzt
 * @date 2019/07/02
 * @description 文章批量操作表单参数
 */
@Data
@Accessors(chain = true)
public class PosterBulkOperation implements Serializable {

    @NotEmpty(message = "请选择海报")
    @JsonProperty("posterId")
    private List<Integer> ids;

    private Integer status;

    private Integer top;

    private Integer groupId;
}