package cn.ideamake.bgyjssq.pojo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: Walt
 * @Data: 2019-07-03 16:41
 * @Version: 1.0
 * 1-全量访问记录，用于给销售观看访客行为，
 * 2-项目访问记录，用户统计访客对于某项目的访问
 * 3-文章阅读记录，用于记录访客对于文章的记录
 * 该dto包含所有字段，根据类型做对应的转换
 */
@Data
public class CollectForAllParamDTO {
    /**
     * 请求uuid
     */
    private String uuid;
    /**
     * 访客id
     */
    private String visitorId;
    /**
     * 销售id
     */
    private String sellerId;

    /**
     * 访问的用户id
     */
    private String userId;

    /**
     * 用户类型
     */
    private Integer userType;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 访问内容,记录访客访问的位置
     */
    private String visitorContent;

    /**
     * 资源id
     */
    private Integer resourceId;

    /**
     * 资源类型1-文章，2-海报，3-项目首页，4-楼书，5-名片，其他待定
     */
    private Integer resourceType;

    /**
     * 访问开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime localDateTime;

    /**
     * 初始跟办销售id
     */
    private String originSellerId;
    /**
     * 经度
     */
    private String longitude;

    /**
     * 维度
     */
    private String latitude;

    /**
     * 停留时长（秒）
     */
    private Integer stayTime;

    /**
     * 来源标记
     */
    private Integer origin;

    /**
     * 采集类型
     */
    private Integer type;

    /**
     * 用户访问的ip地址
     */
    private String ip;
}
