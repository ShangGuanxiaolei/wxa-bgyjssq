package cn.ideamake.bgyjssq.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author: Walt
 * @Data: 2019-07-16 10:26
 * @Version: 1.0
 * 销售隐藏项目信息转换结构
 */
@Data
public class StaffHideProjectDTO {
    /**
     * 销售id
     */
    @NotNull(message = "销售id不能为空")
    private String sellerId;

    /**
     * 项目id
     */
    @NotNull(message = "项目id不能为空")
    private Integer projectId;

    /**
     * 开启或隐藏标识0-隐藏，1-显示
     */
    @NotNull
    private Integer isRecommended;
}
