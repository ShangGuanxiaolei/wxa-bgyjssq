package cn.ideamake.bgyjssq.pojo.dto;

import cn.ideamake.bgyjssq.common.enums.FloorBookPostStatus;
import cn.ideamake.bgyjssq.common.validate.EnumVerify;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author imyzt
 * @date 2019/07/06
 * @description 楼书管理-批量操作
 */
@Data
public class FloorBookBulkOperation implements Serializable {

    /**
     * 楼书id列表
     */
    @NotNull(message = "请选择楼书")
    private List<Integer> ids;

    /**
     * 楼书组id
     */
    private Integer groupId;

    /**
     * 楼书状态,0-下架,1-上架
     */
    @EnumVerify(message = "楼书状态设置错误", enumClass = FloorBookPostStatus.class)
    private Integer status;
}
