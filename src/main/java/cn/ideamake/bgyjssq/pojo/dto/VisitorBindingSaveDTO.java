package cn.ideamake.bgyjssq.pojo.dto;

import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-14 13:54
 * @Version: 1.0
 */
@Data
public class VisitorBindingSaveDTO {
    /**
     * 销售的id
     */
    private String userUuid;
    /**
     * 访客的id
     */
    private String visitorUuid;
    /**
     * 绑定的项目id
     */
    private Integer projectId;
}
