package cn.ideamake.bgyjssq.pojo.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/07/01
 * @description 客户列表返回对象
 */
@Data
public class CustomerManagerDTO implements Serializable {

    private String uuid;
    /**
     * 客户昵称
     */
    private String nickName;
    private String phone;
    /**
     * 拓客类别
     */
    private String visitorSource;

    /**
     * 跟办员工
     */
    private String name;
    private String selfLabel;
    private String projectTitle;
}
