package cn.ideamake.bgyjssq.pojo.dto;

import cn.ideamake.bgyjssq.pojo.bo.WeChatPhone;
import lombok.Data;

/**
 * @Author: Walt
 * @Data: 2019-06-21 15:30
 * @Version: 1.0
 * 用户邀请注册扫码参数
 */
@Data
public class ScanInviteCodeDTO {
    /**
     * 邀请id
     */
    private Integer inviteId;

    /**
     * 验证码
     */
    private String validateCode;

    /**
     * 用户手机号
     */
    private WeChatPhone phone;
}
