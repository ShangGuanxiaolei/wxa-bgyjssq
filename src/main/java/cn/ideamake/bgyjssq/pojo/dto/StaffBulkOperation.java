package cn.ideamake.bgyjssq.pojo.dto;

import cn.ideamake.bgyjssq.common.enums.ContentStaffStatus;
import cn.ideamake.bgyjssq.common.validate.EnumVerify;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * @author gxl
 * @date 2019/08/10
 * @description 员工批量操作表单参数
 */
@Data
@Accessors(chain = true)
public class StaffBulkOperation implements Serializable {

    @NotEmpty(message = "请选择员工")
    private List<String> ids;

    /**
     * 组
     */
    private Integer groupId;

    @EnumVerify(message = "请正确选择履职状态", enumClass = ContentStaffStatus.class)
    private Integer status;

    /**
     * 职务
     */
    private String position;

    /**
     * 项目
     */
    private List<Integer> projectIds;

    /**
     * 所属项目id
     */
    private Integer projectId;

    /**
     * 销售类别,1-普通销售，2-特殊销售
     */
    private Integer staffType;

}