package cn.ideamake.bgyjssq.pojo.dto;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

/**
 * @Author: Walt
 * @Data: 2019-06-17 15:35
 * @Version: 1.0
 * 员工首页名片编辑
 */
@Data
public class BussinessCardHomeEditDTO {

    /**
     * 用户id
     */
    @UserId
    private String userId;

    /**
     * 员工名称
     */
    @NotNull(message = "姓名不能为空")
    private String name;

    /**
     * 员工公司地址
     */
    private String address;

    /**
     * 员工手机号
     */
    @NotNull(message = "手机号不能为空")
    private String phone;

    /**
     * 员工公司
     */
    private String company;

    /**
     * 员工岗位
     */
    private Integer position;

    /**
     * 员工微信id
     */
    private String weChatId;

    /**
     * 员工头像地址
     */
    private MultipartFile avatarUrl;

    /**
     * 员工封面图片
     */
    private MultipartFile coverPicture;

    /**
     * 员工二微信维码
     */
    private MultipartFile weChatQrcode;

    /**
     * 员工邮箱
     */
    private String email;

    /**
     * 销售信息显示/隐藏标识
     */
    private String hideColumn;

    /**
     * 职务
     */
    private String positionName;
}
