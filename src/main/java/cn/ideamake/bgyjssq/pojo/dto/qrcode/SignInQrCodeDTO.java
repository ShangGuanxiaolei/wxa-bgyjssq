package cn.ideamake.bgyjssq.pojo.dto.qrcode;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 签到二维码
 * </p>
 *
 * @author gxl
 * @since 2019-07-10
 */
@Data
public class SignInQrCodeDTO implements Serializable {

    /**
     * 签到码id
     */
    private Integer id;

    /**
     * 项目id
     */
    @NotNull(message = "项目不能为空")
    private Integer projectId;

    /**
     * 活动ID
     */
    @NotNull(message = "活动组ID不能为空")
    private Integer groupId;

    /**
     * 活动名称
     */
    @NotNull(message = "活动名称不能为空")
    private String activityName;

    /**
     * 开始时间
     */
    @NotNull(message = "开始时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startAt;

    /**
     * 结束时间
     */
    @NotNull(message = "结束时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime stopAt;

    /**
     * 员工id
     */
    @NotNull(message = "销售员不能为空")
    private String userUuid;

    /**
     * 发送员工
     */
    private Set<String> sellerIds;

    /**
     * 活动人数累计上限
     */
    private Integer activityNumTop;

    /**
     * 活动人数每日上限
     */
    private Integer activityNumDayTop;

    /**
     * 单人扫码累计上限
     */
    private Integer singleNumTop;

    /**
     * 单人扫码每日上限
     */
    private Integer singleNumDayTop;

    /**
     * 强制要求授权电话
     */
    private Boolean pushAuthPhone;

    /**
     * 强制要求授权定位
     */
    private Boolean pushAuthAddress;

    /**
     * 强制要求指定区域签到
     */
    private Boolean designatedArea;

    /**
     * 签到成功播放音乐
     */
    private Boolean musicAfterSignIn;

    /**
     * 是否显示客户当天签到次数
     */
    private Boolean showDaySignInNum;

    /**
     * 是否显示客户累计签到次数
     */
    private Boolean showAllSignInNum;

    /**
     * 是否显示客户累计到访次数
     */
    private Boolean showVisitNum;

    /**
     * 签到界面图片
     */
    @NotNull(message = "签到界面图片不能为空")
    private String image;

    /**
     * 刷新频率（单位：秒）
     */
    private Integer refreshFrequency;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 纬度
     */
    private String latitude;

    /**
     * 签到半径（单位：米）
     */
    private Integer radius;

    /**
     * 是否有效，默认1有效，0无效
     */
    private Boolean isActive;

}
