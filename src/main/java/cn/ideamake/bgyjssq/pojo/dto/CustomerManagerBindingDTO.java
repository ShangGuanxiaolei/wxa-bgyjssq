package cn.ideamake.bgyjssq.pojo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/07/01
 * @description 客户-项目
 */
@Data
@Accessors(chain = true)
public class CustomerManagerBindingDTO implements Serializable {

    private String name;
    private String selfLabel;
    private String projectTitle;
}
