package cn.ideamake.bgyjssq.pojo.dto;

import cn.ideamake.bgyjssq.common.annotition.UserId;
import cn.ideamake.bgyjssq.common.enums.CustomerCategory;
import cn.ideamake.bgyjssq.common.validate.EnumVerify;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author imyzt
 * @date 2019/07/05
 * @description 员工-客户信息
 */
@Data
public class VisitorInfoDTO implements Serializable {

    /**
     * 销售id
     */
    @NotBlank(message = "员工ID为必填项")
    @JsonProperty("sellerId")
    @UserId
    private String userUuid;

    /**
     * 访客UUID
     */
    @NotBlank(message = "客户ID为必填项")
    @JsonProperty("visitorId")
    private String visitorUuid;

    private String visitorName;

    private String visitorPhone;

    /**
     * 备注
     */
    private String remark;
}
