package cn.ideamake.bgyjssq.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author imyzt
 * @date 2019/07/09
 * @description 跟办对象
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FollowUp {

    /**
     * 用于跳转时作为销售id
     */
    private String id;

    /**
     * 用于页面展示销售姓名
     */
    private String name;
}
