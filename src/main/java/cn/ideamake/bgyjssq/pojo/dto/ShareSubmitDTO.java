package cn.ideamake.bgyjssq.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author: Walt
 * @Data: 2019-07-23 14:55
 * @Version: 1.0
 */
@Data
public class ShareSubmitDTO {

    /**
     * 分享人id
     */
    @NotNull(message = "用户id不能为空")
    private String userId;

    /**
     * 分享人类型0-访客，1-销售
     */
    @NotNull(message = "用户类型不能为空")
    private Integer userType;

    /**
     * 分享内容的类型，暂定1-文章，2-海报，3-项目主页，4-楼书其他待定
     */
    @NotNull(message = "分享资源类型不能为空")
    private Integer resourceType;

    /**
     * 资源id
     */
    private Integer resourceId;

    /**
     * 项目id，默认-1表示不归属项目
     */
    private Integer projectId = -1;

    /**
     * 分享id，由前端生成
     */
    @NotNull(message = "分享id不能为空")
    private String shareId;

    /**
     * 销售id
     */
    @NotNull(message = "销售id不能为空")
    private String sellerId;
}
