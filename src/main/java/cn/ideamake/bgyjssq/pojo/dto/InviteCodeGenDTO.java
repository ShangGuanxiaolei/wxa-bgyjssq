package cn.ideamake.bgyjssq.pojo.dto;

import cn.ideamake.bgyjssq.pojo.query.WeChatQRQuery;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Set;

/**
 * @Author: Walt
 * @Data: 2019-06-21 14:42
 * @Version: 1.0
 */
@Data
public class InviteCodeGenDTO {
    /**
     * 项目列表
     */
    private Set<Integer> projects;

    /**
     * 微信二维码参数
     */
    @JsonProperty("qr_param")
    private WeChatQRQuery qrParam;
}
