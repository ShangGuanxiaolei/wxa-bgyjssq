package cn.ideamake.bgyjssq.common.mybatisplus;

import cn.hutool.core.collection.CollUtil;
import cn.ideamake.bgyjssq.common.annotition.OssPrefixAutoFill;
import cn.ideamake.bgyjssq.common.util.AliyunOssKit;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Properties;

/**
 * @author imyzt
 * @date 2019/07/19
 * @description OSS前缀自动填充
 */
@Slf4j
@Intercepts({
        @Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class})
})
@Component
public class OssPrefixAutoFillInterceptor implements Interceptor {

    private final AliyunOssKit aliyunOssKit;

    public OssPrefixAutoFillInterceptor(AliyunOssKit aliyunOssKit) {
        this.aliyunOssKit = aliyunOssKit;
    }

    @Override
    public Object intercept(Invocation invocation) throws Throwable {

        Object result = invocation.proceed();

        if (null != result) {

            // 先判断是否列表, 如果是列表,判断泛型的包是否是vo包
            if (result instanceof List) {
                List resultList = (List) result;
                // 集合不为空,取出第一号元素,对比vo包名
                Object obj;
                if (CollUtil.isNotEmpty(resultList) && (obj = resultList.get(0)) != null) {
                    if (checkPackage(obj)) {
                        resultList.parallelStream().forEach(r -> {
                            try {
                                autoFillOssPrefix(r);
                            } catch (IllegalAccessException e) {
                                log.error("前缀填充失败, 当前元素不做处理, 继续下一个元素");
                            }
                        });
                    }
                }
            } else if (checkPackage(result)) {
                autoFillOssPrefix(result);
            }
        }
        return result;
    }

    private void autoFillOssPrefix(Object target) throws IllegalAccessException {
        Field[] declaredFields = target.getClass().getDeclaredFields();
        if (declaredFields.length > 0) {
            for (Field field : declaredFields) {
                // 如果属性带有注解
                if (field.isAnnotationPresent(OssPrefixAutoFill.class)) {
                    field.setAccessible(true);
                    Object value = field.get(target);
                    String returnUrl = checkReturnUrl((String) value);
                    field.set(target, returnUrl);
                }
            }
        }
    }

    /**
     * 检查包名是否符合填充规则
     *
     * @param obj 对象
     */
    private boolean checkPackage(Object obj) {
        if (obj == null) {
            return false;
        }
        String packageName = obj.getClass().getPackage().getName();
        String voPackageName = "cn.ideamake.bgyjssq.pojo.vo";
        return null != packageName && packageName.contains(voPackageName);
    }


    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {

    }

    private String checkReturnUrl(String url) {
        if (null == url) {
            return null;
        }
        if ("".equals(url)) {
            return "";
        }
        if (url.contains("http")) {
            return url;
        } else {
            return aliyunOssKit.getUrlByFileName(url);
        }

    }
}
