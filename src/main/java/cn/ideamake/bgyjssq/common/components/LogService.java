package cn.ideamake.bgyjssq.common.components;

import cn.ideamake.bgyjssq.pojo.entity.AdminLog;
import cn.ideamake.bgyjssq.pojo.entity.UserLog;
import cn.ideamake.bgyjssq.pojo.entity.VisitorLog;
import cn.ideamake.bgyjssq.service.IUserLogService;
import cn.ideamake.bgyjssq.service.IVisitorLogService;
import cn.ideamake.bgyjssq.service.permission.IAdminLogService;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @Author: Walt
 * @Data: 2019-06-25 13:40
 * @Version: 1.0
 */
@AllArgsConstructor
@Service
public class LogService {

    private final IUserLogService userLogService;

    private final IVisitorLogService visitorLogService;

    private final IAdminLogService adminLogService;

    private final static String OPERATOR_TYPE = "TRANSFER_BINDING";

    @Async("LogTaskExecutor")
    public void recordVisitorLog(String visitorUuid, String content) {
        VisitorLog visitorLog = new VisitorLog();
        visitorLog.setVisitorUuid(visitorUuid);
        visitorLog.setOperatorContent(content);
        visitorLogService.save(visitorLog);
    }

    @Async("LogTaskExecutor")
    public void recordUserLog(String visitorUuid, String content, boolean flag) {
        UserLog userLog = new UserLog();
        userLog.setUserUuid(visitorUuid);
        userLog.setOperatorContent(content);
        userLog.setOperatorType(flag ? OPERATOR_TYPE : null);
        userLogService.save(userLog);
    }

    @Async("LogTaskExecutor")
    public void recordSystemUserLog(AdminLog adminLog) {
        adminLogService.save(adminLog);
    }

}
