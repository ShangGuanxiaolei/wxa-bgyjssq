package cn.ideamake.bgyjssq.common.components;

import cn.ideamake.bgyjssq.common.enums.ResourceStatus;
import cn.ideamake.bgyjssq.pojo.entity.Resource;
import cn.ideamake.bgyjssq.service.IResourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author: Walt
 * @Data: 2019-08-14 11:18
 * @Version: 1.0
 */
@Component
@Slf4j
public class PublishCheckTask {

    private final IResourceService resourceService;

    public PublishCheckTask(IResourceService resourceService) {
        this.resourceService = resourceService;
    }

    /**
     * 定时跟新已发布且发布时间大于今天的资源的发布状态
     * 秒，分，时，日，月，周
     * 每天晚上1点执行
     */
    @Scheduled(cron = "0 0 1 ? * *")
    public void updateResourcePublishStatus() {
        log.info("检查状态更新");
        List<Resource> list = resourceService.lambdaQuery().select(Resource::getId, Resource::getStatus).le(Resource::getPublishTime, LocalDateTime.now())
                .eq(Resource::getStatus, ResourceStatus.PENDING_PUBLISH).list();
        if (null != list && !list.isEmpty()) {
            list.forEach(resource -> resource.setStatus(ResourceStatus.PUBLISHED.getCode()));
            resourceService.updateBatchById(list);
        }
        log.info("更新资源内容{}", list);
        log.info("检查执行完毕");
    }

}
