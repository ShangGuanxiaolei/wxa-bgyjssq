package cn.ideamake.bgyjssq.common.components;

import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @Author: Walt
 * @Data: 2019-06-17 13:35
 * @Version: 1.0
 */
@Component
public class AsyncTask {

    /**
     * 用户事件记录，用户记录在接口处记录访客行为
     *
     * @return Executor
     */
    @Bean("LogTaskExecutor")
    public Executor logTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(4);
        executor.setQueueCapacity(400);
        executor.setKeepAliveSeconds(60);
        executor.setThreadNamePrefix("logExecutor-");
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        return executor;
    }

    /**
     * 埋点数据采集记录，用户采集用户动态行为信息
     *
     * @return Executor
     */
    @Bean("CollectorTaskExecutor")
    public Executor collectorTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(4);
        executor.setQueueCapacity(800);
        executor.setKeepAliveSeconds(60);
        executor.setThreadNamePrefix("collector-Executor-");
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        return executor;
    }

}
