package cn.ideamake.bgyjssq.common.util;

/**
 * Created by imyzt on 2018/10/22 14:02
 * 全局系统常量
 *
 * @author ideamake
 */
public interface Constants {

    /**
     * 小程序访问分析排名检索类型
     */
    interface VISIT_ANALYSE {
        /**
         * 停留时间
         */
        String STAY_TIME = "stayTime";

        /**
         * 到访次数
         */
        String VISITED_TIMES = "visitedTimes";

        /**
         * 推介人数
         */
        String RECOMMEND_PEOPLE = "recommendNumber";

        /**
         * 留电人数
         */
        String LEFT_PHONE = "leftPhone";

        /**
         * 分享次数
         */
        String SHARE_TIMES = "shareTimes";
    }

    /**
     * 刷新令牌有效期, 分钟为单位
     */
    Integer REFRESH_TOKEN_VALIDITY_PERIOD = 30;

    /**
     * 默认销售id
     */
    String DEFAULT_SELLER_UUID = "1169795000059404289";

    /**
     * 游客id
     */
    String TOURIST_UUID = "2019091115093131182";

    /**
     * 用户类别
     */
    interface USER_TYPE {
        /**
         * 员工标志
         */
        Integer STAFF = 1;

        /**
         * 访客标志
         */
        Integer VISITOR = 0;

        /**
         * 管理员
         */
        Integer ADMIN = 2;
    }

    /**
     * 缓存标签
     */
    interface CACHE_PREFIX_LABEL {
        String WECHAT = "BGYJSSQ_WECHAT::";

        String PROJECT_QRCODE = "BGYJSSQ_PROJECT_QRCODE::";

        String WECHAT_FORM_ID = "BGYJSSQ_PROJECT_WECHAT_FORMID";
    }

    interface REDIS_CACHE {

        /**
         * -------------------------------------------------- 前缀 --------------------------------------------------
         */
        String PREFIX = "BGY_JSSQ:";

        String CACHE = PREFIX + "CACHE:";

        /* -------------------------------------------------- 前缀 -------------------------------------------------- */
        /* -------------------------------------------------- 队列 -------------------------------------------------- */
        /* -------------------------------------------------- 队列 -------------------------------------------------- */
        /* -------------------------------------------------- 缓存 -------------------------------------------------- */

        /**
         * 刷新令牌缓存
         */
        String REFRESH_TOKEN = CACHE + "REFRESH_TOKEN_";

        /**
         * 图片上传保存地址
         */
        String UPLOAD_IMAGE = CACHE + "UPLOAD_IMAGE_HASH";
        /* -------------------------------------------------- 缓存 -------------------------------------------------- */
    }

    /**
     * 访客详情分析数据类型
     */
    interface DataType {

        /**
         * 访问数量
         */
        int NUMBER_OF_VISITS = 1;

        /**
         * 访问人数
         */
        int NUMBER_OF_PEOPLE = 2;

        /**
         * 留电人数
         */
        int PEOPLE_OF_REMAIN_PHONE = 3;

        /**
         * 到访人数
         */
        int PEOPLE_OF_ACCESS = 4;

        /**
         * 分享次数
         */
        int SHARE_COUNT = 5;

        /**
         * 分享人数
         */
        int SHARE_PEOPLE = 6;

        /**
         * 阅读次数
         */
        int READ_COUNT = 7;

        /**
         * 阅读人数
         */
        int READ_PEOPLE = 8;

        /**
         * 员工拓展
         */
        int SELLER_RECOMMEND = 9;

        /**
         * 客户转介
         */
        int CUSTOMER_RECOMMEND = 10;
    }

    /**
     * 是否管理组标识
     */
    interface HasGroup {

        /**
         * 无管理组
         */
        int HAS_NOT_GROUP = 0;

        /**
         * 有管理组
         */
        int HAS_GROUP = 1;

    }

    /**
     * 逻辑删除标记
     */
    interface Archive {
        /**
         * 删除
         */
        Integer IS_DELETE = 1;

    }

    /**
     * 后台权限校验的地址前缀
     */
    String CHECK_URL = "/admin";

    String CHECK_URL_CONTEXT = "/bgyjssq/admin";

    /**
     * 访客电话保护
     */
    String PROTECT_CUSTOMER_PHONE = "protectCustomerPhone";

    /**
     * 员工电话保护
     */
    String PROTECT_STAFF_PHONE = "protectStaffInfoWeChatId";

    /**
     * 员工微信保护
     */
    String PROTECT_STAFF_WE_CHAT = "protectStaffInfoPhone";

    /**
     * 超级管理员角色id
     */
    int ROOT = 9;

}
