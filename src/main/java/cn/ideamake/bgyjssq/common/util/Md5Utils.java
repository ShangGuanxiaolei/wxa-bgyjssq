package cn.ideamake.bgyjssq.common.util;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

/**
 * @author imyzt
 */
public class Md5Utils {

    private static final String HEX_NUMS_STR = "0123456789ABCDEF";
    private static final Integer SALT_LENGTH = 12;

    /**
     * 将16进制字符串转换成字节数组
     *
     * @param hex 字符串
     * @return 字节数组
     */
    public static byte[] hexStringToByte(String hex) {
        int len = (hex.length() / 2);
        byte[] result = new byte[len];
        char[] hexChars = hex.toCharArray();
        for (int i = 0; i < len; i++) {
            int pos = i * 2;
            result[i] = (byte) (HEX_NUMS_STR.indexOf(hexChars[pos]) << 4
                    | HEX_NUMS_STR.indexOf(hexChars[pos + 1]));
        }
        return result;
    }

    /**
     * 将指定byte数组转换成16进制字符串
     *
     * @param b 字节数组
     * @return 字符串
     */
    public static String byteToHexString(byte[] b) {
        StringBuilder hexString = new StringBuilder();
        for (byte value : b) {
            String hex = Integer.toHexString(value & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            hexString.append(hex.toUpperCase());
        }
        return hexString.toString();
    }

    /**
     * 验证口令是否合法
     *
     * @param password     用户输入的密码
     * @param passwordInDb 数据库中的密码
     * @return 校验结果
     * @throws NoSuchAlgorithmException 异常
     */
    public static boolean validPassword(String password, String passwordInDb)
            throws NoSuchAlgorithmException {
        //将16进制字符串格式口令转换成字节数组
        byte[] pwdInDb = hexStringToByte(passwordInDb);
        //声明盐变量
        byte[] salt = new byte[SALT_LENGTH];
        //将盐从数据库中保存的口令字节数组中提取出来
        System.arraycopy(pwdInDb, 0, salt, 0, SALT_LENGTH);
        //创建消息摘要对象
        MessageDigest md = MessageDigest.getInstance("MD5");
        //将盐数据传入消息摘要对象
        md.update(salt);
        //将口令的数据传给消息摘要对象
        md.update(password.getBytes(StandardCharsets.UTF_8));
        //生成输入口令的消息摘要
        byte[] digest = md.digest();
        //声明一个保存数据库中口令消息摘要的变量
        byte[] digestInDb = new byte[pwdInDb.length - SALT_LENGTH];
        //取得数据库中口令的消息摘要
        System.arraycopy(pwdInDb, SALT_LENGTH, digestInDb, 0, digestInDb.length);
        //比较根据输入口令生成的消息摘要和数据库中消息摘要是否相同
        //口令正确返回口令匹配消息
        //口令不正确返回口令不匹配消息
        return Arrays.equals(digest, digestInDb);
    }

    /**
     * 获得加密后的16进制形式口令
     *
     * @param password 密码
     * @return 加密后的密码
     * @throws NoSuchAlgorithmException 异常
     */
    public static String getEncryptedPwd(String password) throws NoSuchAlgorithmException {
        //声明加密后的口令数组变量
        byte[] pwd;
        //随机数生成器
        SecureRandom random = new SecureRandom();
        //声明盐数组变量   12
        byte[] salt = new byte[SALT_LENGTH];
        //将随机数放入盐变量中
        random.nextBytes(salt);

        //声明消息摘要对象
        MessageDigest md;
        //创建消息摘要
        md = MessageDigest.getInstance("MD5");
        //将盐数据传入消息摘要对象
        md.update(salt);
        //将口令的数据传给消息摘要对象
        md.update(password.getBytes(StandardCharsets.UTF_8));
        //获得消息摘要的字节数组
        byte[] digest = md.digest();

        //因为要在口令的字节数组中存放盐，所以加上盐的字节长度
        pwd = new byte[digest.length + SALT_LENGTH];
        //将盐的字节拷贝到生成的加密口令字节数组的前12个字节，以便在验证口令时取出盐
        System.arraycopy(salt, 0, pwd, 0, SALT_LENGTH);
        //将消息摘要拷贝到加密口令字节数组从第13个字节开始的字节
        System.arraycopy(digest, 0, pwd, SALT_LENGTH, digest.length);
        for (byte b : pwd) {
            System.out.print(b);
        }
        //将字节数组格式加密后的口令转化为16进制字符串格式的口令
        return byteToHexString(pwd);
    }


    /**
     * 为了让access_token 和 refresh_token 产生联系. 生成一个伪随机
     *
     * @param accessToken accessToken
     * @return String
     */
    public static String createRefreshToken(String accessToken) {
        // 拿accessToken中的54个字符的int类型, 最高122, accessToken总计263个字符
        int c = (int) accessToken.charAt(54);
        c = c < 50 ? c + 50 : c;
        // 假装倒转一下
        String reverse = StrUtil.reverse(accessToken.substring(c - 30));
        // md5两次
        return DigestUtil.md5Hex(DigestUtil.md5Hex(reverse)) + c;
    }

}
