package cn.ideamake.bgyjssq.common.util;

import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.exception.MultipartFileToByteException;
import cn.ideamake.bgyjssq.common.properties.AliyunOssProperties;
import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.PutObjectResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * @author imyzt
 * 阿里云 OSS 工具
 */
@Component
@Slf4j
public class AliyunOssKit implements InitializingBean {

    private final AliyunOssProperties ossProperties;
    private OSSClient ossClient;
    private String bucketName;
    private String projectName;
    /**
     * https://<bucket>.<endpoint>/<projectName>/<fileName>
     */
    private static final String FILE_URL_FORMAT = "https://%s/%s/%s";

    /**
     * 上传文件类型
     */
    private static final String FILE_TYPE = ".zip";

    @Value("${floor_book.url}")
    private String floorBookUrl;

    public AliyunOssKit(AliyunOssProperties ossProperties) {
        this.ossProperties = ossProperties;
    }

    @Override
    public void afterPropertiesSet() {
        bucketName = ossProperties.getBucketName();
        projectName = ossProperties.getProjectName();
        ossClient = new OSSClient(ossProperties.getEndpoint(),
                ossProperties.getAccessKeyId(), ossProperties.getAccessKeySecret());
    }

    /**
     * 获取文件名, 格式为: projectName/UUID
     *
     * @param name 文件原始名称
     */
    private String getFileName(String name) {
        String suffix = StringUtils.substring(name, StringUtils.lastIndexOf(name, "."));
        return UUID.randomUUID() + suffix;
    }

    /**
     * 获取文件名, 格式为: UUID
     */
    private String getFileName() {
        return UUID.randomUUID().toString();
    }

    /**
     * 判断存储空间是否存在
     */
    public boolean doesBucketExist() {
        return ossClient.doesBucketExist(bucketName);
    }

    /**
     * 判断文件是否存在
     *
     * @param key 文件名
     */
    public boolean doesObjectExist(String key) {
        return ossClient.doesObjectExist(bucketName, key);
    }

    /**
     * 上传文件到OSS
     *
     * @param file 文件
     * @return 文件名, 下载时通过文件名下载
     */
    public String upload(MultipartFile file) {
        String fileName = getFileName(file.getOriginalFilename());
        String key = projectName + "/" + fileName;
        PutObjectResult putObjectResult;
        try {
            putObjectResult = ossClient.putObject(bucketName, key, new ByteArrayInputStream(file.getBytes()));
        } catch (IOException e) {
            throw new MultipartFileToByteException();
        }
        log.debug(putObjectResult.toString());
        return fileName;
    }

    /**
     * 上传zip包到OSS，720楼书使用
     *
     * @param file      文件
     * @param projectId 项目id
     * @return 上传成功后的文件路径
     */
    public String uploadZip(MultipartFile file, Integer projectId) {
        String fileName = file.getOriginalFilename();
        assert fileName != null;
        String suffix = StringUtils.substring(fileName, StringUtils.lastIndexOf(fileName, "."));
        String prefix = fileName.substring(0, fileName.indexOf("."));
        if (!FILE_TYPE.equals(suffix)) {
            throw new BusinessException("请上传zip文件");
        }
        String path = projectName + "/";
        String name = path + projectId + "_" + System.currentTimeMillis();
        String key = name + suffix;
        PutObjectResult putObjectResult;
        try {
            putObjectResult = ossClient.putObject(bucketName, key, new ByteArrayInputStream(file.getBytes()));
        } catch (IOException e) {
            throw new MultipartFileToByteException();
        }
        log.debug(putObjectResult.toString());
        log.info(floorBookUrl + name + "/" + prefix);
        return floorBookUrl + name + "/" + prefix;
    }

    /**
     * 上传文件到OSS
     *
     * @param file 文件
     * @return 文件名, 下载时通过文件名下载
     */
    public String upload(MultipartFile file, String newFile) {
        try {
            if (newFile != null && doesObjectExist(newFile)) {
                ossClient.deleteObject(bucketName, newFile);
            }
        } catch (OSSException | ClientException e) {
            e.printStackTrace();
        }
        return upload(file);
    }

    /**
     * 上传文件到OSS, 认为文件带后缀
     *
     * @param file 文件流
     * @return 文件名, 下载时通过文件名下载
     */
    public String upload(byte[] file) {
        return upload(file, true);
    }

    /**
     * 上传文件到OSS
     *
     * @param file       文件流
     * @param suffixBool 是否包含后缀
     * @return 文件名, 下载时通过文件名下载
     */
    public String upload(byte[] file, boolean suffixBool) {
        if (suffixBool) {
            String fileName = getFileName();
            String key = projectName + "/" + fileName;
            PutObjectResult putObjectResult = ossClient.putObject(bucketName, key, new ByteArrayInputStream(file));
            log.debug(putObjectResult.toString());
            return fileName;
        } else {
            return upload(file);
        }
    }

    /**
     * 上传文件到OSS
     *
     * @param file   文件流
     * @param suffix 文件后缀
     * @return 文件名, 下载时通过文件名下载
     */
    public String upload(byte[] file, String suffix) {
        String fileName = getFileName() + suffix;
        String key = projectName + "/" + fileName;
        PutObjectResult putObjectResult = ossClient.putObject(bucketName, key, new ByteArrayInputStream(file));
        log.debug(putObjectResult.toString());
        return fileName;
    }


    /**
     * 下载文件到本地
     *
     * @param fileName 文件名
     * @return 文件流
     */
    public InputStream download(String fileName) {

        if (doesObjectExist(fileName)) {
            OSSObject object = ossClient.getObject(ossProperties.getBucketName(), fileName);
            log.info(object.getKey());
            return object.getObjectContent();
        }
        return null;
    }

    /**
     * 根据文件名称获取文件地址
     *
     * @param fileName 文件名称
     * @return 文件地址
     */
    public String getUrlByFileName(String fileName) {
        if (null == fileName) {
            return "";
        }
        return String.format(FILE_URL_FORMAT,
                ossProperties.getEndpoint(), ossProperties.getProjectName(), fileName);
    }

    public String getOssUrlPrefix() {
        return getUrlByFileName("");
    }

    public String checkReturnUrl(String url) {
        if (null == url) {
            return null;
        }
        if ("".equals(url)) {
            return "";
        }
        if (url.contains("http")) {
            return url;
        } else {
            return getUrlByFileName(url);
        }

    }
}
