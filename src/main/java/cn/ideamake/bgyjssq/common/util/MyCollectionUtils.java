package cn.ideamake.bgyjssq.common.util;

import java.util.LinkedHashSet;
import java.util.List;

/**
 * @author gxl
 * @version 1.0
 * @description 自定义集合操作工具类
 * @date 2019-08-15 17:22
 */
public class MyCollectionUtils {

    /**
     * 使用 LinkedHashSet 进行去重操作，保持元素排序
     *
     * @param list List<String>
     */
    public static void removeDuplicate(List<String> list) {
        LinkedHashSet<String> set = new LinkedHashSet<>(list.size());
        set.addAll(list);
        list.clear();
        list.addAll(set);
    }
}
