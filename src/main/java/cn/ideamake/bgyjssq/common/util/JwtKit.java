package cn.ideamake.bgyjssq.common.util;


import cn.ideamake.bgyjssq.pojo.bo.Audience;
import cn.ideamake.bgyjssq.pojo.bo.Token;
import cn.ideamake.bgyjssq.pojo.vo.UserSessionVO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import java.util.Date;

/**
 * @author ideamake
 */
public class JwtKit {

    /**
     * 解析前台的JWT信息
     *
     * @param jsonWebToken   JWT串
     * @param base64Security 64位加密密钥
     */
    public static Claims parseJwt(String jsonWebToken, String base64Security) {
        return Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(base64Security))
                .parseClaimsJws(jsonWebToken).getBody();
    }

    /**
     * 创建JWT
     *
     * @param userSessionVO 用户
     * @param audience      JWT配置信息
     */
    public static Token createJwt(UserSessionVO userSessionVO, Audience audience, HttpServletRequest req) {

        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(audience.getBase64Secret());
        SecretKeySpec secretKeySpec = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        JwtBuilder jwtBuilder = Jwts.builder().setHeaderParam("typ", "JWT")
                .claim("userId", userSessionVO.getUserId())
                .claim("sessionKey", null != userSessionVO.getSessionKey() ? userSessionVO.getSessionKey() : "")
                .claim("roleId", userSessionVO.getRoleId())
                .claim("adminId", userSessionVO.getAdminId())
                .setIssuer(audience.getClientId())
                .setAudience(audience.getName())
                .signWith(signatureAlgorithm, secretKeySpec);

        long expMillis = nowMillis + audience.getExpiresSecond() * 1000;
        Date exp = new Date(expMillis);

        jwtBuilder.setExpiration(exp).setNotBefore(now);

        // 刷新令牌和访问令牌
        String accessToken = jwtBuilder.compact();
        String refreshToken = Md5Utils.createRefreshToken(accessToken);

        return new Token(accessToken, refreshToken);
    }
}
