package cn.ideamake.bgyjssq.common.util;

import cn.ideamake.bgyjssq.pojo.bo.URLEntity;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;

/**
 * @Author: Walt
 * @Data: 2019-08-29 15:46
 * @Version: 1.0
 */
public class UrlUtil {

    /**
     * 解析url
     *
     * @param url url
     * @return URLEntity
     */
    public static URLEntity parse(String url) {

        URLEntity entity = new URLEntity();
        if (url == null) {
            return entity;
        }
        url = url.trim();
        if ("".equals(url)) {
            return entity;
        }
        try {
            url = URLDecoder.decode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String[] urlParts = url.split("\\?");
        entity.baseUrl = urlParts[0];
        //没有参数
        if (urlParts.length == 1) {
            return entity;
        }
        //有参数
        String[] params = urlParts[1].split("&");
        entity.params = new HashMap<>(16);
        for (String param : params) {
            String[] keyValue = param.split("=");
            if (keyValue.length > 1) {
                entity.params.put(keyValue[0], keyValue[1]);
            }
        }
        return entity;
    }

    /**
     * 测试
     *
     * @param args args
     */
    public static void main(String[] args) {
        URLEntity entity = parse(null);
        System.out.println(entity.baseUrl + "\n" + entity.params);
        entity = parse("pages/articleView?resourceId=82&promotionType=2&sourceUserType=1&shareId=df2ab34c7d&sourceUserid=1165208520481800194&title=ismisszhao01&projectId=-1&url=https%3A%2F%2Ftest.ideamake.cn%2Ftest1%2Fapi&resourceType=1&o=2&originSellerId=&");
        System.out.println(entity.baseUrl + "\n" + entity.params);
        entity = parse("http://www.123.com?id=1");
        System.out.println(entity.baseUrl + "\n" + entity.params);
        entity = parse("http://www.123.com?id=1&name=小明");
        System.out.println(entity.baseUrl + "\n" + entity.params);
    }
}
