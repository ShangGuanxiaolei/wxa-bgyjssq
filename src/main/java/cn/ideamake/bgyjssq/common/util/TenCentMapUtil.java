package cn.ideamake.bgyjssq.common.util;

import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpStatus;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author imyzt
 * @date 2019/07/09
 * @description 腾讯地图工具
 */
@Component
public class TenCentMapUtil {

    private static final String MAP_URL = "https://apis.map.qq.com/ws/geocoder/v1/?location=%s,%s&key=%s";

    private static String tenCentMapKey;

    @Value("${tencent-map.key}")
    public void setTenCentMapKey(String tmKey) {
        tenCentMapKey = tmKey;
    }

    /**
     * 获取地名
     *
     * @param latitude  纬度
     * @param longitude 经度
     * @return 地名
     */
    public static String getAddress(Double latitude, Double longitude) {

        if (null != latitude && null != longitude) {
            String tencentMapUrl = String.format(MAP_URL, latitude, longitude, tenCentMapKey);
            HttpResponse httpResponse = HttpUtil.createGet(tencentMapUrl).execute();
            int status = httpResponse.getStatus();
            if (HttpStatus.HTTP_OK == status) {
                String body = httpResponse.body();
                return getAddress(body);
            }
        }
        return null;
    }

    private static String getAddress(String jsonStr) {
        JSONObject json = JSONObject.parseObject(jsonStr);
        int status = json.getInteger("status");
        if (0 == status) {
            JSONObject result = json.getJSONObject("result");
            return result.getString("address");
        }
        return null;
    }

}
