package cn.ideamake.bgyjssq.common.util;

import cn.binarywang.wx.miniapp.api.WxMaQrcodeService;
import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.WxMaUserService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import cn.ideamake.bgyjssq.common.config.wxconfig.WxMaConfiguration;
import cn.ideamake.bgyjssq.common.config.wxconfig.WxMaProperties;
import cn.ideamake.bgyjssq.common.config.wxconfig.WxMpProperties;
import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.exception.WechatCreateQrCodeException;
import cn.ideamake.bgyjssq.pojo.bo.WeChatPhone;
import cn.ideamake.bgyjssq.pojo.dto.CodeToTokenDTO;
import cn.ideamake.bgyjssq.pojo.query.WeChatQRQuery;
import cn.ideamake.bgyjssq.pojo.vo.SignatureVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.bean.WxJsapiSignature;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.util.crypto.SHA1;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @Author: Walt
 * @Data: 2019-05-15 20:04
 * @Version: 1.0
 */
@AllArgsConstructor
@Component
@Slf4j
public class WeChatService {

    private final WxMaProperties wxMaProperties;

    private final WxMpService wxService;

    private final WxMpProperties wxMpProperties;

    private final RedisTemplate<String, String> redisTemplate;

    private static final Integer WX_LONG_URL_REQ_LIMIT = 32;

    /**
     * redis key过期时间
     */
    private static final Integer TIMEOUT = 7 << 10;

    /**
     * jspTicket Key
     */
    private static final String TICKET = "wxa-bgyjssq_jsapi_ticket";

    /**
     * 微信授权码
     *
     * @param code code
     * @return WxMaJscode2SessionResult
     */
    @Deprecated
    public WxMaJscode2SessionResult code2Session(String code) {
        final WxMaService wxService = WxMaConfiguration.getMaService(wxMaProperties.getConfigs().get(0).getAppid());
        WxMaJscode2SessionResult session;
        try {
            session = wxService.getUserService().getSessionInfo(code);
        } catch (WxErrorException e) {
            log.error(e.getMessage());
            throw new BusinessException("微信授权失败");
        }
        return session;
    }


    /**
     * 微信授权码
     *
     * @param dto dto
     * @return WxMaJscode2SessionResult
     */
    public WxMaJscode2SessionResult codeAndEncryptedData2Session(CodeToTokenDTO dto) {
        final WxMaService wxService = WxMaConfiguration.getMaService(wxMaProperties.getConfigs().get(0).getAppid());
        WxMaJscode2SessionResult session;
        try {
            session = wxService.getUserService().getSessionInfo(dto.getCode());
            if (StringUtils.isBlank(session.getUnionid()) && StringUtils.isNotBlank(dto.getEncryptedData())) {
                WxMaUserInfo userInfo = wxService
                        .getUserService()
                        .getUserInfo(session.getSessionKey(), dto.getEncryptedData(), dto.getIv());
                Optional.ofNullable(userInfo).ifPresent(
                        u -> session.setUnionid(userInfo.getUnionId())
                );
                //后续优化更新用户微信信息在此处处理#TODO
            } else {
                return session;
            }
        } catch (WxErrorException e) {
            log.error(e.getMessage());
            throw new BusinessException("微信授权失败");
        }
        return session;
    }

    /**
     * 获取公众号授权信息
     *
     * @param code code
     * @return WxMpUser
     */
    public WxMpUser oauth2getUserInfo(String code) {
        String appId = wxMpProperties.getConfigs().get(0).getAppId();
        if (!this.wxService.switchover(appId)) {
            throw new IllegalArgumentException(String.format("未找到对应appId=[%s]的配置，请核实！", appId));
        }
        try {
            WxMpOAuth2AccessToken accessToken = wxService.oauth2getAccessToken(code);
            return wxService.oauth2getUserInfo(accessToken, null);
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取签名算法
     *
     * @param url 小程序请求地址
     * @return WxJsapiSignature
     */
    public WxJsapiSignature getWxJsApiSignature(String url) {
        WxJsapiSignature signature = null;
        try {
            signature = wxService.createJsapiSignature(url);
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return signature;
    }

    /**
     * 获取签名信息
     *
     * @param url 签名路径
     * @return SignatureVO
     */
    @Deprecated
    public SignatureVO getSignature(String url) {
        String appId = wxMpProperties.getConfigs().get(0).getAppId();
        if (!this.wxService.switchover(appId)) {
            throw new IllegalArgumentException(String.format("未找到对应appId=[%s]的配置，请核实！", appId));
        }
        String ticket = getJspTicketFromCache();
        try {
            if (StringUtils.isBlank(ticket)) {
                ticket = wxService.getJsapiTicket();
                cacheJspTicket(ticket);
            }
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        String nonceStr = UUID.randomUUID().toString();
        String timestamp = System.currentTimeMillis() + "";
        String str = "jsapi_ticket=" +
                ticket +
                "noncestr=" +
                nonceStr +
                "&timestamp=" +
                timestamp +
                "&url=" +
                url;
        String signature = SHA1.gen(str);
        if (ticket == null || StringUtils.isBlank(signature)) {
            log.error("签名失败，参数ticket:{}, signature:{}", ticket, signature);
            throw new BusinessException("签名失败");
        }
        return new SignatureVO(appId, timestamp, nonceStr, signature);
    }

    /**
     * 缓存凭证
     *
     * @param ticket 凭证
     */
    private void cacheJspTicket(String ticket) {
        if (StringUtils.isBlank(ticket)) {
            throw new BusinessException("凭证不能为空");
        }
        redisTemplate.opsForValue().set(TICKET, ticket, TIMEOUT, TimeUnit.SECONDS);
    }

    /**
     * 从缓存中获取凭证
     *
     * @return java.lang.String
     */
    private String getJspTicketFromCache() {
        return redisTemplate.opsForValue().get(TICKET);
    }

    /**
     * 微信授权码
     *
     * @param weChatPhone weChatPhone
     * @return String
     */
    public String decodeUserPhone(WeChatPhone weChatPhone) {
        final WxMaService wxService = WxMaConfiguration.getMaService(wxMaProperties.getConfigs().get(0).getAppid());
        WxMaJscode2SessionResult session;
        try {
            session = wxService.getUserService().getSessionInfo(weChatPhone.getCode());

        } catch (WxErrorException e) {
            log.error(e.getMessage());
            throw new BusinessException("微信授权失败", e);
        }
        log.info(weChatPhone.toString());
        log.info("code:{}", weChatPhone.getCode());
        log.info("session{}", session);
        WxMaUserService userService = wxService.getUserService();
        WxMaPhoneNumberInfo phoneNoInfo = userService.getPhoneNoInfo(session.getSessionKey(), weChatPhone.getEncryptedData(), weChatPhone.getIv());
        return phoneNoInfo.getPhoneNumber();
    }

    /**
     * 微信授权码
     *
     * @param weChatPhone weChatPhone
     * @return String
     */
    public String decodeUserPhoneUseTokenSessionKey(WeChatPhone weChatPhone) {
        final WxMaService wxService = WxMaConfiguration.getMaService(wxMaProperties.getConfigs().get(0).getAppid());
        log.info(weChatPhone.toString());
        log.info("code:{}", weChatPhone.getCode());
        WxMaPhoneNumberInfo wxMaPhoneNumberInfo = wxService.getUserService().getPhoneNoInfo(weChatPhone.getSessionKey(), weChatPhone.getEncryptedData(), weChatPhone.getIv());
        return wxMaPhoneNumberInfo.getPhoneNumber();
    }


    /**
     * 根据定制信息生成相应的小程序二维码
     *
     * @param query 参数
     */
    public byte[] getAppletsCode(WeChatQRQuery query) {

        final WxMaService wxService = WxMaConfiguration.getMaService(wxMaProperties.getConfigs().get(0).getAppid());

        WxMaQrcodeService qrcodeService = wxService.getQrcodeService();
        try {
            if (query.getScene().length() > WX_LONG_URL_REQ_LIMIT) {
                log.warn("参数大于32位限制，调用128字节接口生成二维码");
                return qrcodeService.createWxaCodeBytes(
                        query.getPage() + "?scene=" + query.getScene(),
                        query.getWidth(),
                        query.isAutoColor(),
                        query.getLineColor(),
                        query.isHyaline()
                );
            }
            log.info("参数小于32位");
            return qrcodeService.createWxaCodeUnlimitBytes(
                    query.getScene(),
                    query.getPage(),
                    query.getWidth(),
                    query.isAutoColor(),
                    query.getLineColor(),
                    query.isHyaline()
            );
        } catch (WxErrorException e) {
            throw new WechatCreateQrCodeException();
        }
    }

}
