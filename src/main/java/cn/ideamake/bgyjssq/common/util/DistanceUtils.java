package cn.ideamake.bgyjssq.common.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author gxl
 * @version 1.0
 * @description 距离计算工具类
 * @date 2019-09-19 09:04
 */
public class DistanceUtils {

    /**
     * @param coordinate1 定位1
     * @param coordinate2 定位2
     * @return double
     */
    public static double getDistance(Coordinate coordinate1, Coordinate coordinate2) {
        double long1 = coordinate1.getLongitude();
        double lat1 = coordinate1.getLatitude();
        double long2 = coordinate2.getLongitude();
        double lat2 = coordinate2.getLatitude();

        // 地球半径
        double r = 6378137;

        lat1 = lat1 * Math.PI / 180.0;
        lat2 = lat2 * Math.PI / 180.0;

        double a = lat1 - lat2;
        double b = (long1 - long2) * Math.PI / 180.0;

        double sa2 = Math.sin(a / 2.0);
        double sb2 = Math.sin(b / 2.0);

        return 2 * r * Math.asin(Math.sqrt(sa2 * sa2 + Math.cos(lat1) * Math.cos(lat2) * sb2 * sb2));
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Coordinate {

        /**
         * 经度
         */
        double longitude;

        /**
         * 纬度
         */
        double latitude;
    }

}
