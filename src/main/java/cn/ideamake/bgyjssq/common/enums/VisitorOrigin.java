package cn.ideamake.bgyjssq.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @author imyzt
 * @date 2019/07/05
 * @description 访客来源枚举类
 */
@Getter
@ToString
@AllArgsConstructor
public enum VisitorOrigin {

    /**
     * 微楼书
     */
    MICRO_BOOK(1, "微楼书"),
    /**
     * 微图文
     */
    MICRO_TEXT(2, "微图文"),
    /**
     * 微海报
     */
    MICRO_POSTER(3, "微海报"),
    /**
     * 小程序
     */
    APPLET(4,"小程序"),
    /**
     * 专属码
     */
    CODE(5, "专属码"),
    /**
     * 自搜索
     */
    SEARCH(6, "自搜索"),
    /**
     * 其他项
     */
    OTHER(7, "其他项")

    ;

    @EnumValue
    private Integer code;
    private String value;

    @JsonValue
    public String getValue() {
        return value;
    }
}
