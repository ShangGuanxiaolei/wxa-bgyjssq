package cn.ideamake.bgyjssq.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @author imyzt
 * @date 2019/07/02
 * @description 系统内容发布状态,约定同一套标准
 */
@ToString
@AllArgsConstructor
@Getter
public enum ContentPostStatus {

    /**
     * 待发布
     */
    PENDING_RELEASE(1, "待发布"),
    /**
     * 已发布
     */
    PUBLISHED(2, "已发布"),
    /**
     * 已下架
     */
    REMOVED(3, "已下架"),

    ;

    @EnumValue
    private Integer code;
    private String value;

    @JsonValue
    public String getValue() {
        return value;
    }
}
