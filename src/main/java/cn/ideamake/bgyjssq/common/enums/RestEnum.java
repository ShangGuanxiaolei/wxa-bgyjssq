package cn.ideamake.bgyjssq.common.enums;

import lombok.Getter;

@Getter
public enum RestEnum {

    /** 成功 */
    SUCCESS(0, " 操作成功"),

    /** 内部错误 */
    ERROR(500, "内部错误"),


    /** 业务异常 */
    FAIL(500101, "业务异常"),
    /** 参数错误 */
    PARAMETER_ERROR(500102, "参数错误"),
    /**
     * 文件转换出错
     */
    MULTIPART_FILE_TO_BYTE(500103, "文件转换出错"),
    /**
     * 存储空间不存在
     */
    BUCKET_NOT_EXISTS(500104, "存储空间不存在"),
    /**
     * 创建微信太阳码出错
     */
    CREATE_QR_CODE_ERROR(500105, "创建微信太阳码出错"),
    /**
     * 邀请记录生成失败
     */
    INVITE_CODE_GENERATOR_ERROR(500106, "邀请记录生成失败"),
    /**
     * 邀请记录不存在
     */
    INVITE_RECORD_NOT_FOUND(500107, "邀请记录不存在"),

    OVER_MAX_SIGN_IN_TOTAL(500108, "超过最大签到次数"),

    OVER_MAX_TODAY_SIGN_IN_TOTAL(500109, "超过单日最大签到次数"),

    ACTIVITY_NOT_STARTED(500110, "活动尚未开始，请耐心等待"),

    QR_CODE_EXPIRE(500111, "二维码已失效，请重新获取"),

    ACTIVITY_END(500112, "活动已结束"),

    ACTIVITY_NOT_EXIST(500113, "活动不存在"),

    OUT_OF_SCOPE(500114, "请在指定区域范围内签到"),

    /** token过期 */
    TOKEN_EXPIRED(500201, "token expired"),
    /** 未登录 */
    NEED_LOGIN(500202, "no login"),
    TOKEN_ERROR(500203, "token error"),
    REFRESH_TOKEN_EXPIRED(500204, "refresh token expired"),
    USER_NOT_FOUND(500205, "user not found"),
    NOT_BINDING(500206, "visitor not binding for the project"),

    /** 没有访问权限 */
    NO_ACCESS_PRIVILEGES(500301, "没有该资源的访问权限"),
    /** 需要登录 */
    NO_LOGIN(500302, "need login"),
    ;

    private Integer code;
    private String msg;

    RestEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
