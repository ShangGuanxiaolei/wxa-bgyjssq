package cn.ideamake.bgyjssq.common.enums;

/**
 * 分组类型
 *
 * @author gxl
 */
public enum DictType {

    /**
     * 文章
     */
    ARTICLE,

    /**
     * 海报
     */
    POSTER,

    /**
     * 员工
     */
    STAFF,

    /**
     * 项目
     */
    PROJECT,

    /**
     * 楼书
     */
    FLOOR_BOOK,
    /**
     * 活动组
     */
    ACTIVITY;
}
