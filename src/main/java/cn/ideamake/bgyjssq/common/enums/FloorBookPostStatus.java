package cn.ideamake.bgyjssq.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @author imyzt
 * @date 2019/07/02
 * @description 楼书发布状态
 */
@ToString
@AllArgsConstructor
@Getter
public enum FloorBookPostStatus {

    /**
     * 下架
     */
    UN_PUBLISHED(0, "下架"),
    /**
     * 上架
     */
    PUBLISHED(1, "上架"),


    ;

    @EnumValue
    private Integer code;
    private String value;

    @JsonValue
    public String getValue() {
        return value;
    }
}
