package cn.ideamake.bgyjssq.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @Author: Walt
 * @Data: 2019-08-16 15:47
 * @Version: 1.0
 */
@Getter
@ToString
@AllArgsConstructor
public enum AchievementDigType {
    /**
     * 微楼书
     */
    MICRO_BOOK(1, "微楼书"),
    /**
     * 微图文
     */
    MICRO_TEXT(2, "微图文"),
    /**
     * 微海报
     */
    MICRO_POSTER(3, "微海报"),
    /**
     * 小程序
     */
    APPLET(4,"小程序"),
    /**
     * 专属码
     */
    CODE(5, "专属码"),
    /**
     * 自搜索
     */
    SEARCH(6, "自搜索"),
    /**
     * 其他项
     */
    OTHER(7, "其他项"),
    /**
     * 留电人数
     */
    LEFT_PHONE(10010,"留电人数"),
    /**
     * 来访人数
     */
    VISITED_NUMBER(10011,"来访人数"),
    /**
     * 推介总人数
     */
    RECOMMEND_TOTAL_NUMBER(10012,"推介总人数"),

    /**
     * 推介总人数
     */
    BROWSE_COUNT(10013,"浏览人次"),
    /**
     * 推介总人数
     */
    BROWSE_PEOPLE(10014,"浏览人数"),
    /**
     * 推介总人数
     */
    ADD_PEOPLE(100145,"新增人数"),



    ;

    @EnumValue
    private Integer code;
    private String value;

    @JsonValue
    public String getValue() {
        return value;
    }
}
