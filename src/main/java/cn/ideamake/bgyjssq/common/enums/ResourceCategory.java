package cn.ideamake.bgyjssq.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @author imyzt
 * @date 2019/07/02
 * @description 资源类别
 */
@ToString
@Getter
@AllArgsConstructor
public enum ResourceCategory {

    /**
     * 系统通知
     */
    SYSTEM(1, "系统通知"),
    /**
     * 项目通知
     */
    PROJECT(2, "项目通知")


    ;

    @EnumValue
    private Integer code;
    private String value;

    @JsonValue
    public String getValue() {
        return value;
    }
}
