package cn.ideamake.bgyjssq.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @author imyzt
 * @date 2019/07/02
 * @description 系统内容置顶状态,约定同一套标准
 * 用到的需在此登记
 * 1. 文章发布置顶状态(article表)
 * 2. 海报发布置顶状态(resource表)
 */
@ToString
@AllArgsConstructor
@Getter
public enum ContentTopStatus {

    /**
     * 未置顶
     */
    UN_TOP(0, "否"),

    /**
     * 置顶
     */
    TOP(1, "是")


    ;

    @EnumValue
    private Integer code;
    private String value;

    @JsonValue
    public String getValue() {
        return value;
    }
}
