package cn.ideamake.bgyjssq.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @author imyzt
 * @date 2019/07/11
 * @description 海报类别
 */
@Getter
@AllArgsConstructor
@ToString
public enum PosterCategory {

    /**
     * 系统
     */
    SYSTEM(1, "系统"),
    /**
     * 项目
     */
    PROJECT(2, "项目")


    ;

    @EnumValue
    private Integer code;
    private String value;

    @JsonValue
    public String getValue() {
        return value;
    }
}
