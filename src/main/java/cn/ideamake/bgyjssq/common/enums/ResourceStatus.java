package cn.ideamake.bgyjssq.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @Author: Walt
 * @Data: 2019-08-14 11:26
 * @Version: 1.0
 */
@AllArgsConstructor
@ToString
@Getter
public enum ResourceStatus {
    /**
     * 已发布
     */
    PUBLISHED(2, "已发布"),
    /**
     * 待发布
     */
    PENDING_PUBLISH(1, "待发布"),

    /**
     * 已下架
     */
    REMOVED(3,"已下架");

    @EnumValue
    private Integer code;
    private String value;

    @JsonValue
    public String getValue() {
        return value;
    }
}
