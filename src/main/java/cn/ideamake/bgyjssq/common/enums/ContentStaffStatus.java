package cn.ideamake.bgyjssq.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @author gxl
 * @date 2019/08/10
 * @description 员工履职状态
 */
@ToString
@AllArgsConstructor
@Getter
public enum ContentStaffStatus {

    /**
     * 在职
     */
    INCUMBENCY(1, "在职"),
    /**
     * 离职
     */
    Quit(2, "离职"),

    ;

    @EnumValue
    private Integer code;
    private String value;

    @JsonValue
    public String getValue() {
        return value;
    }
}
