package cn.ideamake.bgyjssq.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @author gxl
 * @date 2019/09/09
 * @description 楼书发布状态
 */
@ToString
@AllArgsConstructor
@Getter
public enum FloorBook720Status {

    /**
     * 关闭
     */
    CLOSE(0, "关闭"),
    /**
     * 开启
     */
    OPEN(1, "开启"),

    ;

    @EnumValue
    private Integer code;
    private String value;

    @JsonValue
    public String getValue() {
        return value;
    }
}
