package cn.ideamake.bgyjssq.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @author imyzt
 * @date 2019/07/06
 * @description 资源表类型
 */
@AllArgsConstructor
@ToString
@Getter
public enum ResourceType {

    /**
     * 文章
     */
    ARTICLE(1),

    /**
     * 海报
     */
    POSTER(2),

    /**
     * 楼书
     */
    FLOOR_BOOK(3),

    /**
     * 首页
     */
    HOME(4),

    ;

    private Integer code;

}
