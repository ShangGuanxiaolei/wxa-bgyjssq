package cn.ideamake.bgyjssq.common.enums;

import lombok.Getter;

/**
 * 权限类型
 * @author ideamake
 */
@Getter
public enum PermissionTypeEnum {

    /**
     * 菜单组件访问权限
     */
    MENU("菜单权限"),

    /**
     * 数据访问权限
     */
    DATA("数据权限"),

    /**
     * 其他权限
     */
    OTHER("其他权限"),

    ;

    /**
     * 权限类型名称
     */
    private String name;

    PermissionTypeEnum(String name) {
        this.name = name;
    }

}
