package cn.ideamake.bgyjssq.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @author imyzt
 * @date 2019/07/26
 * @description 客户类别
 */
@ToString
@AllArgsConstructor
@Getter
public enum CustomerCategory {

    /**
     * 一般访客
     */
    GENERAL_VISITOR(1, "一般访客"),

    /**
     * 意向客户
     */
    INTENTION_CUSTOMERS(2, "意向客户"),

    /**
     * 诚意客户
     */
    SINCERITY_CUSTOMER(3, "诚意客户"),

    /**
     * 项目业主
     */
    PROJECT_OWNER(4, "项目业主")


    ;

    @EnumValue
    private Integer code;
    private String value;

    @JsonValue
    public String getValue() {
        return value;
    }
}
