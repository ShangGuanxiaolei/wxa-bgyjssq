package cn.ideamake.bgyjssq.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author imyzt
 * @date 2019/07/02
 * @description 员工通知类型
 */
@Getter
@AllArgsConstructor
public enum StaffNotificationType {

    /**
     * 海报
     */
    POSTER(2),
    /**
     * 文章
     */
    ARTICLE(1),
    ;


    private Integer code;
}
