package cn.ideamake.bgyjssq.common.config;

/**
 * @Author: Walt
 * @Data: 2019-05-23 19:18
 * @Version: 1.0
 */
public class PositionConstant {

    public static String positionTranslate(int positionCode) {
        switch (positionCode) {
            case 0:
                return "销售组长";
            case 1:
                return "普通员工";
            default:
                return "未知岗位";
        }
    }
}
