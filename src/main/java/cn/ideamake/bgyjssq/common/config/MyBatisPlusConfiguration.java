package cn.ideamake.bgyjssq.common.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @author covey
 */
@Configuration
public class MyBatisPlusConfiguration {
	
	/**
	 * SQL执行效率插件  设置 dev test 环境开启
	 * @return PerformanceInterceptor
	 */
	@Bean
	@Profile({"dev","test","local"})
	public PerformanceInterceptor performanceInterceptor() {
		PerformanceInterceptor interceptor = new PerformanceInterceptor();
		interceptor.setFormat(false);
		interceptor.setMaxTime(1000);
		return interceptor;
	}
	/**
	 * 分页插件
	 */
	@Bean
	public PaginationInterceptor paginationInterceptor() {
		PaginationInterceptor interceptor = new PaginationInterceptor();
		interceptor.setDialectType("mysql");
		return interceptor;
	}
}