package cn.ideamake.bgyjssq.common.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @Author: Walt
 * @Data: 2019-05-16 17:01
 * @Version: 1.0
 *
 *  配置公共字段自动填充功能  @TableField(..fill = FieldFill.INSERT)
 *  特别注意，3.0-gamma之前的版本 MetaObjectHandler 是抽象类
 *  3.0-RC之后的版本MetaObjectHandler 是接口
 */
@Component
public class MetaObjectHandlerConfig implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {

        Object createTime = getFieldValByName("createAt", metaObject);
        Object updateTime = getFieldValByName("updateAt", metaObject);
        if (createTime == null){
            //mybatis-plus版本2.0.9+
            setFieldValByName("createAt", LocalDateTime.now(), metaObject);
        }
        if (updateTime == null){
            //mybatis-plus版本2.0.9+
            setFieldValByName("updateAt",LocalDateTime.now(), metaObject);
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        Object updateTime = getFieldValByName("updateAt", metaObject);
        if (updateTime == null) {
            //mybatis-plus版本2.0.9+
            setFieldValByName("updateAt", LocalDateTime.now(), metaObject);
        }
    }
}