package cn.ideamake.bgyjssq.common.annotition;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author imyzt
 * @date 2019/07/19
 * @description OSS 前缀自动填充
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface OssPrefixAutoFill {
}
