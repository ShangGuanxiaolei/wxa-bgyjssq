package cn.ideamake.bgyjssq.common.oss;

import cn.ideamake.bgyjssq.common.exception.BucketNotFoundException;
import cn.ideamake.bgyjssq.common.util.AliyunOssKit;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;


/**
 * @author imyzt
 * @date 2019/06/25
 * @description 存储空间监听器
 */
@ConditionalOnProperty(prefix = "oss.aliyun", name = "bucket-name")
@Component
public class BucketListener implements ApplicationRunner {

    private final AliyunOssKit aliyunOssKit;

    public BucketListener(AliyunOssKit aliyunOssKit) {
        this.aliyunOssKit = aliyunOssKit;
    }

    @Override
    public void run(ApplicationArguments args) {
        // 启动时判断存储空间是否存在.
        if (!aliyunOssKit.doesBucketExist()) {
            throw new BucketNotFoundException();
        }
    }
}
