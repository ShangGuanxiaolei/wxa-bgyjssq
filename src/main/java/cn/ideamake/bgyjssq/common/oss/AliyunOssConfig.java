package cn.ideamake.bgyjssq.common.oss;

import cn.ideamake.bgyjssq.common.properties.AliyunOssProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author imyzt
 * @date 2019/06/25
 * @description 阿里云OSS参数自动配置
 */
@Configuration
@EnableConfigurationProperties(value = AliyunOssProperties.class)
public class AliyunOssConfig {
}
