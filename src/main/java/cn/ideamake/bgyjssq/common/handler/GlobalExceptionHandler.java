package cn.ideamake.bgyjssq.common.handler;


import cn.ideamake.bgyjssq.common.enums.RestEnum;
import cn.ideamake.bgyjssq.common.exception.AbstractException;
import cn.ideamake.bgyjssq.common.response.Rest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

/**
 * @author ideamake
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public Rest handler(HttpServletRequest req, Object handler, Exception e) {

        Rest rest = new Rest();
        if (e instanceof AbstractException) {
            AbstractException exception = (AbstractException) e;
            rest.setCode(exception.getCode()).setMsg(exception.getMsg());
        } else if (e instanceof MissingServletRequestParameterException) {
            rest.setCode(RestEnum.PARAMETER_ERROR.getCode()).setMsg(RestEnum.PARAMETER_ERROR.getMsg());
        } else if (e instanceof HttpMessageNotReadableException) {
            log.error("请求参数不是JSON字符串, 使用JSON.stringify()转换");
            rest.setCode(RestEnum.PARAMETER_ERROR.getCode()).setMsg(RestEnum.PARAMETER_ERROR.getMsg());
        } else if (e instanceof HttpMediaTypeNotSupportedException) {
            log.error("content Type错误:" + e.getMessage());
            rest.setCode(RestEnum.PARAMETER_ERROR.getCode()).setMsg(RestEnum.PARAMETER_ERROR.getMsg());
            return rest;
        } else if (e instanceof IllegalArgumentException) {
            rest.setCode(RestEnum.PARAMETER_ERROR.getCode()).setMsg(RestEnum.PARAMETER_ERROR.getMsg());
            printException(e, req);
            return rest;
        } else {
            rest.setCode(RestEnum.ERROR.getCode()).setMsg(RestEnum.ERROR.getMsg());
        }
        printException(e, req);
        return rest;
    }

    @ExceptionHandler(value = {BindException.class, MethodArgumentNotValidException.class})
    public Rest handler(Exception exception, HttpServletRequest request) {

        Rest<String> rest = new Rest<>();

        BindingResult bindingResult;
        if (exception instanceof MethodArgumentNotValidException) {
            bindingResult = ((MethodArgumentNotValidException) exception).getBindingResult();
        } else {
            bindingResult = ((BindException) exception).getBindingResult();
        }

        String objectName = Objects.requireNonNull(bindingResult.getFieldError()).getObjectName();
        String field = bindingResult.getFieldError().getField();
        String errorMsg = bindingResult.getFieldError().getDefaultMessage();

        rest.setCode(RestEnum.PARAMETER_ERROR.getCode()).setMsg(errorMsg);

        String requestUri = request.getRequestURI();
        log.error("表单参数错误, 请求地址: {}, 参数对象={}, 错误字段={}, 错误详情={}", requestUri, objectName, field, errorMsg);

        return rest;
    }

    private void printException(Exception e, HttpServletRequest request) {

        String requestUri = request.getRequestURI();
        log.error("请求URI = {}", requestUri);
        printAllReqParam(request);
        if (e instanceof AbstractException) {
            AbstractException exception = (AbstractException) e;
            log.error(exception.getMsg(), e);
            return;
        }

        StackTraceElement traceElement = e.getStackTrace()[0];
        String typeName = traceElement.getClassName();
        String method = traceElement.getMethodName();
        int lineNumber = traceElement.getLineNumber();
        log.error(typeName + "#" + method + "第" + lineNumber + "行出错", e);
    }

    private void printAllReqParam(HttpServletRequest request) {
        Map<String, String[]> parameterMap = request.getParameterMap();
        Set<Entry<String, String[]>> entrySet = parameterMap.entrySet();
        for (Entry<String, String[]> entryKey : entrySet) {
            Object key = entryKey.getKey();
            Object value = entryKey.getValue();
            log.error("参数名称: {}, 参数值: {}", key, value);
        }
    }
}
