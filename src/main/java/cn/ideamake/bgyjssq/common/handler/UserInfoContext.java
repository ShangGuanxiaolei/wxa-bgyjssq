package cn.ideamake.bgyjssq.common.handler;


import cn.ideamake.bgyjssq.common.enums.RestEnum;
import cn.ideamake.bgyjssq.common.exception.AuthenticationException;
import cn.ideamake.bgyjssq.pojo.vo.UserSessionVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 用户日志记录中提取上下文对象
 */
@Component
@Slf4j
public class UserInfoContext {

    private static ThreadLocal<UserSessionVO> USER_INFO = new ThreadLocal<>();

    public static void set(UserSessionVO userSessionVO) {
        USER_INFO.set(userSessionVO);
    }

    public static UserSessionVO get() {
        assertObj();
        return USER_INFO.get();
    }

    public static String currentUserId() {
        assertObj();
        return USER_INFO.get().getUserId();
    }

    public static UserSessionVO userInfo() {
        assertObj();
        return USER_INFO.get();
    }

    public static void remove() {
        USER_INFO.remove();
    }

    private static void assertObj() {
        if (null == USER_INFO.get()) {
            log.error("获取请求上下文中的用户对象, 当前请求上下文中不包含任何用户信息");
            throw new AuthenticationException(RestEnum.NEED_LOGIN);
        }
    }

    private static Integer roleId;

    private static Integer adminId;

    public static Integer getRoleId() {
        return roleId;
    }

    public static void setRoleId(Integer roleId) {
        UserInfoContext.roleId = roleId;
    }

    public static Integer getAdminId() {
        return adminId;
    }

    public static void setAdminId(Integer adminId) {
        UserInfoContext.adminId = adminId;
    }
}
