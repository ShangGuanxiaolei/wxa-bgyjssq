package cn.ideamake.bgyjssq.common.handler;

import cn.ideamake.bgyjssq.common.enums.RestEnum;
import cn.ideamake.bgyjssq.common.exception.BusinessException;
import cn.ideamake.bgyjssq.common.response.Rest;
import cn.ideamake.bgyjssq.common.validate.CheckPermission;
import cn.ideamake.bgyjssq.pojo.entity.SysPermission;
import cn.ideamake.bgyjssq.service.permission.IRolePermissionService;
import cn.ideamake.bgyjssq.service.permission.ISysRoleService;
import cn.ideamake.bgyjssq.web.controller.IController;
import io.jsonwebtoken.lang.Collections;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

/**
 * @author gxl
 * @version 1.0
 * @description aop实现权限校验
 * @date 2019-10-22 15:20
 */
@Slf4j
@AllArgsConstructor
@Aspect
@Component
public class PermissionCheckHandle implements IController {

    /**
     * 数据统计接口地址列表
     */
    private static final List<String> DASHBOARD_URL_LIST = new ArrayList<>();

    /**
     * 业绩排名接口地址列表
     */
    private static final List<String> RANK_URL_LIST = new ArrayList<>();

    /**
     * 活动接口列表
     */
    private static final List<String> Q_R_CODE_URL_LIST = new ArrayList<>();

    /**
     * 数据统计
     */
    private static final String DATA_STATISTICS = "/dataOverview/dashboard";

    /**
     * 业绩排名
     */
    private static final String ACHIEVEMENT_RANKING = "/dataOverview/rank";

    /**
     * 批量操作员工
     */
    private static final String BATCH_UPDATE_STAFF = "/admin/staff/batchUpdateStaff";

    /**
     * 批量操作活动
     */
    private static final String BATCH_UPDATE_QR_CODE = "/admin/sign-in-qr-code/batchUpdate";

    /**
     * 更新管理员状态
     */
    private static final String UPDATE_STATUS_ADMIN = "/admin/sys-admin/updateStatus";

    /**
     * 删除项目
     */
    private static final String DELETE_PROJECT = "/admin/project/delete";

    /**
     * 设置默认销售
     */
    private static final String SET_DEFAULT_SELLER = "/admin/project/setDefaultSeller";

    /**
     * 删除项目楼书
     */
    private static final String DELETE_FLOOR_BOOK = "/admin/resource/deleteFloorBook";

    /**
     * 钻取角色成员
     */
    private static final String DRILLING_ROLE_MEMBER = "/admin/sys-role/listMember";

    /**
     * 删除角色
     */
    private static final String DELETE_ROLE = "/admin/sys-role/delete";

    static {
        DASHBOARD_URL_LIST.add("/admin/analysis/customerVisit");
        DASHBOARD_URL_LIST.add("/admin/analysis/articleDataLineChart");
        DASHBOARD_URL_LIST.add("/admin/analysis/articleDataPieChart");
        DASHBOARD_URL_LIST.add("/admin/analysis/totalVisitTrend");
        DASHBOARD_URL_LIST.add("/admin/analysis/getMiniProgramData");

        RANK_URL_LIST.add("/admin/staff/top10");
        RANK_URL_LIST.add("/admin/customer/top10");

        Q_R_CODE_URL_LIST.add("/admin/sign-in-qr-code/batchActivitiesUpdate");
        Q_R_CODE_URL_LIST.add("/admin/sign-in-qr-code/batchUpdateStatus");
        Q_R_CODE_URL_LIST.add("/admin/sign-in-qr-code/batchBindUser");
    }

    private final ISysRoleService sysRoleService;

    private final IRolePermissionService rolePermissionService;

    @Pointcut(value = "execution(public * cn.ideamake.bgyjssq.web.controller.admin..*.*(..))")
    public void start() {
    }

    @Around("start()")
    public Rest access(ProceedingJoinPoint joinPoint) {
        MethodSignature joinPointObject = (MethodSignature) joinPoint.getSignature();
        //获得请求的方法
        Method method = joinPointObject.getMethod();

        //判断是否需要鉴权
        if (hasAnnotationOnMethod(method)) {
            String requestUri = ((ServletRequestAttributes)
                    Objects.requireNonNull(RequestContextHolder.getRequestAttributes()))
                    .getRequest()
                    .getRequestURI();
            log.info("=============请求的url：" + requestUri);
            checkPermission(requestUri.replace("/bgyjssq", ""));
        }

        Rest obj = null;
        try {
            obj = (Rest) joinPoint.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return obj;
    }

    /**
     * 权限校验
     */
    private void checkPermission(String url) {
        Integer roleId = UserInfoContext.getRoleId();
        if (!Optional.ofNullable(sysRoleService.getById(roleId))
                .orElseThrow(() -> new BusinessException(RestEnum.NO_ACCESS_PRIVILEGES.getMsg()))
                .getStatus()) {
            throw new BusinessException(RestEnum.NO_ACCESS_PRIVILEGES.getMsg());
        }

        List<SysPermission> list = rolePermissionService.selectPermissionListByRoleId(roleId);
        if (Collections.isEmpty(list)) {
            throw new BusinessException(RestEnum.NO_ACCESS_PRIVILEGES.getMsg());
        }
        List<String> interfaceUrlList = list.parallelStream()
                .filter(l -> StringUtils.isNotBlank(l.getInterfaceUrl()))
                .map(SysPermission::getInterfaceUrl)
                .collect(toList());

        if (checkUrlPermission(url, interfaceUrlList)) {
            return;
        }

        throw new BusinessException(RestEnum.NO_ACCESS_PRIVILEGES.getMsg());
    }

    /**
     * url校验
     *
     * @param url              请求接口
     * @param interfaceUrlList 接口列表
     * @return boolean
     */
    private boolean checkUrlPermission(String url, List<String> interfaceUrlList) {
        assert url != null;
        //数据统计
        if (DASHBOARD_URL_LIST.contains(url)
                && interfaceUrlList.contains(DATA_STATISTICS)) {
            return true;
        }

        //业绩排名
        if (RANK_URL_LIST.contains(url)
                && interfaceUrlList.contains(ACHIEVEMENT_RANKING)) {
            return true;
        }

        //活动批量操作
        if (Q_R_CODE_URL_LIST.contains(url)
                && interfaceUrlList.contains(BATCH_UPDATE_QR_CODE)) {
            return true;
        }

        //员工批量操作
        if (url.startsWith(BATCH_UPDATE_STAFF)
                && interfaceUrlList.contains(BATCH_UPDATE_STAFF)) {
            return true;
        }

        //更新管理员状态
        if (url.startsWith(UPDATE_STATUS_ADMIN)
                && interfaceUrlList.contains(BATCH_UPDATE_QR_CODE)) {
            return true;
        }

        //删除项目
        if (url.startsWith(DELETE_PROJECT)
                && interfaceUrlList.contains(DELETE_PROJECT)) {
            return true;
        }

        //设置默认销售
        if (url.startsWith(SET_DEFAULT_SELLER)
                && interfaceUrlList.contains(SET_DEFAULT_SELLER)) {
            return true;
        }

        //删除项目楼书
        if (url.startsWith(DELETE_FLOOR_BOOK)
                && interfaceUrlList.contains(DELETE_FLOOR_BOOK)) {
            return true;
        }

        //删除角色
        if (url.startsWith(DELETE_ROLE)
                && interfaceUrlList.contains(DELETE_ROLE)) {
            return true;
        }

        //钻取角色成员
        if (url.startsWith(DRILLING_ROLE_MEMBER)
                && interfaceUrlList.contains(DRILLING_ROLE_MEMBER)) {
            return true;
        }

        //通常处理
        return interfaceUrlList.contains(url);
    }

    /**
     * 判断某方法上是否含有某注解
     *
     * @param method 方法
     * @return boolean
     */
    private boolean hasAnnotationOnMethod(Method method) {
        //使用反射获取注解信息
        Annotation a = method.getAnnotation(CheckPermission.class);
        return a != null;
    }
}