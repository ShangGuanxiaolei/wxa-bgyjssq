package cn.ideamake.bgyjssq.common.validate;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 用于对访问请求的接口做校验
 * 添加该注解的接口会在拦截器中被拦截，对接口做角色校验
 * 默认是访客角色是0，一些需要更大权限的接口可以在拦截器中做校验
 * <p>
 * 未添加该注解的接口默认携带被认证token即可访问
 *
 * @author ideamake
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthCheck {

    /**
     * 访问接口的描述信息
     */
    String value() default "";

    /**
     * 用户角色，用于接口权限处理
     * 默认访问角色是访客
     */
    int role() default 0;

    /**
     * 是否略过权限校验
     * 默认0，表示不略过权限校验
     */
    int skipAuth() default 0;
}
