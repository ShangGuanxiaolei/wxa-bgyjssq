package cn.ideamake.bgyjssq.common.validate;

import cn.hutool.core.util.EnumUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.BooleanUtils;
import org.joor.Reflect;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.LinkedHashMap;
import java.util.Objects;

/**
 * @author imyzt
 * @date 2019/07/06
 * @description 枚举值校验
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EnumVerify.Validator.class)
public @interface EnumVerify {

    /**
     * 错误提示消息
     */
    String message();

    /**
     * 枚举类
     * 必须包含code属性
     */
    Class<? extends Enum> enumClass();

    /**
     * 为空时不校验,默认不校验
     */
    boolean unVerifyNull() default true;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Slf4j
    class Validator implements ConstraintValidator<EnumVerify, Object> {

        private Class<? extends Enum> enumClass;
        private Boolean verifyNull;

        @Override
        public void initialize(EnumVerify enumVerify) {
            enumClass = enumVerify.enumClass();
            verifyNull = enumVerify.unVerifyNull();
        }

        @Override
        @SuppressWarnings("unchecked")
        public boolean isValid(Object value, ConstraintValidatorContext context) {

            // 需要判空
            if (null == value && BooleanUtils.isTrue(verifyNull)) {
                return Boolean.TRUE;
            }

            LinkedHashMap enumMap = EnumUtil.getEnumMap(enumClass);

            for (Object key : enumMap.keySet()) {
                Object entry = enumMap.get(key);
                if (entry instanceof Enum) {
                    int code = Reflect.on(entry).get("code");
                    if (Objects.equals(code, value)) {
                        return true;
                    }
                } else {
                    log.error("枚举类配置错误.请及时修改");
                    return false;
                }
            }
            return false;
        }
    }

}
