package cn.ideamake.bgyjssq.common.exception;

import cn.ideamake.bgyjssq.common.enums.RestEnum;

/**
 * @author imyzt
 * @date 2019/06/25
 * @description 存储空间不存在异常
 */
public class BucketNotFoundException extends AbstractException {
    public BucketNotFoundException() {
        super(RestEnum.BUCKET_NOT_EXISTS);
    }
}
