package cn.ideamake.bgyjssq.common.exception;

import cn.ideamake.bgyjssq.common.enums.RestEnum;

/**
 * @author imyzt
 * @date 2019/06/26
 * @description 创建微信太阳码出错
 */
public class WechatCreateQrCodeException extends AbstractException {

    public WechatCreateQrCodeException() {
        super(RestEnum.CREATE_QR_CODE_ERROR);
    }
}
