package cn.ideamake.bgyjssq.common.exception;

import cn.ideamake.bgyjssq.common.enums.RestEnum;

/**
 * @author imyzt
 * @date 2019/06/25
 * @description 文件转换异常
 */
public class MultipartFileToByteException extends AbstractException {

    public MultipartFileToByteException() {
        super(RestEnum.MULTIPART_FILE_TO_BYTE);
    }
}
