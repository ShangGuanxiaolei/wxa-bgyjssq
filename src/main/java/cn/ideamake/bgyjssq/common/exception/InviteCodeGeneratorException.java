package cn.ideamake.bgyjssq.common.exception;

import cn.ideamake.bgyjssq.common.enums.RestEnum;

/**
 * @author imyzt
 * @date 2019/06/26
 * @description
 */
public class InviteCodeGeneratorException extends AbstractException {
    public InviteCodeGeneratorException() {
        super(RestEnum.INVITE_CODE_GENERATOR_ERROR);
    }
}
