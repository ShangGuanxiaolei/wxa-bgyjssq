package cn.ideamake.bgyjssq.common.exception;

import cn.ideamake.bgyjssq.common.enums.RestEnum;
import lombok.Getter;

/**
 * @author imyzt
 * @date 2019/1/12 13:56
 * @description AuthenticationException
 */
@Getter
public class AuthenticationException extends AbstractException {

    public AuthenticationException(RestEnum restEnum, Exception e) {
        super(restEnum, e);
    }

    public AuthenticationException(RestEnum restEnum) {
        super(restEnum);
    }
}
