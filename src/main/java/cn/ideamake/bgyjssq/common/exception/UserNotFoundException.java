package cn.ideamake.bgyjssq.common.exception;

import cn.ideamake.bgyjssq.common.enums.RestEnum;
import lombok.extern.slf4j.Slf4j;

/**
 * @author imyzt
 * @date 2019/07/10
 * @description 用户不存在
 */
@Slf4j
public class UserNotFoundException extends AbstractException {

    /**
     * 用户不存在抛出该异常
     *
     * @param id 用户id
     */
    public UserNotFoundException(Object id) {
        super(RestEnum.USER_NOT_FOUND);
        log.error("user id = {}, not found", id);
    }
}
