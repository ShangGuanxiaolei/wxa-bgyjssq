package cn.ideamake.bgyjssq.common.exception;

import cn.ideamake.bgyjssq.common.enums.RestEnum;

/**
 * @Author: Walt
 * @Data: 2019-06-27 11:12
 * @Version: 1.0
 */
public class BusinessException extends AbstractException {
    protected BusinessException(RestEnum restEnum, Exception e) {
        super(restEnum, e);
    }

    public BusinessException(String msg) {
        super(msg);
    }

    public BusinessException(RestEnum restEnum) {
        super(restEnum);
    }

    public BusinessException(String errMsg, Exception e) {
        super(errMsg);
    }
}
