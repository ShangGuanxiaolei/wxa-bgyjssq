package cn.ideamake.bgyjssq.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author imyzt
 * 阿里云 OSS配置文件
 */
@ConfigurationProperties("oss.aliyun")
@Data
public class AliyunOssProperties {

    private String endpoint;
    private String accessKeyId;
    private String accessKeySecret;
    private String bucketName;
    private String projectName;
}
