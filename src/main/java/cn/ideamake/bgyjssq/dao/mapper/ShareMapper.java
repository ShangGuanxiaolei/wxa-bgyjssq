package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.Share;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 分享表，用户记录销售或者访客的分享行为 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-07-23
 */
public interface ShareMapper extends BaseMapper<Share> {

}
