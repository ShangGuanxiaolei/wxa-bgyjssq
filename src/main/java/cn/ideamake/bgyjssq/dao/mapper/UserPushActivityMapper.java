package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.UserPushActivity;
import cn.ideamake.bgyjssq.pojo.vo.PushActivitisVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-08-09
 */
public interface UserPushActivityMapper extends BaseMapper<UserPushActivity> {

    List<PushActivitisVO> isExist(@Param("query")PushActivitisVO pushActivitisVO);

}
