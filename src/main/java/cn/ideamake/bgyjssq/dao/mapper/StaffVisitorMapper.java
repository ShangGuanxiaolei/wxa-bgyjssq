package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.StaffVisitor;
import cn.ideamake.bgyjssq.pojo.query.StaffVisitorQuery;
import cn.ideamake.bgyjssq.pojo.vo.StaffVisitorVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 用户员工对于绑定自己的客户的管理 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-06-19
 */
@Repository
public interface StaffVisitorMapper extends BaseMapper<StaffVisitor> {

    /**
     * 销售-客户列表
     *
     * @param query      查询参数
     * @param page       分页对象
     * @param column     排序列
     * @param desc       是否倒序
     * @param userIdList 用户id列表
     * @return 客户列表
     */
    List<StaffVisitorVO> getStaffVisitorByFilter(@Param("query") StaffVisitorQuery query,
                                                 @Param("page") Page<StaffVisitorVO> page,
                                                 @Param("column") String column,
                                                 @Param("desc") boolean desc,
                                                 @Param("userIdList") List<String> userIdList);

    /**
     * 统计销售管理的所有客户
     *
     * @param userId   销售id
     * @param projects 项目id列表
     * @return Long
     */
    Long countVisitorByUserId(@Param("userId") String userId, @Param("projects") List<Integer> projects);

    /**
     * 统计销售管理的所有客户
     *
     * @param userIdList 销售id列表
     * @param projects   项目id列表
     * @return Long
     */
    Long countVisitorByUserIds(@Param("userIdList") List<String> userIdList, @Param("projects") List<Integer> projects);
}
