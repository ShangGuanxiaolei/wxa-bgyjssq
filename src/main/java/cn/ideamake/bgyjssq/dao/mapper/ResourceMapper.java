package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.common.enums.ResourceType;
import cn.ideamake.bgyjssq.pojo.entity.Resource;
import cn.ideamake.bgyjssq.pojo.query.ResourceListQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffNotificationDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffNotificationQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorNotificationQuery;
import cn.ideamake.bgyjssq.pojo.vo.PVAndUvVO;
import cn.ideamake.bgyjssq.pojo.vo.ResourceBaseVO;
import cn.ideamake.bgyjssq.pojo.vo.ResourceDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.ResourceDigDataQuery;
import cn.ideamake.bgyjssq.pojo.vo.ResourceDigDataVO;
import cn.ideamake.bgyjssq.pojo.vo.ResourceListVO;
import cn.ideamake.bgyjssq.pojo.vo.analysis.VisitorListVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.StaffNotificationGroupVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.StaffNotificationResourceVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.VisitorNotificationGroupVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.VisitorNotificationOldVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.VisitorNotificationResourceVO;
import cn.ideamake.bgyjssq.pojo.vo.notify.VisitorNotificationVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
@Repository
public interface ResourceMapper extends BaseMapper<Resource> {

    /**
     * 获取员工海报列表
     *
     * @param query 查询参数
     * @param page  分页对象
     * @return 海报列表
     */
    List<StaffNotificationResourceVO> getStaffPosterList(@Param("query") StaffNotificationQuery query, @Param("page") Page<StaffNotificationResourceVO> page);

    /**
     * 获取资源列表-除了大文本
     *
     * @param query    参数
     * @param page     分页
     * @param typeId   资源类型 {@link ResourceType}
     * @param dictType 字典类型 {@link cn.ideamake.bgyjssq.common.enums.DictType}
     * @return 资源列表
     */
    IPage<ResourceListVO> selectResourceVoList(@Param("page") Page<ResourceListVO> page,
                                               @Param("query") ResourceListQuery query,
                                               @Param("typeId") Integer typeId,
                                               @Param("dictType") String dictType);

    /**
     * 通过项目id获取资源列表
     *
     * @param page
     * @param resourceType
     * @param projectId
     * @param grouping
     * @return
     */
    IPage<ResourceBaseVO> selectResourcesByProjectId(@Param("page") IPage<ResourceBaseVO> page,
                                                     @Param("resourceType") Integer resourceType,
                                                     @Param("projectId") Integer projectId,
                                                     @Param("grouping") Integer grouping
    );

    /**
     * 获取员工文章资源列表
     *
     * @param query             查询参数
     * @param page              分页对象
     * @param publishStatusCode 类型
     * @param resourceDictType  分组
     * @param userIdList        销售列表
     * @return 文章列表
     */
    List<StaffNotificationResourceVO> getStaffResource(@Param("query") StaffNotificationQuery query,
                                                       @Param("page") Page<StaffNotificationResourceVO> page,
                                                       @Param("publishStatusCode") Integer publishStatusCode,
                                                       @Param("resourceDictType") String resourceDictType,
                                                       @Param("userIdList") List<String> userIdList);

    /**
     * 获取员工资源（暂时只处理文章）组列表
     *
     * @param query             查询参数
     * @param page              分页对象
     * @param publishStatusCode 类型
     * @param resourceDictType  分组
     * @param userIdList        销售列表
     * @return 资源列表
     */
    List<StaffNotificationGroupVO> getStaffResourceGroup(@Param("query") StaffNotificationQuery query,
                                                         @Param("page") Page<StaffNotificationGroupVO> page,
                                                         @Param("publishStatusCode") Integer publishStatusCode,
                                                         @Param("resourceDictType") String resourceDictType,
                                                         @Param("userIdList") List<String> userIdList);

    /**
     * 查询访客通知分组
     *
     * @param query            查询条件
     * @param page             分页
     * @param resourceDictType 资源类型
     * @return List<VisitorNotificationGroupVO>
     */
    IPage<VisitorNotificationGroupVO> getVisitorResourceGroup(
            @Param("page") Page<VisitorNotificationGroupVO> page,
            @Param("query") VisitorNotificationQuery query,
            @Param("resourceDictType") String resourceDictType);

    /**
     * 查询pv和uv
     *
     * @param resourceId 资源id
     * @param userIdList 用户id
     * @return List<PVAndUvVO>
     */
    PVAndUvVO selectPvAndUv(@Param("resourceId") Integer resourceId, @Param("userIdList") List<String> userIdList);

    /**
     * 获取访客的通知
     *
     * @param page              page
     * @param vnf               vnf
     * @param publishStatusCode publishStatusCode
     * @param articleDictType   文章对应的字典表的type
     * @return IPage<VisitorNotificationVO>
     */
    IPage<VisitorNotificationOldVO> getVisitorNotification(@Param("page") Page<VisitorNotificationVO> page,
                                                           @Param("vn") VisitorNotificationQuery vnf,
                                                           @Param("publishStatusCode") Integer publishStatusCode,
                                                           @Param("articleDictType") String articleDictType);

    /**
     * 获取访客的通知
     *
     * @param page              page
     * @param vnf               vnf
     * @param publishStatusCode publishStatusCode
     * @param articleDictType   文章对应的字典表的type
     * @return IPage<VisitorNotificationResourceVO>
     */
    IPage<VisitorNotificationResourceVO> getVisitorNotificationNew(@Param("page") Page<VisitorNotificationVO> page,
                                                                   @Param("vn") VisitorNotificationQuery vnf,
                                                                   @Param("publishStatusCode") Integer publishStatusCode,
                                                                   @Param("articleDictType") String articleDictType);

    /**
     * 查询文章访问详情
     *
     * @param query 查询条件
     * @return List<VisitorListVO>
     */
    List<VisitorListVO> selectArticleDetailByArticleId(@Param("query") StaffNotificationDetailQuery query);

    /**
     * 根据资源类型和资源id查找资源
     *
     * @param resourceTypeId 资源类型
     * @param id             资源id
     * @return 资源
     */
    ResourceDetailVO getResourceByTypeAndId(@Param("id") Integer id, @Param("resourceTypeId") Integer resourceTypeId);

    /**
     * 首页特殊处理
     *
     * @param id
     * @param resourceTypeId
     * @return
     */
    ResourceDetailVO getResourceByTypeProjectId(@Param("projectId") Integer id, @Param("resourceTypeId") Integer resourceTypeId);


    /**
     * 通过项目id列表获取资源组id列表
     *
     * @param projects projects
     * @param type     type
     * @return List<Integer>
     */
    List<Integer> getGroupsByProjects(@Param("projects") List<Integer> projects, @Param("type") String type);

    /**
     * 资源访问数据钻取接口
     *
     * @param page
     * @param query
     * @return
     */
    IPage<ResourceDigDataVO> digDataForResource(@Param("page") Page<ResourceDigDataVO> page, @Param("query") ResourceDigDataQuery query);


}
