package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.VisitorLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户日志记录表 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-06-20
 */
public interface VisitorLogMapper extends BaseMapper<VisitorLog> {

}
