package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.ClickRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 点击行为记录统计表 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-08-06
 */
public interface ClickRecordMapper extends BaseMapper<ClickRecord> {

}
