package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.Recommend;
import cn.ideamake.bgyjssq.pojo.query.RecommendDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.RecommendQuery;
import cn.ideamake.bgyjssq.pojo.vo.CustomerShareVO;
import cn.ideamake.bgyjssq.pojo.vo.RecommendCustomerVO;
import cn.ideamake.bgyjssq.pojo.vo.RecommendDetailPageVO;
import cn.ideamake.bgyjssq.pojo.vo.RecommendPageVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 推荐记录管理 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-06-06
 */
public interface RecommendMapper extends BaseMapper<Recommend> {

    /**
     * 查询推荐列表
     *
     * @param myPage         myPage
     * @param recommendQuery recommendQuery
     * @return IPage<RecommendPageVO>
     */
    IPage<RecommendPageVO> selectRecommendVO(Page<RecommendPageVO> myPage,
                                             @Param("rq") RecommendQuery recommendQuery);

    /**
     * 查询推荐详情
     *
     * @param myPage               myPage
     * @param recommendDetailQuery recommendDetailQuery
     * @return IPage<RecommendDetailPageVO>
     */
    IPage<RecommendDetailPageVO> selectRecommendDetailVO(Page<RecommendDetailPageVO> myPage,
                                                         @Param("rd") RecommendDetailQuery recommendDetailQuery);

    /**
     * 查询推荐客户列表
     *
     * @param page     分页
     * @param sourceId 推荐客户id
     * @return IPage<RecommendCustomerVO>
     */
    IPage<RecommendCustomerVO> selectRecommendCustomer(Page<RecommendCustomerVO> page, @Param("sourceId") String sourceId);

    /**
     * 查询客户分享数据
     *
     * @param page     分页
     * @param sourceId 客户id
     * @return IPage<CustomerShareVO>
     */
    IPage<CustomerShareVO> selectCustomerShare(Page<CustomerShareVO> page, @Param("sourceId") String sourceId);
}
