package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.SysRole;
import cn.ideamake.bgyjssq.pojo.vo.permission.SysRoleVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 系统角色表，记录系统中的角色信息 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Repository
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 分页查询角色列表
     *
     * @param page 分页参数
     * @param name 角色名称
     * @return 分页数据
     */
    IPage<SysRoleVO> findByPage(Page<SysRoleVO> page, @Param("name") String name);

    /**
     * 禁用角色
     *
     * @param id 角色id
     * @return int
     */
    @Update("UPDATE im_sys_role SET status = 0 WHERE id = #{id}")
    int prohibit(Integer id);

    /**
     * 查询有效角色
     *
     * @return 角色列表
     */
    @Select("SELECT id, name, status FROM im_sys_role WHERE status = 1")
    List<SysRole> selectEffectiveRole();
}
