package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.UserGroupManager;
import cn.ideamake.bgyjssq.pojo.vo.UserBaseVO;
import cn.ideamake.bgyjssq.pojo.vo.UserGroupVO;
import cn.ideamake.bgyjssq.pojo.vo.UserListVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * <p>
 * 员工分组管理表 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-08-13
 */
public interface UserGroupManagerMapper extends BaseMapper<UserGroupManager> {
    /**
     * 获取组管理员列表
     *
     * @param groupId
     * @return
     */
    List<UserBaseVO> selectManagerByGroupId(Integer groupId);

    /**
     * 根据用户id查询组id
     *
     * @param sellerId   用户id
     * @param projectIds 项目id列表
     * @return List<Integer>
     */
    List<Integer> getGroupIdList(@Param("sellerId") String sellerId, @Param("projectIds") List<Integer> projectIds);

    /**
     * 高等级角色获取管理的员工id列表
     *
     * @param groupIds 组id列表
     * @return List<String>
     */
    List<String> getSellerIdListByGroupIds(@Param("groupIds") List<Integer> groupIds);

    /**
     * 根据用户id查询员工id列表
     *
     * @param sellerId 用户id
     * @return List<String>
     */
    List<String> getSellerIdListByUserId(String sellerId);

    /**
     * 获取组成员列表
     *
     * @param groupId
     * @return
     */
    List<UserBaseVO> selectUserByGroupId(Integer groupId);

    List<UserBaseVO> selectUsers();

    /**
     * 查询所有员工分组
     *
     * @param projectIds 项目id列表
     * @return List<UserGroupVO>
     */
    List<UserGroupVO> getGroup(@Param("projectIds") List<Integer> projectIds);

    /**
     * 根据项目id查询销售组
     *
     * @param projectId 项目id
     * @return List<UserGroupVO>
     */
    List<UserGroupVO> getGroupByProjectId(@Param("projectId") Integer projectId);

    /**
     * 根据组id查询员工
     *
     * @param groupId 组id
     * @return List<UserListVO>
     */
    List<UserListVO> getUser(@Param("groupId") Integer groupId);

    /**
     * 查询未分组员工
     *
     * @return List<UserListVO>
     */
    List<UserListVO> getUserNoGroup();

}
