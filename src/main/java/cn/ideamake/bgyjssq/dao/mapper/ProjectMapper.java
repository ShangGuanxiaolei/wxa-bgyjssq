package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.dto.ProjectBaseInfoDTO;
import cn.ideamake.bgyjssq.pojo.entity.Project;
import cn.ideamake.bgyjssq.pojo.query.VisitorProjectQuery;
import cn.ideamake.bgyjssq.pojo.vo.ProjectGroupListVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.floorbook.ProjectFloorBook720VO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 项目表 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
@Repository
public interface ProjectMapper extends BaseMapper<Project> {

    /**
     * 查询项目(楼盘)列表
     *
     * @param page            分页信息
     * @param query           查询参数
     * @param projectDictType 项目字典类型
     * @return 项目列表
     */
    IPage<VisitorProjectVO> selectProjectList(@Param("page") Page<Object> page,
                                              @Param("query") VisitorProjectQuery query,
                                              @Param("projectDictType") String projectDictType);

    /**
     * 查询项目(720楼书)列表
     *
     * @param page            分页信息
     * @param query           查询参数
     * @param projectDictType 项目字典类型
     * @return 项目列表
     */
    IPage<ProjectFloorBook720VO> selectProjectFloorBook720List(@Param("page") Page<Object> page,
                                                               @Param("query") VisitorProjectQuery query,
                                                               @Param("projectDictType") String projectDictType);

    /**
     * pc-查询楼盘组列表,组包含多少项目
     *
     * @param page            分页信息
     * @param projectDictType 楼盘组字典类型
     * @return 列表
     */
    IPage<ProjectGroupListVO> selectProjectGroupList(@Param("page") Page<ProjectGroupListVO> page,
                                                     @Param("projectDictType") String projectDictType);

    /**
     * 通过组id获取组下所有项目的基本信息
     *
     * @param groupId
     * @return
     */
    List<ProjectGroupListVO> selectProjectBaseByGroupId(Integer groupId);

    /**
     * 根据销售id查询销售管理的项目基础信息列表
     *
     * @param sellerId 销售id
     * @return 楼盘列表
     */
    List<ProjectBaseInfoDTO> selectProjectBaseInfoListBySellerId(@Param("sellerId") String sellerId);

    /**
     * 根据用户id列表查询项目id列表
     *
     * @param userIdList 用户id列表
     * @return List<Integer>
     */
    List<Integer> selectProjectIdListByUserId(@Param("userIdList") List<String> userIdList);

    /**
     * 根据用户id查询项目名称列表
     *
     * @param userId 用户id
     * @return List<String>
     */
    List<String> selectProjectNameByUserId(@Param("userId") String userId);
}
