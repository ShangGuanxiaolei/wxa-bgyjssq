package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.Advertisement;
import cn.ideamake.bgyjssq.pojo.query.AdvertisementQuery;
import cn.ideamake.bgyjssq.pojo.vo.AdvertisementVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-08-07
 */
public interface AdvertisementMapper extends BaseMapper<Advertisement> {

    /**
     * 查询广告列表
     *
     * @param page  分页
     * @param query 传条件
     * @return IPage<AdvertisementVO>
     */
    IPage<AdvertisementVO> selectByAdver(Page<AdvertisementVO> page, @Param("query") AdvertisementQuery query);

    /**
     * 根据项目id查询有效广告
     *
     * @param projectId 项目id
     * @param type      广告类型
     * @return AdvertisementVO
     */
    AdvertisementVO selectEffectiveAdver(@Param("projectId") Integer projectId, @Param("type") Integer type);

}
