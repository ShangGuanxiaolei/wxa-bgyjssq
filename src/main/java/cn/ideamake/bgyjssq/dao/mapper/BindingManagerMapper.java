package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.bo.VisitorActivityStatistics;
import cn.ideamake.bgyjssq.pojo.dto.CustomerManagerDTO;
import cn.ideamake.bgyjssq.pojo.entity.BindingManager;
import cn.ideamake.bgyjssq.pojo.query.CustomerListQuery;
import cn.ideamake.bgyjssq.pojo.query.CustomerManagerQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorAttentionQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorHistoryBindQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorProjectQuery;
import cn.ideamake.bgyjssq.pojo.vo.CustomerInfoDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.CustomerListVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorAttentionVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorHistoryBindVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorProjectVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 员工的访客管理 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-06-12
 */
@Repository
public interface BindingManagerMapper extends BaseMapper<BindingManager> {
    /**
     * 查询访客历史绑定信息
     *
     * @param myPage
     * @param visitorHistoryBindQuery
     * @return
     */
    IPage<VisitorHistoryBindVO> selectVisitorHistoryBind(Page<VisitorHistoryBindVO> myPage, @Param("vhb") VisitorHistoryBindQuery visitorHistoryBindQuery, @Param("ossurl") String ossUrl);

    /**
     * 查询访客我的关注
     *
     * @param myPage
     * @param visitorAttentionQuery
     * @return
     */
    IPage<VisitorAttentionVO> selectVisitorAttention(Page<VisitorAttentionVO> myPage, @Param("va") VisitorAttentionQuery visitorAttentionQuery, @Param("ossurl") String ossUrl);

    /**
     * 更加条件获取访客相关项目
     *
     * @param myPage
     * @param visitorProjectQuery
     * @return
     */
    IPage<VisitorProjectVO> selectConditionAttention(Page<VisitorAttentionVO> myPage, @Param("vp") VisitorProjectQuery visitorProjectQuery);

    /**
     * 查询客户列表
     *
     * @param query  查询对象
     * @param myPage 分页对象
     * @return 简单信息
     */
    List<CustomerManagerDTO> selectCustomerList(@Param("query") CustomerManagerQuery query, Page<CustomerManagerDTO> myPage);

    /**
     * 获取访客动态信息列表
     *
     * @param visitorUuid             访客id
     * @param sellerId                销售id
     * @param articleResourceTypeCode 文章资源类型id
     * @return 访客动态信息列表
     */
    List<VisitorActivityStatistics> getVisitorDynamicInfoList(@Param("visitorUuid") String visitorUuid,
                                                              @Param("sellerId") String sellerId,
                                                              @Param("articleResourceTypeCode") Integer articleResourceTypeCode);

    /**
     * 高级角色获取访客动态信息列表
     * @param visitorUuid 访客id
     * @param articleResourceTypeCode 文章资源类型id
     * @param userIdList 销售id列表
     * @return List<VisitorActivityStatistics>
     */
    List<VisitorActivityStatistics> getVisitorDynamicInfoListForLeader(
            @Param("visitorUuid") String visitorUuid,
            @Param("articleResourceTypeCode") Integer articleResourceTypeCode,
            @Param("userIdList") List<String> userIdList);

    /**
     * 客户列表(PC后台)
     *
     * @param query 查询参数
     * @param page  分页对象
     * @return 返回列表对象
     */
    IPage<CustomerListVO> customerListQuery(@Param("page") Page<CustomerListVO> page,
                                            @Param("query") CustomerListQuery query);

    /**
     * 根据访客id获取访客的详细信息(PC后台)
     *
     * @param visitorId 访客id
     * @param projectId 项目id
     * @param sellerId  销售id
     * @return bindingManager表的唯一一条记录
     */
    CustomerInfoDetailVO getCustomerInfoDetail(@Param("visitorId") String visitorId,
                                               @Param("projectId") String projectId,
                                               @Param("sellerId") String sellerId);
}
