package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 用户角色绑定表，记录用户和角色的绑定关系 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-06-06
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

    /**
     * 根据上级id查询用户id列表
     *
     * @param parentId 上级id
     * @return List<UserRole>
     */
    List<UserRole> selectByParentId(Integer parentId);

}
