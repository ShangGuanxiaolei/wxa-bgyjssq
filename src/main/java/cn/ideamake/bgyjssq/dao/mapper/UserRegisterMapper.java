package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.UserRegister;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 管理员工邀请注册的接口 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-06-21
 */
public interface UserRegisterMapper extends BaseMapper<UserRegister> {

}
