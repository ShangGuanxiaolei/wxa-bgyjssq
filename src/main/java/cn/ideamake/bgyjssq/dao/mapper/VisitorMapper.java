package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.Visitor;
import cn.ideamake.bgyjssq.pojo.query.TopTenQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorLogQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorVisitAnalysisQuery;
import cn.ideamake.bgyjssq.pojo.vo.CustomerTopTenVO;
import cn.ideamake.bgyjssq.pojo.vo.FocusProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorLogVO;
import cn.ideamake.bgyjssq.pojo.vo.analysis.DataAnalysisDetailListVO;
import cn.ideamake.bgyjssq.pojo.vo.analysis.VisitorListVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 访客表 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
public interface VisitorMapper extends BaseMapper<Visitor> {

    /**
     * 查询访客操作日志
     *
     * @param page            page
     * @param visitorLogQuery visitorLogQuery
     * @return IPage<VisitorLogVO>
     */
    IPage<VisitorLogVO> selectVisitorLog(Page<VisitorLogVO> page, @Param("vl") VisitorLogQuery visitorLogQuery);

    /**
     * 客户top10
     *
     * @param query 参数
     * @param sort  排序字段
     * @return top10列表
     */
    List<CustomerTopTenVO> selectCustomerTopTenList(@Param("query") TopTenQuery query, @Param("sort") String sort);

    /**
     * 根据销售id查询访客列表
     *
     * @param query      查询条件
     * @param userIdList 销售id列表
     * @return List<VisitorListVO>
     */
    List<VisitorListVO> selectVisitorByUserId(@Param("query") VisitorVisitAnalysisQuery query,
                                              @Param("userIdList") List<String> userIdList);

    /**
     * 查询客户关注项目信息
     *
     * @param page   分页
     * @param userId 客户id
     * @return IPage<FocusProjectVO>
     */
    IPage<FocusProjectVO> selectFocusProject(Page<FocusProjectVO> page, @Param("userId") String userId);

    /**
     * 获取访客拓客数据
     *
     * @param uuid 访客id
     * @return DataAnalysisDetailListVO
     */
    DataAnalysisDetailListVO getVisitorVO(String uuid);
}
