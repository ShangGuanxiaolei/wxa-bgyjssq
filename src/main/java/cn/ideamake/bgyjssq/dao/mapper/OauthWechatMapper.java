package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.OauthWechat;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户微信登录表 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-06-11
 */
public interface OauthWechatMapper extends BaseMapper<OauthWechat> {

}
