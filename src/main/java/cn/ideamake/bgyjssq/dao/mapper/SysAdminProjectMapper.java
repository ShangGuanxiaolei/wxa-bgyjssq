package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.SysAdminProject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统账户项目权限表 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-10-21
 */
public interface SysAdminProjectMapper extends BaseMapper<SysAdminProject> {

}
