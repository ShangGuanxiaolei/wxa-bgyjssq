package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.UserLog;
import cn.ideamake.bgyjssq.pojo.query.user.UserLogQuery;
import cn.ideamake.bgyjssq.pojo.vo.user.UserLogVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
public interface UserLogMapper extends BaseMapper<UserLog> {

    /**
     * 销售获取日志
     *
     * @param pageInfo 分页数据
     * @param query    查询条件
     * @return IPage<UserLogVO>
     */
    IPage<UserLogVO> selectUserLog(Page<Object> pageInfo, @Param("query") UserLogQuery query);
}
