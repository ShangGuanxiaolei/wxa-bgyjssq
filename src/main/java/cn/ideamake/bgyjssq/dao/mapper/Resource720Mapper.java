package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.Resource720;
import cn.ideamake.bgyjssq.pojo.vo.floorbook.FloorBook720VO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 720楼书 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-09-03
 */
public interface Resource720Mapper extends BaseMapper<Resource720> {

    /**
     * 查询720楼书列表
     *
     * @param projectId 项目id
     * @param status    楼书状态
     * @return List<FloorBook720VO>
     */
    List<FloorBook720VO> selectFloorBook(@Param("projectId") Integer projectId, @Param("status") Integer status);
}
