package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.SysAdmin;
import cn.ideamake.bgyjssq.pojo.query.AdminQuery;
import cn.ideamake.bgyjssq.pojo.vo.permission.SysAdminVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 系统管理员列表 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Repository
public interface SysAdminMapper extends BaseMapper<SysAdmin> {

    /**
     * 分页查询管理员列表
     *
     * @param page       分页
     * @param adminQuery 参数
     * @return 分页数据
     */
    IPage<SysAdminVO> selectByPage(Page<SysAdminVO> page, @Param("adminQuery") AdminQuery adminQuery);

}
