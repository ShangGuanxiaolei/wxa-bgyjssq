package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.Dict;
import cn.ideamake.bgyjssq.pojo.query.StaffGroupQuery;
import cn.ideamake.bgyjssq.pojo.vo.DictVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 字典表，用于记录相同字段结构 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Repository
public interface DictMapper extends BaseMapper<Dict> {

    /**
     * 根据类型分页查询分组数据
     *
     * @param page 分页
     * @param type 分组类型
     * @param query 查询条件
     * @return 分组数据
     */
    IPage<DictVO> selectByType(Page<DictVO> page, @Param("type") String type, @Param("query") StaffGroupQuery query);

}
