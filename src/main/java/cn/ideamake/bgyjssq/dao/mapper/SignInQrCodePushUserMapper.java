package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.SignInQrCodePushUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 签到二维码推送员工列表 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-09-16
 */
public interface SignInQrCodePushUserMapper extends BaseMapper<SignInQrCodePushUser> {

}
