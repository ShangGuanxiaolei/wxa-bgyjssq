package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.User;
import cn.ideamake.bgyjssq.pojo.query.DigDataQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffKpiQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffListQuery;
import cn.ideamake.bgyjssq.pojo.query.StaffVisitorQuery;
import cn.ideamake.bgyjssq.pojo.query.UserProjectQuery;
import cn.ideamake.bgyjssq.pojo.query.UserVisitorProjectQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorInfoDetailQuery;
import cn.ideamake.bgyjssq.pojo.vo.DigDataVO;
import cn.ideamake.bgyjssq.pojo.vo.PushedActivityVO;
import cn.ideamake.bgyjssq.pojo.vo.SellerActivitisVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffKpiVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffListVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffNameVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffVisitorVO;
import cn.ideamake.bgyjssq.pojo.vo.UserProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.UserVisitorProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorInfoDetailVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

    /**
     * 根据条件获取员工项目
     *
     * @param page
     * @param userProjectQuery
     * @return
     */
    IPage<UserProjectVO> selectProjectForStaff(Page<UserProjectVO> page, @Param("up") UserProjectQuery userProjectQuery, @Param("ossurl") String ossurl);

    /**
     * 小程序-获取访客基本信息
     *
     * @param visitorInfoDetailQuery 查询参数
     * @return 访客基本信息
     */
    VisitorInfoDetailVO selectVisitorBaseInfo(@Param("vi") VisitorInfoDetailQuery visitorInfoDetailQuery);

    /**
     * 高等级角色获取访客基本信息
     *
     * @param visitorInfoDetailQuery 查询条件
     * @param userIdList             销售列表
     * @return VisitorInfoDetailVO
     */
    VisitorInfoDetailVO selectVisitorBaseInfoForLeader(
            @Param("vi") VisitorInfoDetailQuery visitorInfoDetailQuery,
            @Param("userIdList") List<String> userIdList);

    /**
     * 获取员工访客统计信息
     *
     * @param page
     * @param staffVisitorQuery
     * @return
     */
    IPage<StaffVisitorVO> selectStaffVisitorByFilter(Page<StaffVisitorVO> page, @Param("sv") StaffVisitorQuery staffVisitorQuery);

    /**
     * 获取对应项目的销售信息
     *
     * @param userVisitorProjectQuery
     * @return
     */
    UserVisitorProjectVO selectProjectUserInfo(@Param("uvp") UserVisitorProjectQuery userVisitorProjectQuery);

    /**
     * PC后台-员工列表
     *
     * @param query 查询参数
     * @param page  分页
     * @return 列表
     */
    List<StaffListVO> getStaffInfoList(@Param("query") StaffListQuery query,
                                       @Param("page") Page<StaffListVO> page);

    /**
     * 查询员工业绩
     *
     * @param query 参数
     * @param page  分页对象
     * @return 获取员工业绩
     */
    List<StaffKpiVO> selectStaffKpiList(@Param("query") StaffKpiQuery query, @Param("page") Page<StaffKpiVO> page);

    /**
     * pc后台销售业绩管理数据钻取,留电人数需特殊处理,用10010来标记
     *
     * @param page  分页对象
     * @param query 参数
     * @return top10销售业绩管理钻取和销售业绩钻取
     */
    IPage<DigDataVO> selectDigDataForStaffAchievement(Page<DigDataVO> page, @Param("query") DigDataQuery query);

    /**
     * pc后台销售业绩管理数据钻取,留电人数需特殊处理,用10010来标记
     *
     * @param page  分页对象
     * @param query 参数
     * @return top10销售业绩管理钻取和销售业绩钻取
     */
    IPage<DigDataVO> selectDigDataForCustomerAchievement(Page<DigDataVO> page, @Param("query") DigDataQuery query);

    /**
     * 查询员工业绩
     *
     * @param sellerId 参数 销售ID
     * @return 获取员工业绩
     */
    List<SellerActivitisVO> getSellerActivities(@Param("sellerId") String sellerId);

    /**
     * 查询可分享员工
     *
     * @param projectId 参数 项目ID
     * @return 列表
     */
    List<StaffNameVO> getSellerName(@Param("projectId") Integer projectId);

    /**
     * 获得被推送的活动
     *
     * @param userUuid 参数
     * @return 列表
     */
    List<PushedActivityVO> getPushActivity(@Param("userUuid") String userUuid);
}
