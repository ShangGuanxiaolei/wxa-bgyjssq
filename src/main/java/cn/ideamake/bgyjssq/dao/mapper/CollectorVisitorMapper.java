package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.CollectorVisitor;
import cn.ideamake.bgyjssq.pojo.query.DashBoardQuery;
import cn.ideamake.bgyjssq.pojo.query.DistributionMapQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorVisitAnalysisQuery;
import cn.ideamake.bgyjssq.pojo.vo.CustomerDistributionMapVO;
import cn.ideamake.bgyjssq.pojo.vo.RecommendAnalysisVO;
import cn.ideamake.bgyjssq.pojo.vo.dashboard.AccessDetailVO;
import cn.ideamake.bgyjssq.pojo.vo.dashboard.CustomerVisitData;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.OriginCountCell;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.TotalStatistics;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.VisitPeriod;
import cn.ideamake.bgyjssq.pojo.vo.visit.analyse.VisitTrendCell;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 项目访问采集 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Repository
public interface CollectorVisitorMapper extends BaseMapper<CollectorVisitor> {

    /**
     * pc后台获取访问项目访问次数(pv)统计，此统计没有销售纬度
     */
    List<VisitTrendCell> getPcPvForProjects(@Param("query")DashBoardQuery dashBoardQuery);

    /**
     * pc后台获取访问项目访问人数(uv)统计，此统计没有销售纬度
     */
    List<VisitTrendCell> getPcUvForProjects(@Param("query")DashBoardQuery dashBoardQuery);
    /**
     * pc后台获取留电人数，此统计没有销售纬度
     */
    List<VisitTrendCell> getPcRemainPhoneForProjects(@Param("query")DashBoardQuery dashBoardQuery);

    /**
     * pc后台获取来访人数，此统计没有销售纬度
     */
    List<VisitTrendCell> getPcVisitedForProjects(@Param("query")DashBoardQuery dashBoardQuery);

    /**
     * 获取客户访问分析中的统计总揽
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return TotalStatistics
     */
    TotalStatistics getVisitorVisitTotalDataDao(
            @Param("vva") VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            @Param("userIdList") List<String> userIdList);

    /**
     * 访问数量
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getNumberOfVisits(
            @Param("vva") VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            @Param("userIdList") List<String> userIdList);

    /**
     * 访问人数
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getNumberOfPeople(
            @Param("vva") VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            @Param("userIdList") List<String> userIdList);

    /**
     * 留电人数
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getPeopleOfRemainPhone(
            @Param("vva") VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            @Param("userIdList") List<String> userIdList);

    /**
     * 到访人数
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getPeopleOfAccess(
            @Param("vva") VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            @Param("userIdList") List<String> userIdList);

    /**
     * 分享次数
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getShareCount(
            @Param("vva") VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            @Param("userIdList") List<String> userIdList);

    /**
     * 分享人数
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getSharePeople(
            @Param("vva") VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            @Param("userIdList") List<String> userIdList);

    /**
     * 阅读次数
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getReadCount(
            @Param("vva") VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            @Param("userIdList") List<String> userIdList);

    /**
     * 阅读人数
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getReadPeople(
            @Param("vva") VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            @Param("userIdList") List<String> userIdList);

    /**
     * 员工拓展
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getSellerRecommend(
            @Param("vva") VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            @Param("userIdList") List<String> userIdList);

    /**
     * 客户转介
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return Long
     */
    Long getCustomerRecommend(
            @Param("vva") VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            @Param("userIdList") List<String> userIdList);


    /**
     * 获取客户访问分析中的来源统计数据
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return List<OriginCountCell>
     */
    List<OriginCountCell> getVisitorVisitOriginDataDao(
            @Param("vva") VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            @Param("userIdList") List<String> userIdList);

    /**
     * 获取客户访问分析中访问趋势数据,目前统计的粒度是访问到退出小程序算一次访问
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return List<VisitTrendCell>
     */
    List<VisitTrendCell> getVisitorVisitTrendVisitTimesDataDao(
            @Param("vva") VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            @Param("userIdList") List<String> userIdList);


    /**
     * 访问分析数据单位查询
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return List<VisitTrendCell>
     */
    List<VisitTrendCell> getVisitorVisitTrendPeopleNumberDataDao(
            @Param("vva") VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            @Param("userIdList") List<String> userIdList);

    /**
     * 获取访客分析中访客活跃时间段分析
     *
     * @param visitorVisitAnalysisQuery 查询条件
     * @param userIdList                用户列表
     * @return List<VisitPeriod>
     */
    List<VisitPeriod> getVisitorVisitPeriodDataDao(
            @Param("vva") VisitorVisitAnalysisQuery visitorVisitAnalysisQuery,
            @Param("userIdList") List<String> userIdList);

    /**
     * PC后台-查全天活跃时段长
     *
     * @param query 参数参数
     * @return 列表信息
     */
    List<CustomerVisitData> getFullTimeActiveTime(@Param("query") DashBoardQuery query);

    /**
     * 查询访问数量
     *
     * @param query 查询条件
     * @return List<PercentVO>
     */
    List<AccessDetailVO> selectNumberOfVisits(@Param("query") DashBoardQuery query);

    /**
     * 查询访问人数
     *
     * @param query 查询条件
     * @return List<PercentVO>
     */
    List<AccessDetailVO> selectNumberOfPeople(@Param("query") DashBoardQuery query);

    /**
     * 查询到访人数
     *
     * @param query 查询条件
     * @return List<PercentVO>
     */
    List<AccessDetailVO> selectPeopleOfAccess(@Param("query") DashBoardQuery query);

    /**
     * 查询留电人数
     *
     * @param query 查询条件
     * @return List<PercentVO>
     */
    List<AccessDetailVO> selectPeopleOfRemainPhone(@Param("query") DashBoardQuery query);

    /**
     * 推荐分析
     *
     * @param sourceId 推荐人id
     * @return RecommendAnalysisVO
     */
    RecommendAnalysisVO selectRecommendAllTotal(String sourceId);

    /**
     * 客户分布地图
     * @param query 参数
     * @return 客户经纬度列表
     */
    List<CustomerDistributionMapVO> selectCustomerDistributionMap(@Param("query") DistributionMapQuery query);
}
