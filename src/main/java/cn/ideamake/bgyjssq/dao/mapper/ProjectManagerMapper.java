package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.ProjectManager;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 员工项目管理表 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
public interface ProjectManagerMapper extends BaseMapper<ProjectManager> {

}
