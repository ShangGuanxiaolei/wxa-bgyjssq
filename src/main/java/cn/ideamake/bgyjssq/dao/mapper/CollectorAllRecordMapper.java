package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.CollectorAllRecord;
import cn.ideamake.bgyjssq.pojo.query.StaffVisitorRankQuery;
import cn.ideamake.bgyjssq.pojo.query.VisitorTrendQuery;
import cn.ideamake.bgyjssq.pojo.vo.StaffVisitorRankVO;
import cn.ideamake.bgyjssq.pojo.vo.VisitorTrendVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户行为全量记录，用于销售对用户行为的观测 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
public interface CollectorAllRecordMapper extends BaseMapper<CollectorAllRecord> {


    /**
     * 高等级角色获取其管理的子角色
     *
     * @param sellerId
     * @return
     */
    List<String> selectManagedSellers(String sellerId);

    /**
     * 员工获取访客动态
     *
     * @param page              page
     * @param visitorTrendQuery visitorTrendQuery
     * @param userIdList        userIdList
     * @return IPage<VisitorTrendVO>
     */
    IPage<VisitorTrendVO> getVisitorTrend(Page<VisitorTrendVO> page,
                                          @Param("vt") VisitorTrendQuery visitorTrendQuery,
                                          @Param("userIdList") List<String> userIdList);

    /**
     * 根据停留时间查询用户排名
     *
     * @param page
     * @param staffVisitorRankQuery
     * @return
     */
    IPage<StaffVisitorRankVO> getStaffVisitorRankForStayTime(
            Page<StaffVisitorRankVO> page,
            @Param("svr") StaffVisitorRankQuery staffVisitorRankQuery);

    /**
     * 根据推荐人数查看用户排名
     *
     * @param page
     * @param staffVisitorRankQuery
     * @return
     */
    IPage<StaffVisitorRankVO> getStaffVisitorRankForRecommend(
            Page<StaffVisitorRankVO> page,
            @Param("svr") StaffVisitorRankQuery staffVisitorRankQuery);


    /**
     * 根据用户分享次数查看用户排名
     *
     * @param page
     * @param staffVisitorRankQuery
     * @return
     */
    IPage<StaffVisitorRankVO> getStaffVisitorRankForShareTimes(
            Page<StaffVisitorRankVO> page,
            @Param("svr") StaffVisitorRankQuery staffVisitorRankQuery);

    /**
     * 根据到用户访问次数查看用户排名
     *
     * @param page
     * @param staffVisitorRankQuery
     * @return
     */
    IPage<StaffVisitorRankVO> getStaffVisitorRankForVisitedTimes(
            Page<StaffVisitorRankVO> page,
            @Param("svr") StaffVisitorRankQuery staffVisitorRankQuery);

    /**
     * 根据到用户访次数查看用户排名
     *
     * @param page
     * @param staffVisitorRankQuery
     * @return
     */
    IPage<StaffVisitorRankVO> getStaffVisitorRankForVisitTimes(
            Page<StaffVisitorRankVO> page,
            @Param("svr") StaffVisitorRankQuery staffVisitorRankQuery);

    /**
     * 根据用户推介留电查看用户排名
     *
     * @param page
     * @param staffVisitorRankQuery
     * @return
     */
    IPage<StaffVisitorRankVO> getStaffVisitorRankForLeftPhone(
            Page<StaffVisitorRankVO> page,
            @Param("svr") StaffVisitorRankQuery staffVisitorRankQuery);


    /**
     * 通过高等级角色id获取其管理的销售带来的访客的停留时间排名
     *
     * @param page
     * @param staffVisitorRankQuery
     * @return
     */
    Page<StaffVisitorRankVO> getStaffRankForHigherUserStayTime(
            Page<StaffVisitorRankVO> page,
            @Param("svr") StaffVisitorRankQuery staffVisitorRankQuery);

    /**
     * 通过高等级角色id获取其管理的销售带来的访客的到访人数排名
     *
     * @param page
     * @param staffVisitorRankQuery
     * @return
     */
    Page<StaffVisitorRankVO> getStaffRankForHigherUserVisitedTimes(
            Page<StaffVisitorRankVO> page,
            @Param("svr") StaffVisitorRankQuery staffVisitorRankQuery);

    /**
     * 通过高等级角色id获取其管理的销售带来的访客的分享次数排名
     *
     * @param page
     * @param staffVisitorRankQuery
     * @return
     */
    Page<StaffVisitorRankVO> getStaffRankForHigherUserShareTimes(
            Page<StaffVisitorRankVO> page,
            @Param("svr") StaffVisitorRankQuery staffVisitorRankQuery);


    /**
     * 通过高等级角色id获取其管理的销售带来的访客的推介人数排名
     *
     * @param page
     * @param staffVisitorRankQuery
     * @return
     */
    Page<StaffVisitorRankVO> getStaffRankForHigherUserRecommendNumber(
            Page<StaffVisitorRankVO> page,
            @Param("svr") StaffVisitorRankQuery staffVisitorRankQuery);

    /**
     * 通过高等级角色id获取其管理的销售带来的访客的留电人数排名
     *
     * @param page
     * @param staffVisitorRankQuery
     * @return
     */
    Page<StaffVisitorRankVO> getStaffRankForHigherUserLeftPhone(
            Page<StaffVisitorRankVO> page,
            @Param("svr") StaffVisitorRankQuery staffVisitorRankQuery);


}
