package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.UserInfo;
import cn.ideamake.bgyjssq.pojo.vo.SellerVO;
import cn.ideamake.bgyjssq.pojo.vo.UserInfoVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-05-23
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

    /**
     * 根据分组id查询用户信息
     *
     * @param groupId 分组id
     * @return List<SellerVO>
     */
    List<SellerVO> getUserInfoList(Integer groupId);

    /**
     * 查询销售信息
     * @param uuid 销售uuid
     * @return SellerVO
     */
    SellerVO getUserInfoVO(String uuid);

    /**
     * 根据分组id查询用户id列表
     *
     * @param groupId 分组id
     * @return List<String>
     */
    List<String> getUserIdList(Integer groupId);

    /**
     * 根据项目id查询销售信息
     *
     * @param projectId 项目id
     * @return List<UserInfoVO>
     */
    List<UserInfoVO> getUserList(Integer projectId);

    /**
     * 根据组列表查询销售列表
     *
     * @param groupIds 组列表
     * @return List<String>
     */
    List<SellerVO> getUserIdListByGroupIds(@Param("groupIds") List<Integer> groupIds);
}
