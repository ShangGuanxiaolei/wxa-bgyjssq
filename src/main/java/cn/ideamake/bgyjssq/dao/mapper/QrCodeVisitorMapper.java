package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.QrCodeVisitor;
import cn.ideamake.bgyjssq.pojo.entity.User;
import cn.ideamake.bgyjssq.pojo.entity.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 签到码签到客户 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-07-10
 */
public interface QrCodeVisitorMapper extends BaseMapper<QrCodeVisitor> {

}
