package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.common.enums.PermissionTypeEnum;
import cn.ideamake.bgyjssq.pojo.entity.SysPermission;
import cn.ideamake.bgyjssq.pojo.vo.PermissionVO;
import cn.ideamake.bgyjssq.pojo.vo.permission.MenuListChildrenVO;
import cn.ideamake.bgyjssq.pojo.vo.permission.MenuListVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 系统权限资源表，记录系统的权限资源信息 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Repository
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

    /**
     * 根据父类id查询
     *
     * @param parentId 父类id
     * @return List<PermissionVO>
     */
    @Select("SELECT id, name as title, parent_id FROM im_sys_permission WHERE parent_id = #{parentId}")
    List<PermissionVO> selectByParentId(Integer parentId);

    /**
     * 根据角色id和权限类型查询权限
     *
     * @param roleId 角色id
     * @param type   权限类型
     * @return 权限VO
     */
    List<SysPermission> selectByRoleIdAndType(@Param("roleId") Integer roleId, @Param("type") PermissionTypeEnum type);

    /**
     * 根据角色id、权限类型、父类id查询权限
     *
     * @param roleId   角色id
     * @param type     权限类型
     * @param parentId 父类id
     * @return 权限对象
     */
    List<MenuListVO> selectByRoleIdAndTypeAndParentId(@Param("roleId") Integer roleId,
                                                      @Param("type") PermissionTypeEnum type,
                                                      @Param("parentId") Integer parentId);

    /**
     * 根据角色id、权限类型、父类id查询权限
     *
     * @param roleId   角色id
     * @param type     权限类型
     * @param parentId 父类id
     * @return 权限对象
     */
    List<MenuListChildrenVO> selectByRoleIdAndTypeAndParentIdChildren(@Param("roleId") Integer roleId,
                                                                      @Param("type") PermissionTypeEnum type,
                                                                      @Param("parentId") Integer parentId);

    /**
     * 根据角色id及权限类型查询权限列表
     *
     * @param roleId 角色id
     * @param type   权限类型
     * @return List<SysPermission>
     */
    List<SysPermission> selectAllByRoleIdAndType(@Param("roleId") Integer roleId,
                                                 @Param("type") PermissionTypeEnum type);
}
