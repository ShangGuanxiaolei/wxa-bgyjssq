package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.RolePermission;
import cn.ideamake.bgyjssq.pojo.entity.SysPermission;
import cn.ideamake.bgyjssq.pojo.vo.PermissionVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 角色权限绑定表，记录角色和权限的绑定关系 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Repository
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

    /**
     * 根据角色id查询权限
     *
     * @param roleId   角色id
     * @param parentId 父类id
     * @return 权限列表
     */
    @Select("select isp.id, isp.name as title\n" +
            "from im_sys_permission isp\n" +
            "inner join im_role_permission irp on isp.id = irp.permission_id\n" +
            "where irp.role_id = #{roleId}\n" +
            "and isp.parent_id = #{parentId}")
    List<PermissionVO> selectPermissionListByRoleId(@Param("roleId") Integer roleId, @Param("parentId") Integer parentId);

    /**
     * 根据角色id查询权限
     *
     * @param roleId 角色id
     * @return 权限列表
     */
    @Select("select isp.id, isp.name, isp.url, isp.type, isp.parent_id, isp.description, isp.interface_url\n" +
            "from im_sys_permission isp\n" +
            "inner join im_role_permission irp on isp.id = irp.permission_id\n" +
            "where irp.role_id = #{roleId}")
    List<SysPermission> selectPermissionByRoleId(Integer roleId);

}
