package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.SignInQrCode;
import cn.ideamake.bgyjssq.pojo.query.ActivityDetailQuery;
import cn.ideamake.bgyjssq.pojo.query.QrCodeQuery;
import cn.ideamake.bgyjssq.pojo.vo.ActivityGroupVO;
import cn.ideamake.bgyjssq.pojo.vo.ActivityTidyVO;
import cn.ideamake.bgyjssq.pojo.vo.ExpireVO;
import cn.ideamake.bgyjssq.pojo.vo.PreSignInProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.ProjectGroupVO;
import cn.ideamake.bgyjssq.pojo.vo.ProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.QrCodeVO;
import cn.ideamake.bgyjssq.pojo.vo.SignInStaffVO;
import cn.ideamake.bgyjssq.pojo.vo.StaffVisitorInProjectVO;
import cn.ideamake.bgyjssq.pojo.vo.qrcode.SignInQrCodeVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 签到二维码 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-07-10
 */
@Repository
public interface SignInQrCodeMapper extends BaseMapper<SignInQrCode> {

    /**
     * 查询签到码列表
     *
     * @param page        分页参数
     * @param qrCodeQuery 查询条件
     * @return IPage<QrCodeVO>
     */
    IPage<QrCodeVO> selectPage(Page<QrCodeVO> page, @Param("qrCodeQuery") QrCodeQuery qrCodeQuery);

    /**
     * 根据id查询签到码对象
     *
     * @param id 签到码id
     * @return SignInQrCodeVO
     */
    SignInQrCodeVO getActivity(@Param("id") Integer id);

    /**
     * 得到可用的活动组
     *
     * @return 查询结果
     */
    List<ActivityGroupVO> getActivityGroup();

    /**
     * 得到可用的活动
     *
     * @param groupId 活动组ID
     * @return 查询结果
     */
    List<ActivityTidyVO> getActivityFromGroup(@Param("groupId") Integer groupId);

    /**
     * 得到可用的项目组
     *
     * @return 查询结果
     */
    List<ProjectGroupVO> getProjectGroup();

    /**
     * 通过项目组Id得到项目列表
     *
     * @param id 项目组ID
     * @return 查询结果
     */
    List<ProjectVO> getProjectFromGroup(@Param("groupId") Integer id);

    /**
     * 通过项目Id得到活动列表
     *
     * @param id 项目id
     * @return 查询结果
     */
    List<ActivityTidyVO> getActivityFromProject(@Param("id") Integer id);

    /**
     * 查看特定二维码中客户信息
     *
     * @param page  分页
     * @param query 查询条件
     * @return IPage<StaffVisitorInProjectVO>
     */
    IPage<StaffVisitorInProjectVO> getActivitiesDetail(Page<StaffVisitorInProjectVO> page,
                                                       @Param("query") ActivityDetailQuery query);

    /**
     * 查询活动累计人数
     *
     * @param id 项目id
     * @return 人数
     */
    Integer getActivitiesCount(@Param("id") Integer id);

    /**
     * 获取当日签到总数
     *
     * @param id 活动id
     * @return Integer
     */
    Integer getActivitiesDayCount(Integer id);

    /**
     * 查询活动累计人数今日值
     *
     * @param id   id值
     * @param date date 值
     * @return 人数
     */
    Integer getActivitisTodayCount(@Param("id") Integer id, @Param("date") String date);

    /**
     * 查询訪客今日簽到次數
     *
     * @param id   活动id
     * @param uuid visitUuid
     * @param date 日期
     * @return 人数
     */
    Integer getVisitTodayCount(@Param("id") Integer id, @Param("uuid") String uuid, @Param("date") String date);

    /**
     * 查询訪客今日簽到次數
     *
     * @param uuid visitUuid
     * @param id   項目id
     * @return 人数
     */
    Integer getVisitCount(@Param("id") Integer id, @Param("uuid") String uuid);

    /**
     * 查询訪客今日簽到次數
     *
     * @param uuid visitUuid
     * @param id   項目id
     * @return 人数
     */
    Integer getVisitDayCount(@Param("id") Integer id, @Param("uuid") String uuid);

    /**
     * 查询二维码是否失效
     *
     * @param query query
     * @return Integer
     */
    Integer getQrExpire(@Param("query") ExpireVO query);

    /**
     * 查询员工信息
     *
     * @param id id
     * @return SignInStaffVO
     */
    SignInStaffVO getSignInStaff(@Param("id") Integer id);

    /**
     * 查询项目信息
     *
     * @param id id
     * @return PreSignInProjectVO
     */
    PreSignInProjectVO getPreSignInProject(@Param("id") Integer id);

    /**
     * 查询登录天数
     *
     * @param visitorId 访客id
     * @return Integer
     */
    Integer selectTotalDays(@Param("visitorId") String visitorId);

}
