package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.dto.ArticleReadPicCountDTO;
import cn.ideamake.bgyjssq.pojo.entity.CollectorResource;
import cn.ideamake.bgyjssq.pojo.query.DashBoardQuery;
import cn.ideamake.bgyjssq.pojo.vo.dashboard.ArticleDataPieVO;
import cn.ideamake.bgyjssq.pojo.vo.dashboard.ArticleDataVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 资源采集 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-07-31
 */
public interface CollectorResourceMapper extends BaseMapper<CollectorResource> {

    /**
     * pc后台-微文章折线图数据统计-阅读
     *
     * @param query 参数
     * @return 列表
     */
    List<ArticleDataVO> getArticleDataLineChart(@Param("query") DashBoardQuery query);

    /**
     * pc后台-微文章折线图数据统计-分享
     *
     * @param query 参数
     * @return 列表
     */
    List<ArticleDataVO> getShareDataLineChart(@Param("query") DashBoardQuery query);

    /**
     * 获取阅读人次百分比
     *
     * @param query 查询条件
     * @return List<ArticleDataPieVO>
     */
    List<ArticleDataPieVO> getReadNumber(@Param("query") DashBoardQuery query);

    /**
     * 获取阅读人数
     *
     * @param query 查询条件
     * @return ArticleReadPicCountDTO
     */
    ArticleReadPicCountDTO getReadCount(@Param("query") DashBoardQuery query);

    /**
     * 获取分享次数百分比
     *
     * @param query 查询条件
     * @return List<ArticleDataPieVO>
     */
    List<ArticleDataPieVO> getShareNumber(@Param("query") DashBoardQuery query);

    /**
     * 获取分享人数
     *
     * @param query 查询条件
     * @return ArticleReadPicCountDTO
     */
    ArticleReadPicCountDTO getShareCount(@Param("query") DashBoardQuery query);

}
