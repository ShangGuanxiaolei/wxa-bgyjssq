package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.AdminLog;
import cn.ideamake.bgyjssq.pojo.query.AdminLogQuery;
import cn.ideamake.bgyjssq.pojo.vo.permission.AdminLogVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 管理员操作记录 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-07-04
 */
@Repository
public interface AdminLogMapper extends BaseMapper<AdminLog> {

    /**
     * 分页查询管理员列表
     *
     * @param page          分页数据
     * @param adminLogQuery 参数
     * @return 分页数据
     */
    IPage<AdminLogVO> selectByPage(Page<AdminLogVO> page, @Param("adminLogQuery") AdminLogQuery adminLogQuery);

}
