package cn.ideamake.bgyjssq.dao.mapper;

import cn.ideamake.bgyjssq.pojo.entity.LoginTicket;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 登录凭证 Mapper 接口
 * </p>
 *
 * @author ideamake
 * @since 2019-07-25
 */
public interface LoginTicketMapper extends BaseMapper<LoginTicket> {

}
