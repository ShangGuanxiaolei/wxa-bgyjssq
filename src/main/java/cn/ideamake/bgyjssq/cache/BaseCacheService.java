package cn.ideamake.bgyjssq.cache;

import org.apache.ibatis.cache.CacheException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * cache缓存
 *
 * @author gxl
 */
public abstract class BaseCacheService<T> {

    private static final Logger log = LoggerFactory.getLogger(BaseCacheService.class);

    /**
     * 缓存过期时间为一分钟
     */
    private final static long EXPIRED_AT = 1000 * 60;

    /**
     * 是否开启自动刷新
     */
    private final static boolean AUTO_REFRESH = true;

    /**
     * 上次缓存的时间
     */
    private ConcurrentHashMap<String, Long> keyCacheTime = new ConcurrentHashMap<>();

    /**
     * 对设置数据进行加锁
     */
    private final Lock lock = new ReentrantLock();

    /**
     * 执行缓存操作并且返还缓存数据
     *
     * @param key  key
     * @param data data
     * @return T
     */
    public T doCache(String key, T data) {

        //是否需要初始化cache
        if (initKeyCacheTime(key)) {
            if (data == null) {
                return null;
            }
            generateCache(key, data);
            return data;
        }

        //是否开启自动刷新
        if (AUTO_REFRESH) {
            //是否下一次需要刷新cache
            final boolean success = isRefreshNextCache(key, data);
            if (success) {
                return null;
            }
        }
        //从缓存中获取cache
        return getCacheData(key);
    }

    private boolean initKeyCacheTime(String key) {
        return keyCacheTime.get(key) == null;
    }

    /**
     * 从缓存中获取cache
     *
     * @param key key
     * @return T
     */
    abstract T getCacheData(String key);

    /**
     * 生成缓存
     *
     * @param key  key
     * @param data data
     */
    private void generateCache(String key, T data) {
        try {
            if (lock.tryLock()) {
                setDataToCache(key, data);
                keyCacheTime.put(key, System.currentTimeMillis());
            }
        } catch (Exception e) {
            log.info(e.getMessage());
            throw new CacheException(e.getMessage());
        } finally {
            lock.unlock();
        }
    }

    private boolean isRefreshNextCache(String key, T data) {
        //如果没有该key的时间代表需要进行初始化刷新
        if (isOverExpiredTime(key) && data != null) {
            generateCache(key, data);
            return false;
        }
        return isOverExpiredTime(key) && data == null;
    }

    /**
     * 是否超过过期时间
     *
     * @return boolean
     */
    private boolean isOverExpiredTime(String key) {
        return System.currentTimeMillis() - (keyCacheTime.get(key) + EXPIRED_AT) > 0;
    }

    /**
     * 将缓存设置到Cache中
     *
     * @param key  key
     * @param data data
     */
    protected abstract void setDataToCache(String key, T data);

}
