package cn.ideamake.bgyjssq.cache;

import cn.ideamake.bgyjssq.cache.constant.ExpireAt;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author gxl
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DoGuavaCache {

    /**
     * 获取缓存的key
     * key 定义在注解上，支持SPEL表达式
     * 注： method的参数支持Javabean和Map
     * method的基本类型要定义为对象，否则没法读取到名称
     * <p>
     * example1:
     * Phone phone = new Phone();
     * "#{phone.cpu}"  为对象的取值
     * example2:
     * Map apple = new HashMap(); apple.put("name","good apple");
     * "#{apple[name]}"  为map的取值
     * example3:
     * "#{phone.cpu}_#{apple[name]}"
     *
     * 调用示例：@DoGuavaCache(key = CacheKeyConstant.KEY, expireAt = ExpireAt.SIXTH)
     *
     * @return String
     */
    String key() default "";

    /**
     * 过期时间默认为60秒
     *
     * @return ExpireAt
     */
    ExpireAt expireAt() default ExpireAt.SIXTH;
}
