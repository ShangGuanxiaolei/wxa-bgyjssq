package cn.ideamake.bgyjssq.cache;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author gxl
 * @param <T>
 */
@Service
public class JvmCacheServiceImpl<T> extends BaseCacheService<T> {

    private final Map<String, T> cacheMap = new HashMap<>(16);

    @Override
    public T getCacheData(String key) {
        return cacheMap.get(key);
    }

    @Override
    protected void setDataToCache(String key, T data) {
        cacheMap.put(key, data);
    }
}
