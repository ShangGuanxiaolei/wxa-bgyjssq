package cn.ideamake.bgyjssq.cache;

import cn.ideamake.bgyjssq.cache.constant.ExpireAt;
import cn.ideamake.bgyjssq.cache.util.AopUtils;
import cn.ideamake.bgyjssq.cache.util.SpringExpressionUtils;
import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @author gxl
 */
@Component
@Aspect
public class CacheAspect {

    private static final Logger logger = LoggerFactory.getLogger(CacheAspect.class);

    private final GuavaCacheManager guavaCacheManager;

    private static final String NULL = "null";

    public CacheAspect(GuavaCacheManager guavaCacheManager) {
        this.guavaCacheManager = guavaCacheManager;
    }

    @Pointcut("@annotation(cn.ideamake.bgyjssq.cache.DoGuavaCache)")
    public void guavaCachePointcut() {
    }

    @Around("guavaCachePointcut()")
    public Object doGuavaCache(ProceedingJoinPoint pjp) throws Throwable {
        Object ret;
        Method method = AopUtils.getMethod(pjp);
        final DoGuavaCache doGuavaCache = method.getDeclaredAnnotation(DoGuavaCache.class);
        if (doGuavaCache == null) {
            return pjp.proceed();
        }
        String key = doGuavaCache.key();
        if (StringUtils.isBlank(key)) {
            return pjp.proceed();
        }
        key = SpringExpressionUtils.parseKey(key, method, pjp.getArgs());
        final ExpireAt expireAt = doGuavaCache.expireAt();
        ret = get(expireAt, key);
        if (ret == null) {
            ret = pjp.proceed();
            put(expireAt, key, ret);
            logger.info("已经缓存 key : {} = value : {}", key, ret);
        }
        return ret;
    }

    private Object get(ExpireAt expireAt, String key) {
        Object ret = null;
        if (expireAt.equals(ExpireAt.SIXTH)) {
            ret = guavaCacheManager.get(key);
        }
        if (expireAt.equals(ExpireAt.FIVE)) {
            ret = guavaCacheManager.getByInstantMap(key);
        }
        ret = objectToNull(ret);
        return ret;
    }

    private void put(ExpireAt expireAt, Object key, Object data) {
        data = nullToObject(data);
        if (expireAt.equals(ExpireAt.SIXTH)) {
            guavaCacheManager.putIfExpire(key, data);
        }
        if (expireAt.equals(ExpireAt.FIVE)) {
            guavaCacheManager.putByInstantMap(key, data);
        }
    }

    private Object objectToNull(Object data) {
        if (NULL.equals(data)) {
            return null;
        }
        return data;
    }

    private Object nullToObject(Object data) {
        if (data == null) {
            return NULL;
        }
        return data;
    }
}

