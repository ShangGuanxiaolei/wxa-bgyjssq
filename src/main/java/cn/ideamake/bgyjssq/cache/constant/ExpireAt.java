package cn.ideamake.bgyjssq.cache.constant;

/**
 * @author gxl
 */
public enum ExpireAt {
    /**
     * 五秒
     */
    FIVE,
    /**
     * 六十秒
     */
    SIXTH
}
