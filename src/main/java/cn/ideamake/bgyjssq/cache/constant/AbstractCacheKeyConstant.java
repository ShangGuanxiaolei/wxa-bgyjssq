package cn.ideamake.bgyjssq.cache.constant;

/**
 * @author gxl
 */
public abstract class AbstractCacheKeyConstant {

    public static final String PERMISSION_TYPE = "/admin/sys-permission/list/type";

    public static final String PERMISSION_LIST = "/admin/sys-admin/findPermission";

    public static final String PERMISSION_LIST_ALL = "/admin/sys-permission/list/permission";
}
